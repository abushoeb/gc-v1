#!/usr/bin/python

from gcycsbplot import *

if __name__ == "__main__":
	plotter = GCPlotter()
	cdf = CDFPlot(title="Read Latencies")
	
	one_files = [
		"latency.readonly.cassandra-base-1rep.ONE.0",		
		"latency.readonly.cassandra-base-2rep.ONE.0",
		"latency.readonly.cassandra-base-3rep.ONE.0"
	]
	
	quorum_files = [
		"latency.readonly.cassandra-base-3rep.QUORUM.0"
	]
	
	all_files = [
		"latency.readonly.cassandra-base-2rep.ALL.0", 
		"latency.readonly.cassandra-base-3rep.ALL.0"
	]
		
	file_lists = [one_files, quorum_files, all_files]
	
	for fl in file_lists:
		cdf = CDFPlot()
		for f in fl:
			results = YCSBResults("/home/wkatsak/wonko_home/YCSB/results/%s" % f, transaction_type=YCSBResults.READ, max_bin=250)
			cdf.addResults(results, label=f)
		plotter.addPlot(cdf)
	
	plotter.doPlot()
	
