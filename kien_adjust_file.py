#!/usr/bin/python

from datetime import datetime
from datetime import timedelta

if __name__ == "__main__":
	
	data = {}
	delta = timedelta(hours=2)
	
	with open("timeline_result.dat", "r") as f:
		for line in f:
			line = line.strip()
			date, time, value = line.split()
			dt_str = date + " " + time
			value = int(value)
			dt = datetime.strptime(dt_str, "%m/%d/%Y %H:%M:%S")
			# do shift
			shifted_dt = dt - delta
			#print dt, shifted_dt
			data[shifted_dt] = value
			
	with open("ask-workload-2.dat", "w") as f:
		for dt in sorted(data.keys()):
			if dt < datetime(month=4, day=1, year=2008):
				continue
			
			value = data[dt]
			dt_str = datetime.strftime(dt, "%m/%d/%Y %H:%M:%S")
			out_str = "%s %d\n" % (dt_str, value)
			f.write(out_str)
	
		
		