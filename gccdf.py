#!/usr/bin/python

import sys
from datetime import datetime
from datetime import timedelta

from gchelpers import *
from gcutil import *
from bisect import bisect_right

def interpolate(p1, p2, x):
	x1, y1 = p1
	x2, y2 = p2

	m = float(y2 - y1)/float(x2-x1)
	b = y1 - m*x1
	return m*x + b

def extrapolate(p1, p2, x):
	x1, y1 = p1
	x2, y2 = p2
	
	y = y1 + ((1.0*x - x1) / (x2 - x1)) * (y2 - y1)
	
	return y

def interpolateCDFs(param1, cdf1, param2, cdf2, targetParam):
	assert len(cdf1) == len(cdf2)
	assert type(cdf1) == type(cdf2)
	
	CDFType = type(cdf1)
	ret = CDFType()
	
	for i in xrange(0, len(cdf1)):
		#value1, percentage1 = cdf1.data[i]
		value1 = cdf1.valueAt(i)
		percentage1 = cdf1.percentageAt(i)

		#value2, percentage2 = cdf2.data[i]
		value2 = cdf2.valueAt(i)
		percentage2 = cdf2.percentageAt(i)

		assert(value1 == value2)
		
		p1 = param1, percentage1
		p2 = param2, percentage2
		newPercentage = interpolate(p1, p2, targetParam)
		
		ret.add(value1, newPercentage)
	
	return ret

def extrapolateCDFs(param1, cdf1, param2, cdf2, targetParam):
	assert len(cdf1) == len(cdf2)
	assert type(cdf1) == type(cdf2)

	CDFType = type(cdf1)
	ret = CDFType()
	
	for i in xrange(0, len(cdf1)):
		value1 = cdf1.valueAt(i)
		percentage1 = cdf1.percentageAt(i)

		value2 = cdf2.valueAt(i)
		percentage2 = cdf2.percentageAt(i)
		
		assert(value1 == value2)
		
		p1 = param1, percentage1
		p2 = param2, percentage2
		newPercentage = extrapolate(p1, p2, targetParam)
		
		ret.add(value1, newPercentage)
	
	return ret

class CDFSet(object):
	def __init__(self, cache={}):
		self.interpolatingDict = MultilevelInterpolatingDict(keySize=2, interpolateFunc=interpolateCDFs, extrapolateFunc=extrapolateCDFs, extrapolate=True)
		self.our_keys = []
		
		self.cache = cache
		
	def __getitem__(self, key):
		#try:
			#cdf = self.interpolatingDict[key]
		#except KeyError:
			#return None
		# It's OK to pass the exception
		if key in self.cache:
			return self.cache[key]
		else:
			results = self.interpolatingDict[key]
			self.cache[key] = results
			return results
	
	def __setitem__(self, key, cdf):
		self.interpolatingDict[key] = cdf
		self.our_keys.append(key)
		if key in self.cache:
			del self.cache[key]
		
	def keys(self):
		return list(self.our_keys)
	
	def __iter__(self):
		#return CDFSetIterator(self.our_keys)
		return self.our_keys.__iter__()
	
	def __contains__(self, key):
		try:
			self.__getitem__(key)
			return True
		except:
			return False
		
class CDF(object):
	def __init__(self):
		self.data = []
	
	def add(self, value, percentage):
		self.data.append((value, percentage))
		#self.data = sorted(self.data, key=itemgetter(1))
	
	def getPercentage(self, value):
		for auxv, percentage in self.data:
			if auxv == value:
				return percentage
	
	def getValue(self, percentage):
		for value, auxpercentage in self.data:
			if percentage < auxpercentage:
				return value
		return 100.0
	
	def __getitem__(self, percentage):
		return self.getValue(percentage)
	
	def __str__(self):
		strOut = "{%s}"
		middle = ""
		for d in self.data:
			v, p = d
			#if v > 50:
			#	break
			middle += "%0.2f:%0.5f " % (v,p)
		
		return strOut % middle
	
	def __len__(self):
		return len(self.data)
	
	def valueAt(self, i):
		value, percentage = self.data[i]
		return value
	
	def percentageAt(self, i):
		value, percentage = self.data[i]
		return percentage
	
	def toPDF(self):
		pass
	
	def write(self, f):
		for d in self.data:
			v, p = d
			f.write("%0.6f %0.6f\n" % (v, p))

class CDFFast(CDF):
	def __init__(self):
		self.values = []
		self.percentages = []
	
	def add(self, value, percentage):
		self.values.append(value)
		self.percentages.append(percentage)
		
	def getPercentage(self, value):
		i = index(self.values, value)
		return self.percentages[i]
	
	def getValue(self, percentage):
		i = bisect_right(self.percentages, percentage)
		if i != len(self.percentages):
			return self.values[i]
		
		return 100.0
	
	def __getitem__(self, percentage):
		return self.getValue(percentage)
	
	def __str__(self):
		'''
		strOut = "{%s}"
		middle = ""
		
		for i in xrange(0, len(values)):
			v = self.values[i]
			p = self.percentages[i]
			
			middle += "%0.2f:%0.5f " % (v,p)
		
		return strOut % middle
		'''
		return "%0.2f" % self.getValue(99)

	def __len__(self):
		return len(self.values)
	
	def valueAt(self, i):
		return self.values[i]
	
	def percentageAt(self, i):
		return self.percentages[i]
	
	def write(self, f):
		for i in xrange(0, len(self.values)):
			v = self.values[i]
			p = self.percentages[i]
			f.write("%0.6f %0.6f\n" % (v, p))

def readCDF(fcdf, classname=CDFFast):
	ret = classname()
	for line in fcdf:
		try:
			response, percentage = line.split(' ')
			response = float(response)
			percentage = float(percentage)
			ret.add(response, percentage)
		except Exception, e:
			print line, '->', e
	return ret

if __name__ == "__main__":
	filename = sys.argv[1]
	
	with open(filename) as f:
		cdfRegular = readCDF(f, CDF)
	
	with open(filename) as f:
		cdfFast = readCDF(f, CDFFast)

	iters = 100000
	
	start = datetime.now()
	for i in xrange(0, iters):
		v = cdfRegular.getValue(99.999)
		p = cdfRegular.getPercentage(v)
	finish = datetime.now()
	print v, p
	
	print "regular took %f seconds per call" % ((finish-start).total_seconds() / float(iters))
	
	start = datetime.now()
	for i in xrange(0, iters):
		v = cdfFast.getValue(99.999)
		p = cdfRegular.getPercentage(v)
	finish = datetime.now()
	print v, p
	
	print "fast took %f seconds per call" % ((finish-start).total_seconds() / float(iters))

	for i in xrange(0, 100):
		print "regular %f, fast %f" % (cdfRegular.getValue(i), cdfFast.getValue(i))

	
	
	