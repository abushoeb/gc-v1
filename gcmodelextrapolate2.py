#!/usr/bin/python

from gcutil import *
from gcmodel import *
from gcresponsetime import *
from gccdf import *

import argparse

def getMinNodes(latencyDict):
	minNodes = 1000
	
	for key in latencyDict.keys():
		nodes, throughput = key
		if nodes < minNodes:
			minNodes = nodes

	return minNodes

def getMaxNodes(latencyDict):
	maxNodes = 0
	
	for key in latencyDict.keys():
		nodes, throughput = key
		if nodes > maxNodes:
			maxNodes = nodes

	return maxNodes

def getMinThroughput(latencyDict):
	minThroughput = 1000000
	
	minNodes = getMinNodes(latencyDict)
	
	for key in latencyDict.keys():
		nodes, throughput = key
		if nodes == minNodes and throughput < minThroughput:
			minThroughput = throughput
	
	return minThroughput

def getMaxThroughput(latencyDict):
	maxThroughput = 0
	
	minNodes = getMinNodes(latencyDict)
	
	for key in latencyDict.keys():
		nodes, throughput = key
		if nodes == minNodes and throughput > maxThroughput:
			maxThroughput = throughput

	return maxThroughput

def getThroughputStep(latencyDict):
	sortedKeys = list(sorted(latencyDict.keys()))
	
	nodes0, throughput0 = sortedKeys[0]
	nodes1, throughput1 = sortedKeys[1]
	
	return throughput1 - throughput0

def getNodesStep(latencyDict):
	sortedKeys = list(sorted(latencyDict.keys()))
	
	firstNodes = None
	secondNodes = None
	
	for key in sortedKeys:
		nodes, throughput = key
		if firstNodes == None:
			firstNodes = nodes
			continue
		
		if firstNodes != nodes:
			secondNodes = nodes
			break
	
	return secondNodes - firstNodes

def calcNewPoints(latencyDict, points):
	newPoints = []
	avgs = {}
	
	minThroughput = getMinThroughput(latencyDict)
	maxThroughput = getMaxThroughput(latencyDict)
	minNodes = getMinNodes(latencyDict)
	maxNodes = getMaxNodes(latencyDict)
	
	throughputStep = getThroughputStep(latencyDict)
	nodesStep = getNodesStep(latencyDict)
	
	for throughput in xrange(minThroughput, maxThroughput+1, throughputStep):
		avgs[throughput] = []
		
		for nodes in xrange(minNodes, maxNodes, nodesStep):
		
			first = nodes
			second = nodes+3
			
			latencyFirst = latencyDict[first, throughput]
			latencySecond = latencyDict[second, throughput]
			
			factor = latencySecond / latencyFirst
			avgs[throughput].append(factor)
			print throughput, factor
	
	for nodes in sorted(points, reverse=True):
		for throughput in xrange(minThroughput, maxThroughput+1, 1000):
			    nextSample = nodes+3
			    #second = nodes+6
			    
			    latency = latencyDict[nextSample, throughput]
			    
			    factor = sum(avgs[throughput]) / len(avgs[throughput])
			    newLatency = math.ceil(latency / factor)
			    
			    latencyDict[nodes, throughput] = newLatency
			    #print "%d\t%d\t%0.2f" % (nodes, throughput, newLatency)
			    newPoints.append((nodes, throughput, newLatency))
	return newPoints

for calcMissingThroughputs(latencyDict, throughputs):
	newPoints = []
	
	
def dumpTextModel(latencyDict):
	keys = latencyDict.keys()
	keys.sort()
	
	dump = ""
	
	for key in keys:
		nodes, throughput = key
		line = "%d\t%d\t%0.2f\n" % (nodes, throughput, latencyDict[nodes, throughput])
		dump += line
	
	return dump

if __name__ == "__main__":
	
	
	parser = argparse.ArgumentParser()
	
	parser.add_argument("--model", action="store", required=True)
	parser.add_argument("--points", action="store", type=int, nargs="+", required=True)
	
	args = parser.parse_args()
	
	model = GCTableResponseTimeModelNew(args.model)
	latencyDict = model.latencyDict
	
	points = set()
	
	for key in latencyDict.keys():
		nodes, throughput = key
		points.add(throughput)
	
	print sorted(points)
		
	
	newPoints = calcNewPoints(latencyDict, args.points)
   	
   	dump = dumpTextModel(latencyDict)
   	
   	outputFileName = args.model.replace(".model", "-MODIFIED.model")
   	
   	f = open(outputFileName, "w")
   	for line in dump:
		f.write(line)
	f.close()
			    
		
	