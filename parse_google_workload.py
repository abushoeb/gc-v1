#!/usr/bin/python

import os
import sys
import time
import gzip
import sqlite3

DB_FILE = "google_workload.db"
TABLES = ["job_events", "machine_attributes", "machine_events", "task_constraints", "task_events", "task_usage"]
TYPES = {"STRING" : str, "STRING_HASH" : str, "STRING_HASH_OR_INTEGER" : str, "INTEGER" : int, "FLOAT" : float, "BOOLEAN" : bool}

# set up sqlite3 connection
con = sqlite3.connect(DB_FILE, detect_types=sqlite3.PARSE_DECLTYPES, timeout=30)
con.row_factory = sqlite3.Row
con.text_factory = str
	
class Schema(object):
	def __init__(self, entries):
		self.entries = entries
	
	def __str__(self):
		s = "Schema("
		for i in xrange(0, len(self.entries)):
			entry = self.entries[i]
			s += "%d-%s-%s-%s-%s, " % (i, entry.name, str(entry.datatype), str(entry.mandatory), str(entry.primary))
		
		s.strip()
		s += ")"
		
		return s

class SchemaEntry(object):
	def __init__(self, name, datatype, mandatory, primary):
		self.name = name
		self.datatype = datatype
		self.mandatory = mandatory
		self.primary = primary

	def __str__(self):
		return "SchemaEntry(name=%s, datatype=%s, mandatory=%s, primary=%s)" % (self.name, str(self.datatype), str(self.mandatory), str(self.primary))

def parse_schema(table):
	#print "parsing", table
	schema_entries = []
	with open("schema.csv", "r") as f:
		for line in f:
			line = line.strip()
			if table in line:
				fields = line.split(",")
				#print fields
				
				# position
				position = int(fields[1])
				
				# name
				name = fields[2].replace(" ", "_").replace("/", "").lower()
				
				# type
				datatype_str = fields[3]
				datatype = None
				for t in TYPES:
					if t == datatype_str:
						datatype = TYPES[t]
						break
				assert datatype != None
				
				# mandatory
				mandatory_str = fields[4]
				if mandatory_str == "YES":
					mandatory = True
				else:
					mandatory = False
				
				entry = SchemaEntry(name, datatype, mandatory, position == 1)
				#print entry
				schema_entries.append(entry)
	
	return Schema(schema_entries)

def create_table(table, schema):
	query = "create table %s(" % table
	
	for entry in schema.entries:
		if query[-1] == '(':
			pass
		else:
			query += ", "
		
		query += entry.name
		#if entry.primary:
		#	query += " primary key"
		if entry.mandatory:
			query += " not null"
			
	query += ")"
	
	#print query
	with con:
		con.execute(query)

def load_table(table, schema):
	files = os.listdir(table)
	for filename in files:
		with gzip.open("%s/%s" % (table, filename)) as f, con:
			for line in f:
				fields_list = []
				values_list = []
				
				fields = line.strip().split(",")
				for i in xrange(0, len(fields)):
					data = fields[i]
					datatype = schema.entries[i].datatype
					
					if data != "":
						fields_list.append(schema.entries[i].name)
						
						if datatype == str:
							value = "\"%s\"" % fields[i]
						else:
							value = "%s" % str(fields[i])
						
						values_list.append(value)
					else:
						assert not schema.entries[i].mandatory

				fields_string =  ", ".join(fields_list)
				values_string = ", ".join(values_list)

				query = "insert into %s(%s) values (%s)" % (table, fields_string, values_string)
				#print query
				try:
					con.execute(query)
				except Exception as e:
					print e
					print query

def create_tables():
	for table in TABLES:
		schema = parse_schema(table)
		create_table(table, schema)
		load_table(table, schema)
			
if __name__ == "__main__":

	
	create_tables()
	
	
	