#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *

from datetime import datetime
from datetime import timedelta


class GCTransitionAnalysis(GCExperimentProcessor):
	reqs = ["actual_workload", "any_transitions", "nodes_ready", "readlatency_99_window"]
	header = "#%12s\t%12s\t%12s" % ("Workload", "Nodes", "Latency")
	
	def __init__(self):
		super(GCTransitionAnalysis, self).__init__(self.reqs)
		
		parser = self.getParser()
		parser.add_argument("--mode", action="store", choices=["normal", "transition"], required=True)
		parser.add_argument("--file", action="store_true", default=False)
	
	def processExperiment(self, args, experiment, results, output):
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)

		transition = []
		normal = []
		
		for i in xrange(1, max_i):
			dt = timestamps[i]
			
			workload = results.getCommonValue(dt, "actual_workload")
			prev_any_transitions = results.getCommonValue(dt, "any_transitions")
			any_transitions = results.getCommonValue(dt, "any_transitions")
			nodes = results.getCommonValue(dt, "nodes_ready")
			readlatency_99_window = results.getCommonValue(dt, "readlatency_99_window")
			
			if prev_any_transitions != any_transitions:
				continue
			
			if any_transitions:
				transition.append((workload, nodes, readlatency_99_window))
			else:
				normal.append((workload, nodes, readlatency_99_window))
		
		if args.mode == "normal":
			data = normal
		elif args.mode == "transition":
			data = transition
			
		if args.file:
			self.outputFile(experiment, data, "%s.%slatency" % (output, args.mode))
		else:
			self.outputPrint(experiment, data)
	
	def outputPrint(self, experiment, data):
		print self.header
		print "# %s" % experiment
		for entry in data:
			print "%12.2f\t%12.2f\t%12.2f" % entry
		pass
	
	def outputFile(self, experiment, data, filename):
		f = open(filename, "w")
		f.write("%s\n" % self.header)
		for entry in data:
			f.write("%12.2f\t%12.2f\t%12.2f\n" % entry)
		f.close()

if __name__ == "__main__":
	processor = GCTransitionAnalysis()
	processor.run()
