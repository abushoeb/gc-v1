#!/usr/bin/python

from datetime import datetime
from datetime import timedelta
from bisect import *


def find_lt(a, x):
	'Find rightmost value less than x'
	i = bisect_left(a, x)
	if i:
		return a[i-1]
	raise ValueError
    
def find_gt(a, x):
	'Find leftmost value greater than x'
	i = bisect_right(a, x)
	if i != len(a):
		return a[i]
	raise ValueError
	
def interpolate(p1, p2, x):
	x1, y1 = p1
	x2, y2 = p2
	x1 = dt_to_unix(x1)
	x2 = dt_to_unix(x2)
	x = dt_to_unix(x)

	m = float(y2 - y1)/float(x2-x1)
	b = y1 - m*x1
	return m*x + b

def dt_to_unix(dt):
	return (dt - datetime(1970, 1, 1)).total_seconds()

class DiscreteFunction:
	values = None
	cached_values = None
	slot_lens = None
	index = None
	
	def __init__(self, filename):
		self.values = {}
		self.cached_values = {}
		self.slot_lens = {}
		self.index = []
		
		# load data
		f = open(filename, "r")
		data = f.read()
		lines = data.strip().split("\n")
	
		for line in lines:
			fields = line.split(" ")
			dt = datetime.strptime(fields[0] + " " + fields[1], "%Y-%m-%d %H:%M:%S.%f")
			value = float(fields[2])
			self.values[dt] = value
		
		# extrapolate slot lengths
		prev = None
		
		for key in sorted(self.values.keys()):
			if prev == None:
				prev = key
				continue
			
			delta = key - prev
			seconds = float(delta.seconds) + float(delta.microseconds * 0.000001)
			self.slot_lens[prev] = seconds
			prev = key
		
		# since we define the slot length as difference between values, we need to have a zero for the last one
		self.slot_lens[key] = 0.0
		
		for key in sorted(self.values.keys()):
			self.index.append(key)
		
	def value(self, dt):
		if dt in self.values.keys():
			return self.values[dt]
		elif dt in self.cached_values.keys():
			return self.cached_values[dt]
		
		return None

	def value_closest(self, dt):
		value = self.value(dt)
		
		if value == None:
			if dt < self.min_param() or dt > self.max_param():
				return 0.0
			
			closest_param = self.closest_param(dt)
			return self.value(closest_param)
		else:
			return value
			
	def value_interpolated(self, dt, cache=False):
		
		if dt < self.min_param() or dt > self.max_param():
			return 0.0
		
		value = self.value(dt)
		
		if value == None:
			result = None
			'''
			start = datetime.now()
			keys = sorted(self.values.keys())
			prev = None			
			for key in keys:
				if prev == None:
					prev = key
					continue
				
				if prev < dt and dt < key:
					prev_tuple = prev, self.values[prev]
					key_tuple = key, self.values[key]
					if self.values[prev] == self.values[key]:
						result = self.values[prev]
					else:
						result = interpolate(prev_tuple, key_tuple, dt)
					break				
				prev = key
			#if result >= 0: print "Linear: " + str(datetime.now() - start), result, 
			'''
			start = datetime.now()
			lt = find_lt(self.index, dt)
			gt = find_gt(self.index, dt)
			lt_tuple = lt, self.values[lt]
			gt_tuple = gt, self.values[gt]
			
			if self.values[lt] == self.values[gt]:
				result = self.values[lt]
			else:
				result = interpolate(lt_tuple, gt_tuple, dt)
			#if result >= 0: print "Binary: " + str(datetime.now() - start), result
			
			if cache == True:
				self.cached_values[dt] = result
				
			return result
			
		else:
			return value
		
	def value_by_offset(self, offset):
		dt = self.min_param()
		dt += timedelta(seconds=offset)
		return self.value_interpolated(dt)
	
	def offset_limit(self):
		delta = self.max_param() - self.min_param()
		return delta.seconds - 1
		
	def slot_len(self, dt):
		if dt in self.slot_lens.keys():
			return self.slot_lens[dt]
		
		return None
		
	def slot_len_closest(self, dt):
		slot_len = self.slot_len(dt)
		
		if slot_len == None:
			if dt < self.min_param() or dt > self.max_param():
				return 0.0			
			
			closest_param = self.closest_param(dt)
			return self.slot_len(closest_param)
		else:
			return slot_len
	
	def closest_param(self, target):
		closest = None
		
		for key in sorted(self.values.keys()):
			if closest == None or abs(target - key) < abs(target - closest):
				closest = key
		
		return closest
	
	def min_param(self):
		keys = sorted(self.values.keys())
		return keys[0]
	
	def max_param(self):
		keys = sorted(self.values.keys())
		return keys[len(keys)-1]
	
	def params(self):
		keys = sorted(self.values.keys())
		return keys
	
	
def integrate_single(func, start=datetime.min, end=datetime.max, name=None):
	params = func.params()
	accum = 0.0

	for param in params:
		#if name=="solar":
		#	print param
			
		if param >=start and param <=end:
			value = func.value(param)
			slot_len = func.slot_len(param)

			#if value == None:
			#	value = 0.0
			#if slot_len == None:
			#	slot_len = 0.0
				
			accum += value * slot_len
		
	return accum

def integrate_difference(func2, func1, start=datetime.min, end=datetime.max):
	params = func2.params()
	
	accum = 0.0
	
	for param in params:
		if param >= start and param <= end:
			value2 = func2.value_closest(param)
			slot_len2 = func2.slot_len_closest(param)
			value1 = func1.value_closest(param)
			slot_len1 = func1.slot_len_closest(param)
			
			if value1 > value2:
				value1 = value2
				
			accum += value2*slot_len2 - value1*slot_len1
	
	return accum

def integrate_single_interpolated(func, start=datetime.min, end=datetime.max, name=None):
	accum = 0.0

	start_dt = max(start, func.min_param())
	end_dt = min(end, func.max_param())
		
	current_dt = start_dt
	
	while current_dt <= end_dt:
		accum += func.value_interpolated(current_dt)
		current_dt += timedelta(seconds=15)
	
	
	return accum*15.0

def integrate_difference_interpolated(func2, func1, start=datetime.min, end=datetime.max):
	accum = 0.0
	
	start_dt = max(min(func2.min_param(), func1.min_param()), start)
	end_dt = min(max(func2.max_param(), func1.max_param()), end)
	
	current_dt = start_dt
	
	while current_dt <= end_dt:
		value2 = func2.value_interpolated(current_dt)
		value1 = func1.value_interpolated(current_dt)
		
		if value1 > value2:
			value1 = value2
			
		accum += value2 - value1
		current_dt += timedelta(seconds=15)
		
	return accum*15.0

def integrate_single_trapz(func, start=datetime.min, end=datetime.max, name=None):
	from scipy import integrate
	result = integrate.trapz(func.values, func.index)
	return result

def integrate_difference_trapz(func, start=datetime.min, end=datetime.max):
	return 0.0

def integrate_single_quad(func, start=datetime.min, end=datetime.max, name=None):
	from scipy import integrate
	result = integrate.quad(func.value_by_offset, 0, func.offset_limit(), limit=10000000)[0]
	return result

def integrate_difference_quad(func, start=datetime.min, end=datetime.max):
	return 0.0
