#!/usr/bin/python

from gcbase import GCManagerBase
from experimentclock import ExperimentClock

class GCSolarManager(GCManagerBase):
	
	def __init__(self, solar_predictor, solar_predictor_perfect):
		self.clock = ExperimentClock.getInstance()
		self.solar_predictor = solar_predictor
		self.solar_predictor_perfect = solar_predictor_perfect
		
	def getValuesForLogging(self):
		values = {}
		predicted_solar = self.solar_predictor.getSolarAt(self.clock.get_current_time())
		used_solar = self.solar_predictor_perfect.getSolarAt(self.clock.get_current_time())
		values["predicted_solar"] = predicted_solar
		values["used_solar"] = used_solar
		
		return values