#!/usr/bin/python

from gccompactionmanager import *
from threading import Thread
from threading import Semaphore
from gcutil import *
from datetime import datetime

#sem = OrderedSemaphore(2)
sem = TokenOrderedSemaphore(1)
#sem = Semaphore(1)

def tfunc(id):
	while True:
		sem.acquire()
		sys.stdout.write("%s: Thread %d running\n" % (str(datetime.now()), id))
		sys.stdout.flush()
		time.sleep(5)
		#print "Thread %d done" % id
		sem.release()

def tfunc_priority(id):
	while True:
		sem.acquire(priority=True)
		sys.stdout.write("Priority thread %d running\n" % id)
		sys.stdout.flush()
		time.sleep(1)
		#print "Thread %d done" % id
		sem.release()
	
def tfunc2(id):
	sem.acquire()
	sys.stdout.write("Thread %d running (sleeping)\n" % id)
	sys.stdout.flush()
	time.sleep(20);
	print sem.counter
	sys.stdout.write("Thread %d done sleeping\n" % id)
	sys.stdout.flush()
	sem.release()

def tfunc3(id):
	sem.superAcquire()
	sys.stdout.write("SuperThread %d running (sleeping)\n" % id)
	sys.stdout.flush()
	print sem.counter
	time.sleep(20)
	sys.stdout.write("SuperThread %d done sleeping\n" % id)
	sys.stdout.flush()
	sem.release()
	

threads = []
for i in xrange(0, 4):
	t = Thread(target=tfunc, args=(i,))
	t.daemon=True
	t.start()
	threads.append(t)

#t = Thread(target=tfunc_priority, args=(0,))
#t.daemon=True
#t.start()
#threads.append(t)

time.sleep(15)
sem.setCount(2)
print "Increase to %s" % sem.getCount()
time.sleep(15)
sem.setCount(3)
print "Increase to %s" % sem.getCount()
time.sleep(15)
sem.setCount(2)
print "Decrease to %s" % sem.getCount()
time.sleep(15)
sem.setCount(1)
print "Decrease to %s" % sem.getCount()

for t in threads:
	t.join()


sys.exit()

threads = []

print sem.counter

for i in xrange(0, 4):
	t = Thread(target=tfunc2, args=(i,))
	t.daemon=True
	t.start()
	threads.append(t)


time.sleep(10)

t = Thread(target=tfunc3, args=(0,))
t.daemon=True
t.start()
threads.append(t)

for t in threads:
	t.join()

print sem.counter
