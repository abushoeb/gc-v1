#!/usr/bin/python

import os
import shutil
import sys
import random
from datetime import *


if os.path.exists("./testdata"):
	shutil.rmtree("./testdata")
os.makedirs("./testdata")

power_f = open("./testdata/est_node_power", "w")
solar_f = open("./testdata/used_solar", "w")


start_dt = datetime(year=2012, month=1, day=1, hour=0, minute=0)
#end_dt = datetime + timedelta(hours=1)
#current_dt = start_dt

dt = start_dt
for i in xrange(0,60*60):
	if i == 0:
		value = 0.0
	else:
		value = 1000.0
	power_f.write("%s %f\n" % (datetime.strftime(dt, "%Y-%m-%d %H:%M:%S.%f"), value))
	dt += timedelta(seconds=1, milliseconds=random.randint(0, 50))
power_f.write("%s %f\n" % (datetime.strftime(dt, "%Y-%m-%d %H:%M:%S.%f"), 0.0))


dt = start_dt + timedelta(minutes=15)
for i in xrange(0, 60*30):
	if i == 0:
		value = 0.0
	else:
		value = 1500.0
	solar_f.write("%s %f\n" % (datetime.strftime(dt, "%Y-%m-%d %H:%M:%S.%f"), value))
	dt += timedelta(seconds=1, milliseconds=random.randint(0, 50))
solar_f.write("%s %f\n" % (datetime.strftime(dt, "%Y-%m-%d %H:%M:%S.%f"), 0.0))


power_f.close()
solar_f.close()

