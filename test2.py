#!/usr/bin/python

from gccommon import *

if __name__ == "__main__":
	
	minimumNodes = list(MINIMUM_NODES)
	oddOptionalNodes = []
	evenOptionalNodes = []
		
	for node in OPTIONAL_NODES:
		if OPTIONAL_NODES.index(node) % 2 == 0:
			evenOptionalNodes.append(node)
		else:
			oddOptionalNodes.append(node)
			
	
	for i in xrange(0, len(minimumNodes)):
		print minimumNodes[i], oddOptionalNodes[i], evenOptionalNodes[i]