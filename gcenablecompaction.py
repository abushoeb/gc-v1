#!/usr/bin/python

import argparse
import signal
import sys

from gccontroller import *
from gcycsb import *
from gchelpers import *
from gcjolokiamanager import *
from gcycsbmanager import *
from pyjolokia import *

JOLOKIA_URL = "http://%s:8778/jolokia/"

def getJolokiaConn(host):
	url = JOLOKIA_URL % host
	jolokia_conn = Jolokia(url, timeout=5)
	return jolokia_conn
	

def enable_auto_compaction():
	mbean = "org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data"
	operation = "enableAutoCompaction"

	max_attrib = "MaximumCompactionThreshold"
	min_attrib = "MinimumCompactionThreshold"
	
	for node in (MINIMUM_NODES + OPTIONAL_NODES):
		try:
			jolokia_conn = getJolokiaConn(node)
		
			max_results = jolokia_conn.request(type="read", mbean=mbean, attribute=max_attrib)["value"]
			min_results = jolokia_conn.request(type="read", mbean=mbean, attribute=min_attrib)["value"]
			
			if max_results > 0 and min_results > 0:
				print "Auto compaction already enabled on %s" % node
				continue

			jolokia_conn.request(type="exec", mbean=mbean, operation=operation)
		
			max_results = jolokia_conn.request(type="read", mbean=mbean, attribute=max_attrib)["value"]
			min_results = jolokia_conn.request(type="read", mbean=mbean, attribute=min_attrib)["value"]
		
			if max_results > 0 and min_results > 0:
				print "Success, enabled auto compaction on %s" % node
			else:
				print "Error: failed to enable auto compaction on %s" % node
		
		except JolokiaError:
			print "JolokiaError, could not connect to %s" % node
			sys.exit()

if __name__ == "__main__":
	enable_auto_compaction()
		

