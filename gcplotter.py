#!/usr/bin/python

import argparse
from optparse import OptionParser
import sys
import os
import math
import matplotlib.pyplot as pyplot
import matplotlib
import pylab
import traceback

from gcresults import *
from gchelpers import *
from gcmodel import *
from gcworkloadpredictor import *
from gcresponsetime import *
from gcanalysis import *
from matplotlib.dates import date2num
from datetime import datetime
from datetime import timedelta
from gcwekaparser import *
from gcstateanalysis import *
from gccompactionmanager import *
from gcscheduler import *

class GCPlotBase(object):
	def __init__(self, title=None, ylim=(None, None), ylabel=None, xlabel=None, legend=False):
		if title == None:
			self.title = value
		else:
			self.title = title
		self.ylim = ylim
		self.ylabel = ylabel
		self.xlabel = xlabel
		self.legend = legend
	
	def doPlot(self):
		# Y range
		ymin, ymax = self.ylim
		if ymin != None:
			pyplot.ylim(ymin=ymin)
		if ymax != None:
			pyplot.ylim(ymax=ymax)
		
		# Title
		pyplot.title(self.title)
		
		# Labels
		if self.ylabel != None:
			pyplot.ylabel(self.ylabel)
		if self.xlabel != None:
			pyplot.xlabel(self.xlabel)
		
		#pyplot.legend(self.legend)
		pyplot.grid(True)

class GCPlot(GCPlotBase):
	def __init__(self, results, indexLine=None, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCPlot, self).__init__(title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.results = results
		self.timestamps = results.availableTimestamps()
		self.nodes = results.availableNodes()
		self.indexLine = indexLine

	def getTimestamps(self):
		return self.timestamps
	
	def getIntervalWidths(self):
		widths = []
		for i in xrange(1, len(self.timestamps)):
			interval = date2num(self.timestamps[i]) - date2num(self.timestamps[i-1])
			widths.append(interval)
		widths.append(interval)
		
		return widths
		
	def getCommonValueData(self, value):
		data = []
		for timestamp in self.timestamps:
			#print timestamp
			v = self.results.getCommonValue(timestamp, value)
			if v == None:
				return v
			data.append(v)
		return data
		
	def getNodeValueData(self, node, value):
		data = []
		for timestamp in self.timestamps:
			v = self.results.getNodeValue(timestamp, node, value)
			if v == None:
				return v
			data.append(v)
		return data
		
	def doPlot(self):
		superclass = super(GCPlot, self)
		superclass.doPlot()
		
		# X range
		xmin = matplotlib.dates.date2num(self.timestamps[0])
		xmax = matplotlib.dates.date2num(self.timestamps[len(self.timestamps)-1])
		pyplot.xlim((xmin, xmax))
		
		values = []
		if self.indexLine != None:
			for dt in self.timestamps:
				values.append(self.indexLine)
			
			pyplot.plot(self.timestamps, values)

class GCSimpleCommonPlot(GCPlot):
	def __init__(self, results, value, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCSimpleCommonPlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.value = value

	def doPlot(self):
		superclass = super(GCSimpleCommonPlot, self)
		superclass.doPlot()
		
		timestamps = self.timestamps
		
		values = superclass.getCommonValueData(self.value)
		
		pyplot.plot(timestamps, values)
		

# plots multiple values from the same GCResults with a common timescale
class GCMultiCommonPlot(GCPlot):
	def __init__(self, results, values, indexLine=None, title=None, ylim=(None, None), ylabel=None, xlabel=None, legend=False):
		super(GCMultiCommonPlot, self).__init__(results, indexLine=indexLine, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.values = values
		self.legend = legend
		
	def doPlot(self):
		superclass = super(GCMultiCommonPlot, self)
		superclass.doPlot()
		
		for value in self.values:
			timestamps = self.timestamps
			values = superclass.getCommonValueData(value)
			pyplot.plot(timestamps, values, label=value.replace('_', ' ').title(), linewidth=2.0)
		
		ymin, ymax = self.ylim
		if ymin != None:
			pyplot.ylim(ymin=ymin)
		if ymax != None:
			pyplot.ylim(ymax=ymax)
		
		#if self.legend:
		#	pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)

class GCResponseTimePlot(GCPlot):
	def __init__(self, results, model_file, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCResponseTimePlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.model = GCTableResponseTimeModel(model_file)
	
class GCPredictionModelOverlayPlot(GCPlot):
	FANCY = 0
	AVG = 1
	MAX = 2
	INSTANTANEOUS = 4
	
	def __init__(self, results, model, predictor, indexLine=None, title=None, ylim=(None, None), ylabel=None, xlabel=None, continuous=True, mode=FANCY, lessnodes=0, factornodes=1.0):
		super(GCPredictionModelOverlayPlot, self).__init__(results, indexLine=indexLine, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		
		self.response_time_model = model
		self.workload_predictor = predictor
		self.continuous = continuous
		self.mode = mode
		
		self.lessnodes = lessnodes
		self.factornodes = factornodes
		
		self.max_throughput = self.response_time_model.peakThroughput(NUM_NODES, 75)
		
	def doPlot(self):
		superclass = super(GCPredictionModelOverlayPlot, self)
		superclass.doPlot()

		values = []
		
		delta = timedelta(minutes=15)
		nodes_ready = superclass.getCommonValueData("nodes_ready")
		nodes_transition = superclass.getCommonValueData("nodes_transition")
		#nodes_ready = superclass.getCommonValueData("nodes_total")
		
		if self.continuous:
			ts = self.timestamps
			for i in xrange(0, len(self.timestamps)):
				dt = self.timestamps[i]
				nodes = nodes_ready[i] - self.lessnodes

				if self.mode == self.FANCY:
					workload_prediction = denormalize(self.workload_predictor.getCalculatedWorkloadForInterval(dt, delta), self.max_throughput)
				elif self.mode == self.AVG:
					workload_prediction = denormalize(self.workload_predictor.getAvgWorkloadForInterval(dt, delta), self.max_throughput)
				elif self.mode == self.MAX:
					workload_prediction = denormalize(self.workload_predictor.getMaxWorkloadForInterval(dt, delta), self.max_throughput)
				elif self.mode == self.INSTANTANEOUS:
					workload_prediction = denormalize(self.workload_predictor.getWorkloadAt(dt), self.max_throughput)
				
				#nodes = self.response_time_model.nodesRequired(workload_prediction, 75)
				
				try:
					predicted_response_time = self.response_time_model.expectedLatency(nodes, workload_prediction)
				except Exception, e:
					predicted_response_time = 200
					print "Exception:", e
				values.append(predicted_response_time)
		else:
			ts = []
			first_dt = self.timestamps[0]
			last_dt = self.timestamps[-1]
			
			prev_dt = first_dt - delta
			
			OFFSET = 0#5
			for i in xrange(0, len(self.timestamps)-OFFSET):
				current_dt = self.timestamps[i]
				#if current_dt < prev_dt + delta:
				#	continue
				
				ts.append(current_dt)
				
				nodes = nodes_ready[i+OFFSET]
				# If we are starting a node, is like having X fewer nodes
				if nodes_transition[i+OFFSET] > 0:
					nodes -= self.lessnodes
					nodes = nodes * self.factornodes
			
				if self.mode == self.FANCY:
					workload_prediction = denormalize(self.workload_predictor.getCalculatedWorkloadForInterval(current_dt, delta), self.max_throughput)
				elif self.mode == self.AVG:
					workload_prediction = denormalize(self.workload_predictor.getAvgWorkloadForInterval(current_dt, delta), self.max_throughput)
				elif self.mode == self.MAX:
					workload_prediction = denormalize(self.workload_predictor.getMaxWorkloadForInterval(current_dt, delta), self.max_throughput)
				elif self.mode == self.INSTANTANEOUS:
					workload_prediction = denormalize(self.workload_predictor.getWorkloadAt(current_dt), self.max_throughput)
				
				try:
					predicted_response_time = self.response_time_model.expectedLatency(nodes, workload_prediction)
				except Exception, e:
					predicted_response_time = 150
					print "Exception:", e
				values.append(predicted_response_time)
				
				print current_dt, workload_prediction, predicted_response_time
				
				prev_dt = current_dt

		pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)
		
		pyplot.plot(ts, values, label=self.response_time_model.model_file)
		#pyplot.plot(ts, values, label=self.title)

class GCModelOverlayPlot(GCPlot):
	
	def __init__(self, results, model, indexLine=None, title=None, ylim=(None,None), ylabel=None, xlabel=None, lessnodes=0, factornodes=1.0, transincr=0.0):
		super(GCModelOverlayPlot, self).__init__(results, indexLine=indexLine, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		
		self.response_time_model = model
		self.lessnodes = lessnodes
		self.factornodes = factornodes
		self.transincr = transincr
		
	def doPlot(self):
		superclass = super(GCModelOverlayPlot, self)
		superclass.doPlot()
		
		dts = []
		values = []
		
		nodes_ready = superclass.getCommonValueData("nodes_ready")
		nodes_transition = superclass.getCommonValueData("nodes_transition")
		workload = superclass.getCommonValueData("actual_workload")
		
		for i in xrange(0, len(self.timestamps)):
			dt = self.timestamps[i]
			
			nodes = nodes_ready[i]
			if nodes_transition[i] > 0:
				nodes -= self.lessnodes
				nodes = nodes * self.factornodes
			
			try:
				predicted_response_time = self.response_time_model.expectedLatency(nodes, workload[i])
			except Exception as e:
				predicted_response_time = 150
				print "Exception:", e
			
			dts.append(dt)
			
			if nodes_transition[i] > 0:
				predicted_response_time += self.transincr
			
			values.append(predicted_response_time)
			
			
		#pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)
		pyplot.plot(dts, values, label="Expected 99th Percentile Response Time")

class GCRegressionOverlayPlot(GCPlot):
	
	def __init__(self, results, model, indexLine=None, title=None, ylim=(None,None), ylabel=None, xlabel=None):
		super(GCRegressionOverlayPlot, self).__init__(results, indexLine=indexLine, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		
		self.response_time_model = model
				
	def doPlot(self):
		superclass = super(GCRegressionOverlayPlot, self)
		superclass.doPlot()
		
		dts = []
		values = []
		
		nodes_ready = superclass.getCommonValueData("nodes_ready")
		nodes_transition = superclass.getCommonValueData("nodes_transition")
		actual_workload = superclass.getCommonValueData("actual_workload")
		any_transitions = superclass.getCommonValueData("any_transitions")
		
		results = self.results
		transitionAnalyzer = TransitionAnalyzer(self.results)
		
		regression = loadRawRegression("weka.regression")
		print regression
		
		for i in xrange(0, len(self.timestamps)):
			dt = self.timestamps[i]
			
			transition = any_transitions[i]
			nodes = nodes_ready[i]
			workload = actual_workload[i]		
			model = self.response_time_model.expectedLatency(nodes, workload)
			model_minus_1 = self.response_time_model.expectedLatency(nodes-1, workload)
			model_minus_2 = self.response_time_model.expectedLatency(nodes-2, workload)
			model_minus_3 = self.response_time_model.expectedLatency(nodes-3, workload)
			model_minus_4 = self.response_time_model.expectedLatency(nodes-4, workload)
			model_minus_5 = self.response_time_model.expectedLatency(nodes-5, workload)
			model_minus_6 = self.response_time_model.expectedLatency(nodes-6, workload)
			model_minus_7 = self.response_time_model.expectedLatency(nodes-7, workload)
			model_minus_8 = self.response_time_model.expectedLatency(nodes-8, workload)

			if transition > 0:
				inputs = {}
				try :
					integrated_workload_off = transitionAnalyzer.getIntegratedOffWorkload(dt)
					off_len = transitionAnalyzer.getOffLength(dt)
					transition_len = transitionAnalyzer.getTransitionLength(dt)
					
					inputs["off_len"] = off_len
					inputs["transition_len"] = transition_len
					inputs["nodes"] = nodes
					inputs["avg_workload_transition"] = workload
					inputs["integrated_workload_off"] = integrated_workload_off
					inputs["model"] = model
					inputs["model_minus_1"] = model_minus_1
					inputs["model_minus_2"] = model_minus_2
					inputs["model_minus_3"] = model_minus_3
					inputs["model_minus_4"] = model_minus_4
					inputs["model_minus_5"] = model_minus_5
					inputs["model_minus_6"] = model_minus_6
					inputs["model_minus_7"] = model_minus_7
					inputs["model_minus_8"] = model_minus_8
					predicted_response_time = applyRegression(regression, inputs)
					if predicted_response_time < 0:
						print inputs
					
					predicted_response_time = abs(predicted_response_time)
					print "got from regression: %0.2f" % predicted_response_time
						
				except Exception as e:
					print "failed to get from regression: %s" % e
					#traceback.print_exc()
					#sys.exit()
					predicted_response_time = model
			else:
				predicted_response_time = model

			#print "predicted", predicted_response_time
			dts.append(dt)
			values.append(predicted_response_time)
			
			
		#pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)
		pyplot.plot(dts, values, label="Expected 99th Percentile Response Time")

class GCLatencyHistogramPlot(GCPlot):
	READ = 1
	WRITE = 2
	
	def __init__(self, results, percentile, window=timedelta(minutes=15), stride=1, type=READ, title=None, ylim=(None, None), ylabel=None, xlabel=None, indexLine=None):
		
		if type == self.READ:
			typeStr = "read"
		elif type == self.WRITE:
			typeStr = "write"
			
		self.ourTitle = "Custom Latency Window - %s, %dth percentile, %d seconds" % (typeStr, percentile, window.total_seconds())
		super(GCLatencyHistogramPlot, self).__init__(results, title=self.ourTitle, ylim=ylim, ylabel=ylabel, xlabel=xlabel, indexLine=indexLine)
		self.percentile = percentile
		self.windowDelta = window
		#self.response_time_data = GCResponseTime(results.data_dir, "histogram.%s.crypt11" % typeStr)
		self.data_field = "histogram_%s_crypt12" % typeStr
		self.stride = stride
		#self.legend = legend
		
	def doPlot(self):
		super(GCLatencyHistogramPlot, self).doPlot()
		
		dts = []
		data = []
		
		for i in xrange(2, len(self.timestamps), self.stride):
			dt = self.timestamps[i]
			
			#for j in reversed(xrange(0, i)):
			#	pastDt = self.timestamps[j]
			#	if pastDt <= dt - self.windowDelta:
			#		break
			
			targetDt = dt - self.windowDelta - timedelta(microseconds=1)
			
			if targetDt < self.timestamps[0]:
				pastDt = self.timestamps[0]
			else:
				pastDt = find_le(self.timestamps, targetDt)
			
			#except Exception as e:
			#	continue
			#
			#	print "Exception:"
			#	print "finding shit for", dt - self.windowDelta
			#	print self.timestamps[0]
			#	print self.timestamps[-1]
				
			#print "PastDt", pastDt
			
			try:
				#histogram = self.response_time_data.getHistogramAt(dt)
				histogram = self.results.getCommonValue(dt, self.data_field)
				#pastHistogram = self.response_time_data.getHistogramAt(pastDt)
				pastHistogram = self.results.getCommonValue(pastDt, self.data_field)
				
				if histogram == None:
					print "histogram", histogram
				if pastHistogram == None:
					print "pastHistogram", pastHistogram
				
				if histogram.isEmpty() or pastHistogram.isEmpty():
					continue
				
				windowHistogram = histogram - pastHistogram
				
				dts.append(dt)
				data.append(windowHistogram.getPercentile(self.percentile))
			except Exception as e:
				print "Exception", e
				sys.exit()
		try:
			pyplot.plot(dts, data, label=self.ourTitle)
		#	if self.legend:
		#		pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)
		except Exception as e:
			print "Exception", e
			sys.exit()
	
class GCSLAOverlayPlot(GCPlot):
	
	def __init__(self, results, response_time, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCSLAOverlayPlot, self).__init__(results, title="SLA Progress", ylim=(0.95,1.0), ylabel=ylabel, xlabel=xlabel, indexLine=0.99)
		
		self.response_time = response_time
		self.response_time_data = GCResponseTime(results.data_dir, "histogram.read.crypt11")
	
	def doPlot(self):
		super(GCSLAOverlayPlot, self).doPlot()
		
		dts = []
		data = []
		for i in xrange(0, len(self.timestamps)):
			dt = self.timestamps[i]
			
			histogram = self.response_time_data.getHistogramAt(dt)
			
			total_ops = histogram.getTotalOperations()
			total_ops_less_than_ms = histogram.getTotalOperationsLessThanMs(self.response_time)
			percentage = total_ops_less_than_ms / total_ops
			#print total_ops, total_ops_less_than_ms, percentage
			
			dts.append(dt)
			data.append(percentage)
			
		pyplot.plot(dts, data, label="Percentage Of Ops Less Than %d ms" % self.response_time)
		#pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)

		
class GCWorkloadPredictionPlot(GCPlot):
	def __init__(self, results, model, predictor, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCWorkloadPredictionPlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		
		self.response_time_model = model
		self.workload_predictor = predictor
		
		self.max_throughput = self.response_time_model.peakThroughput(NUM_NODES, 75)
		
	def doPlot(self):
		superclass = super(GCWorkloadPredictionPlot, self)
		superclass.doPlot()
		
		calculatedValues = []
		percentileValues = []
		maxValues = []
		
		for i in xrange(0, len(self.timestamps)):
			dt = self.timestamps[i]
			#calculated_prediction = denormalize(self.workload_predictor.getCalculatedWorkloadForInterval(dt, timedelta(minutes=15)*2), self.max_throughput) + GCScheduler.WORKLOAD_ERROR_MARGIN
			#percentile_prediction = denormalize(self.workload_predictor.get99thPercentileWorkloadForInterval(dt, timedelta(minutes=15)*2), self.max_throughput) + GCScheduler.WORKLOAD_ERROR_MARGIN
			max_prediction = denormalize(self.workload_predictor.getMaxWorkloadForInterval(dt, timedelta(minutes=15)*2), self.max_throughput) + GCScheduler.WORKLOAD_ERROR_MARGIN
			#print calculated_prediction, percentile_prediction
			
			#calculatedValues.append(calculated_prediction)
			#percentileValues.append(percentile_prediction)
			maxValues.append(max_prediction)
		
		#pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)
		#pyplot.plot(self.timestamps, calculatedValues, label="Predicted Workload (mean+std.dev+fudge[%d])" % GCScheduler.WORKLOAD_ERROR_MARGIN)
		#pyplot.plot(self.timestamps, percentileValues, label="Predicted Workload (99th Percentile+fudge[%d])" % GCScheduler.WORKLOAD_ERROR_MARGIN)
		pyplot.plot(self.timestamps, maxValues, label="Predicted Workload (max+fudge[%d])" % GCScheduler.WORKLOAD_ERROR_MARGIN)

# overlays results from one or more different GCResults with relative timestamps
class GCMultiCommonPlotOverlay(GCPlotBase):
	def __init__(self, results=None, value=None, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCMultiCommonPlotOverlay, self).__init__(title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		
		self.data = []
		self.value = value
		if results != None:
			self.addResults(results, value, label, ylim)
		
	def addResults(self, results, value, label=None, ylim=None):
		if label == None:
			label = value
		
		data = (results, value, label, ylim)
		self.data.append(data)		
		
	def relativeTimestamps(self, timestamps):
		new_timestamps = []
		
		first = timestamps[0]
		
		for ts in timestamps:
			delta = ts - first
			new_ts = delta.seconds + delta.microseconds/1000000
			new_timestamps.append(new_ts)
		
		return new_timestamps
	
	def getCommonValueData(self, value, results):
		data = []
		for timestamp in results.availableTimestamps():
			v =results.getCommonValue(timestamp, value)
			if v == None:
				return None
			data.append(v)
		return data
	
	def doPlot(self):
		superclass = super(GCMultiCommonPlotOverlay, self)
		superclass.doPlot()
		
		for data in self.data:
			results, value, label, ylim = data
			timestamps = self.relativeTimestamps(results.availableTimestamps())
			values = self.getCommonValueData(value, results)

			pyplot.plot(timestamps, values, label=label)
			
		ymin, ymax = self.ylim
		if ymin != None:
			pyplot.ylim(ymin=ymin)
		if ymax != None:
			pyplot.ylim(ymax=ymax)
		
		#if self.legend:
		#	pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)

class GCSimpleNodePlot(GCPlot):
	def __init__(self, results, value, node, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCSimpleNodePlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.value = value
		self.node = node
		
	def doPlot(self):
		superclass = super(GCSimpleNodePlot, self)
		superclass.doPlot()
		
		timestamps = self.timestamps
		values = superclass.getNodeValueData(self.node, self.value)
		#print self.node, self.value, len(values)
		
		pyplot.plot(timestamps, values)
		
		ymin, ymax = self.ylim
		if ymin != None:
			pyplot.ylim(ymin=ymin)
		if ymax != None:
			pyplot.ylim(ymax=ymax)
		
		pyplot.title(self.title)
		pyplot.grid(True)
		
# plot multiple nodes' versions of the same value, overlaid
class GCSimpleMultiNodePlot(GCPlot):
	def __init__(self, results, value, nodes, legend=True, title=None, ylim=(None, None), ylabel=None, xlabel=None):
		super(GCSimpleMultiNodePlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.value = value
		self.nodes = nodes
		self.legend = legend
		
	def doPlot(self):
		superclass = super(GCSimpleMultiNodePlot, self)
		superclass.doPlot()
		
		for node in self.nodes:
			timestamps = self.timestamps
			values = superclass.getNodeValueData(node, self.value)
			pyplot.plot(timestamps, values, label=node)
		
		#if self.legend:
		#	pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=int(math.ceil(len(self.nodes)/10.0)), borderaxespad=0.0)

class GCPowerPlot(GCPlot):
	def __init__(self, results, title="Power", ylim=(None, None), ylabel="Power (W)", xlabel=None):
		super(GCPowerPlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
	
	def doPlot(self):
		superclass = super(GCPowerPlot, self)
		superclass.doPlot()
		
		timestamps = self.timestamps
		
		brown_consumed = superclass.getCommonValueData("brown_consumed")
		solar_consumed = superclass.getCommonValueData("solar_consumed")
		solar_excess   = superclass.getCommonValueData("solar_excess")
		power_total    = superclass.getCommonValueData("power_total")
		
		widths = self.getIntervalWidths()
		
		pyplot.bar(timestamps, solar_consumed, width=widths, color='g', edgecolor='g')
		pyplot.bar(timestamps, brown_consumed, bottom=solar_consumed, width=widths, color='brown', edgecolor='brown')
		pyplot.bar(timestamps, solar_excess, bottom=power_total, width=widths, color='y', edgecolor='y')
		pyplot.plot(timestamps, power_total, color='black')
	
class GCPowerPlotWithBattery(GCPlot):
	def __init__(self, results, title="Power", ylim=(None, None), ylabel="Power (W)", xlabel=None):
		super(GCPowerPlotWithBattery, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
	
	def vectorSum(self, vector):
		new = []
		
		for i in xrange(len(vector[0])):
			s = 0.0
			for v in vector:
				s += v[i]
			new.insert(i, s)
		return new
	
	def doPlot(self):
		superclass = super(GCPowerPlotWithBattery, self)
		superclass.doPlot()
		
		timestamps = self.timestamps
		
		brown_consumed = superclass.getCommonValueData("brown_consumed")
		solar_consumed = superclass.getCommonValueData("solar_consumed")
		solar_excess   = superclass.getCommonValueData("solar_excess")
		power_total    = superclass.getCommonValueData("power_total")
		
		BPowerLoad = superclass.getCommonValueData("b_power_load")
		BattPowerLoad = superclass.getCommonValueData("batt_power_load")
		GPowerLoad = superclass.getCommonValueData("g_power_load")
		GPowerBatt = superclass.getCommonValueData("g_power_batt")
		BPowerBatt = superclass.getCommonValueData("b_power_batt")
		GPowerNet = superclass.getCommonValueData("g_power_net")
		GPowerUnused = superclass.getCommonValueData("g_power_unused")
		PowerLoad = superclass.getCommonValueData("power_load")
		GPower = superclass.getCommonValueData("g_power")
		
		BPowerLoadColor = "brown"
		BattPowerLoadColor = "yellow"
		GPowerLoadColor = "#009933"
		GPowerBattColor = "#FF0000"
		BPowerBattColor = "#9900CC"
		GPowerNetColor = "#FF7519"
		GPowerUnusedColor = "#9AFF79"
		PowerLoadColor = "black"
		GPowerColor = "#005C00"
		
		widths = self.getIntervalWidths()
		
		pyplot.bar(timestamps, GPowerLoad, width=widths, color=GPowerLoadColor, edgecolor=GPowerLoadColor)
		prevTop = self.vectorSum([GPowerLoad])
		pyplot.bar(timestamps, BattPowerLoad, bottom=prevTop, width=widths, color=BattPowerLoadColor, edgecolor=BattPowerLoadColor)
		prevTop = self.vectorSum([prevTop, BattPowerLoad])
		pyplot.bar(timestamps, BPowerLoad, bottom=prevTop, width=widths, color=BPowerLoadColor, edgecolor=BPowerLoadColor)
		prevTop = self.vectorSum([prevTop, BPowerLoad])
		pyplot.bar(timestamps, GPowerBatt, bottom=prevTop, width=widths, color=GPowerBattColor, edgecolor=GPowerBattColor)
		prevTop = self.vectorSum([prevTop, GPowerBatt])
		pyplot.bar(timestamps, BPowerBatt, bottom=prevTop, width=widths, color=BPowerBattColor, edgecolor=BPowerBattColor)
		prevTop = self.vectorSum([prevTop, BPowerBatt])		
		pyplot.bar(timestamps, GPowerNet, bottom=prevTop, width=widths, color=GPowerNetColor, edgecolor=GPowerNetColor)
		prevTop = self.vectorSum([prevTop, GPowerNet])
		pyplot.bar(timestamps, GPowerUnused, bottom=prevTop, width=widths, color=GPowerUnusedColor, edgecolor=GPowerUnusedColor)
		pyplot.plot(timestamps, PowerLoad, color=PowerLoadColor)
		pyplot.plot(timestamps, GPower, color=GPowerColor)
		
class GCNodePlot(GCPlot):
	def __init__(self, results, title="Nodes", ylim=(None, None), ylabel="Nodes", xlabel=None):
		super(GCNodePlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
	
	def doPlot(self):
		superclass = super(GCNodePlot, self)
		superclass.doPlot()
		
		timestamps = self.timestamps
		
		nodes_ready =      superclass.getCommonValueData("nodes_ready")
		nodes_transition = superclass.getCommonValueData("nodes_transition")
		#nodes_required =   superclass.getCommonValueData("nodes_required")
		
		widths = self.getIntervalWidths()
		
		pyplot.bar(timestamps, nodes_ready, width=widths, color='g', edgecolor='g')
		pyplot.bar(timestamps, nodes_transition, bottom=nodes_ready, width=widths, color='y', edgecolor='y')
		
		#if nodes_required != None:
		#	pyplot.plot(timestamps, nodes_required, color='black')
		
class GCNodeStatePlotBarH(GCPlot):
	def __init__(self, results, title="Nodes States", ylim=(None, None), ylabel="Nodes", xlabel=None):
		super(GCNodeStatePlotBarH, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.title = title
		
	def doPlot(self):
		superclass = super(GCNodeStatePlotBarH, self)
		superclass.doPlot()
		
		centers = pylab.arange(len(self.nodes))
		# load node values
		node_values = {}
		for node in self.nodes:
			node_values[node] = superclass.getNodeValueData(node, "state")
		
		accum = 0
		fig, ax = pyplot.subplots()
		color_table = {0 : 'r', 1 : 'y', 2 : 'g'}

		for i in xrange(1,len(self.timestamps)):
			timestamp = self.timestamps[i]
			prev = self.timestamps[i-1]
			
			data = []
			colors = []
			edate, bdate = [matplotlib.dates.date2num(item) for item in (timestamp, prev)]
			
			for node in self.nodes:
				value = node_values[node][i]
				data.append(edate-bdate)
				colors.append(color_table[value])
			
			ax.barh(centers, data, left=bdate, color=colors, edgecolor=colors, align='center', height=1)
			ax.xaxis_date()
		
		pyplot.yticks(centers, self.nodes)
		pyplot.ylim(ymin=0)
		pyplot.ylim(ymax=len(self.nodes))

class GCNodeStatePlot(GCPlot):
	def __init__(self, results, title="Nodes States", ylim=(None, None), ylabel="Nodes", xlabel=None):
		super(GCNodeStatePlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		
	def doPlot(self):
		superclass = super(GCNodeStatePlot, self)
		superclass.doPlot()
		
		timestamps = self.timestamps
		
		centers = pylab.arange(len(self.nodes))
		# load node values
		node_values = {}
		node_centers = {}
		for i in xrange(0, len(self.nodes)):
			node = self.nodes[i]
			node_centers[node] = centers[i]
			node_values[node] = superclass.getNodeValueData(node, "state")			
		
		widths = self.getIntervalWidths()
		
		color_table = {0 : 'r', 1 : 'y', 2 : 'g', 4: 'b'}
		for node in self.nodes:
			data = []
			colors = []
			
			for i in xrange (0, len(timestamps)):
				data.append(1)
				value = node_values[node][i]
				colors.append(color_table[value])
			
			pyplot.bar(timestamps, data, width=widths, bottom=node_centers[node], color=colors, edgecolor=colors)
		pyplot.yticks(centers, self.nodes)
		pyplot.ylim(ymin=0)
		pyplot.ylim(ymax=len(self.nodes))

class GCNodeStatePlotNew(GCPlot):
	
	def __init__(self, results, title="Nodes States", ylim=(None, None), ylabel="Nodes", xlabel=None):
		super(GCNodeStatePlotNew, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		
	def doPlot(self):
		superclass = super(GCNodeStatePlotNew, self)
		superclass.doPlot()
		timestamps = self.timestamps
		centers = pylab.arange(len(self.nodes))
		
		# load node values
		node_values = {}
		node_centers = {}
		node_transitions = {}
		
		for i in xrange(0, len(self.nodes)):
			node = self.nodes[i]
			node_centers[node] = centers[i]
			
		color_table = {State.OFF : 'r', State.TRANS : 'y', State.ON : 'g', State.SCHED : 'b', State.WAIT : 'm'}
				
		stateAnalyzer = StateAnalyzer(self.results)
		states = stateAnalyzer.getStates()
					
		for node in self.nodes:
			dts = []
			data = []
			colors = []
			widths = []
			
			for state in states[node]:
				startDt = state.getStartDt()
				endDt = state.getEndDt()
				
				dts.append(startDt)
				data.append(1)
				colors.append(color_table[state.state])
				widths.append(date2num(endDt) - date2num(startDt))
			
			if len(dts) > 0:
				print dts, widths
				pyplot.bar(dts, data, width=widths, bottom=node_centers[node], color=colors, edgecolor=colors)
		
		pyplot.yticks(centers, self.nodes)
		pyplot.ylim(ymin=0)
		pyplot.ylim(ymax=len(self.nodes))

class GCCompactionPlot(GCPlot):
	def __init__(self, results, title="Compaction", ylim=(None, None), ylabel="Nodes", xlabel=None):
		super(GCCompactionPlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		#self.compaction_data = GCCompactionData(results.data_dir)
		
	def doPlot(self):
		superclass = super(GCCompactionPlot, self)
		superclass.doPlot()
		#timestamps = self.compaction_data.availableTimestamps()
		timestamps = self.results.availableTimestamps()
		max_i = len(timestamps)
		centers = pylab.arange(len(self.nodes))
		
		# load node values
		node_values = {}
		node_centers = {}
		node_transitions = {}
		
		for i in xrange(0, len(self.nodes)):
			node = self.nodes[i]
			node_centers[node] = centers[i]
			
		for node in self.nodes:
			dts = []
			data = []
			colors = []
			widths = []
			
			current_i = 0
			while current_i < max_i:
				#print "cycling, current_i", current_i
				# if we detect a compaction
				
				#compaction = self.compaction_data.getCompactionAt(node, timestamps[current_i])
				compaction = self.results.getNodeValue(timestamps[current_i], node, "compactions")
				#print timestamps[current_i], compaction, type(compaction)
				if compaction == None or type(compaction) == str or compaction.isEmpty():
					current_i += 1
					continue
				
				startIndex = current_i
				endIndex = None
				
				#print node, "found compaction at", timestamps[startIndex]
				
				for i in xrange(current_i+1, max_i):
					#nextCompaction = self.compaction_data.getCompactionAt(node, timestamps[i])
					nextCompaction = self.results.getNodeValue(timestamps[i], node, "compactions")

					#print nextCompaction
					if nextCompaction == None or nextCompaction.isEmpty() or nextCompaction.compactionId != compaction.compactionId:
						endIndex = i-1
						current_i = i
						
						dts.append(timestamps[startIndex])
						data.append(1)
						widths.append(date2num(timestamps[endIndex]) - date2num(timestamps[startIndex]))
						#print "...ended at", timestamps[endIndex]
						break
				
				if endIndex == None:
					#dts.append(timestamps[startIndex])
					#data.append(1)
					#widths.append(date2num(timestamps[max_i-1]) - date2num(timestamps[startIndex]))
					current_i = max_i
					#print "...ended with experiment"
				#else:
				#	current_i += 1
	

			if len(dts) > 0:
				#print node, dts
				pyplot.bar(dts, data, width=widths, bottom=node_centers[node], color='b')# edgecolor='b')
					
		pyplot.yticks(centers, self.nodes)
		pyplot.ylim(ymin=0)
		pyplot.ylim(ymax=len(self.nodes))
		
class GCEpochPlot(GCPlot):
	def __init__(self, results, epochs, title="Epochs", ylim=(None, None), ylabel="Nodes", xlabel=None):
		super(GCEpochPlot, self).__init__(results, title=title, ylim=ylim, ylabel=ylabel, xlabel=xlabel)
		self.epochs = epochs
		
	def doPlot(self):
		superclass = super(GCEpochPlot, self)
		superclass.doPlot()
		
		begin_times = []
		end_times = []
		labels = []
		
		for epoch in self.epochs.getEpochs():
			labels.append(epoch)
			begin_times.append(self.epochs.getEpochBegin(epoch))
			end_times.append(self.epochs.getEpochEnd(epoch))
		
		color = "g"
		
		for i in xrange(len(begin_times)):
			pyplot.axvspan(begin_times[i], end_times[i], facecolor='g', alpha=0.5)
			if labels[i] != "":
				pyplot.annotate(labels[i], xy=(begin_times[i] + ((end_times[i] - begin_times[i])/3), (0.5)),
					size=15, bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 1.0))

class GCPlotGroup(object):
	def __init__(self, all_xticks=False):
		self.plots = []
		self.all_xticks = all_xticks;
	
	def addPlot(self, plot):
		self.plots.append(plot)
		
	def getPlots(self):
		return self.plots
	
	def __len__(self):
		return len(self.plots)

class GCCombinedPlot(object):
	def __init__(self, plots=[], legendPos=(1.0, 1.0), legendLoc=1):
		self.plots = plots
		self.legendPos = legendPos
		self.legendLoc = legendLoc
	
	def addPlot(self, plot):
		self.plots.append(plot)
		
	def doPlot(self):
		for plot in self.plots:
			plot.doPlot()
			#if self.legend:
			#	if self.legendPos != None and self.legendLoc != None:
			#		pyplot.legend(bbox_to_anchor=self.legendPos, loc=self.legendLoc, ncol=1, borderaxespad=0.0)

class GCPlotter(object):
	def __init__(self, xsize=30, ysize=20, limitx=None):
		self.plotGroups = []
		self.groupsByPlot = {}
		self.firstPlotByGroup = {}
		self.lastPlotByGroup = {}
		self.xsize=xsize
		self.ysize=ysize
		self.limitx = limitx
		
	def addPlot(self, plot):
		group = GCPlotGroup()
		group.addPlot(plot)
		self.addPlotGroup(group)
		
	def addPlotGroup(self, group):
		self.plotGroups.append(group)
		plots = group.getPlots()
		for i in xrange(0, len(plots)):
			plot = plots[i]
			self.groupsByPlot[plot] = group
			if i == 0:
				self.firstPlotByGroup[group] = plot
			if i == len(plots)-1:
				self.lastPlotByGroup[group] = plot
	
	def doPlot(self, output=None, format="pdf"):
		pyplot.rcParams.update({'font.size' : (15 if format=="svg" else 12)})
		pyplot.figure(figsize=(self.xsize,self.ysize), frameon=True)
		allPlots = []
		internalPlots = {}
		firstInternalPlots = {}
		
		for group in self.plotGroups:
			for plot in group.getPlots():
				allPlots.append(plot)
		
		div = 1.0 / len(allPlots)
		for i in xrange(0, len(allPlots)):
			if format=="pdf":
				margin = 0.01
			else:
				margin = 0.03 # PNG
			bottom = 1.0-div - i*div + margin
			height = div - 2*margin
			plot = allPlots[i]
			group = self.groupsByPlot[plot]
			firstPlotInGroup = self.firstPlotByGroup[group]
			if plot == firstPlotInGroup:
				x = pyplot.axes([0.03, bottom, 0.95, height])
				firstInternalPlots[group] = x
			else:
				x = pyplot.axes([0.03, bottom, 0.95, height], sharex=firstInternalPlots[group])
			
			internalPlots[plot] = x
			try:
				plot.doPlot()
			except ValueError, e:
				print plot
				print e
				raise ValueError("Missing value in results...")
			#except Exception, e:
			#	print "Error plotting:", plot
			
			if self.limitx:
				dt = self.limitx.timestamps[0]
				minDt = datetime(month=dt.month, day=dt.day, year=dt.year, hour=dt.hour, minute=0, second=0)
				#maxDt = datetime(month=dt.month, day=dt.day, year=dt.year, hour=23, minute=59, second=59)
				maxDt = minDt + timedelta(hours=23, minutes=59, seconds=59)
				pyplot.xlim(xmin=minDt, xmax=maxDt)
		
		# Hide the xticks for all but the last plot in each group
		#for i in xrange(0, len(self.allPlots)):
		for plot in allPlots:
			group = self.groupsByPlot[plot]
			if group.all_xticks == True:
				continue	
			if plot != self.lastPlotByGroup[group] and len(group) > 1:
				pyplot.setp(internalPlots[plot].get_xticklabels(), visible=False)
		
		# Output
		if output != None:
			pyplot.savefig(output, format=format)
		else:
			pyplot.show()
			

if __name__ == "__main__":
	
	print "This file is now only a library. Functionality has been move to gcplotexperiment.py"
	
	'''
	parser = OptionParser()
	parser.add_option("--experiment",  dest="experiment")
	parser.add_option("--format",      dest="format", default="pdf")
	parser.add_option("--hours",       dest="hours", type=int, default=24)
	parser.add_option("--subsample",   dest="subsample", type=int, default=1)
	parser.add_option("--output",      dest="output", type=str, default=None)
	parser.add_option("-p", "--paper", dest="paper", action="store_true")
	parser.add_option("--solarday", action="store")

	(options, args) = parser.parse_args()
	
	# Paper format
	if options.paper:
		options.subsample = 15 # Every 5 minutes
		#options.subsample = 60 # Every 20 minutes
		options.format = "svg"
		options.hours = 24
	
	# Switch the backend to PDF
	if options.format == "pdf":
		pyplot.switch_backend("PDF")
	elif options.format == "svg":
		pyplot.switch_backend("Cairo")
	elif options.format == "png":
		pyplot.switch_backend("AGG")
	
	if options.solarday == "test":
		solarOverride = GCStaticSolarResultsOverride()
	elif options.solarday != None:
		ts = datetime.strptime(options.solarday, "%m-%d-%Y")
		solarOverride = GCHistoricalSolarResultsOverride(ts)
	else:
		solarOverride = None
	
	# find all experiment files
	experiments = []
	files = os.listdir(".")
	for f in sorted(files):
		if f.startswith(options.experiment) and not "." in f:
			experiments.append(f)
	
	experiments = sorted(experiments, key=lambda(v):"-".join(reversed(v.split("-"))))
	print >> sys.stderr, experiments

	for experiment in experiments:
		if options.output == None:
			options.output = experiment
		
		data_dir = experiment
		output_file = "%s.%s" % (options.output, options.format)
		print "plot will be in %s" % output_file
		
		if options.hours == 0:
			max_timedelta = None
			drop_last = True
		else:
			max_timedelta = timedelta(hours=options.hours)
			drop_last = False
		
		print 'Loading results...'
		results = GCResults(data_dir,
			required = 
				[
					"brown_consumed",
					"solar_consumed",
					"solar_excess",
					"power_total",
					"state",
					"actual_workload",
					"target_workload",
					# Average
					"readlatency_avg_window",
					# 95th
					#"readlatency_95",
					#"writelatency_95",
					#"readlatency_95_cum",
					#"writelatency_95_cum",
					#"readlatency_95_window",
					#"writelatency_95_window",
					#99thoptions
					#"readlatency_99",
					#"writelatency_99",
					"readlatency_99_cum",
					"writelatency_99_cum",
					"readlatency_99_window",
					"writelatency_99_window",
					"cassandra_load",
					"nodes_total",
					"nodes_ready",
					"nodes_transition",
					#"ycsb.throughput",
					#"ycsb.target",
				],
			max_time=max_timedelta,
			drop_last = drop_last,
			subsample=options.subsample)
		
		if solarOverride:	
			results.addOverride(solarOverride)
		
		print 'Reading model...'
		response_time_model = GCTableResponseTimeModelNew("1-22-UNIFORM-300kbs-MIXED-ONE.model", extrapolate=True)
		
		print 'Initialize plotter...'
		if options.paper:
			plotter = GCPlotter(xsize=14, ysize=7)
		else:
			plotter = GCPlotter(xsize=30, ysize=50)
		
		print 'Power...'
		plot = GCPowerPlot(results, title=("" if options.paper else "Power"))
		plotter.addPlot(plot)
		
		# Workload
		print 'Workload...'
		if not options.paper:
			try:
				plot1 = GCMultiCommonPlot(results, ["actual_workload", "target_workload"], title="Workload", ylim=(0,9000), ylabel="op/s")
			except:
				plot1 = GCMultiCommonPlot(results, ["ycsb.throughput", "ycsb.target"], title="Workload", ylim=(0,9000), ylabel="op/s")
			plotter.addPlot(plot1)
		
		# Response time prediction
		#print "Workload prediction..."
		workload_predictor = GCAskTraceWorkloadPredictor("timeline_result.datadjusted", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))
		#plot = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="Response time prediction")
		#plotter.addPlot(plot)
		#plot = GCWorkloadPredictionPlot(results, model=response_time_model, predictor=workload_predictor)
		#plotter.addPlot(plot)
		
		#plot = GCCombinedPlot([plot1, plot2], legendPos=(1.0, 0.0), legendLoc=4)
		#plotter.addPlot(plot)
		
		#plot = GCMultiCommonPlot(results, ["readlatency_99", "writelatency_99"], ylim=(0,150))
		#plotter.addPlot(plot)
		
		#plot = GCMultiCommonPlot(results, ["readlatency_95_cum", "writelatency_95_cum"], ylim=(0, 150))
		#plotter.addPlot(plot)
		
		#plot = GCMultiCommonPlot(results, ["readlatency_99_cum", "writelatency_99_cum"], ylim=(0, 150))
		#plotter.addPlot(plot)
		
		# Response time prediction
		
		# Latency cumulative
		print 'Latency...'
		if options.paper:
			plot = GCMultiCommonPlot(results, ["readlatency_99_window", "readlatency_99_cum"], ylim=(0, 150), indexLine=75, title="", ylabel="Response time (ms)") # We add the 75 ms SLA
			plotter.addPlot(plot)
		else:
			# Merge real and prediction
			plot1 = GCMultiCommonPlot(results, ["readlatency_99_window", "writelatency_99_window"], ylim=(0, 150), indexLine=75, title="Window Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
			# Prediction
			#plot2 = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="-0", lessnodes=0)
			#plot3 = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="-1", lessnodes=1)
			#plot4 = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="-2", lessnodes=2)
			#plot4 = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 200), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="-3.5", lessnodes=3.5) # This is the good one
			plot5 = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="-4", lessnodes=4) # This is the good one
			#plot6 = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="*0.9", factornodes=0.9)
			#plot7 = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCModelOverlayPlot.AVG, continuous=False, title="*0.8", factornodes=0.8)
			plot = GCCombinedPlot([plot1, plot5], legendPos=None)
			plotter.addPlot(plot)
			
			plot = GCMultiCommonPlot(results, ["readlatency_99_cum", "writelatency_99_cum"], ylim=(0, 200), indexLine=75, title="Cumulative Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
			plotter.addPlot(plot)
		
		
		#plot = GCModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, indexLine=75, mode=GCModelOverlayPlot.FANCY, continuous=False)
		
		#plotter.addPlot(plot)
		
		#plot = GCCombinedPlot([plot1, plot2])
		#plotter.addPlot(plot)
		
		#plot = GCMultiCommonPlot(results, ["readlatency_99_window", "writelatency_99_window", "readlatency_95_cum", "readlatency_99_cum"], ylim=(0, 150), indexLine=75)
		#plotter.addPlot(plot)

		#plot = GCMultiCommonPlot(results, ["readlatency_95", "readlatency_99"], ylim=(0,200))
		#plotter.addPlot(plot)
		
		#plot = GCMultiCommonPlot(results, ["readlatency_95_window", "readlatency_99_window", "readlatency_avg_window"], ylim=(0, 150))
		#plotter.addPlot(plot)
		

		# Only for debugging
		if not options.paper:
			print 'Nodes...'
			plot = GCNodePlot(results)
			plotter.addPlot(plot)

			plot = GCNodeStatePlot(results)
			plotter.addPlot(plot)
		
			print 'System load...'
			plot = GCSimpleMultiNodePlot(results, "cassandra_load", MINIMUM_NODES, title="Load (min)", legend=True, ylim=(0, 10), ylabel="System load")
			plotter.addPlot(plot)
		
			plot = GCSimpleMultiNodePlot(results, "cassandra_load", OPTIONAL_NODES, title="Load (opt)", legend=True, ylim=(0, 10), ylabel="System load")
			plotter.addPlot(plot)

		print 'Plotting...'
		plotter.doPlot(output=output_file, format=options.format)
		
		print "Plot completed, output file %s" % output_file
	'''