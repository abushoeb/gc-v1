#!/usr/bin/python

from gccommon import *
from gcbase import *
from gchelpers import *

from datetime import datetime
import argparse
from parasol.ganglia import *

GANGLIA_URL="http://sol.cs.rutgers.edu/ganglia2/getgangliaxml.php?c=Parasol"

class GCGangliaManager(GCManagerBase):
	
	node_values = ["power", "load_one", "cpu_user", "cpu_system"]
	
	def getNodeValue(self, value, node, data, values_dict):
		v = data[node][value]
		key = "%s.%s" % (value, node)
		values_dict[key] = float(v)
	
	def getValuesForLogging(self):
		values = {}
		
		s = readGangliaURL(GANGLIA_URL)
		data = parseGangliaXML(s)
		
		for node in MINIMUM_NODES + OPTIONAL_NODES:
			for value in self.node_values:
				self.getNodeValue(value, node, data, values)
		
		values["solar"] = float(data["power"]["solar"])	
		return values

'''
for field in data["power"]:
	#print "%s: %s" % (field, data[field])
	print "%s" % field

for field in data["sol000"]:
	print "%s" % field


print data["sol000"]["power"]
print data["power"]["solar"]
'''

if __name__ == "__main__":
	manager = GCGangliaManager()
	
	values = manager.getValuesForLogging()
	
	for value in sorted(values.keys()):
		print "values[%s] = %f" % (value, values[value])