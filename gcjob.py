#!/usr/bin/python

import os
import time
import sqlitedict
import argparse

from datetime import datetime
from datetime import timedelta

from gccommon import *
from daemon import Daemon

# need to have one function that runs as the daemon, running jobs and monitoring the database for changes/new jobs
# other changes should be done by modifying the database
# database should only be read/modified while a lock file is held

# maybe later add categories/different queues of jobs

# jobs can be inserted in between other jobs
# BASIC-like line numbering? floats for indexing? Whatever works better

pidfile = "/tmp/gcjob.pid"

data = sqlitedict.SqliteDict("/tmp/gcjob.db", autocommit=True)

class GCJobDaemon(Daemon):
	def __init__(self, pidfile, stdout="/tmp/tmpout", stderr="/tmp/tmperr"):
		super(GCJobDaemon, self).__init__(pidfile, stdout=stdout, stderr=stderr)
		self.stopFlag = False
		
	def run(self):
		while True and not self.stopFlag:
			print datetime.now(), "test"
			time.sleep(5)
			
	def kill(self):
		self.stop = True
		
daemon = GCJobDaemon(pidfile)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers(dest="cmd")
	start_parser = subparsers.add_parser("start")
	stop_parser = subparsers.add_parser("stop")
	args = parser.parse_args()
	
	if args.cmd == "start":
		print "starting"
		daemon.start()
		
	elif args.cmd == "stop":
		print "stopping"
		daemon.stop()

	#parser.add_argument("--start", action="store_true"


