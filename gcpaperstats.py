#!/usr/bin/python

import subprocess
import sys
import sqlitedict

from gcpaperconfig import *
from gcutil import percentageDiff

cum_latency_field = "corrected_cum_latency"
#cum_latency_field = "readlatency_99_cum"

cache = sqlitedict.SqliteDict("/tmp/paperstats.db", autocommit=True)

def get_stats(workload, experiment, descriptor):
	if descriptor == ExperimentData.TBD:
		return None
	
	if str(descriptor) in cache:
		#print workload, "cached"
		entry = cache[str(descriptor)]
		#if "caveat" not in entry:
		#	entry["caveat"] = False
			
		return entry
	
	experiment = "%s-%s" % (workload, experiment)
	
	stats_cmd = []
	stats_cmd.append("./gcstats.py")
	stats_cmd.append("--hours")
	stats_cmd.append("24")
	stats_cmd.append("--experiment")
	stats_cmd.append("%s" % descriptor.path)
	
	if descriptor.solar_day != None:
		stats_cmd.append("--solarday")
		stats_cmd.append("%s" % descriptor.solar_day)
		
	print "Getting stats for %s" % experiment
	out = subprocess.check_output(stats_cmd)
	
	#print out
	
	lines = out.strip().split("\n")
	
	results = {}
	results["caveat"] = descriptor.caveat
	
	for line in lines:
		if "GCResults" in line or "Cache" in line:
			continue
		
		line = line.strip()
		line = line.replace(":", "")
		line = line.replace("\t", "")
		
		fields = line.split()
		
		if len(fields) == 1:
			continue
		
		key = str(fields[0])
		value = float(fields[1])
		
		results[key] = value
	
	cache[str(descriptor)] = results
	return results

def output_latex(workload, results, f=sys.stdout):
	f.write("\\begin{table*}\n")
	f.write("\\centering\n")
	f.write("\\begin{tabular}{l l c c c c c c c}\n")
	f.write("\tExperiment & Brown & Green & 99th \\%-ile & Max 20m RT & Br Sav & Br Cost & Br Cost Sav & Green Incr\\\\\n")
	f.write("\t\\hline\n")

	#for key in results.keys():
	#	if "-baseline" in key:
	#		baseline_key = key
	#		break
	
	for experiment in sorted(plots[workload].keys(), key=SortExperimentKey):
		experiment_key = "%s-%s" % (workload, experiment)
		values = results[experiment_key]
		if values == None:
			f.write("\t%s & TBD\\\\\n" % experiment)
			continue
		
		fields = experiment.split("-")
		if MEDSOLAR_SUFFIX in fields or LOWSOLAR_SUFFIX in fields:
			solar_suffix = fields[-1]
			baseline_key = "%s-%s-%s" % (workload, BASELINE_EXPERIMENTS[workload], solar_suffix)
		else:
			baseline_key = "%s-%s" % (workload, BASELINE_EXPERIMENTS[workload])
		
		baseline_brown = results[baseline_key]["brown_consumed"]
		baseline_brown_cost = results[baseline_key]["brown_cost_total"]
		baseline_green = results[baseline_key]["solar_consumed"]
		
		brown = values["brown_consumed"]
		green = values["solar_consumed"]
		response = values[cum_latency_field]
		max_window_response = values["max_window_latency"]
		brown_cost = values["brown_cost_total"]
	
		#brown_savings = ((baseline_brown - brown) / baseline_brown) * 100.0
		#brown_cost_savings = ((baseline_brown_cost - brown_cost) / baseline_brown_cost) * 100.0
		
		if baseline_brown >= brown:
			brown_savings = percentageDiff(baseline_brown, brown)
		else:
			brown_savings = -1.0 * percentageDiff(brown, baseline_brown)
		
		if baseline_brown_cost >= brown_cost:
			brown_cost_savings = percentageDiff(baseline_brown_cost, brown_cost)
		else:
			brown_cost_savings = -1.0 * percentageDiff(brown_cost, baseline_brown_cost)
			
		if baseline_green <= green:
			green_increase = percentageDiff(baseline_green, green)
		else:
			green_increase = -1.0 * percentageDiff(green, baseline_green)
		
		if values["caveat"]:
			caveat = " *"
		else:
			caveat = ""
			
		f.write("\t%s%s & %0.2f KWh & %0.2f KWh & %0.0f ms & %0.0f ms & %0.0f \\%% & %0.2f & %0.0f \\%% & %0.0f \\%% \\\\\n" % (experiment, caveat, brown, green, response, max_window_response, brown_savings, brown_cost, brown_cost_savings, green_increase))
	
	f.write("\\end{tabular}\n")
	f.write("\\caption{%s}\n" % workload)
	f.write("\\end{table*}\n")
	f.write("\n")
	f.flush()
	
def output_text(workload, results):
	print "Experiment\tBrown\t\tGreen\t\t99th %\tBrown Savings\tBrown Cost\tBrown Cost Savings"
	print "------------------------------------------------------------------------------------------------------"
	
	for experiment in sorted(plots[workload].keys(), key=SortExperimentKey):
		experiment_key = "%s-%s" % (workload, experiment)
		values = results[experiment_key]
		if values == None:
			continue
		
		fields = experiment.split("-")
		if MEDSOLAR_SUFFIX in fields or LOWSOLAR_SUFFIX in fields:
			solar_suffix = fields[-1]
			baseline_key = "%s-%s-%s" % (workload, BASELINE_EXPERIMENTS[workload], solar_suffix)
		else:
			baseline_key = "%s-%s" % (workload, BASELINE_EXPERIMENTS[workload])
		
		baseline_brown = results[baseline_key]["brown_consumed"]
		baseline_brown_cost = results[baseline_key]["brown_cost_total"]
	
		brown = values["brown_consumed"]
		green = values["solar_consumed"]
		response = values[cum_latency_field]	
		brown_cost = values["brown_cost_total"]
		
		#brown_savings = ((baseline_brown - brown) / baseline_brown) * 100.0
		#brown_cost_savings = ((baseline_brown_cost - brown_cost) / baseline_brown_cost) * 100.0
		
		if baseline_brown >= brown:
			brown_savings = percentageDiff(baseline_brown, brown)
		else:
			brown_savings = -1.0 * percentageDiff(brown, baseline_brown)
		
		if baseline_brown_cost >= brown_cost:
			brown_cost_savings = percentageDiff(baseline_brown_cost, brown_cost)
		else:
			brown_cost_savings = -1.0 * percentageDiff(brown_cost, baseline_brown_cost)
		
		if values["caveat"]:
			caveat = " *"
		else:
			caveat = ""
			
		print "%s%s\t%0.2f KWh\t%0.2f KWh\t%0.1f ms\t\t%0.2f %%\t\t$%0.2f\t\t%0.2f %%" % (experiment, caveat, brown, green, response, brown_savings, brown_cost, brown_cost_savings)

if __name__ == "__main__":
	
	for workload in plots.keys():
		workload_plots = plots[workload]
		workload_results = {}
		for experiment in workload_plots.keys():
			workload_results["%s-%s" % (workload, experiment)] = get_stats(workload, experiment, workload_plots[experiment])
		
		output_latex(workload, workload_results)
		output_text(workload, workload_results)

		
		
	
	
