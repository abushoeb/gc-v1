#!/usr/bin/python

import sys
import os
import matplotlib.pyplot as pyplot
READ = 1
WRITE = 2

if __name__ == "__main__":
	
	prefix = sys.argv[1]
		
	fileList = [f for f in os.listdir(".") if prefix in f]
	
	datapoints = {}
	
	for fileName in fileList:
		fields = fileName.replace(prefix + "-", "").replace(".", "-").replace("_", "-").split("-")
		readLevel, writeLevel = fields[0], fields[1]
		print readLevel, writeLevel
		
		with open(fileName, "r") as f:
			for line in f:
				if "DATA" in line:
					lineFields = line[line.find("DATA"):].split()
					print lineFields
					nodes, speed, readResponse, writeResponse = int(lineFields[2]), int(lineFields[4]), float(lineFields[6]), float(lineFields[9])
					
					name = "%s-%s" % (readLevel, writeLevel)
					if name not in datapoints:
						datapoints[name] = {}
					
					if nodes not in datapoints[name]:
						datapoints[name][nodes] = {}
						
					datapoints[name][nodes][READ] = readResponse
					datapoints[name][nodes][WRITE] = writeResponse
					
					
	
	for name in sorted(datapoints):
		xs = []
		ys = []
		
		for nodes in sorted(datapoints[name]):
			xs.append(nodes)
			ys.append(datapoints[name][nodes][READ])
		
		pyplot.plot(xs, ys, 'k')
		pyplot.plot(xs, ys, 'o', label=name)
	
	pyplot.xlim(0, 40)
	#pyplot.ylim(0, 60)
	pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0, prop={'size':8})
	#pyplot.show()
	pyplot.savefig("%s-processed.pdf" % prefix, format="pdf")
	
	for n in [9, 18, 27]:
		print "%d nodes" % n
		rPoints = []
		wPoints = []
		for name in sorted(datapoints):
			rPoints.append((name, datapoints[name][n][READ]))
			wPoints.append((name, datapoints[name][n][WRITE]))
			rPoints.sort()
			wPoints.sort()
	
		print "READ"
		for p in rPoints:
			print p
		
		print "WRITE"
		for p in wPoints:
			print p
		
			
			