#!/usr/bin/python

import sys
import math
import argparse
import matplotlib.pyplot as pyplot

from gccommon import *
from gchelpers import parse_timestamp
from array import array
#import ast

class GCResponseTimeHistogram(object):
	
	RESPONSE_TIME_FACTOR = 0.1
	
	def __init__(self, histogram_string="{}", buckets=None):
		if buckets == None:
			#self.buckets = {}
			self.buckets = array('i')
			
			string = histogram_string.replace("{", "").replace("}", "").replace(" ", "").strip()
			raw_buckets = string.split(",")
			if raw_buckets[0] != "":
				for raw_bucket in raw_buckets:
					response_time, count = raw_bucket.split(":")
					response_time = int(response_time)
					count = int(count)	
					#self.buckets[response_time] = count
					#if response_time % 1000 == 0:
					#	print response_time, count
						
					self.buckets.insert(response_time, count)
		
		elif type(buckets) == array:
			self.buckets = array('i', buckets)
		
		elif type(buckets) == dict:
			self.buckets = array('i')
			for k in sorted(buckets.keys()):
				self.buckets.insert(k, buckets[k])
		
		else:
			raise Exception("buckets is invalid type")
			
	def __str__(self):
		string = "{%s}"
		data = ""
		
		#for k in self.buckets.keys():
		for k in xrange(0, len(self.buckets)):
		#for k in xrange(0, 1000):
			b = "%d:%d" % (k, self.buckets[k])
				
			if data == "":
				data += b
			else:
				data += ", %s" % b
				
		return string % data
				
	def __len__(self):
		#return len(self.buckets.keys())
		return len(self.buckets)
	
	def __sub__(self, other):
		#if self.isEmpty() or other.isEmpty():
		#	return GCResponseTimeHistogram()
		
		#if self.isEmpty() and not other.isEmpty():
		#	return GCResponseTimeHistogram(buckets=other.buckets)

		if not self.isEmpty() and other.isEmpty():
			return GCResponseTimeHistogram(buckets=self.buckets)
		
		if self.isEmpty():
			return GCResponseTimeHistogram()
		
		#buckets = {}
		buckets = array('i')
		#assert len(other.buckets.keys()) == len(self.buckets.keys())
		assert len(other.buckets) == len(self.buckets)
		#for k in sorted(self.buckets.keys()):
		for k in xrange(0, len(self.buckets)):
			#buckets[k] = self.buckets[k] - other.buckets[k]
			buckets.insert(k, self.buckets[k] - other.buckets[k])
		
		return GCResponseTimeHistogram(buckets=buckets)
	
	def __add__(self, other):
		if self.isEmpty() and other.isEmpty():
			return GCResponseTimeHistogram()
		
		if not self.isEmpty() and other.isEmpty():
			return GCResponseTimeHistogram(buckets=self.buckets)
		
		if not other.isEmpty() and self.isEmpty():
			return GCResponseTimeHistogram(buckets=other.buckets)
		
		assert len(other.buckets) == len(self.buckets)
		
		buckets = array('i')
		for k in xrange(0, len(self.buckets)):
			buckets.insert(k, self.buckets[k] + other.buckets[k])
		
		return GCResponseTimeHistogram(buckets=buckets)
	
	def isEmpty(self):
		return len(self.buckets) == 0 or sum(self.buckets) == 0
	
	def getPercentile(self, percentile):
		#total_ops = sum(self.buckets.values())
		total_ops = sum(self.buckets)
		
		index = int(math.ceil((percentile/100.0) * total_ops) + 1)
		#print "index", index, "len", len(self.buckets.keys())
		#print total_ops, index	
		#for b in sorted(self.buckets.keys()):
		#for b in xrange(0, len(self.buckets.keys())):
		for b in xrange(0, len(self.buckets)):
			if self.buckets[b] < index:
				index -= self.buckets[b]
			else:
				return b  * self.RESPONSE_TIME_FACTOR
		
		return float("nan")
	
	def getTotalOperations(self):
		#total_ops = sum(self.buckets.values())
		total_ops = sum(self.buckets)
		return total_ops*1.0
	
	def getTotalOperationsLessThanMs(self, response_time):
		target_bucket = int(response_time * (1.0/self.RESPONSE_TIME_FACTOR))
		#print "target_bucket", target_bucket
		
		total = 0
		#for bucket in sorted(self.buckets.keys()):
		for bucket in xrange(0, len(self.buckets)): #sorted(self.buckets):	
			if bucket <= target_bucket:
				total += self.buckets[bucket]
			else:
				break
		
		return total*1.0
		
	def getCDF(self):
		x = []
		y = []
		#total_ops = sum(self.buckets.values())
		total_ops = sum(self.buckets)
		
		if total_ops == 0:
			return x,y
		
		acum = 0.0
		for i in range(0, len(self.buckets)):
			acum += self.buckets[i]
			x.append(i * self.RESPONSE_TIME_FACTOR)
			y.append(100.0*acum/total_ops)
	
		return x, y
		
class GCResponseTime(object):
	
	def __init__(self, data_dir, value):
		# load the timestamps
		timestamp_file = "%s/timestamps" % data_dir
		#print timestamp_file
		if os.path.exists(timestamp_file):
			self.timestamps = []
			with open(timestamp_file, "r") as f:
				for line in f:
					line = line.strip()
					dt = parse_timestamp(line)
					self.timestamps.append(dt)
		else:
			print "Cannot find timestamp file...exiting"
			sys.exit()
		
		value_file = "%s/%s" % (data_dir, value)
		
		if os.path.exists(value_file):
			self.values = {}
			with open(value_file, "r") as f:
				for line in f:
					line = line.strip()
					line = line.replace("= ", "")
					#print line
					index = line.index("{")
					ts_str = line[0:index-1]
					buckets_str = line[index:]
					dt = parse_timestamp(ts_str)
					histogram = GCResponseTimeHistogram(buckets_str)
					del buckets_str
					self.values[dt] = histogram
		else:
			print "Cannot find file for value %s...exiting" % value
			sys.exit()
		
	def availableTimestamps(self):
		return self.timestamps
	
	def getHistogramAt(self, dt):
		return self.values[dt]

class GCResponseTimeFast(object):
	
	def __init__(self, data_dir, value):
		# load the timestamps
		timestamp_file = "%s/timestamps" % data_dir
		#print timestamp_file
		if os.path.exists(timestamp_file):
			self.timestamps = []
			with open(timestamp_file, "r") as f:
				for line in f:
					line = line.strip()
					dt = parse_timestamp(line)
					self.timestamps.append(dt)
		else:
			print "Cannot find timestamp file...exiting"
			sys.exit()
		
		self.value_file = "%s/%s" % (data_dir, value)
		self.offsets = {}
		
		if os.path.exists(self.value_file):
			with open(self.value_file, "r") as f:
				while True:
					seekPos = f.tell()
					line = f.readline().strip()
					line = line.replace("= ", "")
					if len(line) == 0:
						break
						
					index = line.index("{")
					ts_str = line[0:index-1]
					buckets_str = line[index:]
					dt = parse_timestamp(ts_str)
					self.offsets[dt] = seekPos
					#print dt, seekPos
		
		self.cache = {}
		self.cachedKeys = []
		self.cacheSize = 1024
		
		self.valueFile = open(self.value_file, "r")
		
	def availableTimestamps(self):
		return self.timestamps
	
	def getHistogramAt(self, dt):
		if dt not in self.offsets:
			return None
		
		if dt in self.cache:
			return self.cache[dt]
		
		#with open(self.value_file, "r") as f:
		f = self.valueFile
		f.seek(self.offsets[dt])
		line = f.readline().strip()
		
		index = line.index("{")
		ts_str = line[0:index-1]
		buckets_str = line[index:]
		histogram = GCResponseTimeHistogram(buckets_str)
		
		self.cache[dt] = histogram
		self.cachedKeys.append(dt)
		
		if len(self.cachedKeys) > self.cacheSize:
			remove = self.cachedKeys.pop(0)
			del self.cache[remove]
		
		return histogram

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiments", action="store", nargs="+", required=True)
	parser.add_argument("--plotcdf", action="store_true", default=False)
	parser.add_argument("--output", action="store", default="cdf")
	
	args = parser.parse_args()
	
	if args.plotcdf:
		for experiment in args.experiments:
			print "starting", datetime.now()
			rt = GCResponseTime(experiment, "histogram.read.crypt11")
			print "loaded normal", datetime.now()
			frt = GCResponseTimeFast(experiment, "histogram.read.crypt11")
			print "loaded fast", datetime.now()
			
			timestamps = rt.availableTimestamps()
			firstDt = timestamps[0]
			timestamp = timestamps[-1]
			
			for i in reversed(xrange(0, len(timestamps))):
			     lastDt = timestamps[i]
			     if lastDt <= firstDt + timedelta(hours=24):
				timestamp = timestamps[i]
				break
			
			print firstDt, timestamp
			
			histogram = rt.getHistogramAt(timestamp)
			fhistogram = frt.getHistogramAt(timestamp)
			assert histogram.getPercentile(99) == fhistogram.getPercentile(99)
			
			print experiment, histogram.getPercentile(99), fhistogram.getPercentile(99)
			cdf_x, cdf_y = histogram.getCDF()
			pyplot.plot(cdf_x, cdf_y, label=experiment)
		
		pyplot.xlim(0, 20)
		pyplot.ylim(40, 100)
		pyplot.savefig("%s.pdf" % args.output, format="pdf")
	
	
	
	#h1 = "{1:50, 2:50, 3:0}"
	#h2 = "{1:100, 2:100, 3:5}"
	#h3 = "{}"
	#h4 = SAMPLE_RAW_HISTOGRAM
	#histogram1 = GCResponseTimeHistogram(h1)
	#histogram2 = GCResponseTimeHistogram(h2)
	#histogram3 = GCResponseTimeHistogram()
	#histogram4 = GCResponseTimeHistogram(h4)
	#print histogram1, histogram2, histogram2 - histogram1, histogram3
	
	#data_dir = "experimenters/gcmodelbuilder/12-15-UNIFORM-300kbs-MIXED-ONE-27"
	#data_dir = "experimenters/gcmodelbuilder/3-6-LATEST-LATEST-QUORUM-27"
	#value = "histogram.read.crypt11"
	#rt = GCResponseTime(data_dir, value)
	
	#timestamps = rt.availableTimestamps()
#	for ts in timestamps:
#		print rt.getHistogramAt(ts)

	#h = rt.getHistogramAt(timestamps[-1])
	#print timestamps[-1]
	#print h.buckets
	#print h.getCDF()
	#print h.getPercentile(99)

'''	
	print "Testing"
	#print histogram4.getCDF()
	total_ops=histogram4.getTotalOperations()
	less_than_ops=histogram4.getTotalOperationsLessThanMs(75)
	print "99th Percentile", histogram4.getPercentile(99)
	print "Total Operations", total_ops
	print "Total Operations Less than 75ms", less_than_ops
	print "Ratio", float(less_than_ops)/float(total_ops)
	
'''