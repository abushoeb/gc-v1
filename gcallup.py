#!/usr/bin/python
import os
import sys
import time

from gchelpers import *
from gccommon import *
from greencassandraheuristic import *
from gcclustermanager import *
from gcworkloadcontroller import *
from gcgangliamanager import *
from gcsolarmanager import *
from gclogger import *
from gcsolarpredictor import *
from gcworkloadpredictor import *
from gcdatacollector import *
from gcycsb import *
from gcycsbmanager import *
from gcscheduler import *
from gcjolokiamanager import *
from experimentclock import *
from datetime import datetime
from datetime import timedelta


if __name__ == "__main__":
	clock = ExperimentClock(start_time=datetime.now())
	main_logger = GCLogger("/dev/null", GCLogger.DEBUG, self.clock)
	cluster_manager = GCClusterManager(clock, main_logger, mode=GCClusterManager.CASSANDRA_SLEEP)
	
	for node in OPTIONAL_NODES:
		cluster_manager.nodeOn(node)
		
	cluster_manager.waitForTransitions()