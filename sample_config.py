#!/usr/bin/python

import os
import sys
import time
from gchelpers import *
from gccommon import *
from gcbase import *
from datetime import *
from gcworkloadpredictor import *
from gcsolarpredictor import *

import matplotlib.pyplot as pyplot

if __name__ == "__main__":
	start_dt = datetime(year=2013, month=10, day=20, hour=0, minute=0)
	stop_dt = start_dt + timedelta(hours=23, minutes=59, seconds=59)
	
	solar = GCHistoricalSolarPredictor(scale=0.35)
	workload1 = GCTraceWorkloadPredictor("workload.csv", field=1)
	workload2 = GCTraceWorkloadPredictor("workload.csv", field=2)
	
	dts = []
	solar_values = []
	workload1_values = []
	workload2_values = []
	
	dt = start_dt
	while dt <= stop_dt:
		dts.append(dt)
		solar_values.append(solar.getSolarAt(dt))
		workload1_values.append(workload1.getWorkloadAt(dt))
		workload2_values.append(workload2.getWorkloadAt(dt))
		dt += timedelta(minutes=1)
	
	fig = pyplot.figure()
	ax1 = fig.add_subplot(111)
	ax1.set_ylim([-50, 900])
	ax1.plot(dts, solar_values)
	ax2 = ax1.twinx()
	ax2.set_ylim([0.0, 1.0])
	ax2.plot(dts, workload1_values)
	pyplot.show()
	
	pyplot.clf()
	fig = pyplot.figure()
	ax1 = fig.add_subplot(111)
	ax1.set_ylim([-50, 900])
	ax1.plot(dts, solar_values)
	ax2 = ax1.twinx()
	ax2.set_ylim([0.0, 1.0])
	ax2.plot(dts, workload2_values)
	pyplot.show()
	
	
	#pyplot.figure(figsize=(30, 20), frameon=True)
	#pyplot.savefig("sample.pdf", format="pdf")
	
	
	
	
	
	
