#!/usr/bin/python

from gccommon import *
from gcanalysis import GCExperimentProcessor

import math
class GCStatus(GCExperimentProcessor):
	
	def __init__(self):
		super(GCStatus, self).__init__([])
	
	def processExperiment(self, args, experiment, results, output):
		results.preload = False
		
		start_dt = results.availableTimestamps()[0]
		
		for i in reversed(xrange(0, len(results.availableTimestamps()))):
			    dt = results.availableTimestamps()[i]
			    if math.isnan(results.getCommonValue(dt, "readlatency_99_cum")) or results.getCommonValue(dt, "read_histogram").isEmpty():
				    continue
			    else:
				    break
			    
		#dt = results.availableTimestamps()[-1]
		
		if "actual_workload" in results.availableCommonValues():
			actual_workload_key = "actual_workload"
			target_workload_key = "target_workload"
		else:
			actual_workload_key = "ycsb_throughput"
			target_workload_key = "ycsb_target"
		
		#print results.availableCommonValues()
		#print results.availableNodeValues()
		
		seconds = (dt - start_dt).total_seconds()
		hours = int(seconds) / 3600
		minutes = (int(seconds)-hours*3600) / 60
		seconds = int(seconds)-hours*3600 - minutes*60
		
		print "Start Time:\t%s" % start_dt
		print "Exper Time:\t%s" % dt
		print "Elapsed Time:\t%d hours, %d minutes, %d seconds" % (hours, minutes, seconds)
		
		#print "Cum Latency Start:\t%0.2f ms" % results.getCommonValue(start_dt, "readlatency_99_cum")
		print "Cum Latency:\t%0.2f ms" % results.getCommonValue(dt, "readlatency_99_cum")
		
		#firstHist = results.getCommonValue(start_dt, "histogram_read_crypt11")
		#lastHist = results.getCommonValue(dt, "histogram_read_crypt11")
		#diff = lastHist - firstHist
		
		#print firstHist.getTotalOperations()
		#print diff.getTotalOperations()
		#print diff.getPercentile(99)		
		
		#print "Cum Latency Start (Histogram):\t%0.2f ms" % results.getCommonValue(start_dt, "histogram_read_crypt11").getPercentile(99)
		print "Cum (Correct):\t%0.2f ms" % results.getCommonValue(dt, "corrected_cum_latency")
		print "Cum (RW):\t%0.2f ms" % results.getCommonValue(dt, "combined_histogram").getPercentile(99)
		
		print "Win Latency:\t%0.2f ms" % results.getCommonValue(dt, "readlatency_99_window")
		print "Load:\t\t%0.2f ops/sec" % results.getCommonValue(dt, actual_workload_key)
		print "Target:\t\t%0.2f ops/sec" % results.getCommonValue(dt, target_workload_key)
		try:
			print "Solar:\t\t%0.2f W" % results.getCommonValue(dt, "used_solar")
		except:
			pass
		
		print "Nodes On:\t%d" % results.getCommonValue(dt, "nodes_ready")
		print "Nodes Trans:\t%d" % results.getCommonValue(dt, "nodes_transition")
		print "Nodes Off:\t%0d" % results.getCommonValue(dt, "nodes_off")
		
if __name__ == "__main__":
	status = GCStatus()
	status.run()
