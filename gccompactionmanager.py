#!/usr/bin/python

from gccommon import *
from gcbase import GCManagerBase
from gclogger import GCLogger
from gcjolokiahelpers import JMXReaderThread
from gcjolokiahelpers import JMXThreadedClient
from gcjolokiahelpers import JMXResponse
from gcjolokiahelpers import MBeanAttribute
from gcjolokiahelpers import MBeanOperation
from experimentclock import ExperimentClock
from gcutil import OrderedSemaphore
from gchelpers import parse_timestamp

import argparse
import thread
import time
import Queue
from threading import Thread
from threading import Semaphore
from threading import Lock
from threading import Condition

JOLOKIA_URL="http://%s:8778/jolokia/"

class Compaction(object):
	
	def __init__(self, compactionId=None, data=None):
		if compactionId == None and data == None:
			self.empty = True
			self.compactionId = None
			return
		else:
			self.empty = False
			
		self.compactionId = compactionId
		self.columnfamily = data["columnfamily"]
		self.id = int(data["id"])
		self.bytesComplete = int(data["bytesComplete"])
		self.keyspace = data["keyspace"]
		self.taskType = data["taskType"]
		self.totalBytes = int(data["totalBytes"])
	
	def isEmpty(self):
		return self.empty
	
	def __repr__(self):
		if self.isEmpty():
			return "[]"
		else:
			return "[compactionId=%d, columnfamily=%s, id=%d, bytesComplete=%d, keyspace=%s, taskType=%s, totalBytes=%d]" % (self.compactionId, self.columnfamily, self.id, self.bytesComplete, self.keyspace, self.taskType, self.totalBytes)
	
	#def __eq__(self, other):
	#	return self.compactionId == other.compactionId
	
	def __str__(self):
		return self.__repr__()
	
	@staticmethod
	def parseCompaction(string):	
		if string == "[]":
			return Compaction()
			
		string = string.replace("[", "")
		string = string.replace("]", "")
		string = string.replace(" ", "")
		fields = string.split(",")
		
		fieldDict = {}
		
		for field in fields:
			item, value = field.split("=")
			fieldDict[item] = value
		#print fieldDict
		compactionId = int(fieldDict["compactionId"])
		
		return Compaction(compactionId, fieldDict)
		
class GCCompactionData(object):
	
	def __init__(self, data_dir):
		# load the timestamps
		timestamp_file = "%s/timestamps" % data_dir
		if os.path.exists(timestamp_file):
			f = open(timestamp_file, "r")
			lines = f.read().strip().split("\n")
			self.timestamps = []
			for line in lines:
				dt = parse_timestamp(line)
				self.timestamps.append(dt)
			f.close()
		else:
			print "Cannot find timestamp file...exiting"
			sys.exit()
		
		# load the nodes
		nodes_file = "%s/nodes" % data_dir
		
		if os.path.exists(nodes_file):
			f = open(nodes_file, "r")

			lines = f.read().strip().split("\n")
			self.nodes = []
			for line in lines:
				self.nodes.append(line.replace("\n", ""))
			f.close()
		else:
			print "Cannot find nodes file...exiting"
			sys.exit()
			
		self.data = {}
		for node in self.nodes:
			compactions_file = "%s/compactions.%s" % (data_dir, node)
			if os.path.exists(compactions_file):
				f = open(compactions_file, "r")
				lines = f.read().strip().split("\n")
				
				self.data[node] = {}
				for line in lines:
					line = line.strip().replace("\0", "")
					line = line.replace("= ", "", 1)
					print line, node
					index = line.index("[")
					ts_str = line[0:index-1]
					compaction_str = line[index:]
					
					dt = parse_timestamp(ts_str)
					compaction = Compaction.parseCompaction(compaction_str)
					
					self.data[node][dt] = compaction
			else:
				print "Cannot find compactions file for node %s...exiting" % node
				sys.exit()
	
	def availableTimestamps(self):
		return self.timestamps
	
	def getCompactionAt(self, node, dt):
		return self.data[node][dt]

class GCCompactionManager(GCManagerBase):

	compactionAttributes = {
		"compaction" : MBeanAttribute("org.apache.cassandra.db:type=CompactionManager", "Compactions"),
		"compactionId" : MBeanAttribute("org.apache.cassandra.db:type=CompactionManager", "TotalCompactionsCompleted"),
	}
	
	def __init__(self, nodes=(MINIMUM_NODES+OPTIONAL_NODES)):
		self.nodes = nodes
		
		#print "Using nodes = ", self.nodes
		
		self.default = {}
		for node in MINIMUM_NODES+OPTIONAL_NODES:
			self.default[node] = 0
			
		self.results = {}
		self.logger = GCLogger.getInstance().get_logger(self)
	
	def getCompactions(self):
		
		threads = {}
		for node in self.nodes:
			thread = JMXReaderThread(node, self.compactionAttributes.values(), JOLOKIA_URL)
			threads[node] = thread
			thread.start()
		
		for node in self.nodes:
			threads[node].join()
		
		compactions = {}
		
		for node in self.nodes:
			
			if not threads[node].hasAllResults():
				compactions[node] = None
				continue
			
			results = threads[node].getResults()
			
			#print results
			
			compactionId = results[self.compactionAttributes["compactionId"]]
			compactionData = results[self.compactionAttributes["compaction"]]
			try:
				assert len(compactionData) <= 1
			except AssertionError as e:
				print "AssertionError:", e
				for data in compactionData:
					print data
			
			if len(compactionData) == 1:	
				compactions[node] = Compaction(compactionId, compactionData[0])
			else:
				compactions[node] = Compaction()
			
		#for node in self.nodes:
		#	print "compactions[%s] = %s" % (node, compactions[node])
		
		return compactions
			
	def getValuesForLogging(self):
		value_dict = {}
		
		compactions = self.getCompactions()
		for node in self.nodes:
			key_str = "compactions.%s" % node
			
			if compactions[node] == None:
				value_dict[key_str] = Compaction()
			else:
				value_dict[key_str] = compactions[node]
				
		return value_dict
	
	def filterNodesArg(self, nodes):
		if nodes == None:
			return list(self.nodes)
		elif isinstance(nodes, str):
			return [nodes]
		else:
			return nodes
	
	def doError(self, results, msg):
		errorList = []
		for node in sorted(results.keys()):
			if results[node].status == JMXResponse.ERROR:
				errorList.append(node)
		
		if len(errorList) > 0:
			self.logger.info("%s (%s)" % (msg, str(errorList)))
		
	def acquireCompactionLock(self, nodes=None):
		nodes = self.filterNodesArg(nodes)
		op = MBeanOperation("org.apache.cassandra.db:type=CompactionManager", "acquireCompactionLock")
		
		client = JMXThreadedClient(nodes, JOLOKIA_URL, timeout=86400)
		results = client.execOperation(op)
		
		self.doError(results, "Could not acquire compaction lock")
		
		self.logger.log("Acquired compaction lock for %s" % str(nodes))
	
	def releaseCompactionLock(self, nodes=None):
		nodes = self.filterNodesArg(nodes)
		op = MBeanOperation("org.apache.cassandra.db:type=CompactionManager", "releaseCompactionLock")
		
		client = JMXThreadedClient(nodes, JOLOKIA_URL, timeout=86400)
		results = client.execOperation(op)
		
		self.doError(results, "Could not release compaction lock")
		self.logger.log("Released compaction lock for %s" % str(nodes))
	
	def compactionLockHeld(self, nodes=None):
		nodes = self.filterNodesArg(nodes)
		op = MBeanOperation("org.apache.cassandra.db:type=CompactionManager", "compactionLockHeld")
		
		client = JMXThreadedClient(nodes, JOLOKIA_URL, timeout=30)
		results = client.execOperation(op)
		
		self.doError(results, "Could not get compaction lock status")
		
		for node in nodes:
			if results[node].value == False:
				return False
		
		return True
	
	def getPendingCompactions(self, nodes=None):
		nodes = self.filterNodesArg(nodes)
		attrib = MBeanAttribute("org.apache.cassandra.db:type=CompactionManager", "PendingTasks")
		
		client = JMXThreadedClient(nodes, JOLOKIA_URL, timeout=30)
		results = client.readAttribute(attrib)
		
		self.doError(results, "Could not get pending compactions")
		
		total = 0
		
		for node in nodes:
			value = results[node].value
			if value != None:
				total += value
		
		return total
	
	def getCompactionThroughput(self, nodes=None):
		nodes = self.filterNodesArg(nodes)
		attrib = MBeanAttribute("org.apache.cassandra.db:type=StorageService", "CompactionThroughputMbPerSec")
		
		client = JMXThreadedClient(nodes, JOLOKIA_URL, timeout=30)
		results = client.readAttribute(attrib)
		
		self.doError(results, "Could not get compaction throughput")
		
		throughputs = {}
		for node in nodes:
			throughputs[node] = results[node].value
			
		return throughputs
	
	def setCompactionThroughput(self, value, nodes=None):
		nodes = self.filterNodesArg(nodes)
		attrib = MBeanAttribute("org.apache.cassandra.db:type=StorageService", "CompactionThroughputMbPerSec")
		
		client = JMXThreadedClient(nodes, JOLOKIA_URL, timeout=30)
		results = client.writeAttribute(attrib, value)
		
		self.doError(results, "Could not set compaction throughput")
	
	def waitOnTruncateFutures(self, minNode, optNode):
		op = MBeanOperation("org.apache.cassandra.db:type=GreenCassandraConsistencyManager", "waitOnTruncateFutures")
		client = JMXClient(minNode, JOLOKIA_URL, timeout=86400)
		results = client.execOperation(op, args=(optNode,))
		
		if results.status != JMXResponse.SUCCESS:
			self.logger.log("error waiting on truncate futures: minNode=%s, optNode=%s" % (minNode, optNode))
			return
		
		self.logger.log("finished waiting on truncate futures: minNode=%s, optNode=%s" % (minNode, optNode))
		
	def compactAndRelock(self, node):
		self.logger.info("Compacting and relocking for %s" % node)
		
		self.releaseCompactionLock(node)
		
		while self.getPendingCompactions(node) > 0:
			self.logger.info("Compactions still pending (%d) for %s" % (self.getPendingCompactions(node), node))
			time.sleep(10)
		
		self.acquireCompactionLock(node)
	
	def compactAllStaggered(self, nodes=None):
		nodes = self.filterNodesArg(nodes)
		self.logger.info("Compact all staggered for %s" % str(nodes))
		
		for node in nodes:
			self.compactAndRelock(node)
			
	def compactThread(self, node, sem):
		sem.acquire()
		self.compactAndRelock(node)
		sem.release()
		
	def compactAllStaggeredMultiple(self, nodes=None, count=1):
		nodes = self.filterNodesArg(nodes)
		self.logger.info("Compact all staggered (multiple) for %s @ %d" % (str(nodes), count))
		
		sem = Semaphore(count)
		
		threads = {}
		for node in nodes:
			t = Thread(target=self.compactThread, args=(node,sem))
			t.daemon = True
			t.start()
			threads[node] = t
		
		for node in nodes:
			threads[node].join()

class CompactionControllerNodeThread(Thread):
	def __init__(self, node, manager, controller, nodeSem, groupSem, interval=10):
		Thread.__init__(self)
		self.daemon = True
		self.stopFlag = False
		
		self.node = node
		self.manager = manager
		self.controller = controller
		self.interval = interval
		
		self.nodeSem = nodeSem
		self.groupSem = groupSem
		self.currentGlobalSem = None
				
		self.logger = GCLogger.getInstance().get_logger(self)
		
		self.compactionStopFlag = False
		self.exitFlag = False
		
		self.nodeRepaired = False
		self.nodeRepairedCondition = Condition()
		
		self.compactionComplete = False
		self.compactionCompleteCondition = Condition()
		
		self.stopped = False
		self.stoppedCondition = Condition()
	
	def signalNodeRepaired(self):
		self.nodeRepairedCondition.acquire()
		self.nodeRepaired = True
		self.nodeRepairedCondition.notify()
		self.nodeRepairedCondition.release()
	
	def waitForNodeRepaired(self):
		self.nodeRepairedCondition.acquire()
		while not self.nodeRepaired:
			self.nodeRepairedCondition.wait()
		self.nodeRepairedCondition.release()
		
	def signalCompactionComplete(self):
		self.compactionCompleteCondition.acquire()
		self.compactionComplete = True
		self.compactionCompleteCondition.notify()
		self.compactionCompleteCondition.release()
	
	def waitForCompactionComplete(self):
		self.compactionCompleteCondition.acquire()
		while not self.compactionComplete:
			self.compactionCompleteCondition.wait()
		self.compactionCompleteCondition.release()
	
	def signalStopped(self):
		self.stoppedCondition.acquire()
		self.stopped = True
		self.stoppedCondition.notify()
		self.stoppedCondition.release()
	
	def waitForStopped(self):
		self.stoppedCondition.acquire()
		while not self.stopped:
			self.stoppedCondition.wait()
		self.stoppedCondition.release()
		
	def compactionLoop(self):
		self.compactionStopFlag = False
		
		self.logger.log("[%s]: entering main compaction loop" % self.node)
		
		while not self.compactionStopFlag:
			while self.manager.getPendingCompactions(self.node) == 0 and not self.compactionStopFlag:
				time.sleep(self.interval)
				
			if self.compactionStopFlag:
				break
			
			self.logger.log("[%s]: need compaction" % self.node)

			# acquire the semaphores, these can be canceled by stop()
			
			# acquire the node semaphore
			if self.nodeSem.acquire():
				self.logger.log("[%s]: acquired node semaphore" % self.node)
			else:
				# we didn't get any semaphores, so we just bail now
				self.logger.log("[%s]: saw cancel while acquiring node semaphore" % self.node)
				break
			
			# acquire the semaphore first for the group
			if self.groupSem.acquire():
				self.logger.log("[%s]: acquired group semaphore" % self.node)
			else:
				self.logger.log("[%s]: saw cancel while acquiring node semaphore" % self.node)
				
				# we already have node sem, so we need to roll it back
				self.nodeSem.release()
				self.logger.log("[%s]: saw cancel, released node semaphore" % self.node)
				break

			# get global semaphore
			globalSem = self.controller.getGlobalSem()
			self.currentGlobalSem = globalSem
			
			# acquire the global semaphore
			if globalSem.acquire():
				self.logger.log("[%s]: acquired global semaphore" % self.node)
			else:
				self.logger.log("[%s]: saw cancel while acquiring global semaphore" % self.node)
				
				# we already have group sem and node sem, so roll them back
				self.groupSem.release()
				self.logger.log("[%s]: saw cancel, released group semaphore" % self.node)
				
				self.nodeSem.release()
				self.logger.log("[%s]: saw cancel, released node semaphore" % self.node)
				break

			# assert that the compaction lock is already held (compactions disabled)
			assert self.manager.compactionLockHeld(self.node) == True
			
			# release the compcation lock (allow compaction)
			self.manager.releaseCompactionLock(self.node)
			
			self.logger.log("[%s]: waiting for compactions to finish" % self.node)
			
			# poll for compactions to finish
			while self.manager.getPendingCompactions(self.node) > 0:
				if self.compactionStopFlag:
					self.logger.log("[%s]: stopping compactions early" % self.node)
					break
				else:
					time.sleep(self.interval)
			
			if self.manager.getPendingCompactions(self.node) == 0:
				self.logger.log("[%s]: finished compactions" % self.node)
			
			# acquire compaction lock (disable compactions)
			self.manager.acquireCompactionLock(self.node)
			
			# release global semaphore
			globalSem.release()
			self.currentGlobalSem = None
			self.logger.log("[%s]: released global semaphore" % self.node)

			# release group semaphore
			self.groupSem.release()
			self.logger.log("[%s]: released group semaphore" % self.node)
			
			# release node semaphore
			self.nodeSem.release()
			self.logger.log("[%s]: released node semaphore" % self.node)
		
		self.logger.log("[%s]: leaving main compaction loop" % self.node)

	def handleTurnOnCompactions(self):
		self.logger.log("[%s]: node turned on, letting initial compactions run" % self.node)
		
		# release the compaction lock (allow compaction)
		self.manager.releaseCompactionLock(self.node)
		
		# wait for node recovery to complete
		self.waitForNodeRepaired()
		self.logger.log("[%s]: got signal that node is up" % self.node)
		
		# poll for compactions to finish
		initialRecoveryCompactions = self.manager.getPendingCompactions(self.node)
		self.logger.log("[%s]: waiting for initial recovery compactions to finish (%d)" % (self.node, initialRecoveryCompactions))
		
		while self.manager.getPendingCompactions(self.node) > 0:
			time.sleep(1)
		self.logger.log("[%s]: initial recovery compactions complete" % self.node)
			
		# acquire compaction lock (disable compactions)
		self.manager.acquireCompactionLock(self.node)
		
		# signal the cluster manager that the initial compactions are complete
		self.signalCompactionComplete()
	
	def cancelSemaphores(self):
		self.logger.log("[%s]: canceling semaphore calls" % self.node)
		
		#globalSem = self.controller.getGlobalSem()
		globalSem = self.currentGlobalSem
		if globalSem:
			globalSem.cancel(self.ident)
		
		self.groupSem.cancel(self.ident)
		self.nodeSem.cancel(self.ident)
		
	def run(self):
		self.handleTurnOnCompactions()
		self.compactionLoop()
		self.signalStopped()
		self.logger.log("[%s]: compaction thread exiting" % self.node)
		thread.exit()
	
	def stop(self):
		self.logger.log("[%s]: compaction thread stopping" % self.node)
		self.compactionStopFlag = True
		self.cancelSemaphores()
		self.waitForStopped()
		self.logger.log("[%s]: compaction thread stopped" % self.node)
	

class CompactionController(object):
	
	def __init__(self, manager, nodes=(MINIMUM_NODES + OPTIONAL_NODES), count=1, interval=10, semaphore_class=OrderedSemaphore):
		self.manager = manager
		self.nodes = nodes
		self.interval = interval
		self.globalSem = semaphore_class(count)
		self.groupSems = {}
		self.nodeSems = {}
		self.compactionThreads = {}
		
		self.logger = GCLogger.getInstance().get_logger(self)
		
		minimumNodes = list(MINIMUM_NODES)
		oddOptionalNodes = []
		evenOptionalNodes = []
		
		for node in OPTIONAL_NODES:
			if OPTIONAL_NODES.index(node) % 2 == 0:
				evenOptionalNodes.append(node)
			else:
				oddOptionalNodes.append(node)
		
		assert len(minimumNodes) == len(oddOptionalNodes) == len(evenOptionalNodes)
		
		for node in self.nodes:
			self.nodeSems[node] = semaphore_class(1)
			
		for i in xrange(0, len(minimumNodes)):
			groupSem = semaphore_class(1)
			self.groupSems[minimumNodes[i]] = groupSem
			self.groupSems[oddOptionalNodes[i]] = groupSem
			self.groupSems[evenOptionalNodes[i]] = groupSem

	def start(self):
		self.manager.acquireCompactionLock()
		self.setCompactionParameters(len(self.nodes))
		
		for node in self.nodes:
			thread = CompactionControllerNodeThread(node, self.manager, self, self.nodeSems[node], self.groupSems[node], interval=self.interval)
			self.compactionThreads[node] = thread
			thread.signalNodeRepaired()
			thread.start()
	
	def stop(self):
		self.manager.setCompactionThroughput(0)
		
		for node in self.compactionThreads.keys():
			self.compactionThreads[node].stop()
			
		#for node in self.compactionThreads.keys():
		#	self.compactionThreads[node].join()
			
		self.manager.releaseCompactionLock()
	
	def setGlobalLimit(self, count):
		#self.globalSem.setBaseCount(count)
		#self.globalSem = OrderedSemaphore(count)
		self.globalSem.setCount(count)
		
	def getGlobalSem(self):
		return self.globalSem
	
	'''
	def createNodeOnThread(self, node):
		thread = CompactionControllerNodeOnThread(node, self.manager, self, self.nodeSems[node], self.groupSems[node], interval=self.interval, logger=self.main_logger)
		thread.start()
		return thread
	
	def createMinOnThread(self, node, optNode):
		thread = CompactionControllerMinOnThread(node, optNode, self.manager, self, self.nodeSems[node], self.groupSems[node], interval=self.interval, logger=self.main_logger)
		thread.start()
		return thread
	'''
	
	def startNodeThread(self, node):
		thread = CompactionControllerNodeThread(node, self.manager, self, self.nodeSems[node], self.groupSems[node], interval=self.interval)
		self.compactionThreads[node] = thread
		thread.start()
		
		return thread
	
	def stopNodeThread(self, node):
		if node in self.compactionThreads:
			try:
				self.compactionThreads[node].stop()
				self.compactionThreads[node].join()
				del self.compactionThreads[node]
			except KeyError:
				self.logger.log("[%s]: missing node thread on stop" % node)
	
	def setCompactionParameters(self, nodesOn):
		throughput = int(BASE_COMPACTION_THROUGHPUT * (nodesOn*1.0 / len(MINIMUM_NODES + OPTIONAL_NODES)))
		#throughput = int(BASE_COMPACTION_THROUGHPUT)
		self.manager.setCompactionThroughput(throughput)
		self.logger.info("set compaction throughput to %d" % throughput)
		
		#semaphores = int((nodesOn - 1) / 9) + 1
		
		if nodesOn >= 9 and nodesOn <= 18:
			semaphores = 2
		#elif nodesOn >= 16 and nodesOn <= 18:
		#	semaphores = 2
		elif nodesOn >= 19:
			semaphores = 3
		
		self.setGlobalLimit(semaphores)
		self.logger.log("set compaction semaphore limit to %d" % semaphores)
		
	def waitOnTruncateFutures(self, minNode, optNode):
		self.manager.waitOnTruncateFutures(minNode, optNode)

class CompactionControllerOld(Thread):
	
	def __init__(self, manager):
		Thread.__init__(self)
		self.daemon = True
		self.manager = manager
		
		self.count = None
		self.rate = None
	
	def setParams(self, count, rate):
		self.count = count
		self.rate = rate
				
	def run(self):
		while self.count == None or self.rate == None:
			time.sleep(10)
			
		while True:
			self.manager.setCompactionThroughput(self.rate)
			self.manager.compactAllStaggeredMultiple(count=self.count)	
		
		
if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--nodes", action="store", nargs="+", default=MINIMUM_NODES+OPTIONAL_NODES)
	parser.add_argument("--lock", action="store_true")
	parser.add_argument("--unlock", action="store_true")
	parser.add_argument("--islocked", action="store_true")
	parser.add_argument("--pending", action="store_true")
	parser.add_argument("--show", action="store_true")
	parser.add_argument("--setlimit", action="store", type=int)
	parser.add_argument("--getlimit", action="store_true")
	parser.add_argument("--compactandrelock", action="store", type=str)
	parser.add_argument("--compactallstaggered", action="store_true")
	parser.add_argument("--compactallstaggeredmultiple", action="store", type=int)
	parser.add_argument("--controller", action="store_true")
	
	args = parser.parse_args()
	
	from gclogger import GCNullLogger
	clock = ExperimentClock()
	logger = GCNullLogger()
	
	manager = GCCompactionManager(nodes=args.nodes)
	
	if args.lock:
		manager.acquireCompactionLock()
		sys.exit()
	elif args.unlock:
		manager.releaseCompactionLock()
		sys.exit()
	elif args.islocked:
		print manager.compactionLockHeld()
		sys.exit()
	elif args.pending:
		print manager.getPendingCompactions()
		sys.exit()
	elif args.setlimit != None:
		manager.setCompactionThroughput(args.setlimit)
	elif args.getlimit:
		throughputs = manager.getCompactionThroughput()
		for node in sorted(throughputs.keys()):
			print "throughput[%s] = %d" % (node, throughputs[node])
	elif args.compactandrelock != None:
		manager.compactAndRelock(args.compactAndRelock)
	elif args.compactallstaggered:
		manager.compactAllStaggered()
	elif args.compactallstaggeredmultiple:
		manager.compactAllStaggeredMultiple(count=args.compactallstaggeredmultiple)
	elif args.show:
		values = manager.getValuesForLogging()
		for key in sorted(values.keys()):
			print "values[%s] = %s" % (key, values[key])
	elif args.controller:
		controller = CompactionController(manager, count=3)
		controller.start()
		while True:
			time.sleep(10)
	else:
		parser.print_help()
		
