#!/usr/bin/python
import ConfigParser
import os
import sys
import time
import paramiko
import socket
import SocketServer
import signal
import argparse

from gchelpers import *
from gccommon import *
from greencassandraheuristic import *
from gcclustermanager import *
from gcworkloadmanager import *
from gcgangliamanager import *
from gcsolarmanager import *
from gclogger import *
from gcsolarpredictor import *
from gcworkloadpredictor import *
from gcdatacollector import *
from experimentclock import *
from datetime import datetime
from datetime import timedelta

class GCController:
	
	start_time = None
	run_length = None
	accel_rate = None
	
	predictor = None
	cluster_manager = None
	clock = None
	solver = None
	logger = None
	
	def __init__(self, start_time, run_length, accel_rate, logfile, experiment_name, mode=GCMode.NOOP, norestart=False):
		self.start_time = start_time
		self.run_length = run_length
		self.accel_rate = accel_rate
		self.logfile = logfile
		self.experiment_name = experiment_name
		self.mode = mode
		self.norestart = norestart
		
		#self.solver = self.init_solver()
	
	def run(self):		
		if not self.norestart:
			print "Bringing cassandra cluster fully online..."
			start_cassandra_cluster(EXECUTE_NODE)
		
		print "Starting GreenCassandra controller..."
		
		self.clock = ExperimentClock(start_time=self.start_time, accel_rate=self.accel_rate)
		self.main_logger = GCLogger(self.logfile, GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		self.solar_predictor = GCTraceSolarPredictor("ganglia-metrics.csv", self.start_time)
		self.workload_predictor = GCTraceWorkloadPredictor("workload.csv")
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger)
		self.workload_manager = GCWorkloadManager(self.clock, self.workload_predictor, MAX_LOAD_RATE)
		self.ganglia_manager = GCGangliaManager()
		self.solar_manager = GCSolarManager(self.clock, self.solar_predictor)
				
		# start the workload server
		self.workload_manager.start_workload_server()
		
		# start the data collector
		self.data_collector = GCDataCollector(self.clock, self.experiment_name)
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.workload_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.solar_manager)
		self.data_collector.start()
		
		print "Clock started..."
		last_dt = None

		statelog = open("statelog", "w")
		state_track = {}
		nodes, nodeStates, offInfo, transInfo = self.cluster_manager.getInfoForSolver()
		for n in nodes:
			state_track[n.name] = ""
		
		while self.run_length > 0:
			last_dt = self.clock.get_current_time()
			print "Starting cycle, dt=" + str(last_dt)
			self.logger.log("Starting cycle, dt=" + str(last_dt))

			solver = self.init_solver(last_dt)
			solver.simulate(24)
			solver.output(24)
			
			self.logger.log("Completed solver")
			#solver.output(24)
			nodeStates = solver.getNodeStatesAtT(1)
			#raw_input()
			self.logger.log("Adjusting cluster")
			self.adjust_cluster(nodeStates)
			self.run_length -= 1

			#lets log the node states for debugging
			nodes, nodeStates, offInfo, transInfo = self.cluster_manager.getInfoForSolver()
#			statelog.write("dt: %s\n" % str(last_dt))
			for n in nodes:
				if nodeStates[n.name] == GCNodeState.ON:
					state_track[n.name] += "2"
				elif nodeStates[n.name] == GCNodeState.TRANSITION:
					state_track[n.name] += "1"
				elif nodeStates[n.name] == GCNodeState.OFF:
					state_track[n.name] += "0"

			for node in sorted(state_track.keys()):
				print "%s: %s" % (node, state_track[node])
				
			self.logger.log("Cycle complete")
			print "Cycle complete"
			
			while (self.clock.get_current_time() < last_dt + timedelta(minutes=SLOT_LENGTH)):
				#print self.clock.get_current_time()
				#raw_input()
				time.sleep(0.5)
		
		self.workload_manager.stop_workload_server()
		self.data_collector.stop()
		
		print "Exiting..."
		sys.exit()

	def init_solver(self, dt):
		perf_ready, green_ready, nodes, nodeStates, offInfo, transInfo = self.ready_solver_data(dt)
		solver = GreenCassandraHeuristic(perf_ready, green_ready, nodes, nodeStates, offInfo, transInfo, mode=self.mode)
		return solver
	
	def ready_solver_data(self, dt):
		perf = []
		green = []
		for i in xrange(0, 24*4):
			this_dt = dt + timedelta(minutes=15*i)
			#print this_dt
			p = self.workload_predictor.getWorkloadAt(this_dt)
			p = denormalize(p, NUM_NODES)
			perf.append(p)
			#print this_dt
			g = self.solar_predictor.getSolarAt(this_dt)
			green.append(g)
			#print p
			
			
		print perf
		perf_ready = []
		for p in perf:
			perf_ready.append(p)
		
		green_ready = []
		for g in green:
			green_ready.append(g)
			
		nodes, nodeStates, offInfo, transInfo = self.cluster_manager.getInfoForSolver()
		
		return perf_ready, green_ready, nodes, nodeStates, offInfo, transInfo
	
	def adjust_cluster(self, node_states):
		for node in node_states.keys():
			if self.cluster_manager.nodeState(node) == node_states[node]:
				continue
			
			initial = ""
			new = ""
			
			if self.cluster_manager.nodeState(node) == GCNodeState.ON:
				initial = "ON"
			elif self.cluster_manager.nodeState(node) == GCNodeState.OFF:
				initial = "OFF"
			elif self.cluster_manager.nodeState(node) == GCNodeState.TRANSITION:
				initial = "TRANSITION"
			else:
				initial = "ERROR"
				
			if node_states[node] == GCNodeState.ON:
				new = "ON"
			elif node_states[node] == GCNodeState.OFF:
				new = "OFF"
			elif node_states[node] == GCNodeState.TRANSITION:
				new = "TRANSITION"
			else:
				new = "ERROR"
				
			print "%s: %s --> %s" % (node, initial, new)
			
			if self.cluster_manager.nodeState(node) == GCNodeState.ON and node_states[node] == GCNodeState.OFF:
				self.cluster_manager.nodeOff(node)
				#print "Turning off node %s..." % node
			elif self.cluster_manager.nodeState(node) == GCNodeState.OFF and node_states[node] == GCNodeState.TRANSITION:
				self.cluster_manager.nodeOn(node)
				#print "Turning on node %s..." % node
			else:
				continue
		#self.cluster_manager.waitForTransitions()


def sigint(signum, frame):
	print "caught sigint"
	if controller:
		controller.workload_manager.stop_workload_server()
		controller.data_collector.stop()
	sys.exit()
	
if __name__ == "__main__":
	signal.signal(signal.SIGINT, sigint)
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--norestart", action="store_true")
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--cycles", action="store", type=int, required=True)
	parser.add_argument("--accel", action="store", type=int, default=1)
	parser.add_argument("--mode", action="store", default="noop")
	args = parser.parse_args()
	
	if args.mode == "noop":
		mode = GCMode.NOOP
	elif args.mode == "loadonly":
		mode = GCMode.LOADONLY
	elif args.mode == "full":
		mode = GCMode.FULL
	else:
		print "Invalid mode: valid options are noop, loadonly, and full."
		sys.exit()

	dt = datetime(year=2013, month=1, day=05, hour=00, minute=00, second=0)
	controller = GCController(dt, args.cycles, args.accel, "logfile", args.experiment, mode=mode, norestart=args.norestart)
	controller.run()
