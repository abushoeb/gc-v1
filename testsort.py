#!/usr/bin/python

import random
from datetime import datetime

#weights = None

d = {
	"a" : 100000,
	"b" : 10000,
	"c" : 1000,
	"d" : 100,
	"e" : 10,
	"f" : 1
}

'''
d = {
	"a" : 1000,
	"b" : 900,
	"c" : 800,
	"d" : 700,
	"e" : 600,
	"f" : 500
}
'''

'''
def getScores():
	return d

	mv = -10000
	mk = None
	
	for key in d:
		if d[key] > mv:
			mk = key
			mv = d[key]
	
	scores = {}
	for key in d:
		scores[key] = d[key]*1.0 / mv

	return scores

'''	
def compare(a, b):
	return cmp(w[a], w[b])

class FastWeightedRandom(object):
	
	def __init__(self, items, inverse=True, usecmp=True):
		self.master_items = items
		self.items = dict(self.master_items)
		self.inverse = inverse
		self.usecmp = True
	
	def next(self, n=1):
		weights = self.getWeights()
		
		l = list(self.items.keys())
		
		if self.usecmp:
			global w
			w = weights
			l.sort(cmp=compare)
		else:
			l.sort(key=lambda x:weights[x])
		
		if self.inverse:
			index = 0
		else:
			index = -1
		
		if n == 1:
			return l[index]
		
		results = []
		for i in xrange(0, n):
			results.append(l.pop(index))
		
		return results
	
	def reset(self):
		self.items = dict(self.master_items)
	
	def remove(self, key):
		del self.items[key]
		
	def getScores(self):
		return self.items
	
	def getWeights(self):
		scores = self.getScores()
		
		weights = {}
		for key in self.items:
			score = scores[key]
			weight = random.random() * score
			weights[key] = weight
	
		return weights
	
class LegacyWeightedRandom(object):
	
	def __init__(self, items):
		self.items = items
	
	def next(self):
		weights = self.getWeights()
		
		l = []
		for key in self.items:
			for i in xrange(0, weights[key]):
				l.append(key)
		
		random.shuffle(l)
		return l[0]
	
	def getScores(self):
		return self.items
	
	def getWeights(self):
		scores = self.getScores()
		
		weights = {}
		total = 0
		for key in d:
			total += scores[key]
			
		for key in d:
			frac = scores[key] * 1.0 / total
			weight = 100 - frac * 100
			weights[key] = int(weight)
		
		return weights


def evalSelector(d, selector, n=1):
	NUM_SELECTIONS = 1000000 / n
	
	counts = {}
	for key in d:
		counts[key] = 0
	
	start = datetime.now()
	
	for i in xrange(0, NUM_SELECTIONS):
		selection = selector.next(n)
		
		if n == 1:
			counts[selection] += 1
		else:
			for s in selection:
				counts[s] += 1
		
	finish = datetime.now()
	
	length = (finish-start).total_seconds()
	
	avgTime = length*1.0 / NUM_SELECTIONS
	
	for key in sorted(d):
		print key, counts[key]
	print "avgTime: %0.10f seconds" % avgTime
	

def evalSelector2(d, selector, n=1):
	NUM_SELECTIONS = 1000000
	
	counts = {}
	for key in d:
		counts[key] = 0
	
	start = datetime.now()
	
	i = 0
	while i < NUM_SELECTIONS:
		for j in xrange(0, n):
			selection = selector.next()
			selector.remove(selection)
			counts[selection] += 1
			i += 1
		
		selector.reset()
	
	finish = datetime.now()
	
	length = (finish-start).total_seconds()
	
	avgTime = length*1.0 / NUM_SELECTIONS
	
	for key in sorted(d):
		print key, counts[key]
	print "avgTime: %0.10f seconds" % avgTime
	
	
if __name__ == "__main__":
	
	print "FastWeightedRandom(cmp)"
	selector = FastWeightedRandom(d)
	evalSelector(d, selector, n=1)
	
	print "FastWeightedRandom(cmp), n=3 together"
	selector = FastWeightedRandom(d)
	evalSelector(d, selector, n=3)
	
	print "FastWeightedRandom(cmp), n=3 individual"
	selector = FastWeightedRandom(d)
	evalSelector2(d, selector, n=3)
	
	#print "FastWeightedRandom(nocmp)"
	#selector = FastWeightedRandom(d, usecmp=False)
	#evalSelector(d, selector)
	
	#del d["a"]
	#print "FastWeightedRandom(cmp), no a"
	#selector = FastWeightedRandom(d)
	#evalSelector(d, selector, n=1)
	
	#print "LegacyWeightedRandom"
	#selector = LegacyWeightedRandom(d)
	#evalSelector(d, selector)
	
	
	
	
	
	
	