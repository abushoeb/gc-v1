#!/usr/bin/python

from threading import Semaphore
from threading import Lock
from threading import Condition
from threading import Event
import Queue
import thread
import os

from datetime import datetime
from datetime import timedelta
#from gchelpers import *

class KeyErrorLow(KeyError):
	def __init__(self, string, depth):
		super(KeyErrorLow, self).__init__(string)
		self.depth = depth

class KeyErrorHigh(KeyError):
	def __init__(self, string, depth):
		super(KeyErrorHigh, self).__init__(string)
		self.depth = depth

def interpolate(p1, p2, x):
	x1, y1 = p1
	x2, y2 = p2

	m = float(y2 - y1)/float(x2-x1)
	b = y1 - m*x1
	return m*x + b

def extrapolate(p1, p2, x):
	x1, y1 = p1
	x2, y2 = p2
	
	y = y1 + ((1.0*x - x1) / (x2 - x1)) * (y2 - y1)
	
	return y
	
def interpolateFloat(param1, val1, param2, val2, targetParam):
	p1 = param1, val1
	p2 = param2, val2
	newVal = interpolate(p1, p2, targetParam)
	
	return newVal

def extrapolateFloat(param1, val1, param2, val2, targetParam):
	p1 = param1, val1
	p2 = param2, val2
	newVal = extrapolate(p1, p2, targetParam)
	
	return newVal

class MultilevelInterpolatingDict(object):
	
	def __init__(self, keySize=2, interpolateFunc=interpolateFloat, extrapolateFunc=extrapolateFloat, bounds=None, extrapolate=False):
		self.keylist = []
		
		self.baseDict = {}
		self.keySize = keySize
		self.interpolateFunc = interpolateFunc
		self.extrapolateFunc = extrapolateFunc
		self.extrapolate = extrapolate
		self.setBounds(bounds)
	
	def __setitem__(self, key, value):
		if len(key) != self.keySize:
			raise KeyError("Key must be a numeric %d-tuple..." % self.keySize)
		
		#print "setting key", key
		self.keylist.append(key)
		self.setHelper(list(key), value, self.baseDict, self.keySize)
	
	def __getitem__(self, key):
		if len(key) != self.keySize:
			raise KeyError("Key must be a numeric %d-tuple..." % self.keySize)
		
		return self.getHelper(list(key), self.baseDict, self.keySize)
	
	def keys(self):
		return list(self.keylist)
		
	def setBounds(self, bounds):
		if bounds != None:
			self.bounds = True
			self.lowBound, self.highBound = bounds
		else:
			self.bounds = False
		
	def setHelper(self, key, value, currentDict, currentDepth):
		currentKey = key[0]
		
		#print "setHelper: key %s, currentKey %s, currentDepth %s, currentDict %s" % (str(key), str(currentKey), str(currentDepth), str(currentDict))
		
		if currentDepth == 1:
			currentDict[currentKey] = value
			#print ""
		
		else:
			try:
				nextDict = currentDict[currentKey]
			except KeyError:
				currentDict[currentKey] = {}
				nextDict = currentDict[currentKey]
				
			key.pop(0)
			self.setHelper(key, value, nextDict, currentDepth - 1)
		
		
	def getHelper(self, key, currentDict, currentDepth):
		
		currentKey = key[0]
		dictKeys = sorted(currentDict.keys())
		
		#print "getHelper: key %s, currentKey %s, currentDepth %s, currentDict %s" % (str(key), str(currentKey), str(currentDepth), str(currentDict))
		#print "dictKeys", dictKeys
		#print "getHelper:", currentDict
		
		# TODO: are these assumptions generalizable?
		
		needExtrapolate = False
		
		if currentKey < dictKeys[0]:
			#print "key low", currentKey
			if self.bounds:
				if currentDepth == 1:
					#print "high bound"
					return self.highBound
				else:
					#print "low bound"
					return self.lowBound
			elif self.extrapolate:
				keyFirst = dictKeys[0]
				keySecond = dictKeys[1]
				needExtrapolate = True
			else:
				raise KeyErrorLow("key %s, min %s, level=%d" % (str(currentKey), str(dictKeys[0]), currentDepth), currentDepth)
			
		elif currentKey > dictKeys[-1]:
			#print "key high", currentKey
			if self.bounds:
				if currentDepth == 1:
					#print "low bound"
					return self.lowBound
				else:
					#print "high bound"
					return self.highBound
			elif self.extrapolate:
				keyFirst = dictKeys[-1]
				keySecond = dictKeys[-2]
				needExtrapolate = True
			else:
				raise KeyErrorHigh("key %s, max %s, level=%d" % (str(currentKey), str(dictKeys[-1]), currentDepth), currentDepth)
		
		if needExtrapolate:
			# we only get here if we want to extrapolate
			if currentDepth == 1:
				result = self.extrapolateFunc(keyFirst, currentDict[keyFirst], keySecond, currentDict[keySecond], currentKey)
				return result
			else:
				keyToPassLow = list(key)
				keyToPassLow.pop(0)
				
				keyToPassHigh = list(key)
				keyToPassHigh.pop(0)
				
				nextLowDict = currentDict[keyFirst]
				nextHighDict = currentDict[keySecond]
				
				lowValue = self.getHelper(keyToPassLow, nextLowDict, currentDepth - 1)
				highValue = self.getHelper(keyToPassHigh, nextHighDict, currentDepth - 1)
				
				result = self.interpolateFunc(keyFirst, lowValue, keySecond, highValue, currentKey)
				return result
			
		# if the current dictionary contains an exact match, return the value (or recurse)
		if currentKey in dictKeys:
			#print "exact", currentKey
			if currentDepth == 1:
				return currentDict[currentKey]
			else:
				#key.pop(0)
				keyToPass = list(key)
				keyToPass.pop(0)
				return self.getHelper(keyToPass, currentDict[currentKey], currentDepth - 1)
			
		# otherwise, interpolate between the values (or recurse)
		else:
			for i in xrange(1, len(dictKeys)):
				if currentKey >= dictKeys[i-1] and currentKey <= dictKeys[i]:
					keyLow = dictKeys[i-1]
					keyHigh = dictKeys[i]
					break
			
			#print "interpolate", keyLow, keyHigh, currentKey
			if currentDepth == 1:
				#return interpolate((keyLow, currentDict[keyLow]), (keyHigh, currentDict[keyHigh]), currentKey)
				# use changable interpolate function
				result = self.interpolateFunc(keyLow, currentDict[keyLow], keyHigh, currentDict[keyHigh], currentKey)
				#print "result", result
				return result
			else:
				#key.pop(0)
				keyToPassLow = list(key)
				keyToPassLow.pop(0)
				
				keyToPassHigh = list(key)
				keyToPassHigh.pop(0)
				
				nextLowDict = currentDict[keyLow]
				nextHighDict = currentDict[keyHigh]
				
				lowValue = self.getHelper(keyToPassLow, nextLowDict, currentDepth - 1)
				highValue = self.getHelper(keyToPassHigh, nextHighDict, currentDepth - 1)
				#return interpolate((keyLow, lowValue), (keyHigh, highValue), currentKey)
				# use changable interpolate function
				#print "recursed on", lowValue, highValue
				result = self.interpolateFunc(keyLow, lowValue, keyHigh, highValue, currentKey)
				#print "result", result
				return result

class OrderedSemaphore(object):
	def __init__(self, count=1):
		self.spots = count
		
		self.wait_queue = Queue.Queue()
		self.priority_wait_queue = Queue.Queue()
		self.counter = count
		self.counter_lock = Lock()
		self.base_count = count
		
		self.super_counter = 0
		
		self.events = {}
		self.canceled = {}
	
	def acquire(self, priority=False):
		self.counter_lock.acquire()
		self.counter -= 1
		
		if self.counter < 0:
			self.counter_lock.release()
			myevent = Event()
			if priority:
				self.priority_wait_queue.put(myevent)
			else:
				self.wait_queue.put(myevent)
			
			# keep the events for each particular thread in some dictionaries
			ident = thread.get_ident()
			self.events[ident] = myevent
			self.canceled[ident] = False
			myevent.wait()
			
			# if we get here, we have been awoken
			# return the negation of the canceled flag
			retval = not self.canceled[ident]
			
			if ident in self.canceled:
				del self.canceled[ident]
			if ident in self.events:
				del self.events[ident]
			
			return retval
		else:
			self.counter_lock.release()
			return True
	
	def superAcquire(self):
		self.counter_lock.acquire()
		#self.counter -= 1
		self.super_counter += 1
		self.counter_lock.release()
		
	def release(self):
		self.counter_lock.acquire()
		if self.super_counter > 0:
			self.super_counter -= 1
			self.counter_lock.release()
			return
		
		if self.counter < 0:
			wakeup = True
		else:
			wakeup = False

		self.counter += 1
		
		if wakeup:
			woke_one = False
			while not woke_one and not (self.priority_wait_queue.empty() and self.wait_queue.empty()):
				try:
					event = self.priority_wait_queue.get_nowait()
					if event.is_set():
						continue
					
					event.set()
					woke_one = True
				except:
					try:
						event = self.wait_queue.get_nowait()
						if event.is_set():
							continue
						event.set()
						woke_one = True
					except:
						pass

		self.counter_lock.release()
		
	def cancel(self, ident):
		self.counter_lock.acquire()
		
		if ident in self.events:
			self.canceled[ident] = True
			self.events[ident].set()
			self.counter += 1
			
		self.counter_lock.release()

	def getBaseCount(self):
		return self.base_count
	
	def setBaseCount(self, count):
		for i in xrange(0, self.base_count):
			self.acquire()
		
		for i in xrange(0, count):
			self.release()
		
		self.base_count = count

class SemaphoreThreadContext(object):
	
	def __init__(self, ident, event):
		self.ident = ident
		self.event = event
		self.canceled = False
	
class TokenOrderedSemaphore(object):
	
	def __init__(self, count=1):
		self.lock = Lock()
		
		self.count = count
		self.destroy_count = 0

		self.free_tokens = count
		self.used_tokens = 0
		
		self.queue = []
	
	# called with lock held
	def getToken(self):
		if self.free_tokens > 0:
			self.free_tokens -= 1
			self.used_tokens += 1
			return True
		else:
			return False
	
	# called with lock held
	# this will also destroy any excess tokens
	def returnToken(self):
		self.used_tokens -= 1
		self.free_tokens += 1
				
		# if destroy_count > 0, destroy any excess tokens
		while self.destroy_count > 0 and self.free_tokens > 0:
			self.free_tokens -= 1
			self.destroy_count -= 1
			
	def acquire(self):
		with self.lock:
			# easiest case, able to get a token immediately
			if self.getToken():
				return True
			
			# otherwise, we need to queue
			else:
				# create a thread context
				context = SemaphoreThreadContext(thread.get_ident(), Event())
				
				# add it to the wait queue
				self.queue.append(context)
		
		# outside of the lock context, wait on this event
		context.event.wait()

		# if we wake up, we are either:
		# a) ready to run
		# or
		# b) canceled
		
		# return the negation of the canceled flag
		return not context.canceled

	def release(self):
		with self.lock:
			self.returnToken()
			
			if len(self.queue) > 0:
				# try to get a token for this context
				# if we were able to get a token, wake up the thread and return
				# if we didn't it is because the tokens were downsized while this was held
				if self.getToken():
					# find out who we want to wake up
					context = self.queue.pop(0)
					# set their event
					context.event.set()
					
					return
	
	def cancel(self, ident):
		with self.lock:
			for context in self.queue:
				# find the context
				if context.ident == ident:
					# remove from the queue
					self.queue.remove(context)
					# set the canceled flag
					context.canceled = True
					# wake up the event
					context.event.set()
					
					return
	
	def setCount(self, count):
		with self.lock:
			# simple increase, create new tokens and add it to the token list
			if count > self.count:
				needCreate = count - self.count
				self.free_tokens += needCreate
			
			# destroy some tokens
			elif count < self.count:
				needDestroy = self.count - count
				
				# destroy any that are free (simple)
				while needDestroy > 0 and self.free_tokens > 0:
					self.free_tokens -= 1
					needDestroy -= 1
				
				# if any are left, add to destroy_count so they can be destroyed
				# upon release
				self.destroy_count += needDestroy
			
			self.count = count
			
	def getCount(self):
		with self.lock:
			return self.count

def dtrange(start, stop, delta):
	currDt = start
	if isinstance(stop, datetime):
		stopDt = stop
	elif isinstance(stop, timedelta):
		stopDt = start + stop
	else:
		raise Exception("Improper second argument, must be datetime or timedelta")
	
	while currDt < stopDt:
		yield currDt
		currDt += delta

def dtiter(start, delta):
	currDt = start
	
	while True:
		yield currDt
		currDt += delta

class GCSingleton(object):

	instances = {}
	
	def __init__(self, cls=None):
		if cls == None:
			cls = self.__class__
		#import pdb; pdb.set_trace()
		
		if cls in GCSingleton.instances:
			raise Exception("Singleton class already instantiated")
		else:
			GCSingleton.instances[cls] = self

	@classmethod
	def getInstance(cls):
		if cls in GCSingleton.instances:
			return GCSingleton.instances[cls]
		else:
			return None
	
	@classmethod
	def delInstance(cls):
		del GCSingleton.instances[cls]

class LockFile(object):
	
	def __init__(self, filename):
		self.filename = filename
		self.held = False
	
	def __del__(self):
		if self.held:
			os.unlink(self.filename)
	
	def __enter__(self):
		if not self.held:
			self.acquire()
		
		return self
			
	def __exit__(self, type, value, traceback):
		if type != None:
			raise type(value)
		
		if self.held:
			self.release()
		
		return self
	
	def acquire(self):
		while True:
			#print "getting", self.filename
			try:
				fd = os.open(self.filename, os.O_CREAT | os.O_EXCL)
				self.held = True
				break
			except OSError:
				continue
	
	def release(self):
		os.unlink(self.filename)
		self.held = False

def percentageDiff(original, new):
	if new < original:
		diff = (original - new) / original
	elif new > original:
		diff = (new - original) / original
	else:
		diff = 0.0
		
	return diff * 100.0