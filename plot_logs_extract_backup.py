#!/usr/bin/python

import sys
import os
import matplotlib.pyplot as pyplot
from gcresults import *
from gcepochs import *

READ = 1
WRITE = 2
COMBINED = 4

def getHistogram(results, epochBegin, epochEnd, mode):
	
	if mode == READ:
		histogramValue = "read_histogram"
	elif mode == WRITE:
		histogramValue = "write_histogram"
	elif mode == COMBINED:
		histogramValue = "combined_histogram"
		
	timestamps = results.availableTimestamps()
	
	# get begin histogram
	beginDt = find_ge(timestamps, epochBegin)		
	beginHistogram = results.getCommonValue(beginDt, histogramValue)
	
	# in case we get a blank histogram because of a bad sample
	# keep going forward until we find out
	while len(beginHistogram) <= 0:
		print "bad histogram for begin, trying again"
		beginDt = find_gt(timestamps, beginDt)
		beginHistogram = results.getCommonValue(beginDt, histogramValue)
	
	# get end histogram
	endDt = find_le(timestamps, epochEnd)
	endHistogram = results.getCommonValue(endDt, histogramValue)
	
	# in case we get a blank histogram because of a bad sample
	# keep going forward until we find out
	while len(endHistogram) <= 0:
		print "bad histogram for end, trying again"
		endDt = find_lt(timestamps, endDt)
		endHistogram = results.getCommonValue(endDt, histogramValue)
	
	print "beginDt:", beginDt		
	print "endDt:", endDt

	diffHistogram = endHistogram - beginHistogram

	return diffHistogram

if __name__ == "__main__":
	
	
	prefix = sys.argv[1]
	
	if len(sys.argv) >= 3:
		percentile = int(sys.argv[2])
	else:
		percentile = 99
		
	fileList = [f for f in os.listdir(".") if prefix in f]
	
	datapoints = {}
	
	for fileName in fileList:
		fields = fileName.replace(prefix + "-", "").replace(".", "-").replace("_", "-").split("-")
		
		readLevel, writeLevel = fields[0], fields[1]
		print readLevel, writeLevel
		
		with open(fileName, "r") as f:
			for line in f:
				if "DATA" in line:
					lineFields = line[line.find("DATA"):].split()
					print lineFields
					nodes, speed, readResponse, writeResponse = int(lineFields[2]), int(lineFields[4]), float(lineFields[6]), float(lineFields[9])

					name = "%s-%s" % (readLevel, writeLevel)
					if name not in datapoints:
						datapoints[name] = {}
					
					if nodes not in datapoints[name]:
						datapoints[name][nodes] = {}
					
					experimentDir = "/results/%s" % fileName.replace(".log", "")
					results = GCResultsSqliteDisk(experimentDir)
					
					# get epochs
					try:
						epochs = results.getEpochs()
					except Exception as e:
						print "Exception", e
					
					for epoch in epochs.getEpochs():
						begin = epochs.getEpochBegin(epoch)
						end = epochs.getEpochEnd(epoch)
						readHistogram = getHistogram(results, begin, end, READ)
						writeHistogram = getHistogram(results, begin, end, WRITE)
						combinedHistogram = getHistogram(results, begin, end, COMBINED)
					
					# TODO: replace
					try:
						assert readHistogram.getPercentile(percentile) == readResponse
						assert writeHistogram.getPercentile(percentile) == writeResponse
					except:
						print "R", readHistogram.getPercentile(percentile), readResponse
						print "W", writeHistogram.getPercentile(percentile), writeResponse
						
					datapoints[name][nodes][READ] = readHistogram.getPercentile(percentile)
					datapoints[name][nodes][WRITE] = writeHistogram.getPercentile(percentile)
					datapoints[name][nodes][COMBINED] = combinedHistogram.getPercentile(percentile)

	for name in sorted(datapoints):
		xs = []
		ys = []
		
		for nodes in sorted(datapoints[name]):
			xs.append(nodes)
			ys.append(datapoints[name][nodes][READ])
		
		pyplot.plot(xs, ys, 'k')
		pyplot.plot(xs, ys, 'o', label=name)
	
	pyplot.xlim(0, 40)
	#pyplot.ylim(0, 60)
	pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0, prop={'size':8})
	#pyplot.show()
	pyplot.savefig("%s-processed-%dth-READ.pdf" % (prefix, percentile), format="pdf")
	pyplot.clf()
	
	for name in sorted(datapoints):
		xs = []
		ys = []
		
		for nodes in sorted(datapoints[name]):
			xs.append(nodes)
			ys.append(datapoints[name][nodes][WRITE])
		
		pyplot.plot(xs, ys, 'k')
		pyplot.plot(xs, ys, 'o', label=name)
	
	pyplot.xlim(0, 40)
	#pyplot.ylim(0, 60)
	pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0, prop={'size':8})
	#pyplot.show()
	pyplot.savefig("%s-processed-%dth-WRITE.pdf" % (prefix, percentile), format="pdf")
	pyplot.clf()
	
	for name in sorted(datapoints):
		xs = []
		ys = []
		
		for nodes in sorted(datapoints[name]):
			xs.append(nodes)
			ys.append(datapoints[name][nodes][COMBINED])
		
		pyplot.plot(xs, ys, 'k')
		pyplot.plot(xs, ys, 'o', label=name)
	
	pyplot.xlim(0, 40)
	#pyplot.ylim(0, 60)
	pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0, prop={'size':8})
	#pyplot.show()
	pyplot.savefig("%s-processed-%dth-COMBINED.pdf" % (prefix, percentile), format="pdf")
	pyplot.clf()
	
	for n in [9, 18, 27]:
		print "%d nodes" % n
		rPoints = []
		wPoints = []
		cPoints = []
		
		for name in sorted(datapoints):
			rPoints.append((name, datapoints[name][n][READ]))
			wPoints.append((name, datapoints[name][n][WRITE]))
			cPoints.append((name, datapoints[name][n][COMBINED]))
			rPoints.sort()
			wPoints.sort()
			cPoints.sort()
	
		with open("%s-processed-%d-%dth-READ.txt" % (prefix, n, percentile), "w") as f:
			for p in rPoints:
				#print type(p[0]), type(p[1])
				try:
					print p
					f.write("%s - %0.2f\n" % (p[0], p[1]))
				except Exception as e:
					continue

		with open("%s-processed-%d-%dth-WRITE.txt" % (prefix, n, percentile), "w")  as f:
			for p in wPoints:
				try:
					print p
					f.write("%s - %0.2f\n" % (p[0], p[1]))
				except Exception as e:
					continue
		
		with open("%s-processed-%d-%dth-COMBINED.txt" % (prefix, n, percentile), "w")  as f:
			for p in cPoints:
				try:
					print p
					f.write("%s - %0.2f\n" % (p[0], p[1]))
				except Exception as e:
					continue
				
				