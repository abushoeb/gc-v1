#!/usr/bin/python
import time
import socket
from threading import Thread
from gcbase import *
from gchelpers import *
from gccommon import *

class GCWorkloadManager(GCManagerBase):
	clock = None
	workload_predictor = None
	max_load_rate = None
	current_rate = 0.0
	
	def __init__(self, clock, workload_predictor, max_load_rate):
		self.clock = clock
		self.workload_predictor = workload_predictor
		self.max_load_rate = max_load_rate
		
	def start_workload_server(self):
		print "Starting workload server..."
		self.server_thread = Thread(target=self.server_thread)
		self.status_thread = Thread(target=self.status_thread)
		self.trun = True
		self.server_thread.start()
		self.status_thread.start()
		
		
	def stop_workload_server(self):
		self.trun = False
		
	def server_thread(self):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind(('', 9999))
		s.listen(1)
		s.settimeout(1)
		
		while self.trun:
			try:
				conn, addr = s.accept()
				if self.workload_predictor:
					workload_val = self.workload_predictor.getWorkloadAt(self.clock.get_current_time())
					value = denormalize(workload_val, self.max_load_rate)
				else:
					value = self.max_load_rate
					self.max_load_rate += 100
				conn.send("%i" % value)
			except socket.timeout:
				continue
			conn.close()
	
	def status_thread(self):
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind(('', 9998))
		s.listen(1)
		s.settimeout(1)
		
		while self.trun:
			try:
				conn, addr = s.accept()
				received = conn.recv(1024)
				self.current_rate = float(received)
			except socket.timeout:
				continue
			conn.close()
			
	def getValuesForLogging(self):
		values = {}
		target_workload_normalized = self.workload_predictor.getWorkloadAt(self.clock.get_current_time())
		values["actual_workload"] = self.current_rate
		values["target_workload_normalized"] = target_workload_normalized
		values["target_workload"] = denormalize(target_workload_normalized, self.max_load_rate)
		values["nodes_required"] = denormalize(target_workload_normalized, NUM_NODES)
		return values
		
if __name__ == "__main__":
	manager = GCWorkloadManager(None, 10000)
	
	manager.start_workload_server()
	
	time.sleep(30)
	manager.stop_workload_server()
	