#!/usr/bin/python
import sys


if __name__ == "__main__":
	data = sys.stdin.read().strip().split("\n")

	items = []	
	for line in data:
		fields = line.split(" ")[4:]
#		print fields
		items.append((int(fields[2]), int(fields[4]), float(fields[6]), float(fields[9]), float(fields[12])))

	items.sort()
	for l in items:
#		print l[0], l[1], l[2] #, l[3], l[4]
		print "%d\t%d\t%0.2f" % (l[0], l[1], l[2])

