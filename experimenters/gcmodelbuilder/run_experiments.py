#!/usr/bin/python

import argparse
import subprocess
import time

def heat_caches():
	cmd = ["heat_caches.py"]
	print cmd
	subprocess.call(cmd)
	
def reset_cassandra():
	cmd = ["reset_cassandra", "-f"]
	print cmd
	subprocess.call(cmd)

def model_builder(points, interval, nodes, base, operation):
	cmd = []
	cmd.append("./gcmodelbuilder2.py")
	cmd.append("--experiment")
	cmd.append("%s-%s-%d" % (base, operation, nodes))
	cmd.append("--points")
	for p in points:
		cmd.append(str(p))
	cmd.append("--interval")
	cmd.append(str(interval))
	cmd.append("--nodes")
	cmd.append(str(nodes))
	cmd.append("--operation")
	cmd.append(operation)
	
	print cmd	
	subprocess.call(cmd)
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--file", action="store", required=True)
	
	args = parser.parse_args()
	
	f = open(args.file, "r")
	data = f.read().strip()
	lines = data.split("\n")
	
	points = {}
	
	for line in lines:
		target, nodes = line.split(":")
		target = int(target.strip())
		nodes = int(nodes.strip())
		
		try:
			l = points[nodes]
		except KeyError:
			points[nodes] = []
			l = points[nodes]
		
		l.append(target)
		
	print points

	for nodes in sorted(points.keys()):
		reset_cassandra()
		heat_caches()
		model_builder(points[nodes], 1200, nodes, "4-10", "ZIPFIAN")
		time.sleep(60)
	