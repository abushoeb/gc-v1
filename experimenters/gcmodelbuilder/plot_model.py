#!/usr/bin/python
import argparse
import math
import matplotlib.pyplot as pyplot

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--model", action="store", required=True)
	args = parser.parse_args()
	
	f = open(args.model, "r")
	
	lines = f.read().strip().split("\n")
	
	data = {}
	
	for line in lines:
		fields = line.split("\t")
		nodes, throughput, latency = fields[0], fields[1], fields[2]
		nodes = int(nodes)
		throughout = int(throughput)
		latency = float(latency)
		
		try:
			nodesList = data[nodes]
		except KeyError:
			data[nodes] = []
			nodesList = data[nodes]
		
		nodesList.append((throughput, latency))
		
		
	nodeCounts = sorted(data.keys())
	pyplot.rcParams.update({'font.size' : 20})
	pyplot.figure(figsize=(30, 20), frameon=True)

	colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']
	color_index = 0
	
	throughputs = set()
	max_x = 0
	max_y = 0
	
	print nodeCounts
	for nodes in sorted(nodeCounts):
		xs = []		
		ys = []
		
		for throughput, latency in data[nodes]:
			#print throughput, latency
			ys.append(latency)
			xs.append(throughput)
			throughputs.add(throughput)
		
		print max(xs)
		if max(xs) > max_x:
			max_x = math.ceil(float(max(xs)))
		if max(ys) > max_y:
			max_y = math.ceil(float(max(ys)) * 1.1)
			
		pyplot.plot(xs, ys, label="%d nodes" % nodes, color=colors[color_index % len(colors)])
		color_index += 1
	
	ys = []
	for i in xrange(0, len(xs)):
		ys.append(50)
	pyplot.plot(xs,ys)
	
	ys = []
	for i in xrange(0, len(xs)):
		ys.append(75)
	pyplot.plot(xs,ys)
	
	
	print max_x, max_y
	throughputsList = sorted(list(throughputs))
	#pyplot.xticks([float(t) for t in targets])
	pyplot.xticks([float(throughputsList[i]) for i in xrange(0, len(throughputsList), 1)])
	#pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=4, ncol=1, borderaxespad=0.0)
	pyplot.legend(loc=4, ncol=1, borderaxespad=0.35, fontsize=12)
	pyplot.ylim(ymin=0, ymax = max_y)
	pyplot.ylim(ymin=0, ymax = 100)
	pyplot.xlim(xmin=0, xmax = 8000)
	pyplot.savefig("%s.pdf" % args.model, format="pdf")
