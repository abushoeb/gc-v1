#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *

import matplotlib.pyplot as pyplot

def regressionModel(nodes, target):
	nodes = float(nodes)
	target = float(target)/1000.0
	
	#return (-8.7656 * nodes) + (0.0123 * target) + 190.4609

#	return (-4.9557 * nodes) + (0.0099 * target) + (1698.3994 / nodes) + (-190961.0711 / target) + (0.0537 * (target/nodes)) + (11313.6864 * 1.0/(target/nodes)) + 4.1822

#	return (-3.8961 * nodes) + (0.0042 * target) + (1035.0807 / nodes) + (-96471.3712 / target) + (0.1325 * (target/nodes)) + (4625.1666 * 1.0/(target/nodes)) + 36.4737

#	return (-5.0181 * target) + (-1132.6024 * (1.0/nodes)) + (24.0123 * (1.0/target)) + (234.6117 * (target/nodes)) + (-0.6468 * (nodes/target)) + (0.1267 * target**2) + (-0.0597 * nodes**2)  + (0.009 * pow(2.0, target)) + (30.4593 * (target/nodes)**2) + 72.8621

	return (-4.915 * target) + (-1748.0144 * (1.0/nodes)) + (191.1387* (1.0/target)) + (474.7522 * (target/nodes)) + (-5.3096* (nodes/target)) + (-0.4105 * target**2)  + (0.0146 * pow(2.0, target)) + (-68.4296 * (target/nodes)**2) + 21.7174

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--export", action="store_true")
	parser.add_argument("--plot", action="store_true")
	parser.add_argument("--output", action="store_true")
	args = parser.parse_args()
	
	modelData = {}
	regressionData = {}

	node_counts = []
	targets = []
	percentiles = ["avg", "90", "95", "99"]
	experiments = []
	
	files = os.listdir(".")
	for f in sorted(files):
		if f.startswith(args.experiment) and not f.endswith(".log") and not f.endswith(".arff") and not f.endswith(".model"):
			experiments.append(f)
	
	reqs = []
	reqs.append("ycsb.throughput")
	reqs.append("ycsb.target")
	reqs.append("readlatency_95")
	
	labels = "Nodes\tTarget\tActual"
	for percentile in percentiles:
		#reqs.append("readlatency_%s_cum" % percentile)
		reqs.append("readlatency_%s_window" % percentile)
		#reqs.append("writelatency_%s_cum" % percentile)
		#reqs.append("writelatency_%s_window" % percentile)
		labels += "\t%s" % percentile
	
	outfile = open("%s.model" % args.experiment, "w")
	outfile.write("%s\n" % labels)
	print labels
	
	for experiment in experiments:
		splits = experiment.split("-")
		num_nodes = splits[-1]
		node_counts.append(num_nodes)
		try:
			epochs = GCEpochs(experiment)	
		except:
			continue
		
		results = GCResults(experiment, required=reqs, debug=False)
		timestamps = results.availableTimestamps()
		
		# build the tables for the lookup model
		for target in epochs.getEpochs():
			if target not in targets:
				#if int(target) > 5500:
				#	continue
				targets.append(target)
			epochBegin = epochs.getEpochBegin(target)
			epochEnd = epochs.getEpochEnd(target)		
			
			for ts in reversed(timestamps):
				if ts < epochEnd:
					last_ts = ts
				
					accum = 0.0
					count = 0
					
					for avg_ts in reversed(timestamps):
						if avg_ts > epochEnd:
							continue
						
						result = results.getCommonValue(avg_ts, "ycsb.throughput")
						if math.isnan(result):
							continue
						
						accum += result
						count += 1

						if avg_ts < epochBegin:						
							break
						
					avg = int(accum / float(count))
					
					values = "%s\t%s\t%d" % (num_nodes, target, avg)
					key = (num_nodes, target)
					modelData[key] = {}
					
					gotit = False
					for percentile in percentiles:
						r = results.getCommonValue(ts, ("readlatency_%s_window" % percentile))
						if math.isnan(r):
							break
						else:
							gotit = True
						result = int(r)
						values += "\t%d" % result
						modelData[key][percentile] = result
					
					if gotit:
						print values
						outfile.write("%s\n" % values)
						break
					else:
						print "no values"
						sys.exit()
					
					
			# build the data for the regression model
			for ts in timestamps:
				target = results.getCommonValue(ts, "ycsb.target")

				if math.isnan(target):
					continue
				else:
					target = int(target)

				#if target < 1500:
				#	continue
					
				key = (num_nodes, target)
				try:
					l = regressionData[key]
				except KeyError:
					regressionData[key] = []
					l = regressionData[key]
				
				latency = results.getCommonValue(ts, "readlatency_95")
				if not math.isnan(latency):
					l.append(latency)
	
	#close the model file
	outfile.close()
	
	if args.export:
		outfile = open("%s.arff" % args.experiment, "w")
		outfile.write("@RELATION gc-model\n")
		outfile.write("@ATTRIBUTE NUM_NODES NUMERIC\n")
		outfile.write("@ATTRIBUTE TARGET NUMERIC\n")
		outfile.write("@ATTRIBUTE INVNODES NUMERIC\n")
		outfile.write("@ATTRIBUTE INVTARGET NUMERIC\n")
		outfile.write("@ATTRIBUTE TARGETNODE NUMERIC\n")
		outfile.write("@ATTRIBUTE INVTARGETNODE NUMERIC\n")
		outfile.write("@ATTRIBUTE TARGET2 NUMERIC\n")
		outfile.write("@ATTRIBUTE NODE2 NUMERIC\n")
		
		outfile.write("@ATTRIBUTE EXPTARGET NUMERIC\n")
		outfile.write("@ATTRIBUTE EXPNODE NUMERIC\n")
		outfile.write("@ATTRIBUTE TARGETNODE2 NUMERIC\n")
		
		outfile.write("@ATTRIBUTE LATENCY NUMERIC\n")
		outfile.write("@DATA\n")
		auxlist = list(regressionData.keys())
		random.shuffle(auxlist)
		for key in auxlist:
			#print key, regressionData[key]
			num_nodes, target = key
			num_nodes = int(num_nodes)
			target = int(target)/1000.0
			for latency in regressionData[key]:
				latency = float(latency)
				outfile.write("%d %d %f %f %f %f %f %f %f %f %f %f\n" % (num_nodes, target, 1.0/num_nodes, 1.0/target, float(target)/float(num_nodes), float(num_nodes)/float(target),
						    1.0*target*target,
						    1.0*num_nodes*num_nodes,
						    pow(2.0, target), pow(2.0, num_nodes), pow(1.0*target/num_nodes, 2.0),
						    latency))
		outfile.close()
		
		with open("%s-model.arff" % args.experiment, "w") as outfile:
			outfile.write("@RELATION gc-model\n")
			outfile.write("@ATTRIBUTE NUM_NODES NUMERIC\n")
			outfile.write("@ATTRIBUTE TARGET NUMERIC\n")
			
			outfile.write("@ATTRIBUTE INVNODES NUMERIC\n")
			outfile.write("@ATTRIBUTE INVTARGET NUMERIC\n")
			outfile.write("@ATTRIBUTE TARGETNODE NUMERIC\n")
			outfile.write("@ATTRIBUTE INVTARGETNODE NUMERIC\n")
			outfile.write("@ATTRIBUTE TARGET2 NUMERIC\n")
			outfile.write("@ATTRIBUTE NODE2 NUMERIC\n")
			
			outfile.write("@ATTRIBUTE EXPTARGET NUMERIC\n")
			outfile.write("@ATTRIBUTE EXPNODE NUMERIC\n")
			outfile.write("@ATTRIBUTE TARGETNODE2 NUMERIC\n")
			
			outfile.write("@ATTRIBUTE LATENCY NUMERIC\n")
			outfile.write("@DATA\n")
			for num_nodes, target in modelData:
				latency   = modelData[num_nodes, target]["95"]
				num_nodes = int(num_nodes)
				target    = int(target)/1000.0
				if target < 1.5:
					continue
				
				outfile.write("%d %d %f %f %f %f %f %f %f %f %f %f\n" % (num_nodes, target, 1.0/num_nodes, 1.0/target, float(target)/float(num_nodes), float(num_nodes)/float(target),
						    1.0*target*target,
						    1.0*num_nodes*num_nodes,
						    pow(2.0, target), pow(2.0, num_nodes), pow(1.0*target/num_nodes, 2.0),
						    latency))
	
	if args.plot:
		percentile = "95"
		colors = ['r', 'g', 'b', 'c', 'm', 'y']
		print node_counts
		print targets
		index = 0
		
		for count in node_counts:
			xs = []
			ys = []
			for target in targets:
				#print modelData[(count, target)]
				x = float(target)
				y = float(modelData[(count, target)][percentile])
				xs.append(x)
				ys.append(y)
			#pyplot.plot(xs, ys, label="%s nodes" % count, color=colors[index % 5])

			new_ys = []
			window_size = 5
			middle = window_size / 2
			for i in xrange(0, len(ys)):
				if i < middle or i > (len(ys) - middle - 1):
					new_ys.append(ys[i])
					print "continuing, i=%d" % i
					continue			
				print "i = %d" % i
				l = []
				l.append(ys[i])
				for j in xrange(1, middle+1):
					l.append(ys[i-j])	
					l.append(ys[i+j])
			
				l = sorted(l)
			#	new_ys.append(l[middle])
				
				l.remove(l[-1])
				new_ys.append(sum(l) / float(len(l)))
			
			print ys, new_ys
			pyplot.plot(xs, new_ys, label="%s nodes (filtered)" % count, color=colors[index % 5])
			
			index += 1
	
		'''
		for count in node_counts:
			xs = []
			ys = []
			for target in targets:
				x = float(target)
				y = regressionModel(int(count), x)
				xs.append(x)
				ys.append(y)
			pyplot.plot(xs, ys, label="%s nodes (regression)" % count, color=colors[index%5])
			index += 1
		'''
		
		
		pyplot.xticks([float(t) for t in targets])
		pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)
		#pyplot.ylim((0, 120))
		pyplot.show()
		
	if args.output:
		
		
