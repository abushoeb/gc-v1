#!/usr/bin/python

import argparse
import signal
import time

from gccontroller import *
from gcycsb import *
from gchelpers import *
from gcjolokiamanager import *
from gcycsbmanager import *
from gcycsblatencymanager import *
from gccassandraloadmanager import *
from gcscheduler import *
from gcdataloc import *

YCSB_HOSTNAMES=["crypt11"]

class GCModelBuilder(object):
	
	def __init__(self, experiment_name, operation, total_nodes, start, stop, step, interval):	
		self.experiment_name = experiment_name
		self.operation = operation
		self.total_nodes = total_nodes
		self.start_rate = start
		self.stop_rate = stop
		self.step = step
		self.interval = interval*1.25
		
		self.clock = ExperimentClock(datetime.now(), 1)
		self.main_logger = GCLogger("%s.log" % self.experiment_name, GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger, mode=GCClusterManager.CASSANDRA_SLEEP)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManager()
		self.ycsb_manager = GCYCSBManager(YCSB_HOSTNAMES, main_logger=self.main_logger)
		self.ycsb_latency_manager = GCYCSBLatencyManager(YCSB_HOSTNAMES)
		self.cassandra_load_manager = GCCassandraLoadManager()
		
		self.experiment_path = GCOrganizedExperimentPath(os.getcwd(), (), experiment_name)
		
		# set up the data collector
		self.data_collector = GCDataCollector(self.clock, self.experiment_path, logger=self.main_logger, interval=5)
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)
		self.data_collector.register(self.ycsb_manager)
		self.data_collector.register(self.ycsb_latency_manager)
		self.data_collector.register(self.cassandra_load_manager)
		
		# scheduler - this shares the same code as the real controller
		# including the code for using hotness to turn on/off nodes
		self.scheduler = GCArbitraryScheduler(self.clock, self.main_logger, None, None, self.cluster_manager, self.data_collector, None, None)

		# workload
		if operation == "LATEST":
			ycsb_workload = YCSBGreenCassandraReadLatestWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency="ONE", writeconsistency="ALL")
		elif operation == "ZIPFIAN":
			ycsb_workload = YCSBGreenCassandraCustomWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency="ONE", writeconsistency="ALL", read=0.95, write=0.05, requestdistribution="zipfian")

		ycsb_workload.threadcount = 512
		ycsb_workload.maxexecutiontime = 0
		ycsb_workload.targetthroughput = -1
		ycsb_workload.histogram_window = interval
		
		ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.latency_output"
		ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.throughput_output"
		self.ycsb_run = YCSBMultipleRun(ycsb_workload, YCSB_HOSTNAMES, self.main_logger, base_latency_file=ycsb_latency_output_path, base_throughput_file=ycsb_throughput_output_path, client="cassandra-hector")
		
	def run(self):
		self.logger.info("Starting GCModelBuilder")
		self.logger.info("Experiment %s" % args.experiment)
		
		self.logger.info("Starting data collector")
		self.data_collector.start()
		self.data_collector.startWritingData()
		self.logger.info("...done")
		
		self.logger.info("sleeping")
		wait_with_status(10, status_interval=1)
		
		self.ycsb_run.start()
		wait_with_status(10, status_interval=1)
		
		warmupRate = 2000.0 #self.start_rate * 2
		self.logger.info("warming up to sample for scheduler @ %0.2f ops/sec" % warmupRate)
		self.ycsb_manager.setTarget(warmupRate)
		wait_with_status(300, status_interval=10)
		
		self.logger.info("scheduling nodes")
		self.scheduler.schedule(self.total_nodes)
		
		self.logger.info("doing mini warmup @ %0.2f ops/sec" % self.start_rate)
		self.ycsb_manager.setTarget(self.start_rate)
		wait_with_status(600, status_interval=10)
		
		self.logger.info("resetting measurements")
		self.ycsb_manager.resetMeasurements()
		wait_with_status(10, status_interval=1)
		
		self.logger.info("starting model building")
		current_target = self.start_rate
		max_latency = 100.0
		
		while current_target <= self.stop_rate:
			self.logger.info("at target=%d ops/sec" % current_target)
			self.ycsb_manager.setTarget(current_target)
			self.data_collector.epochBegin("%d" % current_target)
			wait_with_status(self.interval, status_interval=10)
			self.data_collector.epochEnd()
			
			# update stuff
			read_latency = self.data_collector.getLastValue("readlatency_99_window")
			write_latency = self.data_collector.getLastValue("writelatency_99_window")
			insert_latency = self.data_collector.getLastValue("insertlatency_99_window")
			self.logger.info("DATA: @ %d -> %d -> %0.2f ms R %0.2f ms W %0.2f ms I" % (self.total_nodes, current_target, read_latency, write_latency, insert_latency))
			
			if read_latency >= max_latency:
				self.logger.info("hit max latency, done")
				break
			if math.isnan(read_latency):
				self.logger.info("error, stopping")
				break
				
			current_target += self.step
		
		self.logger.info("doing tail")
		wait_with_status(60, status_interval=1)
		
		self.shutdown()
		sys.exit()

	def shutdown(self):
		self.logger.info("Shutting down GCModelBuilder...")
		
		self.logger.info("stopping ycsb")
		self.ycsb_run.stop()
				
		self.logger.info("stopping data collector")
		self.data_collector.stop()

		self.logger.info("shutting down cluster manager")
		self.cluster_manager.shutdown(turnOn=False)

def sig_handler(signum, frame):
	print "SIGINT"
	
	if exp:
		exp.shutdown()
	
	print "Done handling SIGINT..."
	
	sys.exit()
	
if __name__ == "__main__":
	signal.signal(signal.SIGINT, sig_handler)
	signal.signal(signal.SIGHUP, sig_handler)
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--nodes", action="store", type=int, required=True)
	parser.add_argument("--operation", action="store", type=str, choices=["LATEST", "ZIPFIAN"], required=True)
	parser.add_argument("--start", action="store", type=int, required=True)
	parser.add_argument("--stop", action="store", type=int, required=True)
	parser.add_argument("--step", action="store", type=int, required=True)
	parser.add_argument("--interval", action="store", type=int, required=True)
	
	args = parser.parse_args()
		
	exp = GCModelBuilder(args.experiment, args.operation, args.nodes, args.start, args.stop, args.step, args.interval)
	exp.run()
