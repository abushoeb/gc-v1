#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *

import matplotlib.pyplot as pyplot

def is_match_experiment(base, name):
	try:
		left = base + "-"
		if name.startswith(left):
			right = name[len(left):]
			n = int(right)
			return True
	except:
		pass
	
	return False

def getNodeCountsAndTargets(data):
	nodeCounts = []
	targets = []
	
	keys = data.keys()
	for nodes, target in keys:
		if node not in nodeCounts:
			nodeCounts.append(nodes)
		
		if target not in targets:
			targets.append(target)
	
	return sorted(nodeCounts), sorted(targets)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--plot", action="store_true")
	
	args = parser.parse_args()
	
	powerData = {}
	loadData = {}

	nodeCounts = []
	targets = []
	experiments = []
	
	# find all experiment files
	files = os.listdir(".")
	for f in sorted(files):
		if f.startswith(args.experiment) and is_match_experiment(args.experiment, f):
			experiments.append(f)
	
	# reqs for GCResults
	reqs = ["node_agg_power", "node_agg_load"]
	
	# for each experiment file
	for experiment in experiments:		
		splits = experiment.split("-")
		nodes = int(splits[-1])
		nodeCounts.append(nodes)
		
		# get the epochs
		try:
			epochs = GCEpochs(experiment)	
		except:
			print "Exception getting epoch"
			continue
		
		# get the GCResults
		results = GCResults(experiment, required=reqs, debug=False)
		timestamps = results.availableTimestamps()
		
		# build the tables for the lookup model
		#print epochs.getEpochs()
		for epoch in epochs.getEpochs():
			target = int(epoch)
			if target not in targets:
				targets.append(target)

			epochBegin = epochs.getEpochBegin(epoch)
			epochEnd = epochs.getEpochEnd(epoch)
			
			#print epochBegin, epochEnd
			powerTotal = 0.0
			powerCount = 0
			
			loadTotal = 0.0
			loadCount = 0
			
			for ts in reversed(timestamps):
				if ts > epochEnd:
					continue
				elif ts <= epochBegin:
					break
				
				power = results.getCommonValue(ts, "node_agg_power")
				load = results.getCommonValue(ts, "node_agg_load")
			
				if math.isnan(power):
					print "nan power continuing"
					continue
				if math.isnan(load):
					print "nan load continuing"
					continue
				
				powerTotal += power
				powerCount += 1
				
				loadTotal += load
				loadCount += 1
			
			key = (nodes, target)
			print "saving data for key", key
			powerData[key] = powerTotal/powerCount
			loadData[key] = loadTotal/loadCount

	if args.plot:
		pyplot.rcParams.update({'font.size' : 15})
		pyplot.figure(figsize=(7, 5), frameon=True)
	
		colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']
		color_index = 0
		
		for nodes in sorted(nodeCounts):
			power_xs = []		
			power_ys = []
			load_xs = []
			load_ys = []
			
			for target in sorted(targets):
				try:
					power_ys.append(powerData[(nodes, target)])
					power_xs.append(target)
				except KeyError:
					pass
				
				try:
					load_ys.append(loadData[(nodes, target)])
					load_xs.append(target)
				except KeyError:
					pass
			
			pyplot.plot(power_xs, power_ys, label="%d nodes (power)" % nodes, color=colors[color_index % len(colors)])
			#pyplot.plot(load_xs, load_ys,label="%d nodes (load)" % nodes, color=colors[color_index % len(colors)])
			color_index += 1
	
		pyplot.xticks([float(t) for t in targets])
		#pyplot.xticks([float(targets[i]) for i in xrange(0, len(targets), 2)])
		#pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=4, ncol=1, borderaxespad=0.0)
		pyplot.legend(loc=4, ncol=1, borderaxespad=0.35, fontsize=12)
		#pyplot.ylim(ymin=0, ymax=150)
		#pyplot.xlim(xmin=0, xmax=10000)
		pyplot.show()

	print nodeCounts
	with open("%s.power" % args.experiment, "w") as outfile:
		for nodes in sorted(nodeCounts):
			print "outputting power for", nodes
			for target in sorted(targets):
				try:
					outfile.write("%d\t%d\t%0.2f\n" % (nodes, target, powerData[(nodes, target)]))
				except KeyError as e:
					print "KeyError", e
	
	with open("%s.load" % args.experiment, "w") as outfile:
		for nodes in sorted(nodeCounts):
			print "outputting load for", nodes
			for target in sorted(targets):
				try:
					outfile.write("%d\t%d\t%0.2f\n" % (nodes, target, loadData[(nodes, target)]))
				except KeyError as e:
					print "KeyError", e	