#!/usr/bin/python

import argparse
import signal
import time
import ast

from gccommon import *
from gchelpers import wait_with_status
from gcycsb import YCSBMultipleRun
from gcycsb import YCSBGreenCassandraCustomWorkload
from gcjolokiamanager import GCJolokiaManagerNew
from gcycsbmanager import GCYCSBManager
from gcycsblatencymanager import GCYCSBLatencyManager
from gccassandraloadmanager import GCCassandraLoadManager
from gcscheduler import GCArbitraryScheduler
from gcdataloc import GCOrganizedExperimentPath
from gccompactionmanager import GCCompactionManager
from gccompactionmanager import CompactionController
from gclogger import GCLogger
from gcclustermanager import GCClusterManager
from gcgangliamanager import GCGangliaManager
from gcjstatmanager import GCJStatManager
from gcdatacollector import GCDataCollector
from experimentclock import ExperimentClock
from gcutil import OrderedSemaphore
from gcutil import TokenOrderedSemaphore
from datetime import datetime

YCSB_HOSTNAMES=[YCSB_HOSTNAME]

INITIAL_WARMUP_INTERVAL = 15*60
#INITIAL_WARMUP_INTERVAL = 24*60*60
#AFTER_SCHEDULE_INTERVAL = 10*60
AFTER_SCHEDULE_INTERVAL = 5*60
AFTER_LOAD_CHANGE_INTERVAL = 2*60
SETTLE_INTERVAL = 10
TAIL_INTERVAL = 5
RESPONSE_TIME_THRESHOLD = 100

class TargetGenerator(object):
	
	def __init__(self):
		self.target = 0
		self.step = 1000
		self.smallStep = 250
		self.lowThreshold = 55.0
		self.highTreshold = 85.0
	
	def next(self, prevLatency):
		if (prevLatency >= self.lowThreshold and prevLatency <= self.highTreshold) or (self.target + self.step) % self.step != 0:
			self.target += self.smallStep
		else:
			self.target += self.step
		
		return self.target

class SingleTargetGenerator(object):
	
	def __init__(self, target):
		self.target = target
		
	def next(self, prevLatency):
		if prevLatency == 0.0:
			return self.target
		else:
			raise StopIteration()
	
class GCFancyModelBuilder(object):
	
	def __init__(self, args):	
		self.experiment_name = args.experiment
		self.operation = args.operation
		self.consistency = args.consistency

		self.nodes = args.startnodes
		self.startnodes = args.startnodes
		self.load = args.startload
		
		self.interval = args.interval
		self.target = args.target
		
		self.nowarmup = args.nowarmup
		self.client = args.client
		
		self.reads = args.reads
		
		if args.nodes:
			self.specific_nodes = [args.nodes]
			self.specific_nodes_iter = iter(self.specific_nodes)
			
		elif not args.specific:
			#self.specific_loads = {}
			#for i in xrange(9, args.startnodes+1, 1):
			#for i in [10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26]:
			#	self.specific_loads[i] = [j for j in xrange(500, 20000+1, 500)]
				
			#self.specific_nodes = sorted(self.specific_loads.keys())#, reverse=True)
			#self.specific_nodes = [i for i in xrange(15, 18+1, 3)]
			#self.specific_nodes = [27, 9, 18, 12, 21, 15, 24]
			self.specific_nodes = [27, 24, 21, 18, 15, 12, 9]
			#self.specific_nodes = [27, 18, 9]
			#self.specific_nodes = [9]
			self.specific_nodes_iter = iter(self.specific_nodes)
		
		elif type(args.specific) == dict:
			self.specific_loads = args.specific
			
			self.specific_nodes = sorted(self.specific_loads.keys())#, reverse=True)
			self.specific_nodes_iter = iter(self.specific_nodes)
				
		elif type(args.specific) == list:
			self.specific_loads = {}
			self.specific_nodes = []
			for t in args.specific:
				n, l = t
				assert type(n) == int
				assert type(l) == list
				self.specific_nodes.append(n)
				self.specific_loads[n] = l		
			
			self.specific_nodes_iter = iter(self.specific_nodes)
			
		#print self.specific_loads
		#print self.specific_nodes
		
		self.measured_points = {}
		
		self.experiment_path = GCOrganizedExperimentPath("/results", (), self.experiment_name)
		self.experiment_path.makeDirs()
		
		self.clock = ExperimentClock(datetime.now(), 1)
		self.main_logger = GCLogger("%s.log" % self.experiment_path.getExperimentId(), GCLogger.DEBUG, stdout=True, stderr=True)
		self.logger = self.main_logger.get_logger(self)
		
		self.compaction_manager = GCCompactionManager()
		self.compaction_controller = CompactionController(self.compaction_manager, count=3, semaphore_class=TokenOrderedSemaphore)
		
		self.cluster_manager = GCClusterManager(mode=GCClusterManager.CASSANDRA_SLEEP, compaction_controller=self.compaction_controller)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManagerNew()
		self.ycsb_manager = GCYCSBManager(YCSB_HOSTNAMES)
		self.ycsb_latency_manager = GCYCSBLatencyManager(YCSB_HOSTNAMES)
		self.cassandra_load_manager = GCCassandraLoadManager()
		#self.jstat_manager = GCJStatManager()
		
		# set up the data collector
		self.data_collector = GCDataCollector(self.experiment_path, interval=5)
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)
		self.data_collector.register(self.ycsb_manager)
		self.data_collector.register(self.ycsb_latency_manager)
		self.data_collector.register(self.cassandra_load_manager)
		self.data_collector.register(self.compaction_manager)
		#self.data_collector.register(self.jstat_manager)
		
		# scheduler - this shares the same code as the real controller
		# including the code for using hotness to turn on/off nodes
		self.scheduler = GCArbitraryScheduler(None, None, None, None)

		# get consistency levels
		readc, writec = self.consistency.split("-")
		
		readFrac = self.reads
		writeFrac = 1.0 - self.reads
		
		# workload
		if self.operation == "LATEST":
			ycsb_workload = YCSBGreenCassandraReadLatestWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency=readc, writeconsistency=writec)
		elif self.operation == "ZIPFIAN":
			ycsb_workload = YCSBGreenCassandraCustomWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency=readc, writeconsistency=writec, read=readFrac, write=writeFrac, requestdistribution="zipfian")
		elif self.operation == "UNIFORM":
			ycsb_workload = YCSBGreenCassandraCustomWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency=readc, writeconsistency=writec, read=0.95, write=0.05, requestdistribution="uniform")
			
		ycsb_workload.threadcount = 512
		ycsb_workload.maxexecutiontime = 0
		ycsb_workload.targetthroughput = -1
		ycsb_workload.histogram_window = int(self.interval)
		
		ycsb_workload.optParams["astyanax.connsperhost"] = 512
		ycsb_workload.optParams["astyanax.latencyupdateinterval"] = 1000
		ycsb_workload.optParams["astyanax.latencyresetinterval"] = 0
		ycsb_workload.optParams["astyanax.latencyscorethreshold"] = 0.50
		ycsb_workload.optParams["astyanax.latencywindowsize"] = 5000
		ycsb_workload.optParams["astyanax.latencyblockedthreshold"] = 256
		ycsb_workload.optParams["astyanax.latencykeepratio"] = 0.75
		
		ycsb_latency_output_path = "/tmp/ycsb.latency.output"
		ycsb_throughput_output_path = "/tmp/ycsb.throughput.output"
		self.ycsb_run = YCSBMultipleRun(ycsb_workload, YCSB_HOSTNAMES, base_latency_file=ycsb_latency_output_path, base_throughput_file=ycsb_throughput_output_path, client="cassandra-%s" % self.client)

	def alreadyMeasured(self, nodes, load):
		try:
			results = self.measured_points[nodes, load]
		except KeyError:
			return False
		
		return results
	
	def markMeasured(self, nodes, load):
		self.measured_points[nodes, load] = True
		
	def run(self):
		self.logger.info("Starting GCFancyModelBuilder")
		self.logger.info("Experiment %s" % self.experiment_path.getExperimentId())
		
		self.logger.info("Starting data collector")
		self.data_collector.start()
		#self.data_collector.startWritingData()
		self.logger.info("...done")
		
		self.logger.info("sleeping")
		wait_with_status(SETTLE_INTERVAL, status_interval=1)
		
		self.logger.info("Starting YCSB with cassandra-%s" % self.client)
		
		self.ycsb_run.start()
		wait_with_status(SETTLE_INTERVAL, status_interval=1)
		
		self.compaction_controller.start()
		
		if not self.nowarmup:
			warmupRate = self.load
			self.logger.info("warming up to sample for scheduler @ %0.2f ops/sec" % warmupRate)
			self.ycsb_manager.setTarget(warmupRate)
			
			# TODO: put this back if need be
			# get enough for the scheduler to work the first time
			#wait_with_status(300, status_interval=10)
			
			# schedule
			#self.scheduler.schedule(self.startnodes)
			
			wait_with_status(INITIAL_WARMUP_INTERVAL, status_interval=10)
			self.ycsb_manager.setTarget(-1)
				
		self.logger.info("resetting measurements")
		self.ycsb_manager.resetMeasurements()
		wait_with_status(SETTLE_INTERVAL, status_interval=1)
				
		nodes_test = []
		
		self.logger.info("starting model building")
		while True:
			try:
				self.nodes = self.specific_nodes_iter.next()
			except StopIteration:
				break
			
			#self.specific_load_iter = iter(self.specific_loads[self.nodes])
			self.specific_load_iter = TargetGenerator()
			#self.specific_load_iter = SingleTargetGenerator(2000)
			
			# do node scheduling
			self.logger.info("setting cluster to %d nodes" % self.nodes)
			retVal = self.scheduler.schedule(self.nodes)
			turningOn, nodesChanged = retVal
			
			self.logger.info("waiting for nodes to enter GC_READY")
			self.scheduler.waitForSchedule()
			
			if turningOn:
				while self.compaction_manager.getPendingCompactions(nodes=nodesChanged) > 0:
					self.logger.info("waiting for compactions from transition to finish...")
					time.sleep(10)
			
			self.logger.info("scheduling complete, sleeping to settle")
			wait_with_status(AFTER_SCHEDULE_INTERVAL, status_interval=5)
			
			prev_read_response = 0.0
			while True:
				try:
					#self.load = self.specific_load_iter.next()
					self.load = self.specific_load_iter.next(prev_read_response)
				except StopIteration:
					break
								
				# set load
				self.logger.info("at nodes=%d, load=%d ops/sec" % (self.nodes, self.load))
				self.ycsb_manager.setTarget(self.load)
				wait_with_status(AFTER_LOAD_CHANGE_INTERVAL, status_interval=10)
				
				self.data_collector.epochBegin("%d-%d" % (self.nodes, self.load))
				wait_with_status(self.interval, status_interval=10)
				self.data_collector.epochEnd()
							
				# get last window response times
				read_response = self.data_collector.getLastValue("readlatency_99_window")
				prev_read_response = read_response
				
				write_response = self.data_collector.getLastValue("writelatency_99_window")
				insert_response = self.data_collector.getLastValue("insertlatency_99_window")
					
				actual_throughput = self.data_collector.getLastValue("ycsb_throughput")
				if actual_throughput < 0.7*self.load:
					read_response = 200.0
				
				self.logger.info("DATA: @ %d -> %d -> %0.2f ms R %0.2f ms W %0.2f ms I" % (self.nodes, self.load, read_response, write_response, insert_response))
				
				if read_response >= RESPONSE_TIME_THRESHOLD:
					break
			
			self.logger.info("disable load while we change node count")
			self.ycsb_manager.setTarget(-1)
		
		self.logger.info("doing tail")
		wait_with_status(TAIL_INTERVAL, status_interval=1)
		
		self.shutdown()
		sys.exit()

	def shutdown(self):
		self.logger.info("Shutting down GCFancyModelBuilder...")
		
		self.logger.info("stopping ycsb")
		self.ycsb_run.stop()
				
		self.logger.info("stopping data collector")
		self.data_collector.stop()

		self.logger.info("stopping compaction controller")
		self.compaction_controller.stop()
		
		self.logger.info("shutting down cluster manager")
		self.cluster_manager.shutdown(turnOn=False)

def sig_handler(signum, frame):
	print "SIGINT"
	
	if exp:
		exp.shutdown()
	
	print "Done handling SIGINT..."
	
	sys.exit()
	
if __name__ == "__main__":
	signal.signal(signal.SIGINT, sig_handler)
	signal.signal(signal.SIGHUP, sig_handler)
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--startnodes", action="store", type=int, default=27)
	#parser.add_argument("--minnodes", action="store", type=int, default=9)
	#parser.add_argument("--maxnodes", action="store", type=int, default=27)
	#parser.add_argument("--stepnodes", action="store", type=int, default=3)
	parser.add_argument("--startload", action="store", type=int, default=1500)
	parser.add_argument("--target", action="store", type=int, default=75)
	#parser.add_argument("--direction", action="store", type=int, default=-1)
	#parser.add_argument("--stepload", action="store", type=int, default=500)
	parser.add_argument("--operation", action="store", type=str, choices=["LATEST", "ZIPFIAN", "UNIFORM"], default="ZIPFIAN")
	parser.add_argument("--consistency", action="store", type=str, choices=["ONE-ONE", "QUORUM-ONE", "ALL-ONE", "ONE-QUORUM", "QUORUM-QUORUM", "ALL-QUORUM", "ONE-ALL", "QUORUM-ALL", "ALL-ALL"], default="ONE-ONE")
	parser.add_argument("--interval", action="store", type=int, default=1200)
	parser.add_argument("--nowarmup", action="store_true", default=False)
	parser.add_argument("--specific", action="store", type=ast.literal_eval)
	parser.add_argument("--client", action="store", type=str, default="hector")
	parser.add_argument("--nodes", action="store", type=int)
	parser.add_argument("--reads", action="store", type=float, default=0.95)
		
	args = parser.parse_args()
	
	exp = GCFancyModelBuilder(args)
	exp.run()
