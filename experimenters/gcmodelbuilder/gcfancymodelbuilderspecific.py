#!/usr/bin/python

import argparse
import signal
import time
import ast

from gccontroller import *
from gcycsb import *
from gchelpers import *
from gcjolokiamanager import *
from gcycsbmanager import *
from gcycsblatencymanager import *
from gccassandraloadmanager import *
from gcscheduler import *
from gcdataloc import *
from gccompactionmanager import *

YCSB_HOSTNAMES=["crypt11"]

INITIAL_WARMUP_INTERVAL = 5*60
AFTER_SCHEDULE_INTERVAL = 5*60
AFTER_LOAD_CHANGE_INTERVAL = 2*60
SETTLE_INTERVAL = 10
TAIL_INTERVAL = 60
BASE_COMPACTION_THROUGHPUT = 2048

class GCFancyModelBuilder(object):
	
	def __init__(self, args):	
		self.experiment_name = args.experiment
		self.operation = args.operation

		self.nodes = args.startnodes
		self.load = args.startload
		
		self.interval = args.interval
		self.target = args.target
		
		self.nowarmup = args.nowarmup
		self.specific_loads = args.specific
		
		self.specific_nodes = sorted(self.specific_loads.keys(), reverse=True)
		self.specific_nodes_iter = iter(self.specific_nodes)
			
		self.measured_points = {}
		
#		self.experiment_path = GCOrganizedExperimentPath(os.getcwd(), (), self.experiment_name)
		self.experiment_path = GCOrganizedExperimentPath("/results", (), self.experiment_name)
		
		self.clock = ExperimentClock(datetime.now(), 1)
		self.main_logger = GCLogger("%s.log" % self.experiment_path.getExperimentId(), GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		
		self.compaction_manager = GCCompactionManager()		
		self.compaction_controller = CompactionController(self.compaction_manager, count=3, interval=10, logger=self.main_logger)
		
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger, mode=GCClusterManager.CASSANDRA_SLEEP, compaction_controller=self.compaction_controller)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManagerNew()
		self.ycsb_manager = GCYCSBManager(YCSB_HOSTNAMES, main_logger=self.main_logger)
		self.ycsb_latency_manager = GCYCSBLatencyManager(YCSB_HOSTNAMES)
		self.cassandra_load_manager = GCCassandraLoadManager()
		
		
		# set up the data collector
		self.data_collector = GCDataCollector(self.clock, self.experiment_path, logger=self.main_logger, interval=5)
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)
		self.data_collector.register(self.ycsb_manager)
		self.data_collector.register(self.ycsb_latency_manager)
		self.data_collector.register(self.cassandra_load_manager)
		self.data_collector.register(self.compaction_manager)
		
		# scheduler - this shares the same code as the real controller
		# including the code for using hotness to turn on/off nodes
		self.scheduler = GCArbitraryScheduler(self.clock, self.main_logger, None, None, self.cluster_manager, self.data_collector, None, None)

		# workload
		if self.operation == "LATEST":
			ycsb_workload = YCSBGreenCassandraReadLatestWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency="ONE", writeconsistency="ALL")
		elif self.operation == "ZIPFIAN":
			ycsb_workload = YCSBGreenCassandraCustomWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency="ONE", writeconsistency="ALL", read=0.95, write=0.05, requestdistribution="zipfian")
		elif self.operation == "UNIFORM":
			ycsb_workload = YCSBGreenCassandraCustomWorkload(MINIMUM_NODES + OPTIONAL_NODES, YCSB_RECORD_COUNT, readconsistency="ONE", writeconsistency="ALL", read=0.95, write=0.05, requestdistribution="uniform")
			
		ycsb_workload.threadcount = 512
		ycsb_workload.maxexecutiontime = 0
		ycsb_workload.targetthroughput = -1
		ycsb_workload.histogram_window = int(self.interval)
		
		#ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.latency_output"
		#ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.throughput_output"
		ycsb_latency_output_path = "/tmp/ycsb.latency.output"
		ycsb_throughput_output_path = "/tmp/ycsb.throughput_output"
		self.ycsb_run = YCSBMultipleRun(ycsb_workload, YCSB_HOSTNAMES, self.main_logger, base_latency_file=ycsb_latency_output_path, base_throughput_file=ycsb_throughput_output_path, client="cassandra-hector")
	
	
	def disableCompaction(self):
		self.logger.info("Acquiring compaction lock")
		try:
			self.compaction_manager.acquireCompactionLock()
		except Exception as e:
			self.logger.info("Exception acquiring compaction lock: %s" % e)
	
	def enableCompaction(self):
		self.logger.info("Releasing compaction lock")
		try:
			self.compaction_manager.releaseCompactionLock()
		except Exception as e:
			self.logger.info("Exception releasing compaction lock: %s" % e)
		
	
	def alreadyMeasured(self, nodes, load):
		try:
			results = self.measured_points[nodes, load]
		except KeyError:
			return False
		
		return results
	
	def markMeasured(self, nodes, load):
		self.measured_points[nodes, load] = True
		
	def run(self):
		self.logger.info("Starting GCFancyModelBuilder")
		self.logger.info("Experiment %s" % self.experiment_path.getExperimentId())
		
		self.logger.info("Starting data collector")
		self.data_collector.start()
		self.data_collector.startWritingData()
		self.logger.info("...done")
		
		self.logger.info("sleeping")
		wait_with_status(SETTLE_INTERVAL, status_interval=1)
		
		self.ycsb_run.start()
		wait_with_status(SETTLE_INTERVAL, status_interval=1)
		
		self.disableCompaction()
		self.compaction_controller.start()
		
		self.compaction_manager.setCompactionThroughput(BASE_COMPACTION_THROUGHPUT)
		
		if not self.nowarmup:
			warmupRate = self.load
			self.logger.info("warming up to sample for scheduler @ %0.2f ops/sec" % warmupRate)
			self.ycsb_manager.setTarget(warmupRate)
			wait_with_status(INITIAL_WARMUP_INTERVAL, status_interval=10)
				
		self.logger.info("resetting measurements")
		self.ycsb_manager.resetMeasurements()
		wait_with_status(SETTLE_INTERVAL, status_interval=1)
				
		nodes_test = []
		
		self.logger.info("starting model building")
		while True:
			try:
				self.nodes = self.specific_nodes_iter.next()
			except StopIteration:
				break
			
			self.specific_load_iter = iter(self.specific_loads[self.nodes])
			
			# do node scheduling
			self.logger.info("setting cluster to %d nodes" % self.nodes)
			retVal = self.scheduler.schedule(self.nodes)
			print retVal
			turningOn, nodesChanged = retVal
			
			self.logger.info("waiting for nodes to enter GC_READY")
			self.scheduler.waitForSchedule()
			
			if turningOn != None:
				self.compaction_manager.setCompactionThroughput(0, nodes=nodesChanged)
				while self.compaction_manager.getPendingCompactions(nodes=nodesChanged) > 0:
					self.logger.info("waiting for compactions from transition to finish...")
					time.sleep(10)
			
			
			# set compaction limit
			#self.logger.info("setting compaction semaphore limit")
			#if self.nodes > 0 and self.nodes <= 9:
			#	self.compaction_controller.setGlobalLimit(1)
			#	self.logger.info("set compaction semaphore limit to 1")
			#elif self.nodes > 9 and self.nodes <= 18:
			#	self.compaction_controller.setGlobalLimit(2)
			#	self.logger.info("set compaction semaphore limit to 2")
			#elif self.nodes > 18 and self.nodes <= 27:
			#	self.compaction_controller.setGlobalLimit(3)
			#	self.logger.info("set compaction semaphore limit to 3")
			
			#newCompactionThroughput = int(BASE_COMPACTION_THROUGHPUT * (self.nodes*1.0 / len(MINIMUM_NODES + OPTIONAL_NODES)))
			#self.compaction_manager.setCompactionThroughput(newCompactionThroughput)
			#self.logger.info("set compaction throughput to %d" % newCompactionThroughput)
			
			self.logger.info("scheduling complete, sleeping to settle")
			wait_with_status(AFTER_SCHEDULE_INTERVAL, status_interval=5)
			
			while True:
				try:
					self.load = self.specific_load_iter.next()
				except StopIteration:
					break
								
				# set load
				self.logger.info("at nodes=%d, load=%d ops/sec" % (self.nodes, self.load))
				self.ycsb_manager.setTarget(self.load)
				wait_with_status(AFTER_LOAD_CHANGE_INTERVAL, status_interval=10)
				
				self.data_collector.epochBegin("%d-%d" % (self.nodes, self.load))
				wait_with_status(self.interval, status_interval=10)
				self.data_collector.epochEnd()
							
				# get last window response times
				read_response = self.data_collector.getLastValue("readlatency_99_window")
				write_response = self.data_collector.getLastValue("writelatency_99_window")
				insert_response = self.data_collector.getLastValue("insertlatency_99_window")
					
				actual_throughput = self.data_collector.getLastValue("ycsb.throughput")
				if actual_throughput < 0.8*self.load:
					read_response = 200.0
				
				self.logger.info("DATA: @ %d -> %d -> %0.2f ms R %0.2f ms W %0.2f ms I" % (self.nodes, self.load, read_response, write_response, insert_response))
		
		self.logger.info("doing tail")
		wait_with_status(TAIL_INTERVAL, status_interval=1)
		
		self.shutdown()
		sys.exit()

	def shutdown(self):
		self.logger.info("Shutting down GCFancyModelBuilder...")
		
		self.logger.info("stopping ycsb")
		self.ycsb_run.stop()
				
		self.logger.info("stopping data collector")
		self.data_collector.stop()

		self.logger.info("stopping compaction controller")
		self.compaction_controller.stop()
		
		self.logger.info("shutting down cluster manager")
		self.cluster_manager.shutdown(turnOn=False)

def sig_handler(signum, frame):
	print "SIGINT"
	
	if exp:
		exp.shutdown()
	
	print "Done handling SIGINT..."
	
	sys.exit()
	
if __name__ == "__main__":
	signal.signal(signal.SIGINT, sig_handler)
	signal.signal(signal.SIGHUP, sig_handler)
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--startnodes", action="store", type=int, default=27)
	parser.add_argument("--minnodes", action="store", type=int, default=9)
	parser.add_argument("--maxnodes", action="store", type=int, default=27)
	parser.add_argument("--stepnodes", action="store", type=int, default=3)
	parser.add_argument("--startload", action="store", type=int, default=8000)
	parser.add_argument("--target", action="store", type=int, default=50)
	parser.add_argument("--direction", action="store", type=int, default=-1)
	parser.add_argument("--stepload", action="store", type=int, default=500)
	parser.add_argument("--operation", action="store", type=str, choices=["LATEST", "ZIPFIAN", "UNIFORM"], default="ZIPFIAN")
	parser.add_argument("--interval", action="store", type=int, default=420)
	parser.add_argument("--nowarmup", action="store_true", default=False)
	parser.add_argument("--specific", action="store", type=ast.literal_eval, required=True)
		
	args = parser.parse_args()
	
	exp = GCFancyModelBuilder(args)
	exp.run()
