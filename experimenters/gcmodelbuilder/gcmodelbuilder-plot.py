#!/usr/bin/python

import argparse
import sys
from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *

class GCSimpleMultiPlot(GCPlot):
	def __init__(self, results=None, title="Simple Plot", ylim=None):
		if self.results != None:
			self.addResults(results)
		self.title = title
		self.ylim = ylim
		
	def addResults(self, results):
		pass
		
	def doPlot(self):
		pass
	

		

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--pdf", action="store_true", default=False)
	args = parser.parse_args()
	
	plotter = GCPlotter(xsize=20, ysize=10)

	experiment = args.experiment
	reqs = [
		"ycsb.throughput",
		"ycsb.target",
	#	"cassandra_cluster_load",
	#	"cluster_cpu_total",
		"readlatency_95",
		"writelatency_95",
		"readlatency_99",
		"writelatency_99",
		"readlatency_avg",
		"writelatency_avg",
		"readlatency_95_cum", 
		"writelatency_95_cum",
		"readlatency_95_window",
		"writelatency_95_window",
		"readlatency_99_cum", 
		"writelatency_99_cum",
		"readlatency_99_window",
		"writelatency_99_window",
	#	"cassandra_read_latency",
	#	"cassandra_read_rate",
	#	"jvm_parnew_time",
	#	"jvm_cms_time",
		"state",
		"cassandra_total_compactions",
		"cassandra_load",
		"sstables_data",
		"sstables_greenhints",
	]
	
	gcresults = GCResults(experiment, required=reqs, drop_last=True)
	
	plot = GCMultiCommonPlot(gcresults, ["ycsb.throughput", "ycsb.target"], title="Throughput vs. Target: %s" % experiment)
	#plot = GCMultiCommonPlotOverlay()
	#plot.addResults(gcresults, "ycsb.throughput", label="throughput")
	#plot.addResults(gcresults, "ycsb.latency", label="latency")
	#plot.addResults(gcresults, "cassandra_cluster_load", label="load")
	plotter.addPlot(plot)
	
	#latency = GCMultiCommonPlot(gcresults, ["readlatency_95", "writelatency_95", "readlatency_avg", "writelatency_avg"], title="Response Time", ylim=(0,100))
	#plotter.addPlot(latency)
	
	#latency = GCMultiCommonPlot(gcresults, ["readlatency_99"], title="Response Time", ylim=(50,200))
	#plotter.addPlot(latency)
	
	
	#latency = GCMultiCommonPlot(gcresults, ["readlatency_95_cum", "writelatency_95_cum"], title="Response Time (Cumulative)", ylim=(0,150))
	#plotter.addPlot(latency)
	
	#latency = GCMultiCommonPlot(gcresults, ["readlatency_95_window", "writelatency_95_window"], title="Response Time (Window)", ylim=(0,150))
	#plotter.addPlot(latency)
	
	latency = GCMultiCommonPlot(gcresults, ["readlatency_99_cum", "writelatency_99_cum"], title="Response Time (Cumulative)", ylim=(0,200))
	plotter.addPlot(latency)
	
	latency = GCMultiCommonPlot(gcresults, ["readlatency_99_window", "writelatency_99_window"], title="Response Time (Window)", ylim=(0,200))
	plotter.addPlot(latency)
	
	
#	plot = GCSimpleMultiNodePlot(gcresults, "cassandra_read_latency", (MINIMUM_NODES + OPTIONAL_NODES), ylim=(0,100000))
#	plotter.addPlot(plot)

	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_total_compactions", (MINIMUM_NODES + OPTIONAL_NODES))
	#plotter.addPlot(plot)

	#plot = GCNodeStatePlot(gcresults)
	#plotter.addPlot(plot)
			
	#node_read_rate = GCSimpleMultiNodePlot(gcresults, "cassandra_read_rate", (MINIMUM_NODES + OPTIONAL_NODES))
	#plotter.addPlot(node_read_rate)
	
	#node_gc = GCSimpleMultiNodePlot(gcresults, "jvm_parnew_time", (MINIMUM_NODES + OPTIONAL_NODES))
	#plotter.addPlot(node_gc)
	
	#node_gc = GCSimpleMultiNodePlot(gcresults, "jvm_cms_time", (MINIMUM_NODES + OPTIONAL_NODES))
	#plotter.addPlot(node_gc)
	
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_load", MINIMUM_NODES, title="Load (min)", legend=False, ylim=(0, 8))
	#plotter.addPlot(plot)
	
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_load", OPTIONAL_NODES, title="Load (opt)", legend=False, ylim=(0, 16))
	#plotter.addPlot(plot)
	
	plot = GCSimpleMultiNodePlot(gcresults, "sstables_data", MINIMUM_NODES, title="SSTables (min)", legend=False, ylim=(0, 8))
	plotter.addPlot(plot)
	
	plot = GCSimpleMultiNodePlot(gcresults, "sstables_data", OPTIONAL_NODES, title="SSTables (opt)", legend=False, ylim=(0, 16))
	plotter.addPlot(plot)
	
	'''
	plot = GCSimpleCommonPlot(gcresults, "cassandra_cluster_load", title="Load")
	plotter.addPlot(plot)
	
	plot = GCSimpleCommonPlot(gcresults, "cluster_cpu_total", title="CPU")
	plotter.addPlot(plot)
	
	plot = GCSimpleNodePlot(gcresults, "cpu_total", "sol032", title="Sol032 cpu")
	plotter.addPlot(plot)
	'''	
	
	if args.pdf:
		plotter.doPlot(output="%s.pdf" % args.experiment)
	else:
		plotter.doPlot()
