#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *
from gcresponsetime import *

import matplotlib.pyplot as pyplot

def is_match_experiment(base, name):
	try:
		left = base + "-"
		if name.startswith(left):
			right = name[len(left):]
			n = int(right)
			return True
	except:
		pass
	
	return False

def getNodeCountsAndTargets(data):
	nodeCounts = []
	targets = []
	
	keys = data.keys()
	for nodes, target in keys:
		if node not in nodeCounts:
			nodeCounts.append(nodes)
		
		if target not in targets:
			targets.append(target)
	
	return sorted(nodeCounts), sorted(targets)

def get_smoothed_data(data):
	filteredData = {}
	
	window_size = 5
	middle = window_size / 2
	
	nodeCounts, targets = getNodeCountsAndTargets(data)
	
	nodeCounts = sorted(nodeCounts)
	
	for nodes in nodeCounts:
		values = []
		new_values = []
		
		for target in sorted(targets):
			try:
				values.append(data[(nodes, target)])
			except KeyError:
				continue
		
		for i in xrange(0, len(values)):
			if i < middle or i > (len(values) - middle - 1):
				new_values.append(values[i])
				continue
		
			l = []
			l.append(values[i])
			for j in xrange(1, middle+1):
				l.append(values[i-j])
				l.append(values[i+j])
			
			l = sorted(l)
			l.remove(l[0])
			l.remove(l[-1])
			new_values.append(sum(l) / float(len(l)))
		
		for i in xrange(1, len(targets)-1):
			if new_values[i] > new_values[i+1]:
				new_values[i] = (new_values[i-1] + new_values[i+1]) / 2

		for i in xrange(0, len(targets)):
			key = (nodes, targets[i])
			filteredData[key] = new_values[i]

def min_filter(data):
	filteredData = {}
	
	nodeCounts, targets = getNodeCountsAndTargets(data)

	# do minimum filtering
	for i in reversed(xrange(1, len(nodeCounts))):
		for j in xrange(0, len(targets)):
			key = (nodeCounts[i], targets[j])
			keyNext = (nodeCounts[i-1], targets[j])

			try:
				if data[key] > data[keyNext]:
	#				print key, keyNext
					#print filteredModelData[key], filteredModelData[keyNext]
					data[key] = data[keyNext]
			except KeyError:
				pass

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--percentile", action="store", type=str, default="99")
	parser.add_argument("--plot", action="store_true")
	parser.add_argument("--paperplot", action="store_true")
	
	args = parser.parse_args()
	
	rawDataLast = {}
	rawDataLowest = {}
	
	histograms = {}

	nodeCounts = []
	targets = []
	experiments = []
	
	# find all experiment files
	files = os.listdir(".")
	for f in sorted(files):
		if f.startswith(args.experiment) and is_match_experiment(args.experiment, f):
			experiments.append(f)
	
	# reqs for GCResults
	reqs = ["readlatency_%s_window" % args.percentile]
	
	# for each experiment file
	for experiment in experiments:
		splits = experiment.split("-")
		nodes = int(splits[-1])
		nodeCounts.append(nodes)
		
		# get the epochs
		try:
			epochs = GCEpochs(experiment)	
		except:
			continue
		
		# get the GCResults
		results = GCResults(experiment, required=reqs, debug=False)
		# get the response time data for the experiment
		responseTime = GCResponseTime(experiment, "histogram.read.crypt11")
		
		timestamps = results.availableTimestamps()
		
		# build the tables for the lookup model
		#print epochs.getEpochs()
		for epoch in epochs.getEpochs():
			target = int(epoch)
			if target not in targets:
				targets.append(target)

			key = (nodes, target)

			epochBegin = epochs.getEpochBegin(epoch) #+ timedelta(minutes=1)
			epochEnd = epochs.getEpochEnd(epoch)

			print key, epochBegin, epochEnd
			
			# get the response time data
			lowest = 100000.0
			last = float("NaN")
			
			for ts in reversed(timestamps):
				if ts > epochEnd:
					continue
				elif ts <= epochBegin:
					break
				
				latency = results.getCommonValue(ts, ("readlatency_%s_window" % args.percentile))
			#	print latency
				if math.isnan(latency):
					continue
				
				if math.isnan(last):
					last = latency
				
				if latency < lowest:
					lowest = latency
			
			rawDataLowest[key] = lowest
			rawDataLast[key] = last
			
			# get the histograms
			for ts in timestamps:
				if ts < epochBegin:
					continue
				elif ts >= epochEnd:
					break
				#print "begin histogram", ts
				histogram = responseTime.getHistogramAt(ts)
				
				if len(histogram) > 0:
					beginHistogram = histogram
					break
			
			for ts in reversed(timestamps):
				if ts > epochEnd:
					continue
				elif ts <= epochBegin:
					break
				
				#print "end histogram", ts
				histogram = responseTime.getHistogramAt(ts)
				
				if len(histogram) > 0:
					endHistogram = histogram
					break
			
			diffHistogram = endHistogram - beginHistogram
			histograms[key] = diffHistogram	
			#print key, lowest, last
	
	# do min filtering
	#min_filter(rawDataLast)

	if args.plot:
		pyplot.rcParams.update({'font.size' : 15})
		pyplot.figure(figsize=(7, 5), frameon=True)
	
		colors = ['r', 'g', 'b', 'c', 'm', 'y', 'k']
		color_index = 0
		
		for nodes in sorted(nodeCounts):
			original_xs = []		
			original_ys = []
			#filtered_xs = []
			#filtered_ys = []
			
			for target in sorted(targets):
				try:
					original_ys.append(rawDataLast[(nodes, target)])
					original_xs.append(target)
				except KeyError:
					pass
				
			#	try:
			#		filtered_ys.append(modelDataRaw[(nodes, target)])
			#		filtered_xs.append(target)
			#	except KeyError:
			#		pass
			
			#pyplot.plot(original_xs, original_ys, label="%d nodes (original)" % nodes, color=colors[color_index % len(colors)])
			pyplot.plot(original_xs, original_ys,label="%d nodes (raw)" % nodes, color=colors[color_index % len(colors)])
			#pyplot.plot(xs, filtered_ys, label="%d nodes (filtered)" % nodes, linestyle="dashed", color=colors[color_index % len(colors)])
			color_index += 1
	
		#pyplot.xticks([float(t) for t in targets])
		pyplot.xticks([float(targets[i]) for i in xrange(0, len(targets), 2)])
		#pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=4, ncol=1, borderaxespad=0.0)
		pyplot.legend(loc=4, ncol=1, borderaxespad=0.35, fontsize=12)
		pyplot.ylim(ymin=0, ymax=150)
		pyplot.xlim(xmin=0, xmax=10000)
		pyplot.show()
		
	with open("%s.model" % args.experiment, "w") as outfile:
		for nodes in sorted(nodeCounts):
			for target in sorted(targets):
				try:
					outString = "%d\t%d\t%0.2f" % (nodes, target, rawDataLast[(nodes, target)])
					print outString
					outfile.write("%s\n" % outString)
				except KeyError:
					pass
	
	for nodes in sorted(nodeCounts):
		for target in sorted(targets):
			print nodes, target
			key = (nodes, target)
			try:
				histogram = histograms[key]
			except KeyError:
				print "Missing data for", key
				continue
			
			outFilename = "%s-%d-%d.cdf" % (args.experiment, nodes, target)
			outfile = open(outFilename, "w")
			x, y = histogram.getCDF()
			try:
				assert(histogram.getPercentile(99) == rawDataLast[key])
				#print "Assertion Successful", key, histogram.getPercentile(99), rawDataLast[key]
			except AssertionError:
				pass
				#print key, histogram
				#print "AssertionError", key, histogram.getPercentile(99), rawDataLast[key]
				#sys.exit()
			except:
				print "weird errror"
				
			for i in xrange(0, len(x)):
				outString = "%f %f" % (x[i], y[i])
				outfile.write("%s\n" % outString)
			outfile.close()
