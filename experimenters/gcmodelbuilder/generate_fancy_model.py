#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *
from gcresponsetime import *

import matplotlib.pyplot as pyplot
import numpy as np

def getHistogram(results, epochBegin, epochEnd):
	timestamps = results.availableTimestamps()
	
	# get begin histogram
	beginDt = find_ge(timestamps, epochBegin)		
	beginHistogram = results.getCommonValue(beginDt, args.histogram)
	
	# in case we get a blank histogram because of a bad sample
	# keep going forward until we find out
	while len(beginHistogram) <= 0:
		print "bad histogram for begin, trying again"
		beginDt = find_gt(timestamps, beginDt)
		beginHistogram = results.getCommonValue(beginDt, args.histogram)
	
	# get end histogram
	endDt = find_le(timestamps, epochEnd)
	endHistogram = results.getCommonValue(endDt, args.histogram)
	
	# in case we get a blank histogram because of a bad sample
	# keep going forward until we find out
	while len(endHistogram) <= 0:
		print "bad histogram for end, trying again"
		endDt = find_lt(timestamps, endDt)
		endHistogram = results.getCommonValue(endDt, args.histogram)
	
	print "beginDt:", beginDt		
	print "endDt:", endDt

	diffHistogram = endHistogram - beginHistogram

	return diffHistogram

def getResponseTimeCurve(results):
	data = []
	
	timestamps = results.availableTimestamps()
	
	firstDt = timestamps[0]
	firstHistogram = results.getCommonValue(firstDt, args.histogram)
	
	if firstHistogram.isEmpty():
		#data[firstDt] = float("NaN")
		data.append(float("Nan"))
	else:
		#data[firstDt] = firstHistogram.getPercentile(99)
		data.append(firstHistogram.getPercentile(99))
	
	for i in xrange(1, len(timestamps)):
		prevDt = timestamps[i-1]
		currentDt = timestamps[i]
		
		prevHistogram = results.getCommonValue(prevDt, args.histogram)
		currentHistogram = results.getCommonValue(currentDt, args.histogram)
		
		#prevHistogram = responseTime.getHistogramAt(prevDt)
		#currentHistogram = responseTime.getHistogramAt(currentDt)
		
		if prevHistogram.isEmpty() or currentHistogram.isEmpty():
			#data[currentDt] = float("NaN")
			data.append(float("NaN"))
		else:
			diffHistogram = currentHistogram - prevHistogram
			#print currentHistogram.getPercentile(99), prevHistogram.getPercentile(99), diffHistogram.getPercentile(99)
			#data[currentDt] = diffHistogram.getPercentile(99)
			data.append(diffHistogram.getPercentile(99))
		
	return data


def findGoodStart(timestamps, responseTimeCurve, i, endI, windowMedian, windowStdDev):
	while i < endI:
		#print "in find good start", responseTimeCurve[i]
		#dt = timestamps[i]
		#if math.isnan(responseTimeCurve[dt]) or responseTimeCurve[dt] >= windowMedian + windowStdDev:
		if math.isnan(responseTimeCurve[i]) or responseTimeCurve[i] >= windowMedian + max(10.0, windowMedian*0.2):
			#print responseTimeCurve[i], "continuing start"
			i += 1
			continue
		else:
			return i
	
	return -1

def findGoodEnd(timestamps, responseTimeCurve, i, endI, windowMedian, windowStdDev):
	while i < endI:
		#print "in find good end", responseTimeCurve[i]

		#dt = timestamps[i]
		#if math.isnan(responseTimeCurve[dt]) or responseTimeCurve[dt] < windowMedian + windowStdDev:
		if math.isnan(responseTimeCurve[i]) or responseTimeCurve[i] < windowMedian + max(10.0, windowMedian*0.2):
			#print responseTimeCurve[i], "continuing end"
			i += 1
			continue
		else:
			return i-1
	
	return -1

def getHistogramSmart(results, responseTimeCurve, timestamps, epochBegin, epochEnd, nodes=None, load=None):
	#timestamps = responseTime.availableTimestamps()
	#responseTimeCurve = getResponseTimeCurve(responseTime)
	
	beginDt = find_ge(timestamps, epochBegin)		
	endDt = find_le(timestamps, epochEnd)
	beginI = index(timestamps, beginDt)
	endI = index(timestamps, endDt)
	
	print "beginI", beginI
	print "endI", endI
	
	window = responseTimeCurve[beginI:endI]
	windowMedian = np.median(window)
	windowStdDev = 0.0 #np.std(window)
	
	periods = {}
	
	print "windowLen:", len(window)
	print "windowMedian:", windowMedian
	print "windowStdDev:", windowStdDev
	
	print "beginDt", beginDt
	print "endDt", endDt
	print "beginIDt", timestamps[beginI]
	print "endIDt", timestamps[endI]
	
	i = beginI
	while i < endI and i > 0:
		start = findGoodStart(timestamps, responseTimeCurve, i, endI, windowMedian, windowStdDev)
		i = start+1
		end = findGoodEnd(timestamps, responseTimeCurve, i, endI, windowMedian, windowStdDev)
		i = end+1
		
		print "found start %d(%s), end %d(%s)" % (start, str(timestamps[start]), end, str(timestamps[end]))
		if start == -1:
			break
		if end == -1:
			end = endI
	
		periods[end-start] = (timestamps[start], timestamps[end])	
	
	for key in sorted(periods.keys()):
		print "Candidate Period:", key, periods[key]
	
	selectedPeriod = periods[max(periods.keys())]
	
	print "Selected Period:", selectedPeriod
	
	beginDt, endDt = selectedPeriod
	
	beginHistogram = results.getCommonValue(beginDt, args.histogram)
	endHistogram = results.getCommonValue(endDt, args.histogram)

	#beginHistogram = responseTime.getHistogramAt(beginDt)
	#endHistogram = responseTime.getHistogramAt(endDt)
	
	diffHistogram = endHistogram - beginHistogram
	
	
	# output epochs
	if nodes != None and load != None:
		selectedPeriods.append((nodes, load, beginDt, endDt))
			 
	return diffHistogram

def getPowerValue(results, epochBegin, epochEnd):
	timestamps = results.availableTimestamps()
	beginDt = find_ge(timestamps, epochBegin)		
	endDt = find_le(timestamps, epochEnd)
	beginI = index(timestamps, beginDt)
	endI = index(timestamps, endDt)
	
	powerTotal = 0.0
	powerCount = 0
	
	timestamps = results.availableTimestamps()
	for i in xrange(beginI, endI):
		powerValue = results.getCommonValue(timestamps[i], "node_agg_power")
		
		if math.isnan(powerValue):
			continue
		
		powerTotal += powerValue
		powerCount += 1
	
	return powerTotal / powerCount

def getLoadValue(results, epochBegin, epochEnd):
	timestamps = results.availableTimestamps()
	beginDt = find_ge(timestamps, epochBegin)		
	endDt = find_le(timestamps, epochEnd)
	beginI = index(timestamps, beginDt)
	endI = index(timestamps, endDt)
	
	loadTotal = 0.0
	loadCount = 0
	
	timestamps = results.availableTimestamps()
	for i in xrange(beginI, endI):
		loadValue = results.getCommonValue(timestamps[i], "node_agg_load")
		
		if math.isnan(loadValue):
			continue
		
		loadTotal += loadValue
		loadCount += 1
	
	return loadTotal / loadCount

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiments", action="store", nargs="+", required=True)
	parser.add_argument("--model", action="store", type=str, required=True)
	parser.add_argument("--percentile", action="store", type=int, default=99)
	parser.add_argument("--simple", action="store_true")
	parser.add_argument("--smart", action="store_true")
	parser.add_argument("--power", action="store_true")
	parser.add_argument("--load", action="store_true")
	parser.add_argument("--histogram", default="read_histogram")
	
	args = parser.parse_args()

	simpleHistograms = {}
	smartHistograms = {}
	powerValues = {}
	loadValues = {}
	
	selectedPeriods = []
	
	# for each experiment file
	for experiment in args.experiments:
		print "Processing", experiment
		if args.simple or args.smart:
			# load the response time data
			'''
			try:
				responseTime = GCResponseTime(experiment, "histogram.read.crypt11")
			except:
				print "No histogram with old format, try new"
				responseTime = GCResponseTime(experiment, "histogram_read_crypt11")
				print "Loaded Response Time Data"
			'''
			
			results = GCResultsSqliteDisk(experiment)
			
			#responseTimeCurve = getResponseTimeCurve(responseTime)
			#responseTimeTimestamps = responseTime.availableTimestamps()
			
			responseTimeCurve = getResponseTimeCurve(results)
			responseTimeTimestamps = results.availableTimestamps()
			
			#print responseTimeCurve
			print "Generated response time curve"		

		#if args.power or args.load:
		#	# get the results
		#	results = GCResultsLegacy(experiment, required=["node_agg_power", "node_agg_load"], debug=False)
		#	print "Loaded GCResults"
		
		# get the epochs
		try:
			#epochs = GCEpochs(experiment)	
			epochs = results.getEpochs()
			print "Got Epochs"
		except Exception as e:
			print "Exception", e
			sys.exit()
			continue

		for epoch in epochs.getEpochs():
			nodes, load = epoch.split("-")

			nodes = int(nodes)
			load = int(load)

			key = (nodes, load)

			# init the dictionaries

			try:
				tmp = smartHistograms[key]
			except KeyError:
				smartHistograms[key] = []
			
			epochBegin = epochs.getEpochBegin(epoch) #+ timedelta(minutes=5)
			epochEnd = epochs.getEpochEnd(epoch)
			
			print "Point:", key
			print "epochBegin", epochBegin
			print "epochEnd:", epochEnd

			if args.simple:
				if not key in simpleHistograms:
					simpleHistograms[key] = []
					
				simpleHistogram = getHistogram(results, epochBegin, epochEnd)
				simpleHistograms[key].append(simpleHistogram)
				print "simpleHistogram(%d, %d): %0.2f ms" % (nodes, load, simpleHistogram.getPercentile(args.percentile))
			
			if args.smart:
				if not key in smartHistograms:
					smartHistograms[key] = []
					
				smartHistogram = getHistogramSmart(results, responseTimeCurve, responseTimeTimestamps, epochBegin, epochEnd, nodes=nodes, load=load)
				smartHistograms[key].append(smartHistogram)
				print "smartHistogram(%d, %d): %0.2f ms" % (nodes, load, smartHistogram.getPercentile(args.percentile))
				
			if args.power:
				if not key in powerValues:
					powerValues[key] = []
					
				powerValue = getPowerValue(results, epochBegin, epochEnd)
				powerValues[key].append(powerValue)
				print "powerValue(%d, %d): %0.2f ms" % (nodes, load, powerValue)
			
			if args.load:
				if not key in loadValues:
					loadValues[key] = []
					
				loadValue = getLoadValue(results, epochBegin, epochEnd)
				loadValues[key].append(loadValue)
				print "loadValue(%d, %d): %0.2f ms" % (nodes, load, loadValue)
	
	if args.simple:
		# output model file
		with open("%s-simple.model" % args.model, "w") as outfile:
			for key in sorted(simpleHistograms.keys()):
				nodes, load = key
				pointList = simpleHistograms[key]
				outString = "%d\t%d" % (nodes, load)
						
				for point in pointList:
					responseTime = point.getPercentile(args.percentile)
					outString += "\t%0.2f" % responseTime
				
				print outString
				outfile.write("%s\n" % outString)
		
		# output CDFs
		for key in sorted(simpleHistograms.keys()):
			nodes, load = key
			pointList = simpleHistograms[key]
			
			for point in pointList:
				outFilename = "%s-simple-%d-%d.%d.cdf" % (args.model, nodes, load, pointList.index(point))
				outfile = open(outFilename, "w")
				
				x, y = point.getCDF()
				for i in xrange(0, len(x)):
					outString = "%f %f" % (x[i], y[i])
					outfile.write("%s\n" % outString)
				outfile.close()
	
	if args.smart:
		# output model file
		with open("%s-smart.model" % args.model, "w") as outfile:
			for key in sorted(smartHistograms.keys()):
				nodes, load = key
				pointList = smartHistograms[key]
				outString = "%d\t%d" % (nodes, load)
						
				for point in pointList:
					responseTime = point.getPercentile(args.percentile)
					outString += "\t%0.2f" % responseTime
				
				print outString
				outfile.write("%s\n" % outString)
		
		# output CDFs
		for key in sorted(smartHistograms.keys()):
			nodes, load = key
			pointList = smartHistograms[key]
			
			for point in pointList:
				outFilename = "%s-smart-%d-%d.%d.cdf" % (args.model, nodes, load, pointList.index(point))
				outfile = open(outFilename, "w")
				
				x, y = point.getCDF()
				for i in xrange(0, len(x)):
					outString = "%f %f" % (x[i], y[i])
					outfile.write("%s\n" % outString)
				outfile.close()
		
		# output custom epochs
		with open("%s-smart.epochs" % args.model, "w") as outfile:
			for period in sorted(selectedPeriods):
				nodes, load, beginDt, endDt = period
				outString = "%d-%d,%s,%s" % (nodes, load, datetime.strftime(beginDt, "%Y-%m-%d %H:%M:%S.%f"), datetime.strftime(endDt, "%Y-%m-%d %H:%M:%S.%f"))
				outfile.write("%s\n" % outString)
	
	if args.power:
		# output model file
		with open("%s.power" % args.model, "w") as outfile:
			for key in sorted(powerValues.keys()):
				nodes, load = key
				pointList = powerValues[key]
				outString = "%d\t%d" % (nodes, load)
						
				for point in pointList:
					powerValue = point
					outString += "\t%0.2f" % powerValue
				
				print outString
				outfile.write("%s\n" % outString)
	
	
	if args.load:
		# output model file
		with open("%s.load" % args.model, "w") as outfile:
			for key in sorted(loadValues.keys()):
				nodes, load = key
				pointList = powerValues[key]
				outString = "%d\t%d" % (nodes, load)
						
				for point in pointList:
					loadValue = point
					outString += "\t%0.2f" % loadValue
				
				print outString
				outfile.write("%s\n" % outString)
	