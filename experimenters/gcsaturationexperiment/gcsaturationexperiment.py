#!/usr/bin/python

import argparse
import signal

from gccontroller import *
from gcycsb import *
from gchelpers import *
from gcjolokiamanager import *
from gcycsbmanager import *

#YCSB_HOSTNAME="crypt11"
#YCSB_HOSTNAMES=["sol.cs", "crypt11", "crypt09"]
YCSB_HOSTNAMES=["crypt11"]

class GCSaturationExperiment(object):
	
	def __init__(self, experiment_name, operation, total_nodes, consistency, start, stop, step, interval, skip, preheat, tail):	
		self.experiment_name = experiment_name
		self.operation = operation
		self.start_rate = start
		self.stop_rate = stop
		self.step = step
		self.interval = interval
		self.preheat = preheat
		self.tail = tail
		
		self.clock = ExperimentClock(datetime.now(), 1)
		self.main_logger = GCLogger("%s.log" % self.experiment_name, GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger, mode=GCClusterManager.CASSANDRA_SLEEP)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManager()
		self.ycsb_manager = GCYCSBManager(YCSB_HOSTNAMES)
		
		# set up the data collector
		self.data_collector = GCDataCollector(self.clock, self.experiment_name, results_dir=os.getcwd(), interval=5)
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)
		self.data_collector.register(self.ycsb_manager)

		if skip:
			off_skip = 2
		else:
			off_skip = 1
		
		self.nodes_off = calcOffNodes(total_nodes, off_skip)
		self.ycsb_nodes = []
		for node in (MINIMUM_NODES + OPTIONAL_NODES):
			if node not in self.nodes_off:
				self.ycsb_nodes.append(node)
		
		# preheat workload
		#if operation == "READ":
		#	ycsb_workload_preheat = YCSBGreenCassandraReadWorkload(self.ycsb_nodes, consistency_level=consistency)
		#elif operation == "WRITE":
		#	ycsb_workload_preheat = YCSBGreenCassandraUpdateWorkload(self.ycsb_nodes, consistency_level=consistency)
		#elif operation == "MIXED":
		ycsb_workload_preheat = YCSBGreenCassandraMixedWorkload(self.ycsb_nodes, read=1.00, write=0.00, consistency_level=consistency)
		
		ycsb_workload_preheat.threadcount = 512
		ycsb_workload_preheat.maxexecutiontime =  5 * 60
		ycsb_workload_preheat.operationcount = 1000000000
		ycsb_workload_preheat.targetthroughput = self.preheat / len(YCSB_HOSTNAMES)
		#ycsb_workload_preheat.requestdistribution = "zipfian"
		ycsb_workload_preheat.requestdistribution = "uniform"
		
		if operation == "READ":
			ycsb_workload = YCSBGreenCassandraReadWorkload(self.ycsb_nodes, consistency_level=consistency)
		elif operation == "WRITE":
			ycsb_workload = YCSBGreenCassandraUpdateWorkload(self.ycsb_nodes, consistency_level=consistency)
		elif operation == "MIXED":
			ycsb_workload = YCSBGreenCassandraMixedWorkload(self.ycsb_nodes, read=0.75, write=0.25, consistency_level=consistency)
		
		ycsb_workload.threadcount = 512
		ycsb_workload.maxexecutiontime = 0
		ycsb_workload.targetthroughput = -1
		#ycsb_workload.requestdistribution = "zipfian"
		ycsb_workload.requestdistribution = "uniform"
		ycsb_workload.histogram_window = 300
		
		ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.latency_output"
		ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.throughput_output"
		self.ycsb_run = YCSBMultipleRun(ycsb_workload, YCSB_HOSTNAMES, self.main_logger, base_latency_file=ycsb_latency_output_path, base_throughput_file=ycsb_throughput_output_path, client="cassandra-hector")
	
		ycsb_latency_output_path_preheat = self.data_collector.getExperimentPath() + "/ycsb.latency_output_preheat"
		ycsb_throughput_output_path_preheat = self.data_collector.getExperimentPath() + "/ycsb.throughput_output_preheat"
		self.ycsb_run_preheat = YCSBMultipleRun(ycsb_workload_preheat, YCSB_HOSTNAMES, self.main_logger, base_latency_file=ycsb_latency_output_path_preheat, base_throughput_file=ycsb_throughput_output_path_preheat, client="cassandra-hector")
	
	def stop(self, s1, s2):
		print "Stopping GCSaturationExperiment..."
		self.ycsb_run.stop()
		self.data_collector.stop()
		sys.exit()
		
	def run(self):
		signal.signal(signal.SIGINT, self.stop)
			
		if len(self.nodes_off) > 0:
			print "turning node(s) off"
			
			for node in self.nodes_off:
				print "...%s" % node
				self.cluster_manager.nodeOff(node)
			print "...done"
			print "sleeping"
			time.sleep(10)
			print "...done"

		if self.preheat > 0:
			print "Running YCSB Preheat"
			self.ycsb_run_preheat.run()

		time.sleep(60)
		
		print "starting data collector"
		self.data_collector.start()
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"
		
		print "starting ycsb"
		self.ycsb_run.start()
		time.sleep(10)
		
		print "doing mini warmup"
		self.ycsb_manager.setTarget(self.start_rate)
		wait_with_status(90, status_interval=1)
		self.ycsb_manager.resetMeasurements()
		wait_with_status(10, status_interval=1)
		
		print "starting experiment"
		current_target = self.start_rate
		while current_target <= self.stop_rate:
			print "at target=%d ops/sec" % current_target
			self.ycsb_manager.setTarget(current_target)
			self.data_collector.epochBegin("%d" % current_target)
			#self.ycsb_manager.resetMeasurements()
			wait_with_status(self.interval, status_interval=1)
			self.data_collector.epochEnd()
			current_target += self.step
		
		print "doing tail"
		wait_with_status(self.tail, status_interval=1)
		
		self.ycsb_run.stop()
		
		print "...done"
		
		print "Sleeping after workload"
		time.sleep(10)
		print "...done"
		
		print "stopping data collector"
		self.data_collector.stop()
		print "...done"
		
		'''
		if len(self.nodes_off) > 0:	
			self.logger.log("Starting repair")
			print "turning node(s) on"
			#self.data_collector.epochBegin("NodeOn")
			for node in self.nodes_off:
				print "...%s" % node
				self.cluster_manager.nodeOn(node)
			print "...done"
				
			print "waiting for node resolution"
			self.cluster_manager.waitForTransitions()
			#self.data_collector.epochEnd()
			print "...done"
			self.logger.log("Completed repair")
		
			print "Sleeping after recovery"
			time.sleep(30)
			print "...done"
		'''
		print "shutting down cluster manager"
		self.cluster_manager.shutdown()
		print "..done"
		
#		if self.operation == "WRITE":
#			print "resetting cluster"
#			self.cluster_manager.resetCluster()
#			print "...done"

		print "ending"

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--nodes", action="store", type=int, required=True)
	parser.add_argument("--operation", action="store", type=str, choices=["READ", "WRITE", "MIXED"], required=True)
	parser.add_argument("--consistency", action="store", type=str, choices=["ONE", "QUORUM", "ALL"], required=True)
	parser.add_argument("--start", action="store", type=int, required=True)
	parser.add_argument("--stop", action="store", type=int, required=True)
	parser.add_argument("--step", action="store", type=int, required=True)
	parser.add_argument("--interval", action="store", type=int, required=True)
	parser.add_argument("--preheat", action="store", type=int, default=-1, required=False)
	parser.add_argument("--tail", action="store", type=int, required=True)
	parser.add_argument("--skip", action="store_true", default=False)
	
	args = parser.parse_args()
	
	print "Running experiment %s" % args.experiment
		
	exp = GCSaturationExperiment(args.experiment, args.operation, args.nodes, args.consistency, args.start, args.stop, args.step, args.interval, args.skip, args.preheat, args.tail)
	exp.run()
