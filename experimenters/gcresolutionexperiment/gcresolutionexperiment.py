#!/usr/bin/python

import argparse
from gccontroller import *
from gcycsb import *
from gchelpers import *
from gcjolokiamanager import *
from gcycsbmanager import *

YCSB_HOSTNAME="crypt11"
class GCResolutionExperiment(object):
	
	def __init__(self, experiment_name, length, nodes_off, nodes_recover, target, stop, simultaneous, early_on):
		self.nodes_off = nodes_off
		self.nodes_recover = nodes_recover
		self.stop = stop
		
		self.nodes_remaining = []
		for node in (MINIMUM_NODES + OPTIONAL_NODES):
			if node not in self.nodes_off:
				self.nodes_remaining.append(node)
		
		self.clock = ExperimentClock()
		self.main_logger = GCLogger("%s.log" % experiment_name, GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger, mode=GCClusterManager.CASSANDRA_SLEEP, simultaneous_on=simultaneous, early_on=early_on)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManager()
		self.ycsb_manager = GCYCSBManager([YCSB_HOSTNAME])
		self.length_minutes = length
		
		# start the data collector
		self.data_collector = GCDataCollector(self.clock, experiment_name, results_dir=os.getcwd())
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)
		self.data_collector.register(self.ycsb_manager)
		
		self.ycsb_workload = YCSBGreenCassandraMixedWorkload(self.nodes_remaining, read=0.75, write=0.25)
		#self.ycsb_workload = YCSBGreenCassandraMixedWorkload(self.nodes_remaining, read=0.0, write=1.00)
		self.ycsb_workload.threadcount = 512
		self.ycsb_workload.targetthroughput = target
		
		ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.out.latency"
		ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.out.throughput"
		self.ycsb_run = YCSBRun(self.ycsb_workload, YCSB_HOSTNAME, self.main_logger, ycsb_latency_output_path, throughput_file=ycsb_throughput_output_path, client="cassandra-hector")
		
	def run(self):
		print "starting data collector"
		self.data_collector.start()
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"
		
		print "turning node(s) off"
		
		for node in self.nodes_off:
			print "...%s" % node
			self.cluster_manager.nodeOff(node)
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"
		
		self.data_collector.epochBegin("YCSB")
		print "starting ycsb"
		#self.ycsb_run.run()
		self.ycsb_run.start()
		print "...done"
		
		#print "sleeping for one minute"
		#time.sleep(60)
		#print "...done"
		
		print "running YCSB for %d minutes" % self.length_minutes
		wait_with_status(self.length_minutes * 60, status_interval=5)
		self.data_collector.epochEnd()
		
		if self.stop:
			print "stopping YCSB"
			self.ycsb_run.stop()
			self.ycsb_run.wait()
		
		self.logger.log("Starting repair")
		print "turning node(s) on"
		self.data_collector.epochBegin("NodeOn")
		for node in self.nodes_recover:
			print "...%s" % node
			self.cluster_manager.nodeOn(node)
		print "...done"
		
		print "waiting for node resolution"
		self.cluster_manager.waitForTransitions()
		self.data_collector.epochEnd()
		print "...done"
		self.logger.log("Completed repair")
		
		if not self.stop:
			print "Letting YCSB recover"
			wait_with_status(30, status_interval=5)
			print "...done"
			
			print "stopping YCSB"
			self.ycsb_run.stop()
			self.ycsb_run.wait()
		
		print "turning on remaining nodes"
		for node in self.nodes_off:
			print "...%s" % node
			self.cluster_manager.nodeOn(node)

		print "waiting for remaining nodes to recover"
		self.cluster_manager.waitForTransitions()
		
		print "stopping data collector"
		self.data_collector.stop()
		#self.cluster_manager.nodeOn("node")
		print "...done"
		
		print "ending"

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--length", action="store", type=int, required=True)
	parser.add_argument("--nodesoff", action="store", type=int, required=True)
	parser.add_argument("--nodesrecover", action="store", type=int, required=True)
	parser.add_argument("--target", action="store", type=int, required=True)
	parser.add_argument("--stop", action="store_true", default=False)
	parser.add_argument("--simultaneous", action="store", type=int, default=1)
	parser.add_argument("--earlyon", action="store_true", default=False)
	
	args = parser.parse_args()
	
	if args.nodesoff > len(OPTIONAL_NODES):
		print "You may only turn off %d nodes" % len(OPTIONAL_NODES)
		sys.exit()
	
	if args.nodesrecover > args.nodesoff:
		print "You may only recover as many nodes as you turn off"
		sys.exit()
	
	remaining = args.nodesoff
	nodes_off = []
	nodes_recover = []
	i = 0
	
	while remaining > 0:
		nodes_off.append(OPTIONAL_NODES[i])
		remaining -= 1
		i += 2
		
		if (i > len(OPTIONAL_NODES) - 1):
			i = 1
		
	for i in xrange(0, args.nodesrecover):
		nodes_recover.append(nodes_off[i])
		
	print nodes_off
	print nodes_recover
		
	print "Running experiment %s for %d minute(s)..." % (args.experiment, args.length)

	#single_node = ["sol043"]
	#ten_nodes = ["sol043", "sol045", "sol047", "sol049", "sol051", "sol053", "sol055", "sol058", "sol060", "sol062"]
	
	exp = GCResolutionExperiment(args.experiment, args.length, nodes_off, nodes_recover, args.target, args.stop, args.simultaneous, args.earlyon)
	exp.run()

	
