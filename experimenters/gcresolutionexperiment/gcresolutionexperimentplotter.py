#!/usr/bin/python

import argparse
import sys
from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
import re
import datetime


class ResolutionResults(TimeseriesResults):
	def __init__(self, experiment_list, times_list):
		super(ResolutionResults, self).__init__()
		
		for i in xrange(0, len(experiment_list)):
			experiment = experiment_list[i]
			f = open("%s.log" % experiment, "r")
			data = f.read().strip()
			lines = data.split("\n")
			
			start_regex = "Starting greenhint"
			start_re = re.compile(start_regex, re.I)
			end_regex = "Completed greenhint"
			end_re = re.compile(end_regex, re.I)
			for line in lines:
				print line
				if start_re.search(line):
					print line
					fields = line.split(" ")				
					timestamp = fields[0] + " " + fields[1]
					start_dt = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
				elif end_re.search(line):
					print line
					fields = line.split(" ")				
					timestamp = fields[0] + " " + fields[1]
					end_dt = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
					
			print start_dt, end_dt
			print str(end_dt - start_dt)
					
			
			#dt = datetime.datetime.strptime(datetimestamp, "%Y-%m-%d %H:%M:%S.%f")
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiments", action="store", nargs="+", required=True)
	args = parser.parse_args()




	'''
	plotter = GCPlotter(xsize=20, ysize=10)
	cdf = CDFPlot(title="YCSB Read Latencies")
	group = GCPlotGroup(all_xticks=True)
	group.addPlot(cdf)
	plotter.addPlotGroup(group)
	
	throughput = SimplePlot(title="Throughput")
	plotter.addPlot(throughput)
	
	load = GCSimpleCommonPlotMultiple(title="Load")
	plotter.addPlot(load)
	'''
	#for experiment in args.experiments:
		#gcresults = GCResults(experiment, required=["cassandra_cluster_load"])		
		#results = YCSBLatencyResults("%s/ycsb.latency" % experiment, transaction_type=YCSBLatencyResults.READ, max_bin=250)
		#cdf.addResults(results, label=experiment)
		#results = YCSBThroughputResults("%s/ycsb.throughput" % experiment)
		#throughput.addResults(results, label=experiment)
		#load.addResults(gcresults, "cassandra_cluster_load", label=experiment, ylim=4.0)
	
	res = ResolutionResults(args.experiments, [])
	
	#plotter.doPlot()