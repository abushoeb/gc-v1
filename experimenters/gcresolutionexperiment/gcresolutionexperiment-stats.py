#!/usr/bin/python

import argparse
from datetime import datetime
import re
from gcresults import *

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	args = parser.parse_args()
	
	# find all experiment files
	experiments = []
	files = os.listdir(".")
	for f in sorted(files):
		if f.startswith(args.experiment) and not "." in f:
			experiments.append(f)
	experiments = sorted(experiments, key=lambda(v):"-".join(reversed(v.split("-"))))
	
	print "Experiment\t\t\t\tMissed Writes\t\tWrite Time\t\tResolve Time\tPercent\t\tLatency (95th)\tLatency (99th)\tLatency (avg)"
	
	for i in xrange(0, len(experiments)):
			experiment = experiments[i]
			
			try:
				results = GCResults(experiment, required=["readlatency_95_cum", "readlatency_99_cum", "readlatency_avg_cum", "ycsb.throughput"], debug=False)
			except Exception as e:
				print e
			#	print experiment
			
			f = open("%s/epochs" % experiment, "r")
			epochs = f.read().strip().split("\n")
			
			for epoch in epochs:
				name, start, stop = epoch.split(",")
				start_dt = datetime.strptime(start, "%Y-%m-%d %H:%M:%S.%f")
				stop_dt = datetime.strptime(stop, "%Y-%m-%d %H:%M:%S.%f")
				if name == "YCSB":	
					write_time = stop_dt - start_dt
					ycsb_start = start_dt
					ycsb_stop = stop_dt
				elif name == "NodeOn":
					resolve_time = stop_dt - start_dt
					#resolve_time -= datetime.timedelta(seconds=45)
					end_time = stop_dt
			
			frac = resolve_time.total_seconds() / write_time.total_seconds()
			
			timestamps = results.availableTimestamps()
			
			readlatency_95 = 0.0
			readlatency_99 = 0.0
			readlatency_avg = 0.0
			missed_writes = 0
			
			for ts in reversed(timestamps):
				if ts <= end_time:
					readlatency_95 = results.getCommonValue(ts, "readlatency_95_cum")
					readlatency_99 = results.getCommonValue(ts, "readlatency_99_cum")
					readlatency_avg = results.getCommonValue(ts, "readlatency_avg_cum")
					break
					
			for i in xrange(1, len(timestamps)):
			   if timestamps[i] >= ycsb_start and timestamps[i] <= ycsb_stop:
				   delta = timestamps[i] - timestamps[i-1]
				   seconds = delta.total_seconds()
				   measurement = results.getCommonValue(timestamps[i], "ycsb.throughput")
				   if not math.isnan(measurement):
					missed_writes += measurement * seconds * 0.25
			
			print "%s\t\t%f\t\t%s\t\t%s\t%02f\t%02f\t%02f\t%02f" % (experiment, missed_writes, write_time, resolve_time, frac, readlatency_95, readlatency_99, readlatency_avg)


