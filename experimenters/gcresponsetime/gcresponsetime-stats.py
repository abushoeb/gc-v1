#!/usr/bin/python
import os
import time
import argparse
import re
import math

from gcycsbplot import *

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	args = parser.parse_args()
	
	files = os.listdir(args.experiment)


	# throughput
	regex = re.compile("ycsb.+.throughput", re.I)
	matching = list()
	throughput_output = dict()
	
	for f in files:
		if regex.search(f):
			matching.append(f)
	
	for datapoint_file in matching:
		crap, target, crap2 = datapoint_file.split(".")

		target = int(target)

		throughput_results = YCSBThroughputResults("%s/%s" % (args.experiment, datapoint_file))
		throughput_values = throughput_results.getValues()
				
		accum = 0.0
		for result in throughput_values:
			accum += result
		mean = accum / len(throughput_values)
		throughput_output[target] = mean
		
	#latency
	regex = re.compile("ycsb.+.latency", re.I)
	matching = list()
	latency_output = dict()
	
	for f in files:
		if regex.search(f):
			matching.append(f)

	
	percentiles = [0.50, 0.75, 0.90, 0.95, 0.99]
	for datapoint_file in matching:
		crap, target, crap2 = datapoint_file.split(".")
		target = int(target)

		latency_results = YCSBLatencyResults("%s/%s" % (args.experiment, datapoint_file), transaction_type=YCSBLatencyResults.UPDATE)
		latency_output[target] = dict()
		
		for percentile in percentiles:
			latency_output[target][percentile] = latency_results.getPercentileLatency(percentile)
	
	header_str = "Target\t\tThroughput"
	
	for percentile in percentiles:
		header_str += "\t%dth" % int(percentile * 100)
	
	print header_str
	
	for key in sorted(throughput_output.keys()):
		data_str = "%5d\t\t%8.2f" % (key, throughput_output[key])
		for percentile in percentiles:
			data_str += "\t%d ms" % latency_output[key][percentile]
		
		print data_str
	