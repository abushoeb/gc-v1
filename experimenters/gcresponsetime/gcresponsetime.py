#!/usr/bin/python

import argparse
import signal
import sys

from gccontroller import *
from gcycsb import *
from gchelpers import *
from gcjolokiamanager import *
from gcycsbmanager import *
from gcscheduler import *

YCSB_HOSTNAMES=["crypt11"]

class GCResponseTimeExperiment(object):
	
	def __init__(self, experiment, operation, consistency, target, response, length, warmup, nodes):
		
		self.experiment = experiment
		self.operation = operation
		self.consistency = consistency
		self.target = target
		self.response = response
		self.length = length
		self.warmup = warmup
		
		self.allNodes = MINIMUM_NODES + OPTIONAL_NODES
		self.onNodes = self.allNodes
		self.offNodes = []
		self.offTimes = {}
		
		self.clock = ExperimentClock(datetime.now(), 1)
		self.main_logger = GCLogger("%s.log" % self.experiment, GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger, mode=GCClusterManager.CASSANDRA_SLEEP)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManager()
		self.ycsb_manager = GCYCSBManager(YCSB_HOSTNAMES, self.main_logger)
			
		modelFile = "../../12-2-UNIFORM-300kbs-MIXED-ONE.model"
		self.response_time_model = GCTableResponseTimeModel(modelFile)
		self.response_time_model_new = GCTableResponseTimeModelNew(modelFile)
		self.power_model = GCParasolPowerModel()
		#self.max_throughput = self.response_time_model_new.peakThroughput(NUM_NODES, self.response)

		self.scheduler = GCScheduler(self.clock, self.main_logger, None, None, self.cluster_manager, self.response_time_model, self.power_model)

		# set up the data collector
		self.data_collector = GCDataCollector(self.clock, self.experiment, results_dir=os.getcwd(), interval=5)
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)
		self.data_collector.register(self.ycsb_manager)
		
		# select nodes to turn off
		if nodes == None:
			self.numNodesOff = NUM_NODES - self.response_time_model.nodesRequired(self.target, self.response)
		else:
			self.numNodesOff = NUM_NODES - nodes
			
		self.onNodes = list(MINIMUM_NODES + OPTIONAL_NODES)
		self.offNodes = []
		self.offTimes = {}
		
		if self.numNodesOff > 0:
			selected = self.scheduler.selectForTurnOff(self.onNodes, self.numNodesOff)
			for node in selected:
				self.onNodes.remove(node)
				self.offNodes.append(node)

		if operation == "READ":
			ycsb_workload = YCSBGreenCassandraReadWorkload(self.onNodes, consistency_level=consistency)
		elif operation == "WRITE":
			ycsb_workload = YCSBGreenCassandraUpdateWorkload(self.onNodes, consistency_level=consistency)
		elif operation == "MIXED":
			ycsb_workload = YCSBGreenCassandraMixedWorkload(self.onNodes, read=0.75, write=0.25, consistency_level=consistency)
		
		ycsb_workload.threadcount = 512
		ycsb_workload.maxexecutiontime =  60 * (self.length + self.warmup)
		ycsb_workload.targetthroughput = self.target
		ycsb_workload.requestdistribution = "uniform"
		
		ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.latency_output"
		ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.throughput_output"
		self.ycsb_run = YCSBMultipleRun(ycsb_workload, YCSB_HOSTNAMES, self.main_logger, base_latency_file=ycsb_latency_output_path, base_throughput_file=ycsb_throughput_output_path, client="cassandra-hector")

	def stop(self, s1, s2):
		print "Stopping GCResponseTimeExperiment..."
		self.ycsb_run.stop()
		self.data_collector.stop()
		sys.exit()
		
	def run(self):
		signal.signal(signal.SIGINT, self.stop)
		
		self.logger.log("Started GCResponseTimeExperiment, parameters:")
		self.logger.log("Experiment Name: %s" % self.experiment)
		self.logger.log("Operation: %s" % self.operation)
		self.logger.log("Consistency: %s" % self.consistency)
		self.logger.log("Target: %s" % self.target)
		self.logger.log("Response Time: %s" % self.response)
		self.logger.log("Length: %s" % self.length)
		self.logger.log("Warmup: %s" % self.warmup)

		print "starting data collector"
		self.data_collector.start()
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"
		
		print "Turning off initial set of nodes"
		if self.numNodesOff > 0:
			for node in self.offNodes:
				print "...turning off %s" % node
				self.cluster_manager.nodeOff(node)
				self.offTimes[node] = self.clock.get_current_time()
				time.sleep(1)
		
		print "sleeping"
		time.sleep(10)
		print "...done"

		print "starting ycsb"
		self.data_collector.epochBegin("YCSB")
		self.ycsb_run.start()
		
		print "mini warmup"
		wait_with_status(self.warmup*60, status_interval=1)
		self.ycsb_manager.resetMeasurements()
		
		print "real workload"
		self.ycsb_run.wait()
		self.data_collector.epochEnd()

		print "Sleeping after workload"
		time.sleep(10)
		print "...done"
		
		print "stopping data collector"
		self.data_collector.stop()
		print "...done"
		
		if len(self.offNodes) > 0:	
			self.logger.log("Starting repair")
			print "turning node(s) on"
			self.data_collector.epochBegin("NodeOn")
			for node in self.offNodes:
				print "...%s" % node
				self.cluster_manager.nodeOn(node)
			print "...done"
				
			print "waiting for node resolution"
			self.cluster_manager.waitForTransitions()
			self.data_collector.epochEnd()
			print "...done"
			self.logger.log("Completed repair")
		
			print "Sleeping after recovery"
			time.sleep(30)
			print "...done"

		print "ending"

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--operation", action="store", type=str, choices=["READ", "WRITE", "MIXED"], default="MIXED")
	parser.add_argument("--consistency", action="store", type=str, choices=["ONE", "QUORUM", "ALL"], default="ONE")
	parser.add_argument("--target", action="store", type=int, required=True)
	parser.add_argument("--response", action="store", type=int, default=None)
	parser.add_argument("--nodes", action="store", type=int, default=None)
	parser.add_argument("--length", action="store", type=int, required=True)
	parser.add_argument("--warmup", action="store", type=int, default=5)
	
	args = parser.parse_args()
	
	exp = GCResponseTimeExperiment(args.experiment, args.operation, args.consistency, args.target, args.response, args.length, args.warmup, args.nodes)
	exp.run()
