#!/usr/bin/python

import argparse
import sys
from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *

class GCSimpleMultiPlot(GCPlot):
	def __init__(self, results=None, title="Simple Plot", ylim=None):
		if self.results != None:
			self.addResults(results)
		self.title = title
		self.ylim = ylim
		
	def addResults(self, results):
		pass
		
	def doPlot(self):
		pass

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--pdf", action="store_true", default=False)
	args = parser.parse_args()
	
	plotter = GCPlotter(xsize=20, ysize=10)

	experiment = args.experiment
	gcresults = GCResults(experiment, required=["ycsb.throughput", "ycsb.target", "cassandra_cluster_load", "cluster_cpu_total", "readlatency_95", "writelatency_95", "readlatency_avg", "writelatency_avg", "readlatency_95_cum", "writelatency_95_cum", "readlatency_95_window", "writelatency_95_window", "nodes_total", "cassandra_read_rate", "cassandra_read_latency", "cassandra_total_compactions", "jvm_parnew_time", "jvm_parnew_count", "jvm_cms_time", "jvm_cms_count", "avg_readlatency_95", "avg_readlatency_avg"])
	
	plot = GCMultiCommonPlot(gcresults, ["ycsb.throughput", "ycsb.target"], title="Throughput vs. Target: %s" % experiment, ylim=(0, None))
	#plot = GCMultiCommonPlotOverlay()
	#plot.addResults(gcresults, "ycsb.throughput", label="throughput")
	#plot.addResults(gcresults, "ycsb.latency", label="latency")
	#plot.addResults(gcresults, "cassandra_cluster_load", label="load")
	plotter.addPlot(plot)

	latency = GCMultiCommonPlot(gcresults, ["readlatency_95", "writelatency_95", "readlatency_avg", "writelatency_avg"], title="Response Time", ylim=(0,40))
	plotter.addPlot(latency)
	
	#latency = GCMultiCommonPlot(gcresults, ["avg_readlatency_95", "avg_readlatency_avg"], title="Response Time", ylim=(0,35))
	#plotter.addPlot(latency)
	
	latency = GCMultiCommonPlot(gcresults, ["readlatency_95_cum", "writelatency_95_cum"], title="Response Time (Cumulative)", ylim=(0, 300))
	plotter.addPlot(latency)
	
	latency = GCMultiCommonPlot(gcresults, ["readlatency_95_window", "writelatency_95_window"], title="Response Time (Window)", ylim=(0, 300))
	plotter.addPlot(latency)
	
	
	#nodes = GCMultiCommonPlot(gcresults, ["nodes_total"], title="Powered Nodes")
	#plotter.addPlot(nodes)
	
	plot = GCNodePlot(gcresults)
	plotter.addPlot(plot)
	
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_read_latency", (MINIMUM_NODES + OPTIONAL_NODES))
	#plotter.addPlot(plot)
			
	#node_read_rate = GCSimpleMultiNodePlot(gcresults, "cassandra_read_rate", (MINIMUM_NODES + OPTIONAL_NODES))
	#plotter.addPlot(node_read_rate)
	
	#node_gc = GCSimpleMultiNodePlot(gcresults, "jvm_parnew_time", (MINIMUM_NODES + OPTIONAL_NODES))
	#plotter.addPlot(node_gc)
	
	ALL_NODES = MINIMUM_NODES + OPTIONAL_NODES
	
	plot = GCSimpleMultiNodePlot(gcresults, "cassandra_load", MINIMUM_NODES, title="Load (min)", legend=False, ylim=(0, 16))
	plotter.addPlot(plot)
	
	plot = GCSimpleMultiNodePlot(gcresults, "cassandra_load", OPTIONAL_NODES, title="Load (opt)", legend=False, ylim=(0, 16))
	plotter.addPlot(plot)
	
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_read_latency", MINIMUM_NODES, title="read latency (min)", legend=False, ylim=(0, 150000))
	#plotter.addPlot(plot)
	
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_read_latency", OPTIONAL_NODES, title="read latency (opt)", legend=False, ylim=(0, 150000))
	#plotter.addPlot(plot)
	
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_total_compactions", MINIMUM_NODES, title="compactions (min)", legend=False, ylim=(0, 80))
	#plotter.addPlot(plot)
	
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_total_compactions", OPTIONAL_NODES, title="compactions (opt)", legend=False, ylim=(0, 80))
	#plotter.addPlot(plot)
	'''
	#plot = GCSimpleMultiNodePlot(gcresults, "cassandra_total_compactions", ALL_NODES, title="compactions (opt)", legend=False, ylim=(0, 80))
	#plotter.addPlot(plot)
	'''
	'''
	plot = GCSimpleCommonPlot(gcresults, "cassandra_cluster_load", title="Load")
	plotter.addPlot(plot)
	
	plot = GCSimpleCommonPlot(gcresults, "cluster_cpu_total", title="CPU")
	plotter.addPlot(plot)
	
	plot = GCSimpleNodePlot(gcresults, "cpu_total", "sol032", title="Sol032 cpu")
	plotter.addPlot(plot)
	'''	

	if args.pdf:
		plotter.doPlot(output="%s.pdf" % args.experiment)
	else:
		plotter.doPlot()
