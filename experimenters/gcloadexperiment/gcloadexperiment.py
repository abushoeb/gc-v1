#!/usr/bin/python

import argparse
from gccontroller import *
from gcjolokiamanager import *
from gcycsb import *

YCSB_HOSTNAME="crypt09"
class GCLoadExperiment(object):
	
	def __init__(self, experiment_name, length, target, consistency_level, nodes, table):	
		self.clock = ExperimentClock(datetime.now(), 1)
		self.main_logger = GCLogger("experiment_log", GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManager()
		
		# start the data collector
		self.data_collector = GCDataCollector(self.clock, experiment_name, nodes=nodes)
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)
		
		load_nodes = nodes #MINIMUM_NODES # + OPTIONAL_NODES
		self.ycsb_workload = YCSBGreenCassandraReadWorkload(load_nodes, table, consistency_level)
		self.ycsb_workload.threadcount = 60 #len(load_nodes)*2
		self.ycsb_workload.targetthroughput = target
		self.ycsb_workload.maxexecutiontime = length
		
		ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.latency"
		ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.throughput"
		self.ycsb_run = YCSBRun(workload=self.ycsb_workload, hostname=YCSB_HOSTNAME, latency_file=ycsb_latency_output_path, throughput_file=ycsb_throughput_output_path, )
		
	def run(self):
		print "starting data collector"
		self.data_collector.start()
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"
		
		print "starting ycsb"
		self.data_collector.epochBegin("YCSB")
		self.ycsb_run.run()
		self.data_collector.epochEnd()
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"
		
		print "stopping data collector"
		self.data_collector.stop()
		print "...done"
		
		print "ending"

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--length", action="store", type=int, required=True)
	parser.add_argument("--target", action="store", type=int, required=True)
	parser.add_argument("--consistency", action="store", type=str, choices=["ONE", "QUORUM", "ALL"], required=True)
	parser.add_argument("--nodes", action="store", type=str, default=MINIMUM_NODES+OPTIONAL_NODES, nargs="*")
	parser.add_argument("--table", action="store", type=str, default="usertable")
	
	args = parser.parse_args()
	
	print "Running experiment %s for %d minute(s) at consistency level %s on table %s..." % (args.experiment, args.length, args.consistency, args.table)
	print "Using nodes:"
	print args.nodes
	
	exp = GCLoadExperiment(args.experiment, args.length*60, args.target, args.consistency, args.nodes, args.table)
	exp.run()
