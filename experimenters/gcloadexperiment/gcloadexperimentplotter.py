#!/usr/bin/python

import argparse
import sys
from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *

class GCSimpleMultiPlot(GCPlot):
	def __init__(self, results=None, title="Simple Plot", ylim=None):
		if self.results != None:
			self.addResults(results)
		self.title = title
		self.ylim = ylim
		
	def addResults(self, results):
		pass
		
	def doPlot(self):
		pass
		

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiments", action="store", nargs="+", required=True)
	args = parser.parse_args()
	
	plotter = GCPlotter(xsize=20, ysize=10)
	cdf = CDFPlot(title="YCSB Read Latencies")
	group = GCPlotGroup(all_xticks=True)
	group.addPlot(cdf)
	plotter.addPlotGroup(group)
	
	throughput = SimplePlot(title="Throughput")
	plotter.addPlot(throughput)
	
	load = GCSimpleCommonPlotMultiple(title="Load")
	plotter.addPlot(load)
	
	for experiment in args.experiments:
		gcresults = GCResults(experiment, required=["cassandra_cluster_load"])		
		load.addResults(gcresults, "cassandra_cluster_load", label=experiment, ylim=4.0)
	
	plotter.doPlot()
