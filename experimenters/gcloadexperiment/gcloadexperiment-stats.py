#!/usr/bin/python

import argparse
import os
import sys
import re

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *

data = {}


def getYCSBFileData(path):
	throughputRe = re.compile("Throughput", re.I)
	latency95Re = re.compile("\[READ\], 95th", re.I)
	#latency99Re = re.compile("\[READ\], 99th", re.I)
		
	filename = path + "/ycsb.latency"
	f = open(filename, 'r')
	contents = f.read()
	f.close()
	lines = contents.strip().split("\n")
	
	for line in lines:
		if throughputRe.search(line):
			print line
			throughput = line.strip().split(" ")[2]
		elif latency95Re.search(line):
			print line
			latency95 = line.strip().split(" ")[2]
	#	elif latency99Re.search(line):
	#		print line
	#		latency99 = line.strip().split(" ")[2]
	
	return (float(throughput), float(latency95))

def getLoadData(path):
	results = GCResults(path, required=["cassandra_cluster_load"])
	
	timestamps = results.availableTimestamps()
	accum = 0.0
	count = 0
	
	for t in timestamps:
		accum += results.getCommonValue(t, "cassandra_cluster_load")
		count += 1
	
	return (accum/count, )
	

def initStructs(num_nodes, consistency):
	if num_nodes not in data.keys():
		data[num_nodes] = {}
	if consistency not in data[num_nodes].keys():
		data[num_nodes][consistency] = []
		
def handleDir(path):
	tuples = path.split("-")
	length = len(tuples)
	consistency = tuples[length-1]
	target = tuples[length-2]
	num_nodes = tuples[length-3]
	experiment_name = tuples[0]
	for part in [tuples[x] for x in xrange(1, length-3)]:
		experiment_name += "-" + part
	#experiment_name, num_nodes, target, consistency = path.split("-")
	#print experiment_name, num_nodes, target, consistency
	
	initStructs(num_nodes, consistency)
	data[num_nodes][consistency].append((float(target),) + getYCSBFileData(path) + getLoadData(path))

def loadPreloadedData(filename):
	f = open(filename, 'r')
	contents = f.read()
	f.close()
	lines = contents.strip().split("\n")
	
	for line in lines:
		num_nodes, consistency, target, throughput, latency95, load = line.split(" ")
		initStructs(num_nodes, consistency)
		data[num_nodes][consistency].append((float(target), float(throughput), float(latency95), float(load)))

def saveData(filename):
	f = open(filename, 'w')
	for n in sorted(data.keys()):
		for c in sorted(data[n].keys()):
			for p in sorted(data[n][c]):
				s = "%s %s " % (n, c)
				for pp in p:
					s += str(pp) + " "
				f.write(s.strip() + "\n")

def loadData(experiment_name, force=False):
	dirs = os.listdir(".")
	experiment_regex = re.compile(experiment_name, re.I)
	preheat_regex = re.compile("preheat", re.I)
	preload_file = "%s.preload" % experiment_name
	
	if force and os.path.exists(preload_file):
		os.remove(preload_file)
	
	if os.path.exists(preload_file):
		loadPreloadedData(preload_file)
	else:
		for d in dirs:
			if experiment_regex.search(d) and not preheat_regex.search(d):
				handleDir(d)
		saveData(preload_file)
	
	for n in sorted(data.keys()):
		for c in sorted(data[n].keys()):
			for p in sorted(data[n][c]):
				print n, c, p
	
	print "Data loaded..."
	
# needs a tuple of two dimensions.
class GCScatterPlot(GCPlotBase):
	
	color_list = ['r', 'g', 'b']
	
	def __init__(self, results=None, label=None, title=None):
		if title == None:
			self.title = "Scatter Plot"
		else:
			self.title = title
			
		self.results = []
		self.labels = []
		self.colors = []
		
		if results != None:
			self.addResults(results, label=label)
			
	def addResults(self, results, label=None):
		print "Adding: ", results, label
		self.results.append(results)
		if label == None:
			self.labels.append("No Label")
		else:
			self.labels.append(label)
		
		self.colors.append(self.color_list[len(self.results) % len(self.color_list)])
			
	def doPlot(self):
		if len(self.results) == 0:
			print "no results found..."
			sys.exit()
			
		for i in xrange(0, len(self.results)):
			print self.results[i]
			
			r = self.results[i]
			pyplot.plot(r[0], r[1], label=self.labels[i])#, color=self.colors[i])
		
		pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0)
		pyplot.title = self.title
		pyplot.grid = True
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--force", action="store_true", default=False)
	args = parser.parse_args()
	
	loadData(args.experiment, force=args.force)
	
	plotter = GCPlotter()
	plot = GCScatterPlot(title="GC Throughout vs. Latency")
	plotter.addPlot(plot)
	
	c = "ONE"
	colors = {}

	for n in sorted(data.keys()):
		for c in sorted(data[n].keys()):
			dim1 = []
			dim2 = []
			for p in sorted(data[n][c]):
				target, throughput, latency95, load = p
				dim1.append(float(latency95))
				dim2.append(float(load))
			plot.addResults((dim1, dim2), label="%s-%s" % (n, c))
		
	
	plotter.doPlot()