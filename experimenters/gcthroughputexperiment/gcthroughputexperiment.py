#!/usr/bin/python

import argparse
from gccontroller import *
from gcycsb import *
from gchelpers import *
from gcjolokiamanager import *

#YCSB_HOSTNAME="crypt11"
YCSB_HOSTNAME="sol.cs"

class GCThroughputExperiment(object):
	
	def __init__(self, experiment_name, operation, length, total_nodes, consistency, targets, skip):	
		self.length_minutes = length
		self.operation = operation
		self.experiment_name = experiment_name
		
		self.clock = ExperimentClock(datetime.now(), 1)
		self.main_logger = GCLogger("%s.log" % self.experiment_name, GCLogger.DEBUG, self.clock)
		self.logger = self.main_logger.get_logger(self)
		self.cluster_manager = GCClusterManager(self.clock, self.main_logger)
		self.ganglia_manager = GCGangliaManager()
		self.jolokia_manager = GCJolokiaManager()
		
		# start the data collector
		self.data_collector = GCDataCollector(self.clock, self.experiment_name, results_dir=os.getcwd())
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.jolokia_manager)

		if skip:
			off_skip = 2
		else:
			off_skip = 1
			
		self.nodes_off = calcOffNodes(total_nodes, off_skip)
		self.ycsb_nodes = []
		for node in (MINIMUM_NODES + OPTIONAL_NODES):
			if node not in self.nodes_off:
				self.ycsb_nodes.append(node)
		
		# preheat workload
		if operation == "READ":
			ycsb_workload_preheat = YCSBGreenCassandraReadWorkload(self.ycsb_nodes, consistency_level=consistency)
		elif operation == "WRITE":
			ycsb_workload_preheat = YCSBGreenCassandraUpdateWorkload(self.ycsb_nodes, consistency_level=consistency)
		elif operation == "MIXED":
			ycsb_workload_preheat = YCSBGreenCassandraMixedWorkload(self.ycsb_nodes, read=0.75, write=0.25, consistency_level=consistency)
		
		ycsb_workload_preheat.threadcount = 60
		ycsb_workload_preheat.maxexecutiontime = 2 * 60
		ycsb_workload_preheat.targetthroughput = targets[0]
		
		ycsb_latency_output_path_preheat = self.data_collector.getExperimentPath() + "/ycsb.latency.preheat"
		ycsb_throughput_output_path_preheat = self.data_collector.getExperimentPath() + "/ycsb.throughput.preheat"
		self.ycsb_run_preheat = YCSBRun(ycsb_workload_preheat, YCSB_HOSTNAME, self.main_logger, ycsb_latency_output_path_preheat, throughput_file=ycsb_throughput_output_path_preheat)
		
		# main workloads
		self.ycsb_runs = list()
		for target in targets:
			if operation == "READ":
				ycsb_workload = YCSBGreenCassandraReadWorkload(self.ycsb_nodes, consistency_level=consistency)
			elif operation == "WRITE":
				ycsb_workload = YCSBGreenCassandraUpdateWorkload(self.ycsb_nodes, consistency_level=consistency)
			elif operation == "MIXED":
				ycsb_workload = YCSBGreenCassandraMixedWorkload(self.ycsb_nodes, read=0.75, write=0.25, consistency_level=consistency)
				
			ycsb_workload.threadcount = 60
			ycsb_workload.maxexecutiontime =  length * 60
			ycsb_workload.targetthroughput = target	
			
			ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.%d.latency" % target
			ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.%d.throughput" % target
			
			ycsb_run = YCSBRun(ycsb_workload, YCSB_HOSTNAME, self.main_logger, ycsb_latency_output_path, throughput_file=ycsb_throughput_output_path)
			self.ycsb_runs.append(ycsb_run)

	def run(self):
		print "starting data collector"
		self.data_collector.start()
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"
		
		print "turning node(s) off"
		
		for node in self.nodes_off:
			print "...%s" % node
			self.cluster_manager.nodeOff(node)
		print "...done"
		
		print "sleeping"
		time.sleep(10)
		print "...done"

		self.data_collector.epochBegin("Preheat")
		print "starting ycsb preheat"
		self.ycsb_run_preheat.run()
		print "...done"
		self.data_collector.epochEnd()
		
		print "Sleeping after preheat"
		time.sleep(30)
		print "...done"
		
		for ycsb_run in self.ycsb_runs:
			target = ycsb_run.workload.targetthroughput
			self.data_collector.epochBegin("YCSB-%d" % target)
			print "starting ycsb-%d" % target
			ycsb_run.run()
			print "...done"
			self.data_collector.epochEnd()
		
			print "Sleeping after workload"
			time.sleep(10)
			print "...done"
		
		self.logger.log("Starting repair")
		print "turning node(s) on"
		self.data_collector.epochBegin("NodeOn")
		for node in self.nodes_off:
			print "...%s" % node
			self.cluster_manager.nodeOn(node)
		print "...done"
		
		print "waiting for node resolution"
		self.cluster_manager.waitForTransitions()
		self.data_collector.epochEnd()
		print "...done"
		self.logger.log("Completed repair")
		
		print "Sleeping after recovery"
		time.sleep(30)
		print "...done"
		
		print "stopping data collector"
		self.data_collector.stop()
		print "...done"
		
#		if self.operation == "WRITE":
#			print "resetting cluster"
#			self.cluster_manager.resetCluster()
#			print "...done"

		print "ending"

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--length", action="store", type=int, required=True)
	parser.add_argument("--nodes", action="store", type=int, required=True)
	parser.add_argument("--operation", action="store", type=str, choices=["READ", "WRITE", "MIXED"], required=True)
	parser.add_argument("--consistency", action="store", type=str, choices=["ONE", "QUORUM", "ALL"], required=True)
	parser.add_argument("--targets", action="store", type=int, nargs="+", required=True)
	parser.add_argument("--skip", action="store_true", default=False)

	args = parser.parse_args()
	
	print "Running experiment %s for %d minute(s)..." % (args.experiment, args.length)
		
	exp = GCThroughputExperiment(args.experiment, args.operation, args.length, args.nodes, args.consistency, args.targets, args.skip)
	exp.run()
