import sys
import numpy
from datetime import timedelta

class GCSolarPredictorBase(object):
	def getSolarAt(self, dt):
		pass
	
	def getMaxSolarForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		current_max = 0.0
		
		current_dt = dt
		while current_dt < (dt + delta):
			power = self.getSolarAt(current_dt)
			print power
			if power > current_max:
				current_max = power
			current_dt += sample_step
		
		return current_max
	
	def getMinSolarForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		current_min = sys.maxint
		
		current_dt = dt
		while current_dt < (dt + delta):
			power = self.getSolarAt(current_dt)
			if power < current_min:
				current_min = power
			current_dt += sample_step
		
		return current_min
	
	def getAvgSolarForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		values = []
		
		current_dt = dt
		while current_dt < (dt + delta):
			power = self.getSolarAt(current_dt)
			values.append(power)
			current_dt += sample_step
		
		arr = numpy.array(values)
		average = numpy.mean(arr, axis=0) 
		
		return average
	
	# return the total *energy* for the interval in watt-hours
	def getSolarEnergyForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		watt_seconds = 0.0
		current_dt = dt
		
		# we work in watt-seconds until the end
		while current_dt < (dt + delta):
			power = self.getSolarAt(current_dt)
			watt_seconds += power * sample_step.total_seconds()
			current_dt += sample_step
		
		# convert to watt-hours and return
		return watt_seconds / 3600.0

class GCWorkloadPredictorBase(object):
	def getWorkloadAt(self, dt):
		pass
	
	def getMaxWorkloadForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		current_max = 0.0
		
		current_dt = dt
		while current_dt < (dt + delta):
			workload = self.getWorkloadAt(current_dt)
			if workload > current_max:
				current_max = workload
			current_dt += sample_step
		
		return current_max
	
	def getAvgWorkloadForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		values = []
		
		current_dt = dt
		while current_dt < (dt + delta):
			workload = self.getWorkloadAt(current_dt)
			values.append(workload)
			current_dt += sample_step
		
		arr = numpy.array(values)
		average = numpy.mean(arr, axis=0) 
		
		return average
	
	def getCalculatedWorkloadForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		
		current_dt = dt
		values = []
		
		while current_dt < (dt + delta):
			workload = self.getWorkloadAt(current_dt)
			values.append(workload)
			current_dt += sample_step
		
		arr = numpy.array(values)
		
		average = numpy.mean(arr, axis=0)
		std_dev = numpy.std(arr, axis=0)
		
		return average + std_dev
	
	def get99thPercentileWorkloadForInterval(self, dt, delta, sample_step=timedelta(seconds=5)):
		
		current_dt = dt
		values = []
		
		while current_dt < (dt + delta):
			workload = self.getWorkloadAt(current_dt)
			values.append(workload)
			current_dt += sample_step
		
		arr = numpy.array(values)
		
		value = numpy.percentile(arr, 99, axis=0)
		
		return value
			
class GCManagerBase(object):
	def getValuesForLogging(self):
		return {}
	
	def getTextDataForLogging(self):
		return {}