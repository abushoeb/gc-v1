#!/usr/bin/python

import sys
from gccdf import *
from scipy import stats

if __name__ == "__main__":
	model = sys.argv[1]
	node_list = []
	load_list = []
	
	data = {}
	
	for filename in sorted(os.listdir(".")):
		if filename.startswith('%s-' % model) and filename.endswith('.cdf'):
			try:
				# Get nodes and load
				match_str = '%s-(\d+)-(\d+).0.cdf' % model
				nodes, load = re.match(match_str, filename).groups()
				nodes = int(nodes)
				load = int(load)
				node_list.append(nodes)
				load_list.append(load)
				
				print "Loaded", nodes, load
				
				# Read CDF
				with open("." + '/' +filename) as fcdf:
					cdf = readCDF(fcdf)	
				
				if nodes not in data:
					data[nodes] = {}
				
				data[nodes][load] = cdf

			except Exception, e:
				print "Cannot parse file", filename
				print e
	
	# interpolate in between any values we have
	maxes = []
	for node in sorted(data):
		loads = data[node]
		keys = sorted(loads.keys())
		
		minimumLoad = keys[0]
		maximumLoad = keys[-1]
		maxes.append(maximumLoad)
		
		for load in xrange(minimumLoad, maximumLoad+1, 250):
			if load in loads:
				continue
			
			lowLoad = find_le(keys, load)
			highLoad = find_gt(keys, load)
			
			new = interpolateCDFs(lowLoad, loads[lowLoad], highLoad, loads[highLoad], load)
			loads[load] = new

		for load in sorted(loads):
			print node, load, loads[load]
	
	
	globalMaximumLoad = max(maxes)
	
	# extrapolate lines out based on linear regression
	print "Starting extrapolation"
	
	for node in sorted(data):
		print "generating for nodes", node
		loads = data[node]
		keys = sorted(loads.keys())
		
		maximumLoad = keys[-1]
		
		## new
		prevLoads = []
		prevCDFs = []
		
		for i in reversed(xrange(1, 3+1)):
			loadValue = keys[-1*i]
			cdfValue = loads[loadValue]
			prevLoads.append(loadValue)
			prevCDFs.append(cdfValue)
		
		#print prevLoads, prevCDFs
		#continue
		
		# need to calc a slope and intercept for every line in the CDFs
		cdfRegressions = []
		
		# this should be the same len for all of them
		cdfLen = len(prevCDFs[0])
		
		# this should also be the same for all of them
		cdfValues = []
		for i in xrange(0, cdfLen):
			cdfValues.append(prevCDFs[0].valueAt(i))
		
		# calculate the slope and intercept for each line
		for i in xrange(0, cdfLen):
			cdfPercentages = []
			for j in xrange(len(prevCDFs)):
				cdfPercentages.append(prevCDFs[j].percentageAt(i))
			
			#if cdfValues[i] <= 75:
			#	print cdfValues[i], prevLoads, cdfPercentages
			slope, intercept, r_value, p_value, std_err = stats.linregress(prevLoads, cdfPercentages)
			cdfRegressions.append((slope, intercept))
		
		# contruct a new cdf for each new data point desired
		for load in xrange(maximumLoad+250, globalMaximumLoad+1, 250):
			newCdf = CDFFast()
			for i in xrange(0, cdfLen):
				value = cdfValues[i]
				slope, intercept = cdfRegressions[i]
				percentage = slope*load + intercept
				newCdf.add(value, percentage)	
			loads[load] = newCdf

		## end new

	output = model + "-EXTENDED"
	testModelFile = model + "-TEST-regression-cdf.model"
	testF = open(testModelFile, "w")
	
	print sorted(data)
	for node in sorted(data):
		print "writing for nodes", node
		
		loads = data[node]
		
		for load in sorted(loads):
			if load < 1000:
				continue
			
			outputFile = "%s-%d-%d.0.cdf" % (output, node, load)
			#print "writing file", outputFile
			with open(outputFile, "w") as f:
				testF.write("%d\t%d\t%0.2f\n" % (node, load, data[node][load].getValue(99)))
				data[node][load].write(f)
	
	testF.close()
			