#!/usr/bin/python
from gcutil import *
import time
import thread

def tfunc(id):
	print "ident", thread.get_ident()
	while True:
		result = sem.acquire()
		print id, "result:", result
		if result:
			sem.release()
		time.sleep(id+1)

def func1():
	pass
	
if __name__ == "__main__":

	#sem = OrderedSemaphore(1)
	sem = TokenOrderedSemaphore(1)

	sem.acquire()
	
	t = Thread(target=tfunc, args=(0,))
	t.start()
	
	t2 = Thread(target=tfunc, args=(1,))
	t2.start()
	
	time.sleep(5)
	sem.cancel(t.ident)
	sem.release()
	
	time.sleep(10)
	t.join()
