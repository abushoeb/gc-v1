#!/usr/bin/python

import time
from contextlib import contextmanager
from threading import Lock
from threading import Thread

lock = Lock()
counter = 0

@contextmanager
def context():
	global counter
	lock.acquire()
	#print "acquire lock"
	yield counter
	counter += 1
	lock.release()
	#print "release lock"

def tfunc(id):
	for i in xrange(0, 10):
		with context() as count:
			print id, count
		time.sleep(2)
	
if __name__ == "__main__":

	ts = []
	
	for i in xrange(0, 2):
		t = Thread(target=tfunc, args=(i,))
		t.start()
		ts.append(t)
	
	for t in ts:
		t.join()
	
	