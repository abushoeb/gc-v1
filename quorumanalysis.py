#!/usr/bin/python

import math
import itertools

class Replica(object):
	MIN = 1
	OPT = 2
	
	def __init__(self, region, num=None, current=False):
		self.current = current
		self.on = True
		self.region = region
		self.num = num
	
	def __str__(self):
		if self.num == None:
			numStr = ""
		else:
			numStr = "%d" % self.num
			
		if self.region == self.MIN:
			baseStr = "M%s(%s)" % (numStr, "%s")
		elif self.region == self.OPT:
			baseStr = "O%s(%s)" % (numStr, "%s")
			
		if self.on:
			if self.current:
				return baseStr % "C"
			else:
				return baseStr % "S"
		else:
			return baseStr % "X"
	
	def __repr__(self):
		return self.__str__()
	
	def __cmp__(self, other):
		if self.region == self.MIN and other.region == self.OPT:
			return -1
		if self.region == self.OPT and other.region == self.MIN:
			return 1
		if self.on and not other.on:
			return -1
		if not self.on and other.on:
			return 1
		if self.current and not other.current:
			return -1
		if not self.current and other.current:
			return 1
		
		return 0
	
	def turnOff(self):
		self.on = False
	
	def turnOn(self):
		self.on = True
		
	def readableGood(self):
		return self.on and self.current
	
	def readableStale(self):
		return self.on and not self.current
	
	def live(self):
		return self.on
	
	def setStale(self):
		self.current = False
	
	def setGood(self):
		self.current = True

class ReplicaSet(object):
	
	def __init__(self, minR, optR):
		self.minReplicas = []
		self.optReplicas = []
		
		for i in xrange(1, minR+1):
			self.minReplicas.append(Replica(Replica.MIN, num=i))
		
		for i in xrange(1, optR+1):
			self.optReplicas.append(Replica(Replica.OPT, num=i))

	def __str__(self):
		return str(sorted(self.getReplicas()))
	
	def __repr__(self):
		return self.__str__()
	
	def getReplicas(self):
		return list(self.minReplicas + self.optReplicas)
	
	def getMinReplicas(self):
		return list(self.minReplicas)
	
	def getOptReplicas(self):
		return list(self.optReplicas)
	
	def turnOffCurrentReplica(self):
		for replica in self.optReplicas:
			if replica.readableGood():
				replica.turnOff()
				return True
		return False
	
	def turnOffStaleReplica(self):
		for replica in self.optReplicas:
			if replica.readableStale():
				replica.turnOff()
				return True
		return False
		
class OperationSimulator(object):
	ONE = 1
	QUORUM = 2
	ALL = 3
	GREEN_ONE = 4
	GREEN_QUORUM = 5

	def __init__(self, consistency):
		self.consistency = consistency
	
	def minResponses(self, replicas):
		liveMin = [r for r in replicas if r.region == Replica.MIN and r.live()]
		
		if self.consistency == self.ONE:
			return 0
		elif self.consistency == self.QUORUM:
			return 0
		elif self.consistency == self.ALL:
			return 0
		elif self.consistency == self.GREEN_ONE:
			return 1
		elif self.consistency == self.GREEN_QUORUM:
			return quorum(len(liveMin))
	
	def optResponses(self, replicas):
		return self.allResponses(replicas) - self.minResponses(replicas)
	
	def allResponses(self, replicas):
		liveAll = [r for r in replicas if r.live()]
		
		if self.consistency == self.ONE:
			return 1
		elif self.consistency == self.QUORUM:
			return quorum(len(liveAll))
		elif self.consistency == self.ALL:
			return len(liveAll)
		elif self.consistency == self.GREEN_ONE:
			return 1
		elif self.consistency == self.GREEN_QUORUM:
			return quorum(len(liveAll))
		
class ReadSimulator(OperationSimulator):
	
	def read(self, replicaSet):
		minReplicas = [r for r in replicaSet.getMinReplicas() if r.live()]
		optReplicas = [r for r in replicaSet.getOptReplicas() if r.live()]
		allReplicas = [r for r in replicaSet.getReplicas() if r.live()]
		
		print "\tLive: %d, Req: %d" % (len(allReplicas), self.allResponses(allReplicas))
				
		minCombos = []
		optCombos = []
		
		for combo in itertools.combinations(minReplicas, self.minResponses(allReplicas)):
			minCombos.append(combo)
			
		for combo in itertools.combinations(optReplicas, self.optResponses(allReplicas)):
			optCombos.append(combo)
		
		
		#print "minCombos", minCombos
		#print "optCombos", optCombos
		combinations = []
		
		for combo in itertools.product(minCombos, optCombos):
			replicas = []
			
			for c in combo:
				for i in c:
					replicas.append(i)
			
			combinations.append(replicas)
		
		failures = 0
		
		if len(combinations) == 0:
			print "combinations is zero", minReplicas, minCombos, optReplicas, optCombos
			
		for combination in combinations:
			satisfiable = False
			for r in combination:
				if r.readableGood():
					satisfiable = True
					break
			
			if not satisfiable:
				print "\t%s FAIL" % str(combination)
				#failures += 1
				return False
		
		return True
		
		
	
class WriteSimulator(OperationSimulator):
	
	def nextReplica(self, replicas):
		minReplicas = [r for r in replicas if r.region == Replica.MIN and r.live()]
		optReplicas = [r for r in replicas if r.region == Replica.OPT and r.live()]
		minReplicas.sort()
		optReplicas.sort()
		
		minResponses = self.minResponses(replicas)
		allResponses = self.allResponses(replicas)

		while minResponses > 0:
			r = minReplicas.pop(0)
			minResponses -= 1
			allResponses -= 1
			yield r
		
		while allResponses > 0:
			if len(optReplicas) > 0:
				r = optReplicas.pop(0)
			else:
				r = minReplicas.pop(0)
			
			allResponses -= 1
			yield r

	def write(self, replicaSet):
		writeReplicas = [r for r in self.nextReplica(replicaSet.getReplicas())]
		#print "writeReplicas", writeReplicas
		for r in writeReplicas:
			r.setGood()
		
def quorum(n):
	return n / 2 + 1

def hasGoodQuorum(replicas):
	
	currentR = 0
	for replica in replicas:
		if replica.on:
			currentR += 1
		
	currentQuorum = quorum(currentR)
	#print "currentR", currentR, "currentQuorum", currentQuorum
	readableStale = 0
	for replica in replicas:
		if replica.readableStale():
			readableStale += 1
		
	return readableStale < currentQuorum
	
def doAnalysis(r):
	for i in xrange(1, r):
		quorumCount = quorum(r)
		
		minSet = []
		optSet = []
		
		minR = i
		optR = r-i
		
		minWait = quorum(minR)
		#minWait = max(minR, quorum(r))
		
		minIdea = math.floor(r / 2.0)
		
		# build the sets
		
		# mins
		for j in xrange(0, minR):
			#minSet.append(Replica(True if quorumCount > 0 else False))
			if minWait > 0:
				minSet.append(Replica(Replica.MIN, True))
				minWait -= 1
				quorumCount -= 1
			else:
				minSet.append(Replica(Replica.MIN, True))
		
		# opts
		for j in xrange(0, optR):
			optSet.append(Replica(Replica.OPT, True if quorumCount > 0 else False))
			quorumCount -= 1
	       
		print "R: %d, minR: %d, optR: %d, quorumR: %d, minIdea: %d" % (r, minR, optR, quorum(r), minIdea)
		print str(minSet), str(optSet), quorum(r)
		
		for j in xrange(1, len(optSet)+1):
			optSet[j-1].turnOff()
			
			if not hasGoodQuorum(minSet) or not hasGoodQuorum(minSet+optSet):
			#if not hasGoodQuorum(minSet+optSet):
				print str(minSet), str(optSet), quorum(r-j), "FAIL"
			else:
				print str(minSet), str(optSet), quorum(r-j)

def doAnalysis2(r):
	for i in xrange(1, r):
		
		minR = i
		optR = r-i
		
		#replicas = []
		#for j in xrange(0, minR):
		#	replicas.append(Replica(Replica.MIN))
		#for j in xrange(0, optR):
		#	replicas.append(Replica(Replica.OPT))
		
		#print "Replicas", replicas
		
		replicaSet = ReplicaSet(minR, optR)
		
		writer = WriteSimulator(OperationSimulator.GREEN_QUORUM)
		writer.write(replicaSet)
		
		reader = ReadSimulator(OperationSimulator.GREEN_QUORUM)
		
		print "R: %d, minR: %d, optR: %d, quorumR: %d" % (r, minR, optR, quorum(r))
		
		for j in xrange(0, optR+1):	
			if j > 0:
				result = replicaSet.turnOffCurrentReplica()
				if not result:
					replicaSet.turnOffStaleReplica()
				
			satisfied = reader.read(replicaSet)
			print "\t", replicaSet, "SUCCESS" if satisfied else "FAIL"	
			
if __name__ == "__main__":
	
	#r = 7
	#doAnalysis(7)
	
	for i in xrange(2, 9):
		#doAnalysis(i)
		doAnalysis2(i)
		
		
		
		
	
		
	