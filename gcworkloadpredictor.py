#!/usr/bin/python

import os
import sys
import time
import paramiko
import socket
from scipy import stats

from gchelpers import *
from gccommon import *
from gcbase import *
from datetime import *
import math
import argparse
from bisect import bisect_left
from bisect import bisect_right
from gchelpers import interpolate as dateInterpolate
from gcmodel import *
from gclogger import *
from gcresults import *
from gcdatacollector import *
from gcsolarpredictor import GCHistoricalSolarPredictor
from gclogger import GCNullLogger

import numpy as np

SLOT_LENGTH = 15

class GCTraceWorkloadPredictor(GCWorkloadPredictorBase):

	def __init__(self, data, shift=timedelta(), interpolate=True):
		self.logger = GCLogger.getInstance().get_logger(self)
		self.data = data
		self.shift = shift
		self.interpolate = interpolate
		self.weekdays = {}
		self.firstCall = False
		
		self.dts = sorted(self.data.keys())
		
		for dt in self.dts:
		#for i in xrange(0, len(self.dts), 60):
		#	dt = self.dts[i]
			weekday = dt.weekday()
			if weekday not in self.weekdays.keys():
				self.weekdays[weekday] = dt
				#/rint "weekday", weekday, dt
			
			if len(self.weekdays.keys()) >= 7:
				break
		
		# final entry to facillitate rollover, value is from *first* entry
		dt = self.dts[-1]
		#print "last dt is", dt
		final_dt = datetime(year=dt.year, month=dt.month, day=dt.day, hour=0, minute=0, second=0) + timedelta(days=1)
		final_value = self.data[self.dts[0]]
		#print "new last dt is", final_dt
		self.data[final_dt] = final_value
		self.dts.append(final_dt)

	def findBounds(self, dt, dts):	
		i = bisect_left(dts, dt)
		if i:
			left = dts[i-1]
		else:
			raise ValueError
		
		i = bisect_right(dts, dt)
		if i != len(dts):
			right = dts[i]
		else:
			raise ValueError
		
		return left, right
		
	def getValue(self, dictionary, keys, dt, interpolation=True):
		# lets see if we happen to have the exact value
		try:
			value = dictionary[dt]
			#print "hit exact match for", dt
			return value
		except KeyError:
			pass
		
		# if not, we need to interpolate
		try:
			left_dt, right_dt = self.findBounds(dt, keys)
		except ValueError:
			print "value error", dt
			sys.exit()
		
		left = (left_dt, dictionary[left_dt])
		right = (right_dt, dictionary[right_dt])
		
		if interpolation:
			return dateInterpolate(left, right, dt)
		else:
			return dictionary[left_dt]
			
	def getWorkloadAt(self, dt):
		prev = None
		
		dt += self.shift
		w_dt = self.weekdays[dt.weekday()]
		dt = datetime(year=w_dt.year, month=w_dt.month, day=w_dt.day, hour=dt.hour, minute=dt.minute, second=dt.second)
		if not self.firstCall:
			self.logger.log("First call to getWorkloadAt(): Got a date of weekday %d (%s)" % (dt.weekday(), str(dt)))
			self.firstCall = True

		return self.getValue(self.data, self.dts, dt, interpolation=self.interpolate)

class GCAskTraceWorkloadPredictor(GCTraceWorkloadPredictor):
	
	RAW = 0
	MOVINGAVG = 1
	EXPMOVINGAVG = 2
	PERIODICAVG = 4
	
	def __init__(self, data_file, shift=timedelta(), interpolate=False, mode=RAW, max_value=None, limit=False):
		self.limit = limit
		
		data = self.loadData(data_file)
		
		if mode == self.RAW:
			pass
		elif mode == self.MOVINGAVG:
			data = self.doMovingAvg(data)
		elif mode == self.EXPMOVINGAVG:
			data = self.doExpMovingAvg(data)
		elif mode == self.PERIODICAVG:
			data = self.doPeriodicAvg(data)
		
		if max_value == None:
			self.max_value = max(data.values())
			#print self.max_value
		else:
			self.max_value = max_value
		
		super(GCAskTraceWorkloadPredictor, self).__init__(data, shift=shift, interpolate=interpolate)		

	def loadData(self, filename):
		data = {}
		firstDt = None
		
		with open(filename, 'r') as f:
			for line in f:
				line = line.strip()
				
				if line == "":
					continue
				
				fields = line.replace("\t", " ").split(" ")
				date_str = fields[0] + " " + fields[1]
				value_str = fields[2]	
				dt = datetime.strptime(date_str, "%m/%d/%Y %H:%M:%S")
				value = float(value_str)
				
				if firstDt == None:
					firstDt = dt
				
				if self.limit and dt - firstDt > timedelta(days=7):
					print "stopping load at dt", dt
					break
				
				data[dt] = value
		
		# we have this weird shifted thing going on, so fix it
		'''
		# sort the keys
		keys = sorted(data.keys())
		# find last dt
		last_dt = keys[-1]
		# find first midnight
		for k in keys:
			if k.hour == 0 and k.minute == 0 and k.second == 0:
				first_dt = k
				break
		# move values until we hit the first midnight
		for dt in keys:
			if dt < first_dt:
				value = data[dt]
				new_dt = datetime(year=last_dt.year, month=last_dt.month, day=last_dt.day, hour=dt.hour, minute=dt.minute, second=dt.second)
				del data[dt]
				data[new_dt] = value
				#print "changed %s to %s" % (str(dt), str(new_dt))
			else:
				break
		'''
		
		return data
	
	def doMovingAvg(self, data, interval=timedelta(minutes=15)):
		newData = {}
		movingAvgWindowKeys = []
		movingAvgWindowValues = []
		
		dts = sorted(data.keys())
		
		movingAvgStartKey = dts[0]
		
		for dt in dts:
			movingAvgWindowKeys.append(dt)
			movingAvgWindowValues.append(data[dt])
			
			if dt > (movingAvgStartKey + interval):
				movingAvgWindowKeys.pop(0)
				movingAvgWindowValues.pop(0)
				movingAvgStartKey = movingAvgWindowKeys[0]
			
			movingAvgValue = sum(movingAvgWindowValues) / len(movingAvgWindowValues)
			newData[dt] = movingAvgValue
		
		return newData
	
	ops = 0
	def countOps(self):
		self.ops += 1
		
		if self.ops % 10000 == 0:
			print self.ops
			
	def doExpMovingAvg(self, data, alpha=0.5, epsilon=0.01):
		newData = {}
		
		dts = sorted(data.keys())
		
		for i in xrange(0, len(dts)):
			currentWeight = 1.0
			
			numerator = 0
			denominator = 0
			
			for dt in dts[i::-1]:
				numerator += data[dt] * currentWeight
				denominator += currentWeight
				
				currentWeight *= alpha
				self.countOps()
				if currentWeight < epsilon:
					break
			
			dt = dts[i]
			newData[dt] = numerator/denominator
		
		return newData
	
	def doPeriodicAvg(self, data, interval=timedelta(seconds=30), precision=timedelta(seconds=1)):
		newData = {}

		dts = sorted(data.keys())
		
		start_dt = dts[0]
		# we cut the data down to 7 days
		stop_dt = start_dt + timedelta(days=7)
		
		dt = start_dt
		while dt < stop_dt:
			accum = 0.0
			count = 0
			
			dt_i = dt
			while dt_i < (dt + interval):
				# use interpolated value here
				# this will normally get an exact value, but this will
				# cover up weird burps in the data where we don't have a
				# value for a particular second
				currentValue = self.getValue(data, dts, dt_i, interpolation=True)
				
				accum += currentValue
				count += 1
				dt_i += precision
			
			newData[dt] = accum/count
			#print "added data for", dt
			dt += interval
			
		return newData
	
	def getWorkloadAt(self, dt):
		value = super(GCAskTraceWorkloadPredictor, self).getWorkloadAt(dt)
		return (value / self.max_value) * 0.75
		#return value

class GCAskPredictionWorkloadPredictor(GCTraceWorkloadPredictor):
	
	def __init__(self, data_file, shift=timedelta(), interpolate=False, startDt=datetime(month=4, day=1, year=2008), max_value=None, factor=1.0, clock=None):
		self.startDt = startDt
		self.clock = clock
		
		data = self.loadData(data_file)
		
		if max_value != None:
			self.max_value = max_value
		
		self.factor = factor
		
		super(GCAskPredictionWorkloadPredictor, self).__init__(data, shift=shift, interpolate=interpolate)
	
	def getValuesForLogging(self):
		values = {}
		
		if self.clock == None:
			valueToReturn = float("NaN")
		else:
			valueToReturn = self.getWorkloadAt(self.clock.get_current_time())
			
		values["prediction"] = valueToReturn
		
		return values
	
	def getTextDataForLogging(self):
		return {}
	
	def loadData(self, filename):
		data = {}
		
		actual_max = 0
		prediction_max = 0
		
		with open(filename, 'r') as f:
			for line in f:
				line = line.strip()
				fields = line.replace("\t", " ").split(" ")
				offset = int(fields[0])
				actual = float(fields[1])
				prediction = float(fields[2])
				
				if actual > actual_max:
					actual_max = actual
				
				if prediction > prediction_max:
					prediction_max = prediction
				
				dt = self.startDt + (offset-1) * timedelta(hours=1)
				value = prediction
				
				self.max_value = actual_max
				
				data[dt] = value
		
		return data
	
	def getWorkloadAt(self, dt):
		value = super(GCAskPredictionWorkloadPredictor, self).getWorkloadAt(dt)
		return (value / self.max_value) * 0.75 * self.factor
		#return value
		
class GCMicrosoftTraceWorkloadPredictor(GCTraceWorkloadPredictor):
	
	def __init__(self, data_file, field=1, shift=timedelta()):
		data = self.loadData(data_file, field)
		super(GCMicrosoftTraceWorkloadPredictor, self).__init__(data, shift=shift)
		
	def loadData(self, filename, field):
		data = {}

		f = open(filename, 'r')
		workload_data = f.read()
		lines = workload_data.split("\n")
		for line in lines:
			if line == "":
				continue
			fields = line.split(",")
			dt = datetime.strptime(fields[0], "%H:%M %d/%m/%y ")
			value = float(fields[field])
			data[dt] = value
	
		return data
	
	def getWorkloadAt(self, dt):
		value = super(GCMicrosoftTraceWorkloadPredictor, self).getWorkloadAt(dt)
		return value * 0.75

class GCSlopeWorkloadPredictor(GCWorkloadPredictorBase):
	
	# currentTime=a datetime that represents when we start
	# startValue=start value
	# changeRate=rate per minute (can be positive or negative)
	def __init__(self, currentTime, startValue, changeRate):
		self.startValue = startValue
		self.changeRate = changeRate
		self.startTime = currentTime
	
	def getWorkloadAt(self, dt):
		delta = dt - self.startTime
		minutes = delta.total_seconds() / 60.0
		return self.startValue + (minutes*self.changeRate)

class GCStaticWorkloadPredictor(GCWorkloadPredictorBase):
	
	def __init__(self, targetRate, maxRate):
		self.staticValue = targetRate / (maxRate * 1.0)
	
	def getWorkloadAt(self, dt):
		return self.staticValue
	

class GCSlopeWrapperWorkloadPredictor(GCWorkloadPredictorBase):
	
	def __init__(self, predictor, interval, startDt=datetime(month=4, day=1, year=2008)):
		self.predictor = predictor
		self.interval = interval
		self.timestamps = []
		self.values = {}
		
		lookAhead = timedelta(minutes=7)
		epoch = timedelta(minutes=15)
		prevLook = timedelta(minutes=15)
		
		w_dt = self.predictor.weekdays[startDt.weekday()]
		startDt = datetime(year=w_dt.year, month=w_dt.month, day=w_dt.day, hour=startDt.hour, minute=startDt.minute, second=startDt.second)
		
		dt = startDt
		while dt < startDt + timedelta(days=7):
			prevDt = dt - prevLook
			currDt = dt
			#prevWorkload = self.predictor.getWorkloadAt(prevDt)
			#currWorkload = self.predictor.getWorkloadAt(currDt)				
			#thisWorkload = extrapolate_dt((prevDt, prevWorkload), (currDt, currWorkload), currDt + delta)
			x = []
			y = []
			
			dtIter = prevDt
			
			while dtIter < currDt:
				sampleUnix = dt_to_unix(dtIter)
				sampleValue = self.predictor.getWorkloadAt(dtIter)
				x.append(sampleUnix)
				y.append(sampleValue)
				#print dtIter, sampleValue
				dtIter += timedelta(seconds=5)
			
			median = np.median(y)
			std = np.std(y)
			
			remove = []
			for i in xrange(0, len(x)):
				if y[i] > (median + 2*std) or y[i] < (median -  2*std):
					remove.append(i)
					print "removing", y[i]
				else:
					print "leaving", y[i]
			
			removed = 0
			for index in remove:
				x.pop(index-removed)
				y.pop(index-removed)
				removed += 1
				
			slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
			
			#if slope > 0.00005:
			#	thisLook = lookAhead * 2
			#else:
			thisLook = lookAhead
				
			thisWorkload = slope * dt_to_unix(dt + thisLook) + intercept
			print dt, slope
			#print dt, prevWorkload, currWorkload, thisWorkload

			self.timestamps.append(dt)
			self.values[dt] = thisWorkload
			
			dt += epoch
			
	def getWorkloadAt(self, dt):
		w_dt = self.predictor.weekdays[dt.weekday()]
		dt = datetime(year=w_dt.year, month=w_dt.month, day=w_dt.day, hour=dt.hour, minute=dt.minute, second=dt.second)
		
		try:
			#print dt, self.timestamps[0]
			firstDt = find_le(self.timestamps, dt)
			secondDt = find_gt(self.timestamps, dt)
		except ValueError as e:
			#print e
			return 0.0
		
#		p1 = (firstDt, self.values[firstDt])
#		p2 = (secondDt, self.values[secondDt])
#		print p1, p2
#		return interpolate_dt(p1, p2, dt)
		
#		except ValueError:
#			return 0.0
		
		return self.values[firstDt]

		'''
		firstDt = find_le(self.timestamps, dt)
		secondDt = find_gt(self.timestamps, dt)
		
		currDt = firstDt
		
		x = []
		y = []
		
		while currDt < secondDt:
			currUnix = dt_to_unix(currDt)
			currValue = self.predictor.getWorkloadAt(currDt)
			x.append(currUnix)
			y.append(currValue)
			print currDt, currValue
			currDt += timedelta(seconds=5)
		
		slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
		
		value = slope*dt_to_unix(dt) + intercept
		return value
		'''

class GCRegressionWorkloadPredictor(GCWorkloadPredictorBase):
	
	def __init__(self, max_throughput, results=None):
		self.data_collector = GCDataCollector.getInstance()
		self.max_throughput = max_throughput
		self.logger = GCLogger.getInstance().get_logger(self)
		self.last_prediction = float("NaN")
		self.results = results

		self.shift = timedelta() #TODO: fix hack
	
	def getValuesForLogging(self):
		values = {}
		values["last_prediction"] = self.last_prediction
		
		return values
	
	def getTextDataForLogging(self):
		return {}
		
	def getMaxWorkloadForInterval(self, dt, delta, sample_step=timedelta()):
		self.logger.log("getMaxWorkloadForInterval entered")
		self.logger.log("dt: %s" % str(dt))
		self.logger.log("delta: %s" % str(delta))
		
		if self.results:
			results = self.results
		else:
			results = self.data_collector.getLiveResults()
		
		if results:
			self.logger.log("Got live data")
			timestamps = results.availableTimestamps()
			
			x = []
			y = []
			
			for ts in reversed(timestamps):
				if ts < dt - delta:
					break
				
				v = results.getCommonValue(ts, "actual_workload")
				if math.isnan(v):
					continue
				
				v = normalize(v, self.max_throughput)
				
				x.append(dt_to_unix(ts))
				y.append(v)
			
			
			self.logger.log("x (before): %s" % str(x))
			self.logger.log("y (before): %s" % str(y))
			
			median = np.median(y)
			std = np.std(y)
			
			remove = []
			for i in xrange(0, len(x)):
				if y[i] > (median + 2*std) or y[i] < (median -  2*std):
					remove.append(i)
					#print "removing", y[i]
				else:
					pass
					#print "leaving", y[i]
			
			removed = 0
			for index in remove:
				x.pop(index-removed)
				y.pop(index-removed)
				removed += 1
			
			self.logger.log("x (after): %s" % str(x))
			self.logger.log("y (after): %s" % str(y))
			
			slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
			self.logger.log("slope = %0.2f, intercept = %0.2f" % (slope, intercept))
			
			targetUnix = dt_to_unix(dt + delta)
			result = slope * targetUnix  + intercept
			
			if math.isnan(result):
				result = self.data_collector.getLastValue("actual_workload")
				self.logger.log("no history yet, using last measured value: %0.2f" % result)
				return normalize(result, self.max_throughput)
			
			denormalized_result = denormalize(result, self.max_throughput)
			self.last_prediction = denormalized_result
			
			self.logger.log("predictedWorkload = %0.2f, denormalized = %0.2f" % (result, denormalized_result))
			
			return result
		else:
			self.logger.log("No live data, this shouldnt happen")
			return self.last_prediction			

class HistoricalWorkloadPredictor(GCWorkloadPredictorBase):

	def __init__(self, results, response_time_model, sla=75):
		self.results = results
		self.model = response_time_model
		self.max_throughput = self.model.peakThroughput(NUM_NODES, sla)
	
	def getWorkloadAt(self, dt):
		return normalize(self.results.getCommonValue(dt, "actual_workload"), self.max_throughput)
	
class GCWiermanWorkloadPredictor(GCTraceWorkloadPredictor):
	
	def __init__(self, shift=timedelta()):
		print "loading data"
		db = sqlitedict.SqliteDict("./wierman.db", autocommit=True, flag='c')
		print "loaded"
		print db.keys()
		data = db["workload"]
		
		super(GCWiermanWorkloadPredictor, self).__init__(data=data, shift=shift)
	
	def getWorkloadAt(self, dt):
		value = super(GCWiermanWorkloadPredictor, self).getWorkloadAt(dt)
		return value #* 0.75
	
if __name__ == "__main__":
	logger = GCNullLogger()
	parser = argparse.ArgumentParser()
	parser.add_argument("--workload", action="store", choices=["messenger", "hotmail", "ask-periodic", "ask-moving", "ask-exp", "ask-prediction", "ask-prediction-interpolate", "ask-unknown", "ask-regression", "hotmail-regression", "wierman"], nargs="+", required=True)
	parser.add_argument("--solar", action="store_true")
	parser.add_argument("--export", action="store_true")
	parser.add_argument("--duration", action="store", choices=["day", "week"], default="day")
	parser.add_argument("--pdf", action="store_true", default=False)
	parser.add_argument("--plot", action="store_true")
	args = parser.parse_args()
	
	#modelFile = "1-22-UNIFORM-300kbs-MIXED-ONE.model"
	#modelFile = "FULL-APR-18.4-smart-MODIFIED.model"
	modelFile = "MODEL-5-12-simple-MODIFIED-EXTENDED.model"
	
	#response_time_model = GCTableResponseTimeModelNew(modelFile)	
	#max_throughput = response_time_model.peakThroughput(NUM_NODES, 75)
	max_throughput = 100
	
	print "Peak Throughput is %d" % max_throughput
	data_dt = datetime(year=2013, month=10, day=21, hour=0, minute=0, second=0)
	
	if args.plot:
		import matplotlib.pyplot as plot
		plot.figure(figsize=(30, 5), frameon=True)
	
	#shift = timedelta(days=1)
	shift = timedelta()
	
	for workload in args.workload:
		if "ask" in workload:
			ask_perfect = GCAskTraceWorkloadPredictor("ask-workload-4.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=shift)
			#ask_perfect = GCAskTraceWorkloadPredictor("timeline_result.datadjusted", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))
			break
	
	for workload in args.workload:
		if "hotmail" in workload:
			hotmail_perfect = GCMicrosoftTraceWorkloadPredictor("workload.csv", field=1, shift=shift)
			break
	
	perfect_shift=timedelta(days=1)
	prediction_shift = timedelta(days=1, minutes=-45)

	for workload in args.workload:
		print "workload", workload
		
		if workload == "messenger":
			predictor = GCMicrosoftTraceWorkloadPredictor("workload.csv", field=2, shift=shift)
		
		elif workload == "hotmail":
			predictor = hotmail_perfect
			
		elif workload == "hotmail-regression":
			predictor = GCSlopeWrapperWorkloadPredictor(hotmail_perfect, 15, startDt=data_dt)
		
		elif workload == "ask-periodic":
			#predictor = GCAskTraceWorkloadPredictor("timeline_result.datadjusted", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=shift))
			predictor = ask_perfect

		elif workload == "ask-moving":
			predictor = GCAskTraceWorkloadPredictor("timeline_result.datadjusted", mode=GCAskTraceWorkloadPredictor.MOVINGAVG, shift=perfect_shift)
			
		elif workload == "ask-exp":
			predictor = GCAskTraceWorkloadPredictor("timeline_result.datadjusted", mode=GCAskTraceWorkloadPredictor.EXPMOVINGAVG, shift=perfect_shift)

		elif workload == "ask-unknown":
			predictor = GCAskTraceWorkloadPredictor("ask_predictions_maybe.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=perfect_shift)#, max_value=ask_perfect.max_value)		
			
		elif workload == "ask-prediction":
			predictor = GCAskPredictionWorkloadPredictor("ask_predictions_workload.dat", shift=prediction_shift, max_value=ask_perfect.max_value, factor=0.91)
			
		elif workload == "ask-prediction-interpolate":
			predictor = GCAskPredictionWorkloadPredictor("ask_predictions_workload.dat", shift=prediction_shift, interpolate=True, max_value=ask_perfect.max_value, factor=0.91)

		elif workload == "ask-regression":
			predictor = GCSlopeWrapperWorkloadPredictor(ask_perfect, 15)
			
		elif workload == "wierman":
			predictor = GCWiermanWorkloadPredictor(shift=perfect_shift)
			
		print "...loaded"
		
		#fig, ax1 = plot.subplots()
		
		if args.plot:
			print "doing plot"
			dts = []
			vals = []
			
			dt = data_dt

			if args.duration == "day":
				days = 1
			elif args.duration == "week":
				days = 7
				
			for i in xrange(0, 24*60*days):
				val = predictor.getWorkloadAt(dt)
				val = denormalize(val, max_throughput)

				dts.append(dt)
				vals.append(val)
				dt += timedelta(minutes=1)
			
			plot.plot(dts, vals, label=workload, linewidth=2)
			'''
			dt = data_dt
			
			for i in xrange(0, 24*60*days):
				val = predictor.getAvgWorkloadForInterval(dt, timedelta(minutes=30))
				val = denormalize(val, max_throughput) + 500
				dts.append(dt)
				vals.append(val)
				dt += timedelta(minutes=1)
			
			plot.plot(dts, vals, label=workload + " smooth")
			'''
				
			if args.solar:
				ax2 = ax1.twinx()
				
				dt = data_dt
				dts = []
				vals = []
				solar_predictor = GCHistoricalSolarPredictor(dts=[data_dt, data_dt+timedelta(days=1)], scale=0.35, experiment_dt=data_dt, solar_dt=data_dt)
				for i in xrange(0, 24*60*days):
					val = solar_predictor.getSolarAt(dt)
					dts.append(dt)
					vals.append(val)
					dt += timedelta(minutes=1)
				
				plot.figure(figsize=(30, 5), frameon=True)
				plot.plot(dts, vals, label="Solar")
			
		if args.export:
			export_sample = 15
			
			filename = "workload-%s-avg.export" % workload
			f = open(filename, "w")
			dt = data_dt
			if args.duration == "day":
				max_dt = dt + timedelta(days=1)
			
			elif args.duration == "week":
				max_dt = dt + timedelta(days=7)
			   
			#max_dt = dt + timedelta(hours=24)
			
			while dt < max_dt:
				#str_dt = datetime.strftime(dt, "%a %H:%M")
				str_dt = str(dt)
				int_wl = denormalize(predictor.getAvgWorkloadForInterval(dt, timedelta(minutes=export_sample)), max_throughput)
				out_str = "%s %d" % (str_dt, int_wl)
				f.write("%s\n" % out_str)
				dt += timedelta(minutes=export_sample)
				
			f.close()
			
			filename = "workload-%s-avg-std.export" % workload
			f = open(filename, "w")
			dt = data_dt
			max_dt = dt + timedelta(hours=24)
			while dt < max_dt:
				str_dt = datetime.strftime(dt, "%H:%M")
				int_wl = denormalize(predictor.getCalculatedWorkloadForInterval(dt, timedelta(minutes=export_sample)), max_throughput)
				out_str = "%s %d" % (str_dt, int_wl)
				f.write("%s\n" % out_str)
				dt += timedelta(minutes=export_sample)
				
			f.close()
			
			filename = "workload-%s-max.export" % workload
			f = open(filename, "w")
			dt = data_dt
			max_dt = dt + timedelta(hours=24)
			while dt < max_dt:
				str_dt = datetime.strftime(dt, "%H:%M")
				int_wl = denormalize(predictor.getMaxWorkloadForInterval(dt, timedelta(minutes=export_sample)), max_throughput)
				out_str = "%s %d" % (str_dt, int_wl)
				f.write("%s\n" % out_str)
				dt += timedelta(minutes=export_sample)
				
			f.close()


	if args.plot:
		plot.legend(bbox_to_anchor=(1.0, 1.0), loc=4, ncol=1, borderaxespad=0.0)
		#plot.ylim((0, 8500))
		#plot.ylim((0, max_throughput))
		if args.pdf:
			plot.savefig("workload.pdf", format="pdf")
		else:
			plot.show()
	
	sys.exit()
