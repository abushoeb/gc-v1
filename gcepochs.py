#!/usr/bin/python

from gchelpers import *

import sqlitedict

class GCEpochs(object):
	
	def __init__(self, data_dir, f=None):
		if f != None:
			epoch_file = f
		else:
			epoch_file = "epochs"
			
		filename = data_dir + "/" + epoch_file
		
		try:
			file = open(filename, "r")
		except:
			raise Exception("Could not load epochs file...")
		
		self.epochs = []
		self.epochBegins = {}
		self.epochEnds = {}
		
		data = file.read()
		lines = data.strip().split("\n")
		for line in lines:
			try:
				epoch, begin_str, end_str = line.split(",")
				begin = parse_timestamp(begin_str)
				end = parse_timestamp(end_str)
				self.epochs.append(epoch)
				self.epochBegins[epoch] = begin
				self.epochEnds[epoch] = end
			except:
				continue
		
	def getEpochs(self):
		return self.epochs
	
	def getEpochBegin(self, epoch):
		return self.epochBegins[epoch]
	
	def getEpochEnd(self, epoch):
		return self.epochEnds[epoch]	

class GCEpochsSqlite(GCEpochs):
	
	def __init__(self, data_dir):
		self.data_dir = data_dir
		self.metadata = sqlitedict.SqliteDict("%s/metadata.db" % self.data_dir, autocommit=True, flag='c')
		self.epochs = self.metadata["epochs"]
		
	def getEpochs(self):
		return sorted(self.epochs.keys(), key=lambda(x):self.epochs[x][0])
	
	def getEpochBegin(self, epoch):
		return self.epochs[epoch][0]
	
	def getEpochEnd(self, epoch):
		return self.epochs[epoch][1]
	
if __name__ == "__main__":
	
	epochs = GCEpochs("testenergy")
		
	for epoch in epochs.getEpochs():
		print "%s: %s - %s" % (epoch, str(epochs.getEpochBegin(epoch)), str(epochs.getEpochEnd(epoch)))
		