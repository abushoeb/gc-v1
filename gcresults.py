#!/usr/bin/python

import sys
import os
import argparse
import math
import cPickle
import sqlitedict
import gcsqlite

from gccommon import *
from gchelpers import parse_timestamp
from datetime import datetime
from datetime import timedelta
from gcdict import DiskBackedDictionary
from gchelpers import find_le, find_lt, find_ge, find_gt, index
from gcutil import LockFile
from gcresponsetime import GCResponseTimeFast, GCResponseTimeHistogram
from gcepochs import GCEpochsSqlite
from threading import Lock

class GCResultsType(object):
	OLD_FORMAT = 1
	NEW_FORMAT = 2
	SQL_FORMAT = 4
	SQLITEDICT_FORMAT = 8
	
	@classmethod
	def getType(self, directory):
		data_format = self.OLD_FORMAT
		
		for filename in os.listdir(directory):
			if "-metadata" in filename:
				data_format = self.NEW_FORMAT
				break
			if "sqlite-data.db" in filename:
				data_format = self.SQL_FORMAT
				break
			elif ".db" in filename:
				data_format = self.SQLITEDICT_FORMAT
				break
		
		return data_format

def load_time_values(filename):
	new_dict = {}
	try:
		file = open(filename, "r")
	except:
		return None
		
	data = file.read()
	lines = data.strip().split("\n")
	for line in lines:
		fields = line.split(" ")
		timestamp = fields[0] + " " + fields[1]
		dt = parse_timestamp(timestamp)
		value = float(fields[2])
		new_dict[dt] = value

	return new_dict

class GCResultsOverride(object):
	
	COMMON = 1
	NODE = 2
	
	def __init__(self, value, type):
		self.value = value
		self.type = type
	
	def getCommonValue(self, dt):
		pass	      
		      
      	def getNodeValue(self, dt, node):
		pass

class GCResultsDep:
	COMMON = 1
	NODE = 2
	
	def __init__(self, value, type, synth=False):
		self.value = value
		self.type = type
		self.synth = synth

'''	
class GCResultsTransform(object):
	pass
	
TRANSFORMS = {
	"simple_moving_avg" : "simpleMovingAvg",
}
'''

class GCResultsCache(object):
	
	def __init__(self, size=-1):
		self.size = size
		
		self.fifo = []
		self.data = {}
		
		self.hits = 0
		self.misses = 0
		
		self.lock = Lock()
		
	def __getitem__(self, key):
		with self.lock:
			try:
				value = self.data[key]
				self.hits += 1
				return value
			except KeyError as e:
				self.misses += 1
				raise e
		
	def __setitem__(self, key, value):
		with self.lock:
			self.data[key] = value
			self.fifo.append(key)
			
			if self.size != -1 and len(self.fifo) > self.size:
				removeKey = self.fifo.pop(0)
				del self.data[removeKey]
	
	def hitRate(self):
		if self.hits + self.misses == 0:
			return 0.0
		
		return (1.0 * self.hits) / (self.hits + self.misses)
	
class GCResults(object):
	
	SYNTH_COMMON_VALUES = {
		"power_transition" : "powerTransition",
		"power_ready" : "powerReady",
		"power_total" : "powerTotal",
		"solar_consumed" : "solarConsumed",
		"solar_excess" : "solarExcess",
		"brown_consumed" : "brownConsumed",
		"nodes_off" : "nodesOff",
		"nodes_transition" : "nodesTransition",
		"nodes_ready" : "nodesReady",
		"nodes_total" : "nodesTotal",
		"cassandra_cluster_load" : "cassandraClusterLoad",
		"node_avg_power" : "nodeAvgPower",
		"node_avg_load" : "nodeAvgLoad",
		"node_agg_power" : "nodeAggPower",
		"node_agg_load" : "nodeAggLoad",
		"cluster_cpu_total" : "clusterCpuTotal",
		"avg_readlatency_95" : "avgReadLatency95",
		"avg_readlatency_99" : "avgReadLatency99",
		"avg_readlatency_avg" : "avgReadLatencyAvg",
		"load_over_nodes_ready" : "loadOverNodesReady",
		"any_transitions" : "anyTransitions",
		"workload_moving_avg" : "workloadMovingAvg",
		"workload_exp_moving_avg" : "workloadExpMovingAvg",
		"greenhint_delivery_rate_total" : "greenHintDeliveryRateTotal",
		"greenhint_deliveries_total" : "greenHintDeliveriesTotal",
		"pending_compactions" : "pendingCompactions",
		"corrected_cum_latency" : "correctedCumLatency",
		"read_histogram" : "readHistogram",
		"write_histogram" : "writeHistogram",
		"combined_histogram" : "combinedHistogram",
		"readlatency_99_instant" : "instantLatency",
	}
	
	SYNTH_NODE_VALUES = {
		"cpu_total" : "cpuTotal"
	}
	
	SYNTH_VALUE_DEPS = {
		"power_transition" : [
			GCResultsDep("state", GCResultsDep.NODE),
			GCResultsDep("power", GCResultsDep.NODE)
		],
		"power_ready" : [
			GCResultsDep("state", GCResultsDep.NODE),
			GCResultsDep("power", GCResultsDep.NODE)
		],
		"power_total" : [
			GCResultsDep("power_transition", GCResultsDep.COMMON, synth=True),
			GCResultsDep("power_ready", GCResultsDep.COMMON, synth=True)
		],
		"solar_consumed" : [
			GCResultsDep("power_total", GCResultsDep.COMMON, synth=True),
			GCResultsDep("used_solar", GCResultsDep.COMMON)
		],
		"solar_excess" : [
			GCResultsDep("power_total", GCResultsDep.COMMON, synth=True),
			GCResultsDep("used_solar", GCResultsDep.COMMON)
		],
		"brown_consumed" : [
			GCResultsDep("power_total", GCResultsDep.COMMON, synth=True),
			GCResultsDep("used_solar", GCResultsDep.COMMON)
		],
		"nodes_off" : [
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"nodes_transition" : [
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"nodes_ready" : [
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"nodes_total" : [
			GCResultsDep("nodes_transition", GCResultsDep.COMMON, synth=True),
			GCResultsDep("nodes_ready", GCResultsDep.COMMON, synth=True)
		],
		"cassandra_cluster_load" : [
			GCResultsDep("state", GCResultsDep.NODE),
			GCResultsDep("cassandra_load", GCResultsDep.NODE),	
		],
		"node_avg_power" : [
			GCResultsDep("power", GCResultsDep.NODE),
			GCResultsDep("state", GCResultsDep.NODE),
			GCResultsDep("cassandra_load", GCResultsDep.NODE),
		],
		"node_avg_load" : [
			GCResultsDep("cassandra_load", GCResultsDep.NODE),
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"node_agg_power" : [
			GCResultsDep("power", GCResultsDep.NODE),
			GCResultsDep("state", GCResultsDep.NODE),
			GCResultsDep("cassandra_load", GCResultsDep.NODE),
		],
		"node_agg_load" : [
			GCResultsDep("cassandra_load", GCResultsDep.NODE),
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"cpu_total" : [
			GCResultsDep("cpu_user", GCResultsDep.NODE),
			GCResultsDep("cpu_system", GCResultsDep.NODE)
		],
		"cluster_cpu_total" : [
			GCResultsDep("cpu_total", GCResultsDep.NODE, synth=True),
		],
		"avg_readlatency_95" : [
			GCResultsDep("readlatency_95", GCResultsDep.COMMON),
		],
		"avg_readlatency_99" : [
			GCResultsDep("readlatency_99", GCResultsDep.COMMON),
		],
		"avg_readlatency_avg" : [
			GCResultsDep("readlatency_avg", GCResultsDep.COMMON),
		],
		
		"load_over_nodes_ready" : [
			GCResultsDep("actual_workload", GCResultsDep.COMMON),
			GCResultsDep("nodes_ready", GCResultsDep.COMMON),
		],
		"any_transitions" : [
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"workload_moving_avg" : [
			GCResultsDep("actual_workload", GCResultsDep.COMMON),
		],
		"workload_exp_moving_avg" : [
			GCResultsDep("actual_workload", GCResultsDep.COMMON),
		],
		"greenhint_delivery_rate_total" : [
			GCResultsDep("greenhint_delivery_rate", GCResultsDep.NODE),
		],
		"greenhint_deliveries_total" : [
			GCResultsDep("greenhint_deliveries", GCResultsDep.NODE),
		],
		"corrected_cum_latency" : [
			GCResultsDep("read_histogram", GCResultsDep.COMMON),
		],
		"read_histogram" : [
			
		],
		"write_histogram" : [
			
		],
		"combined_histogram" : [
			GCResultsDep("read_histogram", GCResultsDep.COMMON),
			GCResultsDep("write_histogram", GCResultsDep.COMMON),
		],
		"readlatency_99_instant" : [
			GCResultsDep("read_histogram", GCResultsDep.COMMON),
		],
	}
	
	def __init__(self, timestamps, nodes, max_time=None, roundStart=False, drop_last=False, subsample=1):
		
		# do timestamp stuff
		self.timestamps = timestamps
		
		# remove all timestamps past the max_time
		if max_time != None:
			if roundStart:
				real_first_dt = self.timestamps[0]
				first_dt = datetime(month=real_first_dt.month, day=real_first_dt.day, year=real_first_dt.year, hour=real_first_dt.hour, minute=0, second=0)
			else:
				first_dt = self.timestamps[0]
			#for ts in self.timestamps:
			for i in reversed(xrange(0, len(self.timestamps))):
				ts = self.timestamps[i]
				if ts > (first_dt + max_time):
					del self.timestamps[i]
					
		# if we want to do drop last
		if drop_last == True:
			for i in range(0, DROP_TIMESTAMPS):
				del self.timestamps[-1]
		
		# subsample timestamps if requested
		newTimestamps = []
		if subsample > 1:
			for i in xrange(0, len(self.timestamps), subsample):
				newTimestamps.append(self.timestamps[i])
		
			self.timestamps = newTimestamps
					

		# store nodes
		self.nodes = nodes
		
		# create a list for overrides
		self.overrides = []
		
		# Synth caches - cache synthetic values so they don't have
		# to be computed every time they are required
		
		# init synth cache for common values
		#self.cached_synth_common_values = {}
		#for value in self.SYNTH_COMMON_VALUES.keys():
		#	self.cached_synth_common_values[value] = {}

		# init synth cache for node values
		#self.cached_synth_node_values = {}
		#for node in self.nodes:
		#	self.cached_synth_node_values[node] = {}
		#	for value in self.SYNTH_NODE_VALUES.keys():				
		#		self.cached_synth_node_values[node][value] = {}
		
		# init general cache
		#self.cache = GCResultsCache(size=2*len(self.timestamps))
		self.cache = GCResultsCache()
	
	# functions that SHOULD be overridden by subclasses
	def availableCommonValues(self):
		pass

	def availableNodeValues(self):
		pass

	def getCommonValueInternal(self, dt, value):
		pass
	
	def getNodeValueInternal(self, dt, node, value):
		pass

	# subclasses MAY override these, losing any transforms done here
	def availableTimestamps(self):
		return list(self.timestamps)
	
	def availableNodes(self):
		return list(self.nodes)
	
	def getEpochs(self):
		pass

	# functions that should NOT be overridden
	def addOverride(self, override):
		self.overrides.append(override)
		
	def getCommonValue(self, dt, value):
		try:
			v = self.cache[dt, value]
			return v
		except KeyError:
			pass
		
		for override in self.overrides:
			if override.value == value:
				v = override.getCommonValue(dt)
				self.cache[dt, value] = v
				return v

		if value in self.availableCommonValues():
			v = self.getCommonValueInternal(dt, value)
			self.cache[dt, value] = v
			return v

		elif value in self.SYNTH_COMMON_VALUES.keys():
			v = self.getSynthCommonValue(dt, value)
			self.cache[dt, value] = v
			return v
		else:
			raise Exception("Common value \"%s\" is not available" % value)	
	
	def getNodeValue(self, dt, node, value):
		try:
			return self.cache[dt, node, value]
		except KeyError:
			pass
		
		for override in self.overrides:
			if override.value == value:
				v = override.getNodeValue(dt, node)
				self.cache[dt, node, value] = v
				return v
		
		if value in self.availableNodeValues():
			v = self.getNodeValueInternal(dt, node, value)
			self.cache[dt, node, value] = v
			return v
		
		elif value in self.SYNTH_NODE_VALUES.keys():
			v = self.getSynthNodeValue(dt, node, value)
			self.cache[dt, value] = v
			return v
		else:
			raise Exception("Node value \"%s.%s\" is not available" % (value, node))
	
	# synth value functions
	# internal functions to dereference value functions
	def getSynthCommonValue(self, dt, value):
		#try:		
		#	v = self.cached_synth_common_values[value][dt]
		#except KeyError:
		#	#else:
		
		func = getattr(self, self.SYNTH_COMMON_VALUES[value])
		v = func(dt)
		#self.cached_synth_common_values[value][dt] = v

		return v

	def getSynthNodeValue(self, dt, node, value):
		#try:
		#	v = self.cached_synth_node_values[node][value][dt]
		#except KeyError:
		#	#else:
		func = getattr(self, self.SYNTH_NODE_VALUES[value])
		v = func(dt, node)
		#self.cached_synth_node_values[node][value][dt] = v
		
		return v

	# Synth value implementation functions
	# Reference to any other values used for composition must be made through get***Value function(s).

	# power_transition
	def powerTransition(self, dt):
		power = 0.0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 1:
				power += self.getNodeValue(dt, node, "power")
		return power

	# power_ready
	def powerReady(self, dt):
		power = 0.0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 2:
				power += self.getNodeValue(dt, node, "power")
		return power

	# power_total
	def powerTotal(self, dt):
		return self.getCommonValue(dt, "power_transition") + self.getCommonValue(dt, "power_ready")

	# solar_consumed
	def solarConsumed(self, dt):
		power_total = self.getCommonValue(dt, "power_total")
		solar_total = self.getCommonValue(dt, "used_solar")

		if solar_total > power_total:
			return power_total
		else:
			return solar_total

	# solar_excess
	def solarExcess(self, dt):
		power_total = self.getCommonValue(dt, "power_total")
		solar_total = self.getCommonValue(dt, "used_solar")

		if solar_total > power_total:
			return solar_total - power_total
		else:
			return 0.0

	# brown_consumed
	def brownConsumed(self, dt):
		power_total = self.getCommonValue(dt, "power_total")
		solar_total = self.getCommonValue(dt, "used_solar")

		if power_total > solar_total:
			return power_total - solar_total
		else:
			return 0.0

	# nodes_off
	def nodesOff(self, dt):
		count = 0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 0:
				count += 1
		return count

	# nodes_transition
	def nodesTransition(self, dt):
		count = 0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 1:
				count += 1
		return count

	# nodes_ready
	def nodesReady(self, dt):
		count = 0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 2:
				count += 1
		return count

	# nodes_total
	def nodesTotal(self, dt):
		return self.getCommonValue(dt, "nodes_transition") + self.getCommonValue(dt, "nodes_ready")

	# load average of entire cassandra cluster
	def cassandraClusterLoad(self, dt):
		total = 0.0
		count = 0.0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") != 0:
				total += self.getNodeValue(dt, node, "cassandra_load")
				count += 1
		return total / count
	
	# average power consumption of *live* nodes
	# weighted average based on load
	def nodeAvgPower(self, dt):
		total = 0.0
		count = 0.0
		
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") != 0:
				load = self.getNodeValue(dt, node, "cassandra_load")
				total += self.getNodeValue(dt, node, "power")*load
				count += load
		
		return total / count;
		
	def nodeAvgLoad(self, dt):
		total = 0.0
		count = 0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") != 0:
				total += self.getNodeValue(dt, node, "cassandra_load")
				count +=1
				
		return total / count
	
	def nodeAggPower(self, dt):
		total = 0.0
		
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") != 0:
				total += self.getNodeValue(dt, node, "power")
		
		return total
		
	def nodeAggLoad(self, dt):
		total = 0.0
		
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") != 0:
				total += self.getNodeValue(dt, node, "cassandra_load")
				
		return total
	
	# cpu_total
	def cpuTotal(self, dt, node):
		return self.getNodeValue(dt, node, "cpu_user") + self.getNodeValue(dt, node, "cpu_system")
	
	# cluster_cpu_total
	def clusterCpuTotal(self, dt):
		total = 0.0
		for node in self.nodes:
			total += self.getNodeValue(dt, node, "cpu_total")
		
		return total
	
	def movingAvgHelper(self, dt, value, window=timedelta(minutes=15)):
		# use bisect functions to find the index
		try:
			dt_index = find_le(self.timestamps, dt)
			ts_index = index(self.timestamps, dt_index)
		except ValueError:
			return float("nan")
		
		past_data = []

		for current in reversed(xrange(0, ts_index+1)):
			current_dt = self.timestamps[current]
			if current_dt < dt - window:
				break
				
			data = self.getCommonValue(current_dt, value)
			
			if not math.isnan(data):
				past_data.append(data)
		
		if len(past_data) > 0:
			return sum(past_data) / len(past_data)
		
		return float("nan")
	
	def expMovingAvgHelper(self, dt, value, alpha=0.75, window=timedelta(minutes=15)):
		# use bisect functions to find the index
		try:
			dt_index = find_le(self.timestamps, dt)
			ts_index = index(self.timestamps, dt_index)
		except ValueError:
			return float("nan")
		
		currentWeight = 1.0
		numerator = 0.0
		denominator = 0.0
		cycles = 0
		
		for current_dt in self.timestamps[ts_index::-1]:
			numerator += self.getCommonValue(current_dt, value) * currentWeight
			denominator += currentWeight
			
			currentWeight *= alpha
			cycles += 1
			#if currentWeight < epsilon:
			#	print "Broke after %d cycles" % cycles
			#	break
			if current_dt < dt - window:
				break
			
		return numerator / denominator
		
	def avgReadLatency95(self, dt):	
		return self.movingAvgHelper(dt, "readlatency_95")
	
	def avgReadLatency99(self, dt):	
		return self.movingAvgHelper(dt, "readlatency_99", window=timedelta(minutes=5))

	def avgReadLatencyAvg(self, dt):
		return self.movingAvgHelper(dt, "readlatency_avg")
	
	def loadOverNodesReady(self, dt):
		workload = self.getCommonValue(dt, "actual_workload")
		nodes_ready = self.getCommonValue(dt, "nodes_ready")
		
		return workload / nodes_ready
		
	def anyTransitions(self, dt):
		for node in self.nodes:
			state = self.getNodeValue(dt, node, "state")
			if state == 1:
				return 1
		
		return 0
	
	def workloadMovingAvg(self, dt):
		return self.movingAvgHelper(dt, "actual_workload", window=timedelta(minutes=15))
		
	def workloadExpMovingAvg(self, dt):
		return self.expMovingAvgHelper(dt, "actual_workload", alpha=0.90, window=timedelta(minutes=15))
		
	def greenHintDeliveryRateTotal(self, dt):
		total = 0.0
		
		for node in self.nodes:
			value = self.getNodeValue(dt, node, "greenhint_delivery_rate")
			if not math.isnan(value):
				total += value
		return total
	
	def greenHintDeliveriesTotal(self, dt):
		total = 0.0
		
		for node in self.nodes:
			value = self.getNodeValue(dt, node, "greenhint_deliveries")
			if not math.isnan(value):
				total += value
		
		return total

	def pendingCompactions(self, dt):
		total = 0
		
		for node in self.nodes:
			value = self.getNodeValue(dt, node, "cassandra_pending_compactions")
			if not math.isnan(value):
				total += value
		
		return total
	
	def correctedCumLatency(self, dt):
		start_dt = self.availableTimestamps()[0]
		
		if type(self) == GCResultsLegacy:
			try:
				responseTimeData = self._responseTimeData
			except AttributeError:
				self._responseTimeData = GCResponseTimeFast(self.data_dir, "histogram.read.crypt11")
				responseTimeData = self._responseTimeData
			
			startHistogram = responseTimeData.getHistogramAt(start_dt)
			thisHistogram = responseTimeData.getHistogramAt(dt)			
		else:
			value = "read_histogram"
			startHistogram = self.getCommonValue(start_dt, value)
			thisHistogram = self.getCommonValue(dt, value)

		diffHistogram = thisHistogram - startHistogram
		
		#print start_dt, dt
		
		return diffHistogram.getPercentile(99)
	
	def readHistogram(self, dt):
		found = False
		for value in self.availableCommonValues():
			if "histogram_read" in value:
				found = True
				break
		
		if not found:
			return GCResponseTimeHistogram()
		
		return self.getCommonValue(dt, value)
	
	def writeHistogram(self, dt):
		found = False
		for value in self.availableCommonValues():
			if "histogram_write" in value:
				found = True
				break
		
		if not found:
			return GCResponseTimeHistogram()
		
		return self.getCommonValue(dt, value)
	
	def combinedHistogram(self, dt):
		return self.getCommonValue(dt, "read_histogram") + self.getCommonValue(dt, "write_histogram")
	
	def instantLatency(self, dt):
		index = self.availableTimestamps().index(dt)
		
		#if index in [0, 1, 2, 4, 5]:
		#	return self.getCommonValue(dt, "read_histogram").getPercentile(99)
		
		start_dt = self.availableTimestamps()[index-1]
		end_dt = dt
		
		#print start_dt, end_dt
		
		startHistogram = self.getCommonValue(start_dt, "read_histogram")
		endHistogram = self.getCommonValue(end_dt, "read_histogram")
		
		diffHistogram = endHistogram - startHistogram
		
		return diffHistogram.getPercentile(99)		
		
	# testing functions
	def testOne(self):
		dts = self.availableTimestamps()
		dt = dts[len(dts) / 2]
		node = self.nodes[0]

		print "Common Values:"
		for value in self.availableCommonValues():
			print "%s\t= %s" % (value, self.getCommonValue(dt, value))

		print "Node Values:"
		for value in self.availableNodeValues():
			print "%s[%s]\t= %s" % (value, node, self.getNodeValue(dt, node, value))

	def testAll(self):
		print "Starting test..."
		for dt in self.availableTimestamps():
			for value in self.availableCommonValues():
				v = self.getCommonValue(dt, value)
				if v == None:
					print "Error: ", dt, value
					continue
					
			for node in self.availableNodes():
				for value in self.availableNodeValues():
					v = self.getNodeValue(dt, node, value)
					if v == None:
						print "Error: ", dt, node, value
						continue
		print "Completed test..."
		

class GCResultsLegacy(GCResults):
	DEFAULT_COMMON_VALUES = [
		"actual_workload",
		"target_workload",
		"solar",
		"used_solar",
		"ycsb.target",
		"ycsb.throughput",
		"target_workload_normalized", 
		"nodes_required",
		"readlatency_95",
		"writelatency_95",
		"readlatency_99",
		"writelatency_99",
		"readlatency_avg",
		"writelatency_avg",
		"readlatency_90_cum",
		"writelatency_90_cum",
		"readlatency_90_window",
		"writelatency_90_window",
		"readlatency_95_cum",
		"writelatency_95_cum",
		"readlatency_95_window",
		"writelatency_95_window",
		"readlatency_99_cum",
		"writelatency_99_cum",
		"readlatency_99_window",
		"writelatency_99_window",
		"readlatency_avg_cum",
		"writelatency_avg_cum",
		"readlatency_avg_window",
		"writelatency_avg_window",
	]
	DEFAULT_NODE_VALUES = [
		"state",
		"power",
		"load_one",
		"cpu_user",
		"cpu_system",
		"cassandra_load",
		"cassandra_read_rate",
		"cassandra_write_rate",
		"cassandra_read_latency",
		"cassandra_write_latency",
		"cassandra_total_compactions",
		"jvm_parnew_time",
		"jvm_parnew_count",
		"jvm_cms_time",
		"jvm_cms_count",
		"sstables_data",
		"sstables_greenhints",
		"greenhint_delivery_rate",
		"greenhint_deliveries",
		"cassandra_greenhint_write_rate",
		"cassandra_greenhint_write_latency",
		"read_tracker_recent",
		"read_tracker_total",
		"write_tracker_recent",
		"write_tracker_total",
	]
	
	def __init__(self, data_dir=None, nodes=None, required=None, debug=False, max_time=None, roundStart=False, drop_last=False, subsample=1):
		self.data_dir = data_dir

		# load the available values
		common_values = set()
		node_values = set()
		files = os.listdir(data_dir)
		
		for f in sorted(files):
			if f == "nodes" or f == "timestamps" or f == "epochs":
				continue
			
			parts = f.split(".")
			if len(parts) == 2 and parts[0] == "ycsb":
				common_values.add(parts[0] + "." + parts[1])
			elif len(parts) == 1:
				common_values.add(parts[0])
			elif len(parts) == 2:
				node_values.add(parts[0])
				
		self.COMMON_VALUES = list(common_values)
		self.NODE_VALUES = list(node_values)
		
		# load which nodes we have data for
		if nodes == None:
			nodes_file = "%s/nodes" % data_dir
			if os.path.exists(nodes_file):
				f = open(nodes_file)
				data = f.read()
				f.close()
				nodes = data.strip().split("\n")
				nodes = sorted(nodes)
		else:
			nodes = sorted(nodes)

		if required == None:
			self.requiredList = self.COMMON_VALUES + self.NODE_VALUES + self.SYNTH_COMMON_VALUES.keys() + self.SYNTH_NODE_VALUES.keys()
		else:
			self.requiredList = self.buildRequiredList(required)
		
		if debug:
			print "GCResultsLegacy: required: ", self.requiredList
			print self.COMMON_VALUES

		# load data for individual nodes
		self.node_values = {}
		self.node_values_available = {}
		for node in nodes:
			self.node_values[node] = {}
			self.node_values_available[node] = []

			for value in self.NODE_VALUES:
				if value not in self.requiredList:
					continue
				
				values = load_time_values("%s/%s.%s" % (data_dir, value, node))
				if values != None:
					self.node_values[node][value] = values
					self.node_values_available[node].append(value)
				else:
					if value in self.requiredList:
						raise Exception("Could not load required node value: %s.%s" % (node, value))

		# load data common for all nodes
		self.common_values = {}
		self.common_values_available = []
		for value in self.COMMON_VALUES:
			if value not in self.requiredList:
					continue
			
			values = load_time_values("%s/%s" % (data_dir, value))
			if values != None:
				self.common_values[value] = values
				self.common_values_available.append(value)
			else:
				if value in self.requiredList:
					raise Exception("Could not load required common value: %s" % value)

		# load the timestamps
		timestamps = []
		timestamp_file = "%s/timestamps" % data_dir
		if os.path.exists(timestamp_file):
			f = open(timestamp_file, "r")
			lines = f.read().strip().split("\n")
			self.timestamps = []
			for line in lines:
				dt = parse_timestamp(line)
				timestamps.append(dt)
			f.close()
		
		
		super(GCResultsLegacy, self).__init__(timestamps=timestamps, nodes=nodes, max_time=max_time, roundStart=roundStart, drop_last=drop_last, subsample=subsample)
		
		if debug:
			print "GCResultsLegacy: data loaded"
	
	def resolveDependencies(self, value, requiredList, depth=0):
		prefix = ""
		for i in xrange(0, depth):
			prefix += "... "

		requiredList.append(value)
		deps = self.SYNTH_VALUE_DEPS[value]
		for dep in deps:
			#print "%sResolving synth: %s" % (prefix, dep.value)
			if dep.synth == False and dep.value not in requiredList:
			#	print "%sDep %s is native, adding..." % (prefix, dep.value)
				requiredList.append(dep.value)
			elif dep.synth == True:
			#	print "%sDep %s is synth, recursing..." % (prefix, dep.value)
				self.resolveDependencies(dep.value, requiredList, depth+1)
		return

	def buildRequiredList(self, required_values):
		requiredList = []
		
		# lets assume that native values (non synth values) don't need to be enumerated
		# we can just add them to the list
		
		for value in required_values:
			if value in self.SYNTH_COMMON_VALUES.keys() or value in self.SYNTH_NODE_VALUES.keys():
				self.resolveDependencies(value, requiredList)
			else:
				requiredList.append(value)
			
			'''
			if value in self.COMMON_VALUES or value in self.NODE_VALUES:
			#	print "Value is native: adding %s to list..." % value
				requiredList.append(value)
			else:
			#	print "Value is synth: resolving dependencies for %s..." % value
				self.resolveDependencies(value, requiredList)
			'''
		return requiredList	
	
	# user functions
	def availableCommonValues(self):
		return sorted(self.common_values_available)

	def availableNodeValues(self):
		available = []
		for value in self.NODE_VALUES:
			if value in self.requiredList:
				available.append(value)

		return sorted(available)

	def getCommonValueInternal(self, dt, value):
		return self.common_values[value][dt]
			
	def getNodeValueInternal(self, dt, node, value):
		return self.node_values[node][value][dt]

class GCResultsDictionary(GCResults):
	
	def __init__(self, dictionaries, timestamps, nodes, max_time=None, roundStart=False, drop_last=False, subsample=1):
		self.dictionaries = dictionaries
		self.timestamps = timestamps
		self.nodes = nodes
		super(GCResultsDictionary, self).__init__(timestamps=timestamps, nodes=nodes, max_time=max_time, roundStart=roundStart, drop_last=drop_last, subsample=subsample)

	def availableCommonValues(self):
		available = []
		for key in self.dictionaries:
			if "." not in key:
				available.append(key)
		
		return available
	
	def availableNodeValues(self):
		available = set()
		for key in self.dictionaries:
			if "." in key:
				value, node = key.split(".")
				available.add(value)
		
		return list(available)
	
	def getCommonValueInternal(self, dt, value):
		return self.dictionaries[value][dt]
	
	def getNodeValueInternal(self, dt, node, value):
		key = "%s.%s" % (value, node)
		return self.dictionaries[key][dt]

class GCResultsDisk(GCResultsDictionary):
		
	def __init__(self, data_dir, max_time=None, roundStart=False, drop_last=False, subsample=1):
		self.data_dir = data_dir
		
		# load the nodes file
		nodes_file = "%s/nodes" % data_dir
		if os.path.exists(nodes_file):
			f = open(nodes_file)
			data = f.read()
			f.close()
			nodes = data.strip().split("\n")
			nodes = sorted(nodes)
		
		with LockFile("%s/sync.lock" % data_dir):
			# load the timestamps
			timestamps_synced_file = "%s/timestamps-synced" % data_dir
			timestamp_file = "%s/timestamps" % data_dir
			if os.path.exists(timestamps_synced_file):
				print "got timestamp sync file"
				with open(timestamps_synced_file, "rb") as f:
					timestamps = cPickle.load(f)
				print timestamps[0], timestamps[-1]
				
			elif os.path.exists(timestamp_file):
				timestamps = []
				with open(timestamp_file, "r") as f:
					lines = f.read().strip().split("\n")
					self.timestamps = []
					for line in lines:
						dt = parse_timestamp(line)
						timestamps.append(dt)
			else:
				raise Exception("No timestamps found")
			
			files = os.listdir(data_dir)
			dictionaries = {}
			
			for f in sorted(files):
				#print f
				if "%s-metadata" % f in files:
					key = f
					dataFile = "%s/%s" % (data_dir, key)
					d = DiskBackedDictionary(dataFile, mode=DiskBackedDictionary.READONLY | DiskBackedDictionary.KEEPOPEN | DiskBackedDictionary.LAZYLOAD)
					dictionaries[key] = d
		
		super(GCResultsDisk, self).__init__(dictionaries=dictionaries, timestamps=timestamps, nodes=nodes, max_time=max_time, roundStart=roundStart, drop_last=drop_last, subsample=subsample)
		
class GCResultsDiskLive(GCResultsDictionary):
	
	def __init__(self, dictionaries, timestamps, nodes):
		super(GCResultsDiskLive, self).__init__(dictionaries=dictionaries, timestamps=timestamps, nodes=nodes)

class GCResultsSQL(GCResults):
	
	def __init__(self, data_dir, max_time=None, roundStart=False, drop_last=False, subsample=1):
		
		self.data_dir = data_dir
		
		timestamps = self.loadTimestamps()
		nodes = self.loadNodes()
		
		super(GCResultsSQL, self).__init__(timestamps=timestamps, nodes=nodes, max_time=max_time, roundStart=roundStart, drop_last=drop_last, subsample=subsample)
		
		self.loadAvailable()
		
		#self.CACHE_LEN = 1024
		#self.CACHE_READAHEAD = 128
		self.commonCache = {}
		self.nodeCache = {}
		#self.commonCacheMembers = []
		#self.nodeCacheMembers = []
		
	def getCon(self):
		return gcsqlite.getSQLiteConn("%s/sqlite-data.db" % self.data_dir, timeout=30) 
	
	def loadTimestamps(self):
		timestamps = []
		con = self.getCon()
		cur = con.cursor()
		cur.execute("select timestamp from timestamps order by timestamp")
		for row in cur:
			timestamps.append(row["timestamp"])
			
		#print timestamps
		return timestamps
			  
	def loadNodes(self):
		nodes = []

		con = self.getCon()
		cur = con.cursor()
		cur.execute("select node from nodes order by node")
		for row in cur:
			nodes.append(row["node"])
			
		return nodes
		
	def loadAvailable(self):
		self.availableCommon = []
		self.availableNode = []
		
		con = self.getCon()
		cur = con.cursor()
		#cur.execute("select name from main.sqlite_master where type='table'")
		cur.execute("select key from keys")
		for row in cur:
			#value = row["name"]
			value = row["key"]
			#if value in ["nodes", "epochs", "timestamps"]:
			#	continue
			
			#print "value", value
			if "$" in value:
				key, node = value.split("$")
				self.availableNode.append(key)
			else:
				self.availableCommon.append(value)
		
	def availableCommonValues(self):
		return list(self.availableCommon)
	
	def availableNodeValues(self):
		return list(self.availableNode)
	
	def getCachedCommonValue(self, dt, key):
		if key not in self.commonCache:
			return None
		
		try:
			return self.commonCache[key][dt]
		except KeyError:
			return None
	
	def getCachedNodeValue(self, dt, node, key):
		print "checking cache", node, key, dt

		if node not in self.nodeCache:
			print "bailing on node"
			return None
		
		if key not in self.nodeCache[node]:
			print "bailing on key"
			return None
		
		print "got to try"
		try:
			return self.nodeCache[node][key][dt]
		except KeyError:
			return None
		
	def cacheCommonValue(self, dt, key, value):
		if key not in self.commonCache:
			print "new dict"
			self.commonCache[key] = {}
		
		self.commonCache[key][dt] = value
	
	def cacheNodeValue(self, dt, node, key, value):
		if node not in self.nodeCache:
			print "new node dict"
			self.nodeCache[node] = {}
		
		if key not in self.nodeCache[node]:
			print "new other dict"
			self.nodeCache[node][key] = {}
		
		self.nodeCache[node][key][dt] = value
	
	def getCommonValueInternal(self, dt, value):
		print "reading", str(dt), value
		cached = self.getCachedCommonValue(dt, value)
		if cached != None:
			print "cache hit"
			return cached
		else:
			print "cache miss"
		
		con = self.getCon()
		cur = con.cursor()
		key = value
		sql = "select timestamp, %s from data where timestamp >= ? order by timestamp limit 1024" % key
		print sql
		cur.execute(sql, (dt,))
		count = 0
		for row in cur:
			timestamp = row["timestamp"]
			valueRead = row[key]
			
			#return row[key]
			
			self.cacheCommonValue(timestamp, value, valueRead)
			count +=1
			
		print "cached", count, dt, key, value
		
		return self.getCachedCommonValue(dt, key)
		
	def getNodeValueInternal(self, dt, node, value):
		print "reading", str(dt), node, value
		cached = self.getCachedNodeValue(dt, node, value)
		print "read from cache", cached
		if cached != None:
			print "cache hit"
			return cached
		else:
			print "cache miss"
		
		con = self.getCon()
		cur = con.cursor()
		key = str("%s$%s" % (value, node))
		sql = "select timestamp, %s from data where timestamp >= ? order by timestamp limit 1024" % key
		print sql
		#print key, type(key)
		cur.execute(sql, (dt,))
		count = 0
		for row in cur:
			#for r in row:
			#	print r
			timestamp = row["timestamp"]
			valueRead = row[key]
			assert valueRead != None
			
			self.cacheNodeValue(timestamp, node, value, valueRead)
			
			count += 1
			#return row[key]
			
		print "cached", count, dt, node, value, valueRead
		
		ret = self.getCachedNodeValue(dt, node, key)
		assert ret != None
		return ret

class GCResultsSqlite(GCResults):
	def __init__(self, sqlitedicts, metadata, preload=True, max_time=None, roundStart=False, drop_last=False, subsample=1):
		self.sqlitedicts = sqlitedicts
		self.metadata = metadata
		self.preload = preload
		
		timestamps = self.metadata["timestamps"]
		nodes = self.metadata["nodes"]
		self.keys = self.metadata["keys"]
		
		super(GCResultsSqlite, self).__init__(timestamps=timestamps, nodes=nodes, max_time=max_time, roundStart=roundStart, drop_last=drop_last, subsample=subsample)

	def availableCommonValues(self):
		available = []
		
		for key in self.keys:
			if "." not in key:
				available.append(key)
			
		return sorted(available)
	
	def availableNodeValues(self):
		available = set()
		
		for key in self.keys:
			if "." in key:
				value, node = key.split(".")
				available.add(value)
		
		return sorted(list(available))
	
	def parseStrTimestamp(self, str_dt):
		if "." in str_dt:
			return datetime.strptime(str_dt, "%Y-%m-%d %H:%M:%S.%f")
		else:
			return datetime.strptime(str_dt, "%Y-%m-%d %H:%M:%S")
		
	def getCommonValueInternal(self, dt, value):
		# lets try to short circuit the loading and get stuff into cache ASAP
		# if preload flag is set
		if self.preload:
#			start = datetime.now()
			for k_dt, v in self.sqlitedicts[value].iteritems():
				k_dt = self.parseStrTimestamp(k_dt)
				self.cache[k_dt, value] = v

			#finish = datetime.now()
			#print "cached %s, elapsed time %0.2f seconds" % (value, (finish-start).total_seconds())
		
		return self.sqlitedicts[value][dt]
	
	def getNodeValueInternal(self, dt, node, value):
		key = "%s.%s" % (value, node)
		
		# lets try to short circuit the loading and get stuff into cache ASAP
		# if preload flag is set
		if self.preload:
			#start = datetime.now()
			for k_dt, v in self.sqlitedicts[key].iteritems():
				k_dt = self.parseStrTimestamp(k_dt)
				self.cache[k_dt, node, value] = v
			#finish = datetime.now()
			#print "cached %s, elapsed time %0.2f seconds" % (key, (finish-start).total_seconds())
		
		try:
			v = self.sqlitedicts[key][dt]
		except Exception as e:
			#print "Exception getting (%s,%s)" % (key, str(dt))
			return float("nan")
		
		return v
	
class GCResultsSqliteLive(GCResultsSqlite):
	
	def __init__(self, sqlitedicts, metadata, max_time=None, roundStart=False, drop_last=False, subsample=1):
		super(GCResultsSqliteLive, self).__init__(sqlitedicts, metadata, max_time=None, roundStart=False, drop_last=False, subsample=1, preload=False)

class GCResultsSqliteDisk(GCResultsSqlite):
	
	def __init__(self, data_dir, max_time=None, roundStart=False, drop_last=False, subsample=1):
		self.data_dir = os.path.realpath(data_dir)
		
		metadata = sqlitedict.SqliteDict("%s/metadata.db" % self.data_dir, autocommit=True, flag='c')
		sqlitedicts = {}
		for key in metadata["keys"]:
			sqlitedicts[key] = sqlitedict.SqliteDict("%s/%s.db" % (self.data_dir, key), autocommit=True, flag='c')
		
		super(GCResultsSqliteDisk, self).__init__(sqlitedicts, metadata, max_time=None, roundStart=False, drop_last=False, subsample=1)
	
	def getEpochs(self):
		return GCEpochsSqlite(self.data_dir)
	
if __name__ == "__main__":
	
	'''
	if len(sys.argv) < 2:
		print "Please specify dir..."
		sys.exit()

	results = GCResults(sys.argv[1])
	print "Data loaded..."
	#results.testOne()
	results.testAll()
	'''
