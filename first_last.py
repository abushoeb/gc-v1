#!/usr/bin/python

import sys

if __name__ == "__main__":
	filename = sys.argv[1]
	lines = []
	with open(filename, "r") as f:
		for line in f:
			lines.append(line.strip())
	
	print lines[0]
	print lines[-1]
