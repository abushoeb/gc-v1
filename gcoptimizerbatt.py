#!/usr/bin/python

import os
import re
import sys
import sqlitedict

from operator import itemgetter
from gccommon import *
from gccdf import *
from gcutil import *
from gclogger import GCLogger
from gcbatterymodel import GCParasolPowerManager

from multiprocessing import Process, Queue

try:
	from gurobipy import *
except Exception as e:
	print e

class OnlineOptimizerBatt(object):
	PATH = "."
	POWER_NODE = 27.0 #25.5
	PERCENTILE = 99
	RESPONSE_TIME = 75
	EPOCHS = range(0, 24*4)
	NODES = range(9, 27+1, 1)
	TRANS_NODES_OVERHEAD = 0
	MAX_EPOCH_LATENCY = math.ceil(75.0 * 1.33)
	EPOCH_LEN_HOURS = 0.25
	MIN_BATTERY_LEVEL = 0.65
	NET_METER_FACTOR = 0.40
	
	def __init__(self, model="MODEL-10-17-simple-EXTENDED-ADJUSTED"): #"MODEL-5-12-CLEAN-smart-EXTENDED"):
		
		self.model = model	
		self.logger = GCLogger.getInstance().get_logger(self)
		self.logger.info("using model %s" % self.model)
		
		# Read the CDFs
		self.cdfs = CDFSet()
		self.runCount = 0
		
		self.node_list = []
		self.load_list = []
		
		cdfPoints = {}
		
		for filename in sorted(os.listdir(self.PATH)):
			if filename.startswith('%s-' % self.model) and filename.endswith('.cdf'):
				try:
					# Get nodes and load
					match_str = '%s-(\d+)-(\d+).0.cdf' % self.model
					nodes, load = re.match(match_str, filename).groups()
					nodes = int(nodes)
					load = int(load)
					self.node_list.append(nodes)
					self.load_list.append(load)
					
					# Read CDF
					with open(self.PATH + '/' +filename) as fcdf:
						cdf = readCDF(fcdf)
					
					if not nodes in cdfPoints:
						cdfPoints[nodes] = []
					
					cdfPoints[nodes].append((load, cdf))
					
				except Exception, e:
					self.logger.log("Cannot parse file: %s" % filename)
					self.logger.log("Exception: %s" % str(e))
		
		for nodes in cdfPoints.keys():
			cdfPoints[nodes].sort()

		for nodes in cdfPoints.keys():
			for point in cdfPoints[nodes]:
				load, cdf = point
				self.cdfs[load, nodes] = cdf
				self.logger.info("Have CDF for %d, %d: %0.2f ms" % (load, nodes, cdf.getValue(99)))

	def getOptSchedule(self, workload, solar, brownPrices, prevNumNodes, prevTotal, prevLessThanMs, peakPower, peakPowerPrice, maxWindowSLA, prevBattLevel):
		data = {}
		data["workload"] = workload
		data["solar"] = solar
		data["brownPrices"] = brownPrices
		data["prevNumNodes"] = prevNumNodes
		data["prevTotal"] = prevTotal
		data["prevLessThanMs"] = prevLessThanMs
		data["peakPower"] = peakPower
		data["peakPowerPrice"] = peakPowerPrice
		data["maxWindowSLA"] = maxWindowSLA
		data["prevBattLevel"] = prevBattLevel
		
		print "Data before pipe:", data
		
		squeue = Queue()
		rqueue = Queue()
		squeue.put(data)
		
		process = Process(target=self._getOptSchedule, args=(squeue, rqueue))
		process.start()
		ret = rqueue.get()
		
		if self.runCount == 0:
			self.cdfs = rqueue.get()
		
		process.join()
		
		self.runCount += 1
		return ret
	
	def _getOptSchedule(self, squeue, rqueue):
		self.logger.info("Getting data...")
		data = squeue.get()
		workload = data["workload"]
		solar = data["solar"]
		brownPrices = data["brownPrices"]
		prevNumNodes = data["prevNumNodes"]
		prevTotal = data["prevTotal"]
		prevLessThanMs = data["prevLessThanMs"]
		peakPower = data["peakPower"]
		peakPowerPrice = data["peakPowerPrice"]
		maxWindowSLA = data["maxWindowSLA"]
		prevBattLevel = data["prevBattLevel"]
		
		cdfs = self.cdfs
		
		self.logger.info("running getOptSchedule")
		self.logger.info("at start, maxWindowSLA %d" % maxWindowSLA)
		print "Data after pipe:", data
		
		worktrace = {}
		solartrace = {}
		
		for i in xrange(0, len(workload)):
			worktrace[i] = workload[i]
		for i in xrange(0, len(solar)):
			solartrace[i] = solar[i]

		ret = {}
		m = Model()
		# Params
		# Variables
		schedule = {}
		self.logger.info("doing schedule constraints")
		
		# set up schedule table
		for epoch in self.EPOCHS:
			work = worktrace[epoch]
			for node in self.NODES:
				#print "Check", epoch, work, node
				if (work, node) not in cdfs:
					# If we have no model is that it's not even possible
					schedule[epoch, node] = 0
				else:
					schedule[epoch, node] = m.addVar(vtype=GRB.BINARY, name="schedule(%d,%d)" % (epoch, node))

		m.update()	
		self.logger.info("done with schedule constraints")

		# Extracted variable (bring together the schedules)
		# TODO: it seems this is unused?
		'''
		numnodes = {}
		for epoch in self.EPOCHS:
			numnodes[epoch] = quicksum(schedule[epoch, node]*node for node in self.NODES)
		'''
		
		# Setting previous status
		for node in self.NODES:
			schedule[-1,node] = 0
		schedule[-1,prevNumNodes] = 1
	
		# Constraints
		# We can only choose one config of nodes per epoch
		for epoch in self.EPOCHS:
			m.addConstr(quicksum(schedule[epoch, node] for node in self.NODES) == 1, "onlyoneconfigperepoch(%d)" % epoch)
		m.update()
			
		# Constraint on 99th percentile (main one for GC)
		self.logger.info("Doing constraints that require CDFs")
		auxless = 0.0
		auxtotal = 0.0
		
		# Window for the SLA
		self.logger.info("maxWindowSLA %d" % maxWindowSLA)
		for epoch in self.EPOCHS:
			if epoch >= maxWindowSLA:
				self.logger.info("epoch %d >= maxWindowSLA %d" % (epoch, maxWindowSLA))
				break
			
			work = worktrace[epoch]
			for node in self.NODES:
				# TODO: Inigo check this, i brought this over from for-inigo/milp.py
				auxless += schedule[epoch,node]*work*cdfs[work, node].getPercentage(self.RESPONSE_TIME)/100.0

			auxtotal += work
		
		# add the previous state
		m.addConstr(auxless+prevLessThanMs >= ((self.PERCENTILE)/100.0)*(auxtotal + prevTotal), "slaconstraint")
		print self.PERCENTILE, prevLessThanMs, auxtotal, prevTotal
		m.update()
		
		self.logger.info("Done with constraints that require CDFs")

		# At every epoch the Y-%ile response has to be lower than X
		for epoch in self.EPOCHS:
			work = worktrace[epoch]
			m.addConstr(quicksum(schedule[epoch,node]*cdfs[work, node].getValue(self.PERCENTILE) for node in self.NODES) <= self.MAX_EPOCH_LATENCY) # 90 ms
		m.update()
		
		### new stuff
		
		# build NumNodes and PowerLoad variables and set up constraints
		NumNodes = {}
		PowerLoad = {}
		for epoch in self.EPOCHS:
			NumNodes[epoch] = m.addVar(vtype=GRB.INTEGER, name="NumNodes(%d)" % epoch)
			PowerLoad[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="PowerLoad(%d)" % epoch)
		m.update()
		
		for epoch in self.EPOCHS:	
			m.addConstr(NumNodes[epoch] == quicksum(schedule[epoch, node]*node for node in self.NODES))
			m.addConstr(PowerLoad[epoch] == NumNodes[epoch] * self.POWER_NODE)
		m.update()

		# set prev num nodes
		NumNodes[-1] = prevNumNodes
		
		# available green power in each epoch (given in a table)
		GPower = solartrace
		
		# brown prices
		BPrice = brownPrices
		
		# other variables
		BPower = {}
		BPowerLoad = {}
		#BPowerLoadInternal = {}
		#BPowerLoadCoefficient = {}
		
		BPowerBatt = {}
		#BPowerBattInternal = {}
		#BPowerBattCoefficient = {}
		
		GPowerLoad = {}
		
		GPowerBatt = {}
		#GPowerBattInternal = {}
		#GPowerBattCoefficient = {}
		
		GPowerNet = {}
		#GPowerNetInternal = {}
		#GPowerNetCoefficient = {}
		
		BattPowerLoad = {}
		#BattPowerLoadInternal = {}
		#BattPowerLoadCoefficient = {}
		
		LevelBatt = {}
		#TotalPowerBatt = {}
		#TotalPowerBattInternal = {}
		#TotalPowerBattCoefficient = {}
		
		# this comes in with the parameters
		LevelBatt[-1] = prevBattLevel
		
		b1 = {}
		b2 = {}
		b3 = {}
		b4 = {}
		b5 = {}
		
		for epoch in self.EPOCHS:
			BPower[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="BPower(%d)" % epoch)
			
			# need binary coefficient
			#BPowerLoadInternal[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="BPowerLoadInternal(%d)" % epoch)
			BPowerLoad[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="BPowerLoad(%d)" % epoch)
			#BPowerLoadCoefficient[epoch] = m.addVar(vtype=GRB.BINARY, name="BPowerLoadCoefficient(%d)" % epoch)
						
			# need binary coefficient
			#BPowerBattInternal[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="BPowerBattInternal(%d)" % epoch)
			BPowerBatt[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="BPowerBatt(%d)" % epoch)
			#BPowerBattCoefficient[epoch] = m.addVar(vtype=GRB.BINARY, name="BPowerBattCoefficient(%d)" % epoch)

			GPowerLoad[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="GPowerLoad(%d)" % epoch)
			
			# need binary coefficient
			#GPowerBattInternal[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="GPowerBattInternal(%d)" % epoch)
			GPowerBatt[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="GPowerBatt(%d)" % epoch)
			#GPowerBattCoefficient[epoch] = m.addVar(vtype=GRB.BINARY, name="GPowerBattCoefficient(%d)" % epoch)
			
			# need binary coefficient
			#GPowerNetInternal[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="GPowerNetInternal(%d)" % epoch)
			GPowerNet[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="GPowerNet(%d)" % epoch)
			#GPowerNetCoefficient[epoch] = m.addVar(vtype=GRB.BINARY, name="GPowerNetCoefficient(%d)" % epoch)
			
			# need binary coefficient
			#BattPowerLoadInternal[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="BattPowerLoadInternal(%d)" % epoch)
			BattPowerLoad[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="BattPowerLoad(%d)" % epoch)
			#BattPowerLoadCoefficient[epoch] = m.addVar(vtype=GRB.BINARY, name="BattPowerLoadCoefficient(%d)" % epoch)
			
			# total battery power
			#TotalPowerBattInternal[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="TotalPowerBattInternal(%d)" % epoch)  
			#TotalPowerBatt[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="TotalPowerBatt(%d)" % epoch)
			#TotalPowerBattCoefficient[epoch] = m.addVar(vtype=GRB.BINARY, name="TotalPowerBattCoefficient(%d)" % epoch)
			
			LevelBatt[epoch] = m.addVar(vtype=GRB.CONTINUOUS, name="LevelBatt(%d)" % epoch)
			
			# coefficients to avoid quadratic constraints
			b1[epoch] = m.addVar(vtype=GRB.BINARY, name="b1(%d)" % epoch)
			b2[epoch] = m.addVar(vtype=GRB.BINARY, name="b2(%d)" % epoch)
			b3[epoch] = m.addVar(vtype=GRB.BINARY, name="b3(%d)" % epoch)
			b4[epoch] = m.addVar(vtype=GRB.BINARY, name="b4(%d)" % epoch)
			b5[epoch] = m.addVar(vtype=GRB.BINARY, name="b5(%d)" % epoch)
			
		m.update()
		
		# set up constraints
		for epoch in self.EPOCHS:
			# compound variables
			'''
			m.addQConstr(BPowerLoad[epoch] == BPowerLoadInternal[epoch] * BPowerLoadCoefficient[epoch])
			m.addQConstr(BPowerBatt[epoch] == BPowerBattInternal[epoch] * BPowerBattCoefficient[epoch])
			m.addQConstr(GPowerBatt[epoch] == GPowerBattInternal[epoch] * GPowerBattCoefficient[epoch])
			m.addQConstr(GPowerNet[epoch] == GPowerNetInternal[epoch] * GPowerNetCoefficient[epoch])
			m.addQConstr(BattPowerLoad[epoch] == BattPowerLoadInternal[epoch] * BattPowerLoadCoefficient[epoch])
			#####m.addConstr(TotalPowerBatt[epoch] == BPowerBatt[epoch] + GPowerBatt[epoch])
			m.addQConstr(TotalPowerBatt[epoch] == TotalPowerBattInternal[epoch] * TotalPowerBattCoefficient[epoch])
			'''
			# total batt power
			#m.addConstr(TotalPowerBatt[epoch] == BPowerBatt[epoch] + GPowerBatt[epoch])
			
			# definition of brown power to load
			m.addConstr(BPowerLoad[epoch] == PowerLoad[epoch] - GPowerLoad[epoch] - BattPowerLoad[epoch])
			# definition of total brown power
			m.addConstr(BPower[epoch] == BPowerLoad[epoch] + BPowerBatt[epoch])
			# constraint on green usage (all sinks must be <= supply)
			m.addConstr(GPowerLoad[epoch] + GPowerBatt[epoch] + GPowerNet[epoch] <= GPower[epoch])
			# capacity of batteries in watt-hours, considering internal losses
			# this is charging/discharging
			m.addConstr(LevelBatt[epoch] == LevelBatt[epoch - 1] +
							GCParasolPowerManager.BATT_LOSS_FACTOR * (GPowerBatt[epoch] + BPowerBatt[epoch]) * self.EPOCH_LEN_HOURS -
							BattPowerLoad[epoch] * self.EPOCH_LEN_HOURS)
			# cannot consume more than we have in the batteries
			m.addConstr(BattPowerLoad[epoch] * self.EPOCH_LEN_HOURS <= LevelBatt[epoch])
			# cannot charge faster than max charge rate of the batteries
			m.addConstr(GPowerBatt[epoch] + BPowerBatt[epoch] <= GCParasolPowerManager.MAX_RATE_CHARGE_BATT)
			# cannot discharge faster than max discharge rate of the batteries
			m.addConstr(BattPowerLoad[epoch] <= GCParasolPowerManager.MAX_RATE_DISCHARGE_BATT)
			# cannot charge the battery past the capacity
			m.addConstr(LevelBatt[epoch] <= GCParasolPowerManager.MAX_LEVEL_BATT)
			# cannot discharge the battery past the low bound
			m.addConstr(LevelBatt[epoch] >= self.MIN_BATTERY_LEVEL * GCParasolPowerManager.MAX_LEVEL_BATT)
			
			#x>=0
			#y>=0
			#x<=b*M
			#y<=(1-b)*M 
			M = 3200 #sys.float_info.max
			
			# cannot charge the battery with green and do netmetering at the same time
			#m.addConstr(GPowerBattCoefficient[epoch] + GPowerNetCoefficient[epoch] <= 1)
			m.addConstr(GPowerBatt[epoch] >= 0)
			m.addConstr(GPowerNet[epoch] >= 0)
			m.addConstr(GPowerBatt[epoch] <= b1[epoch] * M)
			m.addConstr(GPowerNet[epoch] <= (1 - b1[epoch]) * M)
			
			# cannot charge the battery with brown and do netmetering at the same time
			#m.addConstr(BPowerBattCoefficient[epoch] + GPowerNetCoefficient[epoch] <= 1)
			m.addConstr(BPowerBatt[epoch] >= 0)
			m.addConstr(GPowerNet[epoch] >= 0)
			m.addConstr(BPowerBatt[epoch] <= b2[epoch] * M)
			m.addConstr(GPowerNet[epoch] <= (1 - b2[epoch]) * M)
			
			# cannot use the battery and do netmetering at the same time
			#m.addConstr(BattPowerLoadCoefficient[epoch] + GPowerNetCoefficient[epoch] <=1)
			m.addConstr(BattPowerLoad[epoch] >= 0)
			m.addConstr(GPowerNet[epoch] >= 0)
			m.addConstr(BattPowerLoad[epoch] <= b3[epoch] * M)
			m.addConstr(GPowerNet[epoch] <= (1 - b3[epoch]) * M)
			
			# cannot use the grid and do netmetering at the same time
			#m.addConstr(BPowerLoadCoefficient[epoch] + GPowerNetCoefficient[epoch] <= 1)
			m.addConstr(BPowerLoad[epoch] >= 0)
			m.addConstr(GPowerNet[epoch] >= 0)
			m.addConstr(BPowerLoad[epoch] <= b4[epoch] * M)
			m.addConstr(GPowerNet[epoch] <= (1 - b4[epoch]) * M)
						
			# cannot charge and discharge the batteries at the same time
			#m.addConstr(BattPowerLoadCoefficient[epoch] + (GPowerBattCoefficient[epoch] + BPowerBattCoefficient[epoch]) == 0)
			#m.addConstr(BattPowerLoadCoefficient[epoch] + TotalPowerBattCoefficient[epoch] <= 1)
			m.addConstr(BattPowerLoad[epoch] >= 0)
			m.addConstr(GPowerBatt[epoch] + BPowerBatt[epoch] >= 0)
			m.addConstr(BattPowerLoad[epoch] <= b5[epoch] * M)
			m.addConstr(GPowerBatt[epoch] + BPowerBatt[epoch] <= (1 - b5[epoch]) * M)
			
		m.update()
		
		# Optimization function
		self.logger.info("Adding constraints optimization")
		
		# new cost function
		cost = 0
		for epoch in self.EPOCHS:
			epochCost = (BPower[epoch] * BPrice[epoch]) - (GPowerNet[epoch] * self.NET_METER_FACTOR*BPrice[epoch])
			cost += epochCost
			
		'''
		cost = 0
		# Energy costs
		for epoch in self.EPOCHS:
			for node in self.NODES:
				# Brown used
				energyreq = node*self.POWER_NODE - solartrace[epoch]
				if energyreq < 0:
					energyreq = 0
				cost += brownPrices[epoch]*energyreq*schedule[epoch, node]
				# Green surplus
				greensurplus = solartrace[epoch] - node*self.POWER_NODE
				if greensurplus < 0:
					greensurplus = 0
				cost += greensurplus*schedule[epoch, node]
		'''
		# TODO green is free! solartrace[hour], node*POWER_NODE
		
		m.setObjective(cost, GRB.MINIMIZE)
		m.update()
		m.setParam("OutputFlag", 1)
		m.setParam("TimeLimit", 200)
		m.setParam("Threads", 2)
		m.update()
		m.write("/tmp/model.lp")
		
		self.logger.info("Starting optimizer...")
		startTime = datetime.now()
		m.optimize()
		self.logger.info("Optimization time: %s" % str(datetime.now() - startTime))

		if m.status == GRB.status.INFEASIBLE or m.status == GRB.status.UNBOUNDED:
			self.logger.info("Cannot solve, returning none")
			rqueue.put(None)
		elif m.status == GRB.status.TIME_LIMIT:
			self.logger.info("Exceeded time limit")
		
		# Solution
		'''
		totalnodes = 0.0
		for epoch in self.EPOCHS:
			for node in self.NODES:
				if schedule[epoch, node] != 0 and schedule[epoch, node].x > 0.1:
					ret[epoch] = node
		'''
		# new solution
		for epoch in self.EPOCHS:
			ret[epoch] = {}
			ret[epoch]["NumNodes"] = int(NumNodes[epoch].X)
			ret[epoch]["PowerLoad"] = PowerLoad[epoch].X
			ret[epoch]["BPower"] = BPower[epoch].X
			ret[epoch]["BPowerLoad"] = BPowerLoad[epoch].X
			ret[epoch]["BPowerBatt"] = BPowerBatt[epoch].X
			ret[epoch]["GPower"] = GPower[epoch] # not a gurobi variable
			ret[epoch]["GPowerLoad"] = GPowerLoad[epoch].X
			ret[epoch]["GPowerBatt"] = GPowerBatt[epoch].X
			ret[epoch]["GPowerNet"] = GPowerNet[epoch].X
			ret[epoch]["BattPowerLoad"] = BattPowerLoad[epoch].X
			
		rqueue.put(ret)
		if self.runCount == 0:
			rqueue.put(cdfs)
		return
