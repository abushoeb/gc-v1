#!/usr/bin/python

from gcmodel import *
import matplotlib.pyplot as pyplot

def ops_sec_node(workload, write_coefficient, total_nodes, replication_factor):
	return (workload * write_coefficient) / (total_nodes / replication_factor)

def extra_ops_sec_node_old(workload, write_coefficient, total_nodes, replication_factor, off_nodes, min_nodes, opt_nodes, opt_replication_factor):
	W = ops_sec_node(workload, write_coefficient, total_nodes, replication_factor)
	
	return (min(off_nodes, (opt_nodes / opt_replication_factor)) * W) / min_nodes

def extra_ops_sec_node(workload, write_coefficient, total_nodes, replication_factor, off_nodes, min_nodes, opt_nodes, opt_replication_factor):
	W = ops_sec_node(workload, write_coefficient, total_nodes, replication_factor)
	
	return (off_nodes * W) / min_nodes

if __name__ == "__main__":
	
	#model = GCTableResponseTimeModelNew("3-16-ZIPFIAN.model")
	model = GCTableResponseTimeModelNew("2-26-MODEL-ADJUSTED.model")
	
	LATENCY_TARGET = 75
	MIN_NODES = 9
	OPT_NODES = 18
	TOTAL_NODES = MIN_NODES + OPT_NODES
	MIN_REP_FACTOR = 1
	OPT_REP_FACTOR = 2
	TOTAL_REP_FACTOR = MIN_REP_FACTOR + OPT_REP_FACTOR
	WRITE_FRAC = 0.25
	
	xs = []
	ys = []
	
	for i in xrange(MIN_NODES, TOTAL_NODES+1):
		#max_workload = model.peakThroughput(i, LATENCY_TARGET)
		max_workload = 2000
		W = ops_sec_node(max_workload, WRITE_FRAC, TOTAL_NODES, TOTAL_REP_FACTOR)
		EW = extra_ops_sec_node(max_workload, WRITE_FRAC, TOTAL_NODES, TOTAL_REP_FACTOR, TOTAL_NODES-i, MIN_NODES, OPT_NODES, OPT_REP_FACTOR)
		print "%d: %0.2f ops/sec" % (i, W+EW)
		
		xs.append(i)
		ys.append(W+EW)
		
	pyplot.plot(xs, ys)
	pyplot.savefig("gcopsmodel.pdf", format="pdf")	
		
