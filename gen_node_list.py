#!/usr/bin/python

import sys
from gccommon import *

f_placement = open("gc.placement", "w")

for i in xrange(0, len(MINIMUM_NODES)):
	f_placement.write("%s m\n" % MINIMUM_NODES[i])

for i in xrange(0, len(OPTIONAL_NODES)):
	f_placement.write("%s o\n" % OPTIONAL_NODES[i])

f_placement.close()

