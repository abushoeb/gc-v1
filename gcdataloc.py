#!/usr/bin/python

from gccommon import *
from datetime import datetime
from datetime import timedelta
import os
import shutil

class GCExperimentPath(object):
	
	def __init__(self, base_path):
		self.base_path = base_path
		
	def getPath(self):
		raise Exception("This class is intended to be subclassed...")
	
	def makeDirs(self, force=False):
		path = self.getPath()
		
		if os.path.exists(path) and not force:
			raise Exception("Path already exists: %s" % path)
		
		os.makedirs(self.getPath())
		
class GCTempExperimentPath(GCExperimentPath):
	def __init__(self, experiment_name):
		super(GCTempExperimentPath, self).__init__(RESULTS_DIR + "/tmp")
		self.experiment_name = experiment_name
		
		if os.path.exists(self.base_path):
			shutil.rmtree(self.base_path)
	
	def getPath(self):
		return self.base_path + "/" + self.experiment_name
		
class GCLegacyExperimentPath(GCExperimentPath):
	
	def __init__(self, base_path, experiment_name):
		super(GCLegacyExperimentPath, self).__init__(base_path)
		
		self.experiment_name = experiment_name
		
	def getPath(self):
		return self.base_path + "/" + self.experiment_name

class GCFancyExperimentPath(GCExperimentPath):
	
	def __init__(self, base_path, id_tuple):
		super(GCFancyExperimentPath, self).__init__(base_path)
		
		self.id_tuple = id_tuple
		
	def getPrefix(self):
		prefix = self.base_path + "/"
		for part in self.id_tuple:
			prefix += part + "/"
		
		return prefix
	
	def getPath(self):
		return self.getPrefix() + self.experiment_id
		
	def getNewExperimentId(self, alternative=None):
		now = datetime.now()		
		prefix = self.getPrefix()
		
		#base = datetime.strftime(now, "%m-%d")
		base = "%d-%d" % (now.month, now.day)
		suffix = ""
		
		path = prefix + "/%s%s"
		nextId = 2
		
		while os.path.exists(path % (base, suffix)):
			suffix = "_%d" % nextId
			nextId += 1
		
		return base + suffix

class GCOrganizedExperimentPath(GCExperimentPath):
	
	def __init__(self, base_path, id_tuple, experiment_id=None):
		super(GCOrganizedExperimentPath, self).__init__(base_path)
		
		self.id_tuple = id_tuple
		
		if experiment_id == None:
			self.experiment_id = self.getFinalExperimentId(self.getDateExperimentId())
		else:
			self.experiment_id = self.getFinalExperimentId(experiment_id)
		
	def getPrefix(self):
		prefix = self.base_path + "/"
		for part in self.id_tuple:
			prefix += part + "/"
		
		return prefix
	
	def getDateExperimentId(self):
		now = datetime.now()		
		base = "%d-%d" % (now.month, now.day)
		
		return base
		
	def getExperimentId(self):
		return self.experiment_id
			
	def getPath(self):
		return self.getPrefix() + self.experiment_id
	
	def makeDirs(self, force=False):
		path = self.getPath()
		
		if os.path.exists(path) and not force:
			raise Exception("Path already exists: %d" % path)
		
		os.makedirs(self.getPath())
		
	def getFinalExperimentId(self, experiment_id):
		prefix = self.getPrefix()
		base = experiment_id
		suffix = ""
		
		path = prefix + "/%s%s"
		nextId = 2
		
		while os.path.exists(path % (base, suffix)):
			suffix = "_%d" % nextId
			nextId += 1
		
		return base + suffix
	
if __name__ == "__main__":
	
	workload = "ask"
	sched = "opt"
	param = "test"
	
	path = GCOrganizedExperimentPath("testorg", (workload, sched, param))
	
	print path.getPath()
	path.makeDirs()