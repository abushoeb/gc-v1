#!//usr/bin/python

import ConfigParser
import os
import sys
import time
import paramiko
import socket
from threading import Thread
from threading import Semaphore
from threading import Lock
from Queue import Queue
from gccommon import *
from gchelpers import *
from gclogger import GCLogger
from gcbase import GCManagerBase
from gcutil import GCSingleton
from experimentclock import ExperimentClock
from datetime import datetime

class GCNodeOnInfo(object):
	NORMAL = 1
	DEADLINE = 2
	DONE = 4
	
	def __init__(self, node, method=NORMAL):
		self.node = node
		self.method = method
		self.lock = Lock()
	
	def __str__(self):
		return "GCNodeOnInfo: node=%s, method=%d" % (self.node, self.method)
	      
class GCClusterManager(GCManagerBase, GCSingleton):	

	CASSANDRA_NOOP		= 1
	CASSANDRA_KILL 		= 2
	CASSANDRA_SLEEP 	= 4
	CASSANDRA_SLEEP_S3 	= 8

	minimumNodes = []
	optionalNodes = []
	nodeIPs = {}
	nodeNamesByIP = {}
	clock = None
	logger = None
	poweroff = None

	# we need to protect this list!!!
	threads = []
	threadsLock = Lock()

	nodeOffTimes = {}

	RESTORE_GREENHINTS = 1
	RESTORE_ANTIENTROPY = 2
	RESTORE_NONE = 3

	ON_NORMAL = 1
	ON_DEADLINE = 2

	def __init__(self, nodes=(MINIMUM_NODES+OPTIONAL_NODES), mode=CASSANDRA_NOOP, simultaneous_on=len(OPTIONAL_NODES), early_on=False, no_repair=False, compaction_controller=None):
		super(GCClusterManager, self).__init__(GCClusterManager)
		
		self.logger = GCLogger.getInstance().get_logger(self)
		self.nodes = nodes
		self.mode = mode
		self.simultaneous_on = simultaneous_on
		self.early_on = early_on
		self.no_repair = no_repair
		self.on_semaphore = Semaphore(self.simultaneous_on)
		self.on_queue = []
		self.on_queue_lock = Lock()
		self.deadline_queue = []
		self.deadline_queue_lock = Lock()
		
		self.compaction_controller = compaction_controller
		
		minimum_nodes = [] #MINIMUM_NODES
		optional_nodes = [] #OPTIONAL_NODES
		
		for n in MINIMUM_NODES:
			if n in self.nodes:
				minimum_nodes.append(n)
		for n in OPTIONAL_NODES:
			if n in self.nodes:
				optional_nodes.append(n)
		
		all_nodes = minimum_nodes + optional_nodes
		self.clock = ExperimentClock.getInstance()
		
		self.logger.log("Getting node IPs...")
		for n in all_nodes:
			ip = socket.gethostbyname(n)
			self.nodeIPs[n] = ip
			self.nodeNamesByIP[ip] = n

		for n in minimum_nodes:			
			gcn = GCNode(n, self.nodeIPs[n], GCNodeRegion.MINIMUM, GCNodeState.ON)
			self.minimumNodes.append(gcn)
		for n in optional_nodes:			
			gcn = GCNode(n, self.nodeIPs[n], GCNodeRegion.OPTIONAL, GCNodeState.ON)
			self.optionalNodes.append(gcn)
		
		self.hostname = socket.gethostname()
	
	def getInfoForSolver(self):
		nodes = self.minimumNodes + self.optionalNodes
		nodeStates = {}
		offInfo = {}
		transInfo = {}
		
		for n in self.minimumNodes + self.optionalNodes:
			nodeStates[n.name] = n.state
			if n.state == GCNodeState.OFF:
				offInfo[n.name] = self.nodeSlotsOff(n.name)
			else:
				offInfo[n.name] = 0
			#TODO Check this			
			if n.state == GCNodeState.TRANSITION:
				transInfo[n.name] = (self.nodeSlotsOff(n.name) / 4) - self.nodeSlotsInTransition(n.name) 
			else:
				transInfo[n.name] = 0
		
		return nodes, nodeStates, offInfo, transInfo
	
	def getNodeStates(self):
		nodes = self.minimumNodes + self.optionalNodes
		nodeStates = {}
		nodeStateFlags = {}
		for n in nodes:
			nodeStates[n.name] = n.state
			nodeStateFlags[n.name] = n.stateFlags
		
		nodeOffTimes = dict(self.nodeOffTimes)
		
		return nodeStates, nodeStateFlags, nodeOffTimes
	
	def clusterQuickstartThread(self, node):
		self.logger.log("Starting Cassandra on node %s" % node)
		start_cassandra_node(node)
		cassandra_nodetool_direct(node, "gcnodetoready")
		self.logger.log("Completed starting Cassandra on node %s" % node)
		
	def clusterQuickstart(self):
		self.logger.log("Initiating cluster quickstart")
		threads = []
		allNodes = (MINIMUM_NODES + OPTIONAL_NODES)
		
		for node in allNodes:
			thread = Thread(target=self.clusterQuickstartThread, args=(node,))
			threads.append(thread)
			thread.start()
		
		for thread in threads:
			thread.join()
			
		self.logger.log("Completed cluster quickstart")
	
	def clusterKillThread(self, node):
		self.logger.log("Killing Cassandra on node %s" % node)
		kill_cassandra_node(node)
		self.logger.log("Completed killing Cassandra on node %s" % node)
		
	def clusterKill(self):
		self.logger.log("Initiating cluster kill")
		threads = []
		allNodes = (MINIMUM_NODES + OPTIONAL_NODES)
		
		for node in allNodes:
			thread = Thread(target=self.clusterKillThread, args=(node,))
			threads.append(thread)
			thread.start()
		
		for thread in threads:
			thread.join()
			
		self.logger.log("Completed cluster kill")
	
	def nodeResetThread(self, node):
		self.logger.log("Deleting all greenhints on node %s" % node)
		cassandra_nodetool_direct(node, "gcdeletehints")
		self.logger.log("Completed deletion of greenhints on node %s" % node)
		
		self.logger.log("Compacting usertable on node %s" % node)
		cassandra_nodetool_direct(node, "compact usertable")
		self.logger.log("Completed compaction of usertable on node %s" % node)
		
		self.logger.log("Compacting system on node %s" % node)
		cassandra_nodetool_direct(node, "compact system")
		self.logger.log("Completed compaction of system on node %s" % node)
		
	def resetCluster(self):
		self.logger.log("Initiating cluster reset")
		
		self.clusterQuickstart()
		
		threads = []
		allNodes = (MINIMUM_NODES + OPTIONAL_NODES)
		for node in allNodes:
			thread = Thread(target=self.nodeResetThread, args=(node,))
			threads.append(thread)
			thread.start()
		
		for thread in threads:
			thread.join()
			
		self.clusterKill()
		self.clusterQuickstart()
		
		self.logger.log("Completed cluster reset")
	
	def nodePowerOff(self, node):
		if self.mode == self.CASSANDRA_SLEEP:
			sleep_cassandra_node(node)
		elif self.mode == self.CASSANDRA_SLEEP_S3:
			sleep_cassandra_node(node)
			parasol_s3(EXECUTE_NODE, node)
		elif self.mode == self.CASSANDRA_KILL:
			kill_cassandra_node(node)
		elif self.mode == self.CASSANDRA_NOOP:
			#print "Cluster manager configured as NOOP, NOT turning off %s" % node
			pass

		# update our state
		gcn = self.findNode(node)
		gcn.state = GCNodeState.OFF
		gcn.offTime = self.clock.get_current_time()	
		self.logger.log("Node %s off" % node)
	
	def nodePowerOn(self, node):
		if self.mode == self.CASSANDRA_SLEEP:
			wake_cassandra_node(node)
		elif self.mode == self.CASSANDRA_SLEEP_S3:
			wake_cassandra_node(node)
			parasol_wake(EXECUTE_NODE, node)
			parasol_wake(EXECUTE_NODE, node)
		elif self.mode == self.CASSANDRA_KILL:
			start_cassandra_node(node)
		elif self.mode == self.CASSANDRA_NOOP:
			#print "Cluster manager configured as NOOP, NOT turning on %s" % node
			#time.sleep(1)
			pass
	
	def nodeOnThreadOld(self, node, method):
		self.logger.log("Starting node on thread for node %s" % node)
		self.transitionSlotsRequired = self.nodeSlotsOff(node) / 4
		self.nodePowerOn(node)
		
		gcn = self.findNode(node)
		if gcn in self.minimumNodes:
			gcn.state = GCNodeState.ON
			return
		
		gcn.transitionTime = self.clock.get_current_time()
		
		# sleep to allow the node time to come up and start gossiping
		#time.sleep(60)
		
		self.logger.log("Node %s on" % node)

		if self.mode != self.CASSANDRA_NOOP:
			## if we are in optional set
			if method == self.RESTORE_ANTIENTROPY:
				self.nodeAIRepair(node)
			elif method == self.RESTORE_GREENHINTS:
				self.nodeGCRepair(node)
				self.nodeGCToReady(node)

		self.logger.log("Node %s repaired" % node)
	
	def nodeOnThread(self):
		# get the semaphore
		self.on_semaphore.acquire()

		# see who we are turning on
		self.on_queue_lock.acquire()
		info = self.on_queue.pop(0)
		self.on_queue_lock.release()

		node = info.node
		gcn = self.findNode(node)
		
		self.logger.log("In nodeOnThread for node %s" % node)
		self.logger.log("Acquired semaphore for turn on of node %s" % node)
		
		info.lock.acquire()
		if info.method == GCNodeOnInfo.NORMAL:
			info.lock.release()
			self.logger.log("Using NORMAL turnon method")
			
			# if we didn't do early_on, then we have to turn the node on now
			if not self.early_on:
				self.nodePowerOn(node)
				self.logger.log("Node %s is on (normal)" % node)
				# put the node into TRANSITION here
				if gcn.region == GCNodeRegion.OPTIONAL:
					gcn.state = GCNodeState.TRANSITION
			
			# if we are a minimum node, we are done
			if gcn in self.minimumNodes:
				gcn.state = GCNodeState.ON
				self.on_semaphore.release()
				return
			# otherwise
			gcn.transitionTime = self.clock.get_current_time()
			
			# start compaction thread
			if self.compaction_controller:
				compactionThread = self.compaction_controller.startNodeThread(node)
			
			# start greenhint repair
			if not self.no_repair:
				self.nodeGCRepair(node)
					
			if self.compaction_controller:
				# signal the thread that the node is on (green hints delivered)
				compactionThread.signalNodeRepaired()
			
			# we can start another recovery now
			self.on_semaphore.release()
			
			if self.compaction_controller:
				# wait for the initial compactions to finish
				compactionThread.waitForCompactionComplete()
			
			# send the node to ready
			self.nodeGCToReady(node)
			
			# clean up
			# TODO: see why an issue happened here
			try:
				del self.nodeOffTimes[node]
			except:
				pass
			gcn.stateFlags = GCNodeStateFlags.NONE
			
			if not self.compaction_controller:
				self.on_semaphore.release()
				
			self.logger.log("Node %s repaired" % node)
		
		elif info.method == GCNodeOnInfo.DEADLINE:
			info.lock.release()
			
			self.deadline_queue_lock.acquire()
			self.deadline_queue.append(info)
			self.deadline_queue_lock.release()

			self.logger.log("USING DEADLINE turnon method")
			self.logger.log("Deadline queue: %s" % str(self.deadline_queue))
			
			self.nodePowerOn(node)
			self.logger.log("Node %s is on" % node)
			
			gcn = self.findNode(node)
			if gcn.region == GCNodeRegion.OPTIONAL:
				gcn.state = GCNodeState.TRANSITION
			
			gcn.transitionTime = self.clock.get_current_time()
			self.nodeGCRepair(node)
			self.nodeGCToReady(node)
			gcn.stateFlags = GCNodeStateFlags.NONE
			
			info.lock.acquire()
			# if we are still going for deadline, turn off
			if info.method == GCNodeOnInfo.DEADLINE:
				self.nodeOff(node)
				info.method = GCNodeOnInfo.DONE
			info.lock.release()
			
			self.deadline_queue_lock.acquire()
			self.deadline_queue.remove(info)
			self.deadline_queue_lock.release()
			
			# release the semaphore
			self.on_semaphore.release()
			self.logger.log("Node %s repaired and turned back off" % node)
		
	def nodeOnOld(self, node, method=RESTORE_GREENHINTS, delay=0):
		gcn = self.findNode(node)
		
		if gcn.state == GCNodeState.ON:
			self.logger.log("Node %s already on" % node)
			return
		
		self.logger.log("Initiating node turn on for %s" % node)
		self.on_queue_lock.acquire()
		self.on_queue.append(node)
		self.on_queue_lock.release()		
		
		#thread = Thread(target=self.nodeOnThread, args=(node, method))
		thread = Thread(target=self.nodeOnThread, args=(node, method))
		with self.threadsLock:
			self.threads.append(thread)
		thread.start()
		
		#if gcn.region == GCNodeRegion.OPTIONAL:
		#	gcn.state = GCNodeState.TRANSITION
			
		#if delay > 0 and self.mode != self.CASSANDRA_NOOP:
		#	time.sleep(delay)
	
	def nodeOn(self, node, method=ON_NORMAL):	
	#def nodeOn(self, node, stateFlag=GCNodeStateFlags.NONE):
		gcn = self.findNode(node)
		if gcn.state == GCNodeState.ON:
			self.logger.log("Node %s already on" % node)
			return
		
		self.logger.log("Initiating node turn on for %s" % node)
		
		if method == self.ON_NORMAL:
			info = GCNodeOnInfo(node, GCNodeOnInfo.NORMAL)
			flags = GCNodeStateFlags.NONE
		elif method == self.ON_DEADLINE:
			info = GCNodeOnInfo(node, GCNodeOnInfo.DEADLINE)
			flags = GCNodeStateFlags.DEADLINE
		
		self.on_queue_lock.acquire()
		self.on_queue.append(info)
		self.on_queue_lock.release()
		
		if self.early_on and method == self.ON_NORMAL:
			self.nodePowerOn(node)
			self.logger.log("Node %s is on (early_on)" % node)
			gcn.state = GCNodeState.TRANSITION
		else:		
			gcn.state = GCNodeState.SCHEDULED

		gcn.stateFlags = flags
		thread = Thread(target=self.nodeOnThread)
		with self.threadsLock:
			self.threads.append(thread)
		thread.daemon = True
		thread.start()
	
	def tryToPromoteDeadlineNode(self, insteadOf):
		
		self.logger.log("Trying to promote deadline node instead of %s" % insteadOf)
		  
		evenNodes = []
		oddNodes = []
		length = len(OPTIONAL_NODES)
		for i in range(0, length, 2):
			evenNodes.append(OPTIONAL_NODES[i])
		for i in range(1, length, 2):
			oddNodes.append(OPTIONAL_NODES[i])
		
		
		self.deadline_queue_lock.acquire()
		self.logger.log("Got deadline queue lock")
		self.logger.log("Even nodes: %s" % str(evenNodes))
		self.logger.log("Odd nodes: %s" % str(oddNodes))
		self.logger.log("Deadline queue: %s" % str(self.deadline_queue))
		
		if insteadOf in evenNodes:
			self.logger.log("replacing even")
			firstList = evenNodes
			secondList = oddNodes
		elif insteadOf in oddNodes:
			self.logger.log("replacing odd")
			firstList = oddNodes
			secondList = evenNodes
		
		for info in self.deadline_queue:
			info.lock.acquire()
			print "Considering1", info
			if info.node in firstList and info.method == GCNodeOnInfo.DEADLINE:
				info.method = GCNodeOnInfo.NORMAL
				info.lock.release()
				self.deadline_queue_lock.release()
				self.logger.log("Promoted %s" % info.node)
				return True
			info.lock.release()
		
		for info in self.deadline_queue:
			info.lock.acquire()
			print "Considering2", info
			if info.node in secondList and info.method == GCNodeOnInfo.DEADLINE:
				info.method = GCNodeOnInfo.NORMAL
				info.lock.release()
				self.deadline_queue_lock.release()
				self.logger.log("Promoted %s" % info.node)
				return True
			info.lock.release()
		
		self.deadline_queue_lock.release()
		self.logger.log("Exited without promoting anyone")
		
		return False
	
	
	def nodeOffThread(self, node):
		self.nodeOffTimes[node] = self.clock.get_current_time()
		self.nodePowerOff(node)
		
		if self.compaction_controller:
			self.compaction_controller.stopNodeThread(node)
		
	def nodeOff(self, node, delay=0):
		gcn = self.findNode(node)
			
		if gcn.state == GCNodeState.OFF:
			self.logger.log("Node %s already off" % node)
			return
			
		self.logger.log("Initiating node turn off for %s" % node)
		#self.nodePowerOff(node)
		gcn.state = GCNodeState.OFF
		thread = Thread(target=self.nodeOffThread, args=(node,))
		with self.threadsLock:
			self.threads.append(thread)
		thread.daemon = True
		thread.start()
		
		if delay > 0 and self.mode != self.CASSANDRA_NOOP:
			time.sleep(delay)

	def waitForTransitions(self):
		# we should only wait for transitions that were in process when
		# this was called!

		# make a copy of the list of currently pending threads
		# then, empty the list
		with self.threadsLock:
			threadsCopy = list(self.threads)
			self.threads = []

		for thread in threadsCopy:
			thread.join()
			#self.threads.remove(thread)

	def setCompactionParameters(self, nodesOn):
		if self.compaction_controller:
			self.compaction_controller.setCompactionParameters(nodesOn)
	
	def nodeSlotsOff(self, node):
		gcn = self.findNode(node)
		if gcn.state != GCNodeState.OFF:
			return 0
		
		dt_now = self.clock.get_current_time()
		diff = dt_now - gcn.offTime
		diff_slots = int(diff.seconds / 60 / 15)
		
		return diff_slots

	def nodeSlotsInTransition(self, node):
		gcn = self.findNode(node)
		if gcn.state != GCNodeState.TRANSITION:
			return 0
		
		dt_now = self.clock.get_current_time()
		diff = dt_now - gcn.transitionTime
		diff_slots = int(diff.seconds / 60 / 15)
		
		return diff_slots
	
	def nodeAIRepair(self, node):
		self.logger.log("Starting antientropy repair for %s" % node)
		cmd = "nodetool -host %s repair" % node
		out, err = execute_remote_command_sync(EXECUTE_NODE, USERNAME, cmd)
		self.logger.log("Completed antientropy repair for %s" % node)

	def nodeGCRepair(self, node):
		if self.mode == self.CASSANDRA_NOOP:
			time.sleep(70)
			self.logger.log("greenhint repair ordered for %s, but in NOOP mode" % node)
			return
		
		self.logger.log("Starting greenhint repair for %s" % node)
#		cmds = []
#		for n in self.minimumNodes:
#			cmd = "nodetool -host %s gcdeliverhints %s" % (n.name, node)
#			cmds.append(cmd)
#		out, err = execute_remote_commands_parallel(EXECUTE_NODE, USERNAME, cmds)
		
		#threads = {}
		#if self.compaction_controller:
		#	for minNode in MINIMUM_NODES:
		#		threads[minNode] = self.compaction_controller.createMinOnThread(minNode, node)
		
		self.logger.log("executing cassanda_deliver_greenhints for %s" % node)
		
		cmd = "cassandra_deliver_greenhints %s" % node
		out, err = execute_remote_command_sync(node, USERNAME, cmd)
		self.logger.log("cassandra_deliver_greenhints [1] results: out %s, err %s" % (str(out), str(err)))
		
		#out, err = execute_remote_command_sync(node, USERNAME, cmd)
		#self.logger.log("cassandra_deliver_greenhints [2] results: out %s, err %s" % (str(out), str(err)))
		
		#if self.compaction_controller:
		#	for minNode in MINIMUM_NODES:
		#		#self.compaction_controller.waitOnTruncateFutures(minNode, node)
		#		threads[minNode].signalDone()
				
		'''
		print out
		print err
		f = open("/tmp/debugfile", "w")
		for line in out:
			f.write(line + "\n")
		for line in err:
			f.write(line + "\n")

		#print out
		#time.sleep(10)
		'''
		'''
		out, err = execute_remote_command_sync(node, USERNAME, cmd)
		print out
		print err
		
		for line in out:
			f.write(line + "\n")
		for line in err:
			f.write(line + "\n")
		f.close()
		'''
		self.logger.log("Completed greenhint repair for %s" % node)

	def nodeGCToReady(self, node):
		self.logger.log("Bringing optional node to ready: %s" % node)

		if self.mode == self.CASSANDRA_NOOP:
			self.logger.log("gcnodetoready ordered for %s, but in NOOP mode" % node)
		else:
			cmd = "nodetool -host %s gcnodetoready" % node
			out, err = execute_remote_command_sync(node, USERNAME, cmd)
		
		gcn = self.findNode(node)
		gcn.state = GCNodeState.ON
		self.logger.log("Brought optional node to ready: %s" % node)

	def findNode(self, node):
		for n in self.minimumNodes:
			if n.name == node:
				return n
		for n in self.optionalNodes:
			if n.name == node:
				return n
		return None
	
	def nodeState(self, node):
		gcn = self.findNode(node)
		if gcn:
			return gcn.state
	
	def nodeOffTime(self, node):
		gcn = self.findNode(node)
		if gcn:
			return gcn.offTime
	
	def nodeRegion(self, node):
		gcn = self.findNode(node)
		if gcn:
			return gcn.region

	def getValuesForLogging(self):
		values = {}
		#total_on = 0
		
		for gcn in self.minimumNodes + self.optionalNodes:
		#	if gcn.state != GCNodeState.OFF:
		#		total_on += 1
			key = "state.%s" % (gcn.name)
			values[key] = gcn.state
		
		#values["nodes_on"] = total_on
		return values
	
	def shutdown(self, turnOn=True):
		if turnOn:
			self.logger.log("Shutting down GCClusterManager, turning OFF nodes back on...")
			print "Shutting down GCClusterManager, turning OFF nodes back on..."
			
			self.logger.log("Waiting for already started transitions")
			print "Waiting for already started transitions"
			self.waitForTransitions()
			print "...done"
			
			self.logger.log("Turning on all remaining nodes")
			print "Turning on all remaining nodes..."
			size = len(OPTIONAL_NODES)
			self.logger.debug("Replacing on_semaphore with new Semaphore of size %d" % size)
			self.on_semaphore = Semaphore(size)
			
			for node in OPTIONAL_NODES:
				self.nodeOn(node)
			
			self.waitForTransitions()
			print "...done"
			
'''	
	def get_ring_info(self, host):
		ringInfo = {}
		
		cmd = NODETOOL_PATH + " ring"
		out, err = execute_remote_command_sync(host, USERNAME, cmd)
		out.pop(0)
		out.pop(0)
		for line in out:
			line = line.replace("  ", " ")
			fields = line.split(" ")
			goodFields = []
			for f in fields:
				if f != '':
					goodFields.append(f)
			
			fieldDict = {}
			fieldDict["ip"] = goodFields[0]
			fieldDict["dc"] = goodFields[1]
			fieldDict["region"] = goodFields[2]
			fieldDict["status"] = goodFields[3]
			ringInfo[self.nodeNamesByIP[goodFields[0]]] = fieldDict
		return ringInfo
	
	def get_gc_info(self, host):
		gcInfo = {}
		
		cmd = NODETOOL_PATH + " gossipinfo"
		out, err = execute_remote_command_sync(host, USERNAME, cmd)
		wholeout = ""
		
		for line in out:
			if "/" in line:
				ip = line.split("/")[1]
			if "GREENCASSANDRA_STATUS" in line:
				status = line.strip().split(":")[1]
				gcInfo[self.nodeNamesByIP[ip]] = status
		return gcInfo
		
'''

if __name__ == "__main__":
	clock = ExperimentClock(start_time=datetime.now())
	main_logger = GCLogger("gcclustermanager-test.log", GCLogger.DEBUG, clock)
	manager = GCClusterManager(simultaneous_on=2)
	
	for node in OPTIONAL_NODES:
		manager.nodeOff(node)
	
	for node in OPTIONAL_NODES:
		manager.nodeOn(node)
		
	manager.waitForTransitions()
	
	
