#!/usr/bin/python

from gcycsbplot import *

if __name__ == "__main__":
	plotter = GCPlotter()
	cdf = CDFPlot(title="Read Latencies")
	
	min_files = [
		"latency.readonly.gc-min.ONE.0",		
		"latency.readonly.gc-min.QUORUM.0",
		"latency.readonly.gc-min.ALL.0"
	]
	
	all_files = [
		"latency.readonly.gc-all.ONE.0", 
		"latency.readonly.gc-all.QUORUM.0", 
		"latency.readonly.gc-all.ALL.0", 
	]
		
	file_lists = [min_files, all_files]
	
	for fl in file_lists:
		cdf = CDFPlot()
		for f in fl:
			results = YCSBResults("/home/wkatsak/wonko_home/YCSB/results/%s" % f, transaction_type=YCSBResults.READ, max_bin=250)
			cdf.addResults(results, label=f)
		plotter.addPlot(cdf)
	
	plotter.doPlot()
	
