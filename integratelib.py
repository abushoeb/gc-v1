#!/usr/bin/python
import os
import sys
import numpy as np
import scipy
import matplotlib.pyplot as plot
from scipy import integrate

values = list()
numValues = list()

def isNumber(string):
	try:
		float(string)
		return True
	except ValueError:
		return False

def powerFunc(i):
	fracPart = i - int(i)
#	print i
	if fracPart > 0.50:
		index = int(i) + 1
	else:
		index = int(i)

	return numValues[index]

def integrate_power_file(input_file, baseline):
	f = open(input_file, 'r')
	data = f.read()
	lines = data.splitlines()
	start_line = 3

	for i in xrange(start_line, len(lines)):
		fields = lines[i].split(" ")
		if len(fields) >=3 and isNumber(fields[2]):
			values.append(fields[2])


	for i in xrange(len(values)):
		value = float(values[i])-baseline
		if value < 0.0:
			value = 0.0
		numValues.append(value)
	
	break_points = list()
	for i in xrange(len(values)):
		break_points.append(float(i))
	
	num_points = len(values)
	energy = integrate.quad(powerFunc, 0, len(values)-1,limit=1000000)[0]
	avg = energy / num_points

	return energy/3600.0, num_points, avg

