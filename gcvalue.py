#!/usr/bin/python

from gcanalysis import GCExperimentProcessor

class GCValue(GCExperimentProcessor):
	
	def __init__(self):
		super(GCValue, self).__init__([])
		
		parser = self.getParser()
		parser.add_argument("--last", action="store_true")
		group = parser.add_mutually_exclusive_group(required=True)
		group.add_argument("--common", action="store")
		group.add_argument("--node", nargs=2, action="store")
		group.add_argument("--available", action="store_true")
	
	def handleDt(self, args, dt, results):
		if args.common:
			value = results.getCommonValue(dt, args.common)
		elif args.node:
			node = args.node[0]
			name = args.node[1]
			value = results.getNodeValue(dt, node, name)
		
		print "%s %s" % (str(dt), str(value))
				
	def processExperiment(self, args, experiment, results, output):
		
		if args.available:
			print "COMMON:"
			for key in results.availableCommonValues():
				print "\t", key
			print "NODE:"
			for key in results.availableNodeValues():
				print "\t", key
			
		elif args.last:
			dt = results.availableTimestamps()[-1]
			self.handleDt(args, dt, results)
		else:
			for dt in results.availableTimestamps():
				self.handleDt(args, dt, results)
		
if __name__ == "__main__":
	status = GCValue()
	status.run()
