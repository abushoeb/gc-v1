#!/usr/bin/python

from gccommon import *
from gchelpers import *
from gcmodel import *
from gcworkloadpredictor import *
from gclogger import GCLogger

from scipy import stats
from datetime import datetime
from datetime import timedelta

def getMaxWorkloadForInterval(dt, delta, perfect, max_throughput):
	x = []
	y = []
	
	dtIter = dt
	while dtIter > dt - delta:
		x.append(dt_to_unix(dtIter))
		y.append(perfect.getWorkloadAt(dtIter))
		dtIter -= timedelta(seconds=5)
		
	slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
	
	targetUnix = dt_to_unix(dt + delta)
	result = slope * targetUnix + intercept
	
	return denormalize(result, max_throughput)

if __name__ == "__main__":
	logger = GCLogger("mylog.txt", GCLogger.DEBUG)
	
	SLA = 75
	#perfect = GCAskTraceWorkloadPredictor("ask-workload-4.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))
	perfect = GCMicrosoftTraceWorkloadPredictor("workload.csv", field=1)
	model = GCTableResponseTimeModelNew("MODEL-5-12-smart-EXTENDED-EXTENDED-MODIFIED.model")
	
	NUM_NODES = 27
	max_throughput = model.peakThroughput(NUM_NODES, SLA)
	
	start_dt = datetime(year=2013, month=10, day=20, hour=6, minute=0, second=0)
	
	dt = start_dt
	
	while dt < start_dt + timedelta(hours=24):
		
		current = denormalize(perfect.getWorkloadAt(dt), max_throughput)
		#future = getMaxWorkloadForInterval(dt, timedelta(minutes=15), perfect, max_throughput)
		future = denormalize(perfect.getMaxWorkloadForInterval(dt, timedelta(minutes=15)), max_throughput)
		
		nextTarget = max(current, future)
		nextNodes = model.nodesRequired(nextTarget, SLA)
		
		print "%s:" % datetime.strftime(dt, "%m-%d-%Y %H:%M")
		print "\tcurrent = %d, future = %d, nextTarget = %d, nodes = %d" % (current, future, nextTarget, nextNodes)
		
		dt += timedelta(minutes=15)
		
		
		
	

	
	
