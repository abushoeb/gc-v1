#!/usr/bin/python

from gcdataanalysis import *
from datetime import datetime
import sys

directory = sys.argv[1]


solar_func = DiscreteFunction(directory + "/used_solar")
power_func = DiscreteFunction(directory + "/est_node_power")


def timefunc(func, params):
	start = datetime.now()
	func(*params)
	print "Elapsed Time: %s\n" % str(datetime.now() - start)
	
def do_integrations(single_func, diff_func):
	green_produced = single_func(solar_func, name="solar") / 3600.00
	energy_consumed = single_func(power_func) / 3600.00
	brown_consumed = diff_func(power_func, solar_func) / 3600.00
	green_consumed = energy_consumed - brown_consumed
	green_wasted = green_produced - green_consumed
	
	print "Energy consumed: %0.2f Wh" % energy_consumed
	print "Brown consumed: %0.2f Wh" % brown_consumed
	print "Green consumed: %0.2f Wh" % green_consumed
	print "Green produced: %0.2f Wh" % green_produced
	print "Green wasted: %0.2f Wh" % green_wasted
	
	

#print "Discrete integrations:"
#timefunc(do_integrations, [integrate_single, integrate_difference])

print "Interpolated integrations:"
timefunc(do_integrations, [integrate_single_interpolated, integrate_difference_interpolated])

#print "Quad integrations:"
#timefunc(do_integrations, [integrate_single_quad, integrate_difference_quad])



