#!/usr/bin/python
import os
import sys
import numpy as np
import scipy
import matplotlib.pyplot as plot
import datetime
import argparse
from scipy import integrate

values = []
numValues = []

def isNumber(string):
	try:
		float(string)
		return True
	except ValueError:
		return False

def powerFunc(i):
	fracPart = i - int(i)
#	print i
	if fracPart > 0.50:
		index = int(i) + 1
	else:
		index = int(i)

	return numValues[index]

def parse_datetime(arg):
	data = arg.strip().split(" ")
	date = data[0]
	time = data[1].split(".")
	dt_string = date + " " + time[0]
	dt = datetime.datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")
	dt = dt.replace(microsecond=int(time[1]))
	return dt

def integrate_power_file(input_file, baseline, start_dt, end_dt):
	global values
	global numValues
	
	values = []
	numValues = []
	
	try:
		f = open(input_file, 'r')
	except IOError as err:
		print "File not found..."
		sys.exit(0)

	data = f.read()
	lines = data.splitlines()
	start_line = 0

	for i in xrange(start_line, len(lines)):
		fields = lines[i].split(" ")
		dt_string = fields[0] + " " + fields[1]
		dt = parse_datetime(dt_string)

		if len(fields) >=3 and isNumber(fields[2]) and (dt >=start_dt and dt <=end_dt):
			values.append(fields[2])

	for i in xrange(len(values)):
		value = float(values[i])-baseline
		if value < 0.0:
			value = 0.0
		numValues.append(value)
	
	break_points = list()
	for i in xrange(len(values)):
		break_points.append(float(i))
	
	num_points = len(values)
	if num_points == 0:
		print "No data points found..."
		sys.exit(0)

	energy = integrate.quad(powerFunc, 0, len(values)-1,limit=1000000)[0]
	avg = energy / num_points

	return energy/3600.0, num_points, avg


if __name__ == "__main__":
	parser = argparse.ArgumentParser()

	parser.add_argument("input_file", metavar="DATA_FILE", help="Data file name")
	parser.add_argument("--baseline", dest="baseline", metavar="BASELINE_POWER", action="store", help="Baseline power (integrate over this)")
	parser.add_argument("--starttime", dest="start_dt", metavar="START_TIME", action="store", help="Start time bound")
	parser.add_argument("--endtime", dest="end_dt", metavar="END_TIME", action="store", help="End time bound")
	parser.add_argument("--timeoffset", dest="timeoffset", metavar="TIME_OFFSET", action="store", help="Time offset (hours) in data file")

	args = parser.parse_args()

	input_file = args.input_file

	try:
		open(input_file, 'r')
	except IOError as err:
		print "Could not open file..."
		sys.exit(0)

	if args.baseline:
		baseline = float(args.baseline)
	else:
		baseline = 0.0

	if args.start_dt:
		try:
			start_dt = parse_datetime(args.start_dt)
		except ValueError as err:
			print "Bad parameter for starttime"
			sys.exit(0)
	else:
		start_dt = datetime.datetime.min

	if args.end_dt:
		try:
			end_dt = parse_datetime(args.end_dt)
		except ValueError as err:
			print "Bad parameter for endtime"
			sys.exit(0)
	else:
		end_dt = datetime.datetime.max

	if args.timeoffset:
		timeoffset = datetime.timedelta(hours=int(args.timeoffset))
	else:
		timeoffset = datetime.timedelta(hours=0)

	start_dt += timeoffset
	end_dt += timeoffset

	energy, num_points, avg = integrate_power_file(input_file, baseline, start_dt, end_dt)

	#avg = (energy*3600)/numPoints

	#plot.plot(numValues, 'g')
	#plot.show()

	print "File\t\tPoints\tEnergy\tPower"
	print "%s\t%d\t%.3f\t%.3f" % (os.path.split(input_file)[1], num_points, energy, avg)
	#print "Number of Data Points: " + str(num_points)
	#print "Total Energy: " + str(energy) + " wH"
	#print "Avg Watts Per Second: " + str(avg)
