#!/usr/bin/python

from threading import Lock
from gccommon import *
from gcbase import GCManagerBase
from gclogger import GCLogger, GCNullLogger
from datetime import datetime, timedelta
from experimentclock import ExperimentClock
from gcdatacollector import GCDataCollector
from gclogger import GCLogger

# need to simulate battery and netmetering, and how much green is used by servers
# vs. used for netmetering and charging battery

class GCParasolPowerManager(GCManagerBase, GCSingleton):

	# structure to hold all allocated values at one cycle
	class GCParasolPowerAllocation(object):		
		def __init__(self):
			self.GPower = 0.0
			self.PowerLoad = 0.0
			self.BPower = 0.0
			self.BPowerLoad = 0.0
			self.BPowerBatt = 0.0
			self.GPowerLoad = 0.0
			self.GPowerBatt = 0.0
			self.GPowerNet = 0.0
			self.BattPowerLoad = 0.0
			self.GPowerUnused = 0.0

			self.unsatisfiedPowerLoad = 0.0
			self.remainingGreenPower = 0.0
	
	class GCParasolBattery(object):
		def __init__(self, initDt, startLevel, minLevel, maxLevel, maxRateCharge, maxRateDischarge, lossFactor):
			self.clock = ExperimentClock.getInstance()
			self.dataCollector = GCDataCollector.getInstance()
			self.prevDt = initDt
			self.currentLevel = startLevel
			self.minLevel = minLevel
			self.maxLevel = maxLevel
			self.maxRateCharge = maxRateCharge
			self.maxRateDischarge = maxRateDischarge
			self.lossFactor = lossFactor
			
			self.logger = GCLogger.getInstance().get_logger(self)

		def calcNetWatts(self, allocations):
			#watts = (self.lossFactor * (allocations.BPowerBatt + allocations.GPowerBatt)) - allocations.BattPowerLoad
			watts = allocations.BPowerBatt + allocations.GPowerBatt - allocations.BattPowerLoad
			return watts

		def calcHours(self):
			# use the dt of the current data collection cycle
			dt = self.dataCollector.cycleDt
			# figure out how many seconds elapsed since the last cycle
			seconds = (dt - self.prevDt).total_seconds()
			# convert to hours
			hours = seconds / (60.0*60.0)
			
			return hours

		def applyToBattery(self, watts):
			# calc hours
			hours = self.calcHours()
			# convert to watt-hours
			wattHours = watts * hours
			
			# if we are charging (positive watts), calculate the loss
			if wattHours > 0:
				wattHours *= self.lossFactor
				
			# see how this would affect the battery level
			newLevel = self.currentLevel + wattHours
			
			# successful
			if newLevel >= self.minLevel and newLevel <= self.maxLevel:
				self.currentLevel = newLevel
				return 0
			
			# we will discharge too much...we mostly worry about this
			elif newLevel < self.minLevel:
				return -1

			# we will charge too much
			elif newLevel > self.maxLevel:
				return 1

		# these are discharge watts (negative)
		def calcNewWattsLow(self):
			hours = self.calcHours()
			watts = (self.currentLevel - self.minLevel) / hours
			return -watts

		# these are charge watts (positive)
		def calcNewWattsHigh(self):
			hours = self.calcHours()
			watts = (self.maxLevel - self.currentLevel) / (self.lossFactor * hours)
			return watts

		# update the battery state with the allocations
		def updateBatteryState(self, allocations):
			# this can be either positive or negative
			watts = self.calcNetWatts(allocations)

			result = self.applyToBattery(watts)

			if result == 0:
				pass
			elif result < 0:
				# fix low
				watts = self.calcNewWattsLow()
				assert self.applyToBattery(watts) == 0
				allocations.BattPowerLoad = abs(watts)
			elif result > 0:
				# fix high
				watts = self.calcNewWattsHigh()
				assert self.applyToBattery(watts) == 0

				# if we have more green than we can charge with
				# allocate it all
				if allocations.GPowerBatt >= watts:
					allocations.GPowerUnused += allocations.GPowerBatt - watts
					allocations.GPowerBatt = watts
					allocations.BPowerBatt = 0.0

				# if green isnt enough to max out
				elif allocations.GPowerBatt < watts:
					stillNeedAfterGreen = watts - allocations.GPowerBatt
					allocations.BPowerBatt = stillNeedAfterGreen

				# if we have more brown than we can charge with
				elif allocations.BPowerBatt >= watts:
					allocations.BPowerBatt = watts

			hours = self.calcHours()
			wattHours = hours * watts
			self.prevDt = self.dataCollector.cycleDt

			self.logger.log("(Result was %d): Updated battery state: hours=%0.2f, watts=%0.2f, wattHours=%0.2f" % (result, hours, watts, wattHours))

		def getBatteryLevel(self):
			return self.currentLevel
		
		def getBatteryPercentage(self):
			return (self.currentLevel / self.maxLevel) * 100.0

	SCALE_FACTOR = 0.35
	MAX_LEVEL_BATT = 32000.0 * SCALE_FACTOR  # Wh
	START_LEVEL_BATT = MAX_LEVEL_BATT * 0.65 # Wh
	MIN_LEVEL_BATT = 0.0  # Wh *** we don't enforce minimum level here, this is policy decision
	MAX_RATE_CHARGE_BATT = 8000 * SCALE_FACTOR  # W
	MAX_RATE_DISCHARGE_BATT = 8000 * SCALE_FACTOR  # W
	BATT_LOSS_FACTOR = 0.90 # factor

	def __init__(self, solarPredictor=None):
		super(GCParasolPowerManager, self).__init__(GCParasolPowerManager)
		self.clock = ExperimentClock.getInstance()
		self.logger = GCLogger.getInstance().get_logger(self)
		self.dataCollector = GCDataCollector.getInstance()
		self.solarPredictor = solarPredictor

		self.battery = GCParasolPowerManager.GCParasolBattery(self.clock.get_current_time(), self.START_LEVEL_BATT, self.MIN_LEVEL_BATT, self.MAX_LEVEL_BATT, self.MAX_RATE_CHARGE_BATT, self.MAX_RATE_DISCHARGE_BATT, self.BATT_LOSS_FACTOR)

		self.targetBrownPowerBatt = 0.0
		self.targetGreenPowerBatt = 0.0
		self.targetGreenPowerNet = 0.0
		self.targetBattPowerLoad = 0.0
		self.targetLock = Lock()

		self.logger.log("Initialized GCParasolPowerManager")

	def setTargetParams(self, brownPowerBatt, greenPowerBatt, greenPowerNet, battPowerLoad):
		with self.targetLock:
			self.targetBrownPowerBatt = round(brownPowerBatt, 2)
			self.targetGreenPowerBatt = round(greenPowerBatt, 2)
			self.targetGreenPowerNet = round(greenPowerNet, 2)
			self.targetBattPowerLoad = round(battPowerLoad, 2)
			self.logger.info("Set parameters: targetBrownPowerBatt %f, targetGreenPowerBatt %f, targetGreenPowerNet %f, targetBattPowerLoad %f" % (self.targetBrownPowerBatt, self.targetGreenPowerBatt, self.targetGreenPowerNet, self.targetBattPowerLoad))

	def calcAllocation(self):
		self.targetLock.acquire()

		self.logger.log("Doing power allocation")
		self.logger.log("Parameters: targetBrownPowerBatt %f, targetGreenPowerBatt %f, targetGreenPowerNet %f, targetBattPowerLoad %f" % (self.targetBrownPowerBatt, self.targetGreenPowerBatt, self.targetGreenPowerNet, self.targetBattPowerLoad))
		
		allocation = GCParasolPowerManager.GCParasolPowerAllocation()

		# these are the CURRENT values, meaning the ones that have been
		# collected in the CURRENT data fetch cycle
		# we can do this because of the new support in the data collector :)

		# build our list of needs
		needs = []
		needs.append("used_solar")
		for node in MINIMUM_NODES + OPTIONAL_NODES:
			needs.append("power.%s" % node)
			needs.append("state.%s" % node)

		# get all needs satisfied (this guarantees that they are all from the same cycle)
		values = self.dataCollector.getCurrentValues(needs)

		#self.logger.log("getting used_solar")
		allocation.GPower = values["used_solar"]

		powerTotal = 0.0
		for node in MINIMUM_NODES + OPTIONAL_NODES:
			power = values["power.%s" % node]
			state = values["state.%s" % node]
			if state != 0:
				powerTotal += power

		allocation.PowerLoad = powerTotal
		
		# keep these in allocation
		allocation.unsatisfiedPowerLoad = allocation.PowerLoad
		allocation.remainingGreenPower = allocation.GPower

		self.logger.log("Before allocation, PowerLoad=%f, unsatisfiedPowerLoad=%f, remainingGreenPower=%f" % (allocation.PowerLoad, allocation.unsatisfiedPowerLoad, allocation.remainingGreenPower))
		
		# allocate green

		# in this case, all load satisfied by green
		if allocation.unsatisfiedPowerLoad <= allocation.remainingGreenPower:
			# we have some green left
			allocation.remainingGreenPower -= allocation.unsatisfiedPowerLoad
			# all load satisfied
			allocation.unsatisfiedPowerLoad = 0.0
			# all load is green load
			allocation.GPowerLoad = allocation.PowerLoad
			
			self.logger.log("Green case 1: remainingGreenPower %f, unsatisfiedPowerLoad %f, GPowerLoad %f" % (allocation.remainingGreenPower, allocation.unsatisfiedPowerLoad, allocation.GPowerLoad))

		# here, all green allocated and load unsatisifed
		else:
			# some load left to satisify
			allocation.unsatisfiedPowerLoad -= allocation.remainingGreenPower
			# all green gone
			allocation.remainingGreenPower = 0.0
			# all green went to load
			allocation.GPowerLoad = allocation.GPower
			
			self.logger.log("Green case 2: remainingGreenPower %f, unsatisfiedPowerLoad %f, GPowerLoad %f" % (allocation.remainingGreenPower, allocation.unsatisfiedPowerLoad, allocation.GPowerLoad))


		# if we have any green left, we need to allocate it someplace
		if allocation.remainingGreenPower > 0:
			# allocate leftover to where ever its going
			# these options are mutually exclusive
			# allocate everything, whatever the target limit says
			# (no place else to go)
			if self.targetGreenPowerBatt > 0:
				allocation.GPowerBatt = allocation.remainingGreenPower
			elif self.targetGreenPowerNet > 0:
				allocation.GPowerNet = allocation.remainingGreenPower
			# TODO: check this
			else:
				allocation.GPowerUnused = allocation.remainingGreenPower

			allocation.remainingGreenPower = 0.0
			
			self.logger.log("Remaining green power: allocation.GPowerBatt %f, allocation.GPowerNet %f, allocation.GPowerUnused %f, allocation.remainingGreenPower %f" % (allocation.GPowerBatt, allocation.GPowerNet, allocation.GPowerUnused, allocation.remainingGreenPower)) 
				
			# constraint
			#assert allocation.GPowerLoad + allocation.GPowerBatt + allocation.GPowerNet + allocation.GPowerUnused == allocation.GPower
			if allocation.GPowerLoad + allocation.GPowerBatt + allocation.GPowerNet + allocation.GPowerUnused != allocation.GPower:
				import code
				code.interact(local=locals())

		# if we still have some load to satisfy, lets try to do with batteries
		if allocation.unsatisfiedPowerLoad > 0:
			self.logger.log("Unsatisfied load: allocation.unsatisfiedPowerLoad %f" % allocation.unsatisfiedPowerLoad)
			# see if we are supposed to use battery (and cant use battery if we are charging it, but solver shouldn't do this)
			if self.targetBattPowerLoad > 0 and allocation.GPowerBatt == 0:
				self.logger.log("We can use battery for load, targetBattPowerLoad=%f" % self.targetBattPowerLoad)
				# if we can satisfy all with batteries
				if allocation.unsatisfiedPowerLoad <= self.targetBattPowerLoad:
					self.logger.log("Satisfied all with batteries")
					# satisfy all with batteries
					allocation.BattPowerLoad = allocation.unsatisfiedPowerLoad
					# all done
					allocation.unsatisfiedPowerLoad = 0.0
				# otherwise, allocate what we can
				else:
					self.logger.log("allocated what we can")
					allocation.BattPowerLoad = self.targetBattPowerLoad
					allocation.unsatisfiedPowerLoad -= self.targetBattPowerLoad
					# TODO: Consider this carefully!
					#allocation.BattPowerLoad = allocation.unsatisfiedPowerLoad
					#allocation.unsatisfiedPowerLoad = 0.0

		# do brown battery charging (can only happen if we aren't discharging the battery)
		if allocation.BattPowerLoad == 0:
			allocation.BPowerBatt = self.targetBrownPowerBatt

		# this guy will try to apply battery state changes
		# but, if it is impossible, will change the allocation(s)
		self.battery.updateBatteryState(allocation)

		# if we STILL have some unsatisfied load, we have to use the grid
		if allocation.unsatisfiedPowerLoad > 0:
			allocation.BPowerLoad = allocation.unsatisfiedPowerLoad
			allocation.unsatisfiedPowerLoad = 0.0

		# total brown power
		allocation.BPower = allocation.BPowerLoad + allocation.BPowerBatt

		self.targetLock.release()

		return allocation

	# each time we get polled, we need to update our internal state
	# This involves integrating what happened since the last poll for the battery
	# everything is is power, not energy
	def getValuesForLogging(self):
		allocation = self.calcAllocation()

		values = {}
		values["g_power"] = allocation.GPower
		values["power_load"] = allocation.PowerLoad
		values["b_power"] = allocation.BPower
		values["b_power_load"] = allocation.BPowerLoad
		values["b_power_batt"] = allocation.BPowerBatt
		values["g_power_load"] = allocation.GPowerLoad
		values["g_power_batt"] = allocation.GPowerBatt
		values["g_power_net"] = allocation.GPowerNet
		values["batt_power_load"] = allocation.BattPowerLoad
		values["g_power_unused"] = allocation.GPowerUnused
		values["batt_level"] = self.battery.getBatteryLevel()
		values["batt_percent"] = self.battery.getBatteryPercentage()

		return values
	
	def getTextDataForLogging(self):
		return {}
