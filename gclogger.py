#!/usr/bin/python

import sys
from experimentclock import ExperimentClock
from gcutil import GCSingleton
#from StringIO import StringIO

class GCLogger(GCSingleton):
	NORMAL = 0
	DEBUG = 1

	def __init__(self, logfile, level, stdout=False, stderr=False):
		super(GCLogger, self).__init__(GCLogger)
		
		self.level = level
		self.logfile = open(logfile, 'w')
		self.clock = ExperimentClock.getInstance()
		if not self.clock:
			self.clock = ExperimentClock()
		
		self.stdout = sys.stdout
		if stdout:
			sys.stdout = GCStdioLogger(self, "stdout")
		
		self.stderr = sys.stderr
		if stderr:
			sys.stderr = GCStdioLogger(self, "stderr")

	def writeToLog(self, msg, echo=False):
		self.logfile.write("%s %s" % (self.clock, msg))
		self.logfile.flush()
		
		if echo:
			self.stdout.write("%s %s" % (self.clock, msg))
			self.stdout.flush()

	def flushLog(self):
		self.logfile.flush()
		self.stdout.flush()
		self.stderr.flush()
		
	def get_logger(self, source_class):
		source_name = source_class.__class__.__name__
		return GCSubLogger(self, source_name)

class GCNullLogger(GCLogger):
	
	def __init__(self):
		logfile = "/dev/null"
		level = GCLogger.NORMAL
		super(GCNullLogger, self).__init__(logfile, level)

class GCSubLogger(object):
	#logger = None
	#source_name = None
	def __init__(self, logger, source_name):
		self.logger = logger
		self.source_name = source_name

	def log(self, msg):
		self.logger.writeToLog("LOG %s: %s\n" % (self.source_name, msg))
		
	def info(self, msg):
		self.logger.writeToLog("INFO %s: %s\n" % (self.source_name, msg), echo=True)

	def debug(self, msg):
		if self.logger.level == GCLogger.DEBUG:
			self.logger.writeToLog("DEBUG %s: %s\n" % (self.source_name, msg))

class GCStdioLogger(GCSubLogger):
	
	def __init__(self, logger, source_name):
		super(GCStdioLogger, self).__init__(logger, source_name)
		self.buf = ""
		
	def write(self, msg):
		for char in msg:
			if char != '\n':
				self.buf += char
			else:
				self.writeBuf()
	
	def writeBuf(self):
		self.logger.writeToLog("STDIO %s: %s\n" % (self.source_name, self.buf), echo=True)
		self.buf = ""
		
	def flush(self):
		if self.buf != "":
			self.writeBuf()
		
		self.logger.flushLog()
		
from experimentclock import *
if __name__ == "__main__":
	logger = GCLogger("logfile", GCLogger.DEBUG)
	log = logger.get_logger(logger)
	log.log("Hello, world!")
	log.debug("Debug, world!")

