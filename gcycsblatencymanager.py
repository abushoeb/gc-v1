#!/usr/bin/python

import argparse

from gcbase import GCManagerBase
from gclogger import GCLogger
from gcresponsetime import GCResponseTimeHistogram
from threading import Thread
from pyjolokia import Jolokia
from pyjolokia import JolokiaError
from datetime import datetime

JOLOKIA_URL = "http://%s:7777/jolokia/"

class GCYCSBLatencyManager(GCManagerBase):
	mbean = "com.yahoo.ycsb.ClientController:type=ClientController"
	func = "getLatencyHistogram"

	values = [
		("READ", "histogram_read"),
		("UPDATE", "histogram_write"),
		("INSERT", "histogram_insert"),
	]
	
	# needs a list of YCSB hosts
	def __init__(self, ycsb_hosts):
		
		self.ycsb_hosts = ycsb_hosts
		self.jolokia_conns = {}
		self.results = {}
		self.logger = GCLogger.getInstance().get_logger(self)
		
	def getJolokiaConn(self, host):
		url = JOLOKIA_URL % host
		jolokia_conn = Jolokia(url, timeout=5)
		for value in self.values:
			op, label = value
			jolokia_conn.add_request(type="exec", mbean=self.mbean, operation=self.func, arguments = [op])
		
		return jolokia_conn
					 
	def getValuesFromHostThread(self, host):
		data = None
		values = {}
		jolokia_conn = self.getJolokiaConn(host)
		
		for i in xrange(0, 3):
			try:
				data = jolokia_conn.getRequests()
				break
			except JolokiaError as e:
				#print "Jolokia error"
				#print e
				continue
			except Exception as e:
				#print "Random error getting jolokia data, trying again..."
				#print e
				continue
		
		if data != None:
			for entry in data:
				op = entry["request"]["arguments"][0]
				v = entry.get("value")
				
				try:
					if v == None:
						value = {}
					else:
						value = v
				except KeyError:
					value = {}
				except TypeError:
					return None
					
				values[op] = value
		#else:
			#values = None
		
		#print host
		self.results[host] = values
	
	def getValuesForLogging(self):
		# init empty dict
		value_dict = {}
		
		# dispatch threads to get values
		threads = {}
		for ycsb_host in self.ycsb_hosts:
			threads[ycsb_host] = Thread(target=self.getValuesFromHostThread, args=(ycsb_host, ))
			threads[ycsb_host].daemon = True
			threads[ycsb_host].start()
		
		for ycsb_host in self.ycsb_hosts:
			threads[ycsb_host].join()

		# combine values
		for host in self.ycsb_hosts:
			for value in self.values:
				op, label = value
				key_str = "%s_%s" % (label, host)
				
				try:
					data = self.results[host][op]
					keys = sorted(data.keys(), key=lambda(x):int(x))
					
					'''
					string = "{%s}"
					middle = ""
					for key in keys:
						tmp = "%s:%d" % (key, data[key])
						if middle == "":
							middle += tmp
						else:
							middle += ", "
							middle += tmp
					
					output = string % middle
					'''
					
					newDict = {}
					for key in keys:
						keyInt = int(key)
						bucketInt = int(data[key])
						newDict[keyInt] = bucketInt
					
					output = GCResponseTimeHistogram(buckets=newDict)
						
				except KeyError:
					#output = "{}"
					output = GCResponseTimeHistogram()
				
				value_dict[key_str] = output

		return value_dict

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--hosts", action="store", nargs="+", required=True)
	
	args = parser.parse_args()
	
	from gclogger import GCNullLogger
	logger = GCNullLogger()
	manager = GCYCSBLatencyManager(args.hosts)

	values = manager.getValuesForLogging()

	for value in sorted(values.keys()):
		print "values[%s] = %s (%0.2f)" % (value, values[value], values[value].getPercentile(99))
		#pass
		