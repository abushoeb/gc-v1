#!/usr/bin/python

import os
import re
import sqlitedict

from operator import itemgetter
from gccommon import *
from gccdf import *
from gcutil import *
from gclogger import GCLogger

from multiprocessing import Process, Queue

try:
	from gurobipy import *
except Exception as e:
	print e

class OnlineOptimizer(object):
	PATH = "."
	POWER_NODE = 27.0 #25.5
	PERCENTILE = 99
	RESPONSE_TIME = 75
	EPOCHS = range(0, 24*4)
	#NODES = range(9, NUM_NODES+1, 1)
	NODES = range(9, 27+1, 1)
	TRANS_NODES_OVERHEAD = 0
	#MAX_TRANS = 27
	MAX_EPOCH_LATENCY = math.ceil(75.0 * 1.33)
	
	def __init__(self, model="MODEL-10-17-simple-EXTENDED-ADJUSTED"): #"MODEL-5-12-CLEAN-smart-EXTENDED"):
		
		self.model = model
		
		self.logger = GCLogger.getInstance().get_logger(self)
		
		self.logger.info("OnlineOptimizer: using model %s" % self.model)
		
		#cdfCache = sqlitedict.SqliteDict("/tmp/%s-cache.db" % self.model, autocommit=True)

		# Read the CDFs
		self.cdfs = CDFSet()
		self.runCount = 0
		
		self.node_list = []
		self.load_list = []
		
		cdfPoints = {}
		
		for filename in sorted(os.listdir(self.PATH)):
			if filename.startswith('%s-' % self.model) and filename.endswith('.cdf'):
				try:
					# Get nodes and load
					match_str = '%s-(\d+)-(\d+).0.cdf' % self.model
					nodes, load = re.match(match_str, filename).groups()
					nodes = int(nodes)
					load = int(load)
					self.node_list.append(nodes)
					self.load_list.append(load)
					
					# Read CDF
					with open(self.PATH + '/' +filename) as fcdf:
						#print fcdf.read()
						cdf = readCDF(fcdf)
						
						#self.cdfs[load, nodes] = cdf
						# 99th percentile and percentage of request less than X
						#print load, nodes, cdf[99], cdf.getPercentage(65)
					
					if not nodes in cdfPoints:
						cdfPoints[nodes] = []
					
					cdfPoints[nodes].append((load, cdf))
					
				except Exception, e:
					self.logger.log("Cannot parse file: %s" % filename)
					self.logger.log("Exception: %s" % str(e))
		
		for nodes in cdfPoints.keys():
			cdfPoints[nodes].sort()
			
		
		'''
		newCdfPoints = {}
		for nodes in cdfPoints.keys():
			newCdfPoints[nodes] = []
			
			points = cdfPoints[nodes]
			for i in xrange(0, len(points)-1):
				thisLoad, thisCdf = points[i]
				nextLoad, nextCdf = points[i+1]
				
				if thisLoad % 1000 == 0 and nextLoad % 1000 == 0:
					newLoad = (thisLoad + nextLoad) / 2
					newCdf = interpolateCDFs(thisLoad, thisCdf, nextLoad, nextCdf, newLoad)
					newCdfPoints[nodes].append((newLoad, newCdf))
					print "Generated new CDF point:", nodes, newLoad
		
		for nodes in newCdfPoints.keys():
			for point in newCdfPoints[nodes]:
				cdfPoints[nodes].append(point)
			cdfPoints[nodes].sort()
		'''
		for nodes in cdfPoints.keys():
			for point in cdfPoints[nodes]:
				load, cdf = point
				self.cdfs[load, nodes] = cdf
				self.logger.info("Have CDF for %d, %d: %0.2f ms" % (load, nodes, cdf.getValue(99)))
	
	
	def getOptSchedule(self, workload, solar, brownPrices, prevNumNodes, prevTotal, prevLessThanMs, peakPower, peakPowerPrice, maxWindowSLA):
		data = {}
		data["workload"] = workload
		data["solar"] = solar
		data["brownPrices"] = brownPrices
		data["prevNumNodes"] = prevNumNodes
		data["prevTotal"] = prevTotal
		data["prevLessThanMs"] = prevLessThanMs
		data["peakPower"] = peakPower
		data["peakPowerPrice"] = peakPowerPrice
		data["maxWindowSLA"] = maxWindowSLA
		
		squeue = Queue()
		rqueue = Queue()
		squeue.put(data)
		
		#process = Process(target=self._getOptSchedule, args=(workload, solar, brownPrices, prevNumNodes, prevTotal, prevLessThanMs, peakPower, peakPowerPrice, maxWindowSLA, queue))
		process = Process(target=self._getOptSchedule, args=(squeue, rqueue))
		process.start()
		ret = rqueue.get()
		
		if self.runCount == 0:
			self.cdfs = rqueue.get()
		
		process.join()
		
		self.runCount += 1
		return ret
	
	#def _getOptSchedule(self, workload, solar, brownPrices, prevNumNodes, prevTotal, prevLessThanMs, peakPower, peakPowerPrice, maxWindowSLA, queue):
	def _getOptSchedule(self, squeue, rqueue):
		self.logger.info("Getting data...")
		data = squeue.get()
		workload = data["workload"]
		solar = data["solar"]
		brownPrices = data["brownPrices"]
		prevNumNodes = data["prevNumNodes"]
		prevTotal = data["prevTotal"]
		prevLessThanMs = data["prevLessThanMs"]
		peakPower = data["peakPower"]
		peakPowerPrice = data["peakPowerPrice"]
		maxWindowSLA = data["maxWindowSLA"]
		#cdfs = data["cdfs"]
		cdfs = self.cdfs
		
		self.logger.info("running getOptSchedule")
		
		worktrace = {}
		solartrace = {}
		
		for i in xrange(0, len(workload)):
			worktrace[i] = workload[i]
		for i in xrange(0, len(solar)):
			solartrace[i] = solar[i]
		
		
		ret = {}
		m = Model()
		# Params
		# Variables
		schedule = {}
		#transition = {}
		#energyreq = {}
		#brownener
		self.logger.info("doing schedule constraints")
		
		for epoch in self.EPOCHS:
			work = worktrace[epoch]
			for node in self.NODES:
				#print "Check", epoch, work, node
				if (work, node) not in cdfs:
					# If we have no model is that it's not even possible
					schedule[epoch, node] = 0
				else:
					schedule[epoch, node] = m.addVar(vtype=GRB.BINARY, name="schedule(%d,%d)" % (epoch, node))
				#transition[epoch] = m.addVar(vtype=GRB.BINARY, name="trans(%d,%d)" % (epoch,node))
		#transition[-1] = m.addVar(vtype=GRB.BINARY, name="trans(%d,%d)" % (-1,node))

		#maxPeakPower = m.addVar(vtype=GRB.CONTINUOUS, lb=0)
		m.update()
		
		self.logger.info("done with schedule constraints")
		
		# Extracted variable
		numnodes = {}
		for epoch in self.EPOCHS:
			numnodes[epoch] = quicksum(schedule[epoch, node]*node for node in self.NODES)
	
	
		# Setting previous status
		numnodes[-1] = prevNumNodes
		for node in self.NODES:
			schedule[-1,node] = 0
		schedule[-1,prevNumNodes] = 1
	
		# Constraints
		for epoch in self.EPOCHS:
			m.addConstr(quicksum(schedule[epoch, node] for node in self.NODES) == 1, "onlyonenode(%d)" % epoch)
		# Constraint on 99th
		perhour = False
		auxless = 0.0
		auxtotal = 0.0
		self.logger.info("Doing constraints that require CDFs")
		
		for epoch in self.EPOCHS:
			# Window for the SLA
			# TODO: CHECK THIS WELL
			if epoch >= maxWindowSLA:
				self.logger.info("epoch %d >= maxWindowSLA %d" % (epoch, maxWindowSLA))
				break
			
			# Start accounting
			if perhour:
				aux = 0
			work = worktrace[epoch]
			for node in self.NODES:
				#if (work, node) in self.cdfs:
				if perhour:					
					#auxless += (1-transition[epoch])*schedule[epoch,node]*self.cdfs[work, node].getPercentage(self.RESPONSE_TIME)/100.0
					#auxless +=   (transition[epoch])*schedule[epoch,node]*self.cdfs[work, node-self.TRANS_NODES_OVERHEAD].getPercentage(self.RESPONSE_TIME)/100.0
					# TODO: Inigo check this, i brought this over from for-inigo/milp.py
					auxless += schedule[epoch,node]*cdfs[work, node].getPercentage(self.RESPONSE_TIME)/100.0

				else:
					#auxless += (1-transition[epoch])*schedule[epoch,node]*work*self.cdfs[work, node].getPercentage(self.RESPONSE_TIME)/100.0
					#auxless +=   (transition[epoch])*schedule[epoch,node]*work*self.cdfs[work, node-self.TRANS_NODES_OVERHEAD].getPercentage(self.RESPONSE_TIME)/100.0
					# TODO: Inigo check this, i brought this over from for-inigo/milp.py
					auxless += schedule[epoch,node]*work*cdfs[work, node].getPercentage(self.RESPONSE_TIME)/100.0
					#print epoch, node, work, self.cdfs[work, node].getPercentage(self.RESPONSE_TIME)
					
			if perhour:
				m.addConstr(auxless >= self.PERCENTILE/100.0)
			auxtotal += work
		
		self.logger.info("Done with constraints that require CDFs")
		
		if not perhour:
			#margin = 0.01 # this adds some margin
			margin = 0.0
			
			#print "auxtotal", auxtotal
			#print "prevtotal", prevTotal
			
			#print "auxless", auxless
			#print "prevlessthanms", prevLessThanMs
			
			# add the previous state
			m.addConstr(auxless+prevLessThanMs >= ((self.PERCENTILE+margin)/100.0)*(auxtotal + prevTotal), "slaconstraint")
			print self.PERCENTILE, prevLessThanMs, auxtotal, prevTotal
			#print '??? +', prevLessThanMs,'>=', sum(worktrace.values()), '+', prevTotal
			
		# At every epoch the Y-%ile response has to be lower than X
		for epoch in self.EPOCHS:
			work = worktrace[epoch]
			m.addConstr(quicksum(schedule[epoch,node]*cdfs[work, node].getValue(self.PERCENTILE) for node in self.NODES) <= self.MAX_EPOCH_LATENCY) # 90 ms
		m.update()
		
		'''
		# Peak power
		for epoch in self.EPOCHS:
			m.addConstr(maxPeakPower >= )
		m.update()
		'''
		
		# remove this constraint for paper experiments
		# remove max trans
		#####for epoch in self.EPOCHS:
			# Maximum simultaneous transitions (turn ons)
			#####m.addConstr(numnodes[epoch]-numnodes[epoch-1] <= self.MAX_TRANS)
			# Maximum simultaneous transitions (turn offs)
		#	m.addConstr(numnodes[epoch-1]-numnodes[epoch] <= self.MAX_TRANS)
		
		'''
		for epoch in self.EPOCHS:
			for node in self.NODES:
				m.addConstr(transition[epoch] >= schedule[epoch,node] - schedule[epoch-1,node]) # Consider the transition when starting
				#m.addConstr(transition[epoch-1] >= schedule[epoch,node] - schedule[epoch-1,node]) # Consider the transition before starting
		'''
		m.update()
		
		# Optimization function
		self.logger.info("Adding constraints optimization")
		cost = 0
		# Energy costs
		for epoch in self.EPOCHS:
			for node in self.NODES:
				# Brown used
				energyreq = node*self.POWER_NODE - solartrace[epoch]
				if energyreq < 0:
					energyreq = 0
				cost += brownPrices[epoch]*energyreq*schedule[epoch, node]
				# Green surplus
				greensurplus = solartrace[epoch] - node*self.POWER_NODE
				if greensurplus < 0:
					greensurplus = 0
				cost += greensurplus*schedule[epoch, node]
			#cost += transition[hour]
		# Peak power costs
		#cost += maxPeakPower*peakPowerPrice # TODO apply the right cost model
				
		
		# TODO green is free! solartrace[hour], node*POWER_NODE
		m.setObjective(cost, GRB.MINIMIZE)
		m.update()
		m.setParam("OutputFlag", 1)
		m.setParam("TimeLimit", 200)
		m.setParam("Threads", 2)
		m.update()
		m.write("/tmp/model.lp")
		
		self.logger.info("Starting optimizer...")
		startTime = datetime.now()
		m.optimize()
		self.logger.info("Optimization time: %s" % str(datetime.now() - startTime))

		if m.status == GRB.status.INFEASIBLE or m.status == GRB.status.UNBOUNDED:
			self.logger.info("Cannot solve, returning none")
			return None
		elif m.status == GRB.status.TIME_LIMIT:
			self.logger.info("Exceeded time limit")
		
		# Solution
		totalnodes = 0.0
		for epoch in self.EPOCHS:
			for node in self.NODES:
				if schedule[epoch, node] != 0 and schedule[epoch, node].x > 0.1:
					ret[epoch] = node
		
		rqueue.put(ret)
		if self.runCount == 0:
			rqueue.put(cdfs)
		return
		#return ret
