#!/usr/bin/python

import sys
import thread

from threading import Thread
from gccommon import *
from gcbase import GCManagerBase
from gchelpers import execute_remote_command_sync

field_names = ["S0C", "S1C", "S0U", "S1U", "EC", "EU", "OC", "OU", "PC", "PU", "YGC", "YGCT", "FGC", "FGCT", "GCT"]

class JStatThread(Thread):
	
	def __init__(self, node):
		Thread.__init__(self)
		self.daemon = True
		
		self.node = node
		self.values = {}
		
	def run(self):
		cmd = "jstat -gc $(cassandra_pid)"
		try:
			out, err = execute_remote_command_sync(self.node, USERNAME, cmd, max_tries=1, timeout=5)
			lines = out
			assert len(lines) == 2
			
			#field_names = lines[0].split()
			fields = lines[1].split()
		
			for i in xrange(0, len(fields)):
				name = field_names[i]
				value = float(fields[i])
				#print name, value
				self.values[name] = value
		
		# either could not read or assertion failed
		except:
			for field_name in field_names:
				self.values[field_name] = float("NaN")
		
		thread.exit()
		
class GCJStatManager(GCManagerBase):
	
	def __init__(self, nodes=(MINIMUM_NODES+OPTIONAL_NODES)):
		self.nodes = nodes
	
	def getValuesForLogging(self):
		values_dict = {}
		
		threads = {}
		for node in self.nodes:
			t = JStatThread(node)
			t.start()
			threads[node] = t
		
		for node in threads:
			threads[node].join()
			
			for key in threads[node].values:
				globalKey = "gc_%s.%s" % (key, node)
				values_dict[globalKey] = threads[node].values[key]
		
		return values_dict


if __name__ == "__main__":
	if len(sys.argv) == 2:
		node = sys.argv[1]
		nodes = [node]
	else:
		nodes = MINIMUM_NODES + OPTIONAL_NODES
	
	manager = GCJStatManager(nodes)
	values = manager.getValuesForLogging()
	
	for value in sorted(values):
		print "values[%s] = %0.2f" % (value, values[value])