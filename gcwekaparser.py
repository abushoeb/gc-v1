#!/usr/bin/env python

#from conf import *
#from commons import *
from operator import itemgetter
from subprocess import Popen, PIPE
import math

# Learn from data file
def loadRawRegression(filename):
	correlation = -1000.0
	regression = {}
	formula = 'default'
	regression[formula] = {}
	regression['tree'] = []
	
	# Temporary results
	auxcorrelation = None
	auxregression = {}
	auxformula = 'default'
	auxregression[auxformula] = {}
	auxregression['tree'] = []
	
	f = open(filename, "r")
	lines = f.read().strip().split("\n")
	f.close()
	
	for line in lines:
		line = line.replace('\n', '')

		# Parse line
		if line != '':
			if line.find('Model')<0 and line.find('model')<0:
				if line.find(':') < 0:
					# Parse formula
					auxregression[auxformula] = parseLinearFormula(auxregression[auxformula], line)
				else:
					# Parse tree
					line = line.replace(' ', '')
					if line.startswith('NumberofRules'):
						pass
					elif line.startswith('LMnum:'):
						auxformula = 'LM'+line[6:]
						auxregression[auxformula] = {}
					else:
						# Tree description
						if line.find('(')>0:
							line = line[:line.find('(')]
							# Add the decission
							auxregression['tree'].append(line[:line.find(':')+1])
							# Add the formula to be called
							callformula = line[line.find(':')+1:]
							auxregression['tree'].append('|'*(line.count('|')+1)+callformula)
						else:
							auxregression['tree'].append(line)

	correlation = auxcorrelation
	regression = auxregression
	formula = auxformula

	# Return array with all the formulas
	return regression

# Parses a formula into a dictionary
def parseLinearFormula(formula, line):
	if line.find('=') > 0:
		formula = {}
	else:
		line = line.replace(' ', '')
		if line != '+' and line.find('%') < 0:
			lineSplit = line.split('*')
			attr = 'constant'
			if len(lineSplit) > 1:
				attr = lineSplit[1].split('+')[0]
			value = float(lineSplit[0])
			formula[attr] = value
	
	return formula

# Save regression into a file
def saveRegressionFile(regression, filename):
	with open(filename, 'w') as f:
		if 'tree' in regression:
			if len(regression['tree'])>0:
				for line in regression['tree']:
					f.write(line+'\n')
			del regression['tree']
		for formula in sorted(regression.keys()):
			auxformula = []
			for key in sorted(regression[formula].keys()):
				auxformula.append("'%s': %f" % (key, regression[formula][key]))
			strformula = "{"+", ".join(auxformula)+"}"
			#strformula = str(regression[formula])
			f.write(str(formula)+': '+strformula+'\n')

# Read regression from a file
def readRegressionFile(filename):
	regression = {}
	regression['tree'] = []
	with open(filename, 'r') as f:
		line = f.readline()
		formulas = False
		while line:
			line = line.replace('\n', '')
			
			if line.startswith('LM') or line.startswith('default'):
				formulas = True
			if formulas:
				# Parse a line with a formula into a dictionary
				callformula = line[:line.find(':')]
				#print "cf", callformula
				formula = {}
				formulaaux = line[line.find('{')+1:line.find('}')]
				#print "fa", formulaaux
				for keyvalue in formulaaux.split(', '):
					if keyvalue != '':
						key, value = keyvalue.split(': ')
						#print key, value
						key = key.replace("'", "")
						formula[key] = float(value)
				regression[callformula] = formula
			else:
				# Read tree line
				regression['tree'].append(line)
			line = f.readline()
	return regression

def applyRegression(regression, inputs):
	try:
		skiplevel = 0
		tree = regression['tree']
		if len(tree) == 0:
			# No tree
			return applyFunction(regression['default'], inputs)
		else:
			for branch in tree:
				level = branch.count('|')
				#print level, 'Branch', branch
				if level > skiplevel:
					#print '   S'
					pass
				elif isFunction(branch):
					# Apply function to the inputs
					#print '   Apply function', branch
					function = branch.replace('|', '')
					return applyFunction(regression[function], inputs)
				elif doesBranchApply(branch, inputs):
					#print '   V', branch, 'applies for', inputs
					skiplevel = level+1
				else:
					skiplevel = level
	except Exception, e:
		
		#print 'Error applying regression:'
		#print '\tException: ', e
		#print '\tRegression:', regression
		#print '\tInput:     ', inputs
		
		pass
	return None

def isFunction(branchline):
	ret = False
	branchline = branchline.replace('|', '')
	if branchline.startswith('LM') or branchline.startswith('default'):
		ret = True
	return ret

def applyFunction(function, inputs):
	ret = 0.0
	
	for attr in function:
		if attr == 'constant':
			ret += function['constant']
		else:
			ret += inputs[attr] * function[attr]
	
	return ret

def doesBranchApply(branchline, inputs):
	ret = False
	
	branchline = branchline.replace('|', '')
	if branchline.find(':')<0:
		ret = True
	else:
		branchline = branchline.replace(':', '')
		if branchline.find('<=') >= 0:
			attr, val2 = branchline.split('<=')
			val1 = inputs[attr]
			val2 = float(val2)
			if val1 <= val2:
				ret = True
		elif branchline.find('>=') >= 0:
			attr, val2 = branchline.split('>=')
			val1 = inputs[attr]
			val2 = float(val2)
			if val1 >= val2:
				ret = True
		elif branchline.find('<') >= 0:
			attr, val2 = branchline.split('<')
			val1 = inputs[attr]
			val2 = float(val2)
			if val1 < val2:
				ret = True
		elif branchline.find('>') >= 0:
			attr, val2 = branchline.split('>')
			val1 = inputs[attr]
			val2 = float(val2)
			if val1 > val2:
				ret = True
		
	return ret
