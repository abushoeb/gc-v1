#!/usr/bin/python

from greencassandraheuristic import *
import sys
import math
from datetime import *
#import matplotlib.pyplot as plot
from gccommon import *

def loadPerformance():
	performance = {}
	performance_smooth = {}
	f = open("workload.csv", 'r')
	workload_data = f.read()
	lines = workload_data.split("\n")
	time_counter = 0

	for i in xrange(0, len(lines)):
		if lines[i] == "":
			continue
		
		fields = lines[i].split(",")
		
		if i < (len(lines) - 1) and lines[i+1] != "":
			next_fields = lines[i+1].split(",")
		else:
			next_fields = fields
		
		#print fields, next_fields
	
		dt = datetime.strptime(fields[0], "%H:%M %d/%m/%y ")
		value = float(fields[1]) * float(GreenCassandraHeuristic.NUM_NODES)
		next_value = float(next_fields[1]) * float(GreenCassandraHeuristic.NUM_NODES)
		diff = next_value - value
		#print value, next_value, diff
		
		points_per_hour = (60/GreenCassandraHeuristic.SLOT_LENGTH)
		step = diff / points_per_hour
		for i in xrange(0, points_per_hour):
			performance[time_counter] = int(math.ceil(value))
			performance_smooth[time_counter] = int(math.ceil(value + (step*i)))
			
			time_counter += 1
	#dt = datetime.datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")

	#for entry in performance:
	# print entry, performance[entry]
	
	return performance, performance_smooth
  
def loadEnergy():
  energy = {}
  time_counter = 0
  f = open("ganglia-metrics.csv", 'r')
  green_data = f.read().replace("\"", "")
  lines = green_data.split("\n")
  dt_incr = timedelta(minutes=GreenCassandraHeuristic.SLOT_LENGTH)
  
  for i in xrange(0, 7):
    dt_counter = datetime.strptime("2012-12-04T00:00:00", "%Y-%m-%dT%H:%M:%S")
    for line in lines:
	if line == "":
	  continue
	fields = line.split(",")
	dt = datetime.strptime(fields[0], "%Y-%m-%dT%H:%M:%S-05:00")
	dt = dt - timedelta(hours=1)
	
	#print dt, dt_counter, dt_counter+dt_incr
	if dt >= dt_counter and dt < (dt_counter + dt_incr):
	  energy[time_counter] = float(fields[1]) * 2.0
	  dt_counter += dt_incr
	  time_counter += 1

  #for entry in energy:
   #   print entry, energy[entry]
      
  return energy

performance, performance_smooth = loadPerformance()
energy = loadEnergy()
nodes = []

for n in MINIMUM_NODES:
	nodes.append(n)
	
for n in OPTIONAL_NODES:
	nodes.append(n)
	

nodes = MINIMUM_NODES + OPTIONAL_NODES

nodeStates = {}
offInfo = {}
transInfo = {}

for n in nodes:
	nodeStates[n] = GCNodeState.OFF
	offInfo[n] = 8
	transInfo[n] = 0

gc = GreenCassandraHeuristic(performance_smooth, energy, nodes, nodeStates, offInfo, transInfo)
onNodes = gc.simulate(24)
gc.output(24)

sys.exit()


perf = []
perf_smooth = []
times = []
green = []
nodes = []

for i in xrange(0, 96):
	times.append(i)
	perf.append(performance[i]*GreenCassandraHeuristic.NODE_POWER)
	perf_smooth.append(performance_smooth[i]*GreenCassandraHeuristic.NODE_POWER)
	green.append(energy[i])
	nodes.append(onNodes[i]*GreenCassandraHeuristic.NODE_POWER)

#ax = plot.subplot(111)
#plot.plot(times, perf)
#plot.plot(times, perf_smooth)
#ax=plot.twinx()
#plot.plot(times, green)
#plot.bar(times, nodes)
#plot.show()





