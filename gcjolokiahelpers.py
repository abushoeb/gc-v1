#!/usr/bin/python

from pyjolokia import *
from threading import Thread

def mbean_uniform(mbean):
	prefix, rest=  mbean.split(":")
	restTokens = rest.split(",")
	restTokens = sorted(restTokens)
	
	restSorted = ",".join(restTokens)
	
	output = "%s:%s" % (prefix, restSorted)
	return output

class MBeanAttribute(object):
	
	def __init__(self, mbean, attribute):
		self.mbean = mbean_uniform(mbean)
		self.attribute = attribute

	def __hash__(self):
		return hash((self.mbean, self.attribute))
	
	def __eq__(self, other):
		return (self.mbean, self.attribute) == (other.mbean, other.attribute)
	
	def __repr__(self):
		return "MBeanAttribute(mbean=%s, attribute=%s)" % (self.mbean, self.attribute)

class MBeanOperation(object):
	def __init__(self, mbean, operation):
		self.mbean = mbean_uniform(mbean)
		self.operation = operation
	
	def __hash__(self):
		return hash((self.mbean, self.operation))
	
	def __eq__(self, other):
		return (self.mbean, self.operation) == (other.mbean, other.operation)
	
	def __repr__(self):
		return "MBeanOperation(mbean=%s, operation=%s)" % (self.mbean, self.operation)

class JMXResponse(object):
	SUCCESS = 0
	ERROR = 1
	VALUE_MISSING = 2
	VALUE_EMPTY = 4
	
	def __init__(self, status, value=None, results=None, error=None):
		self.status = status
		self.value = value
		self.error = error
		self.results = results
	
	def statusString(self):
		if self.status == self.SUCCESS:
			return "SUCCESS"
		elif self.status == self.ERROR:
			return "ERROR"
		else:
			return "UNKNOWN"
		
	def __repr__(self):
		return "JMXResponse(status=%s, value=%s, error=%s)" % (self.statusString(), self.value, self.error)
	      
class JMXClient(object):
	
	def __init__(self, hostname, url, timeout=3):
		self.hostname = hostname
		self.url = url
		self.timeout = timeout
		#print hostname, url % hostname, timeout
		
	def getConn(self):
		url = self.url % self.hostname
		#print url
		return Jolokia(url, timeout=self.timeout)
	
	def execOperation(self, op, args=[], retries=3):
		#print op, args, retries
		
		args = list(args)
		conn = self.getConn()
		response = None
		
		for i in xrange(0, retries):
			try:
				response = conn.request(type="exec", mbean=op.mbean, operation=op.operation, arguments=args)
				break
			except JolokiaError as e:
				#print "Jolokia Error:", e
				continue
			except Exception as e:
				#print "Exception:", e
				continue
		
		if response == None:
			return JMXResponse(JMXResponse.ERROR, error="empty response")
		
		try:
			if response["error"] != None:
				#for field in response.keys():
				#	print field, response[field]
				return JMXResponse(JMXResponse.ERROR, error=response["error"])
		# if this exception fires, no error
		except:
			pass
		
		return JMXResponse(JMXResponse.SUCCESS, value=response["value"])

	def readAttribute(self, attrib, retries=3):
		conn = self.getConn()
		response = None
		
		for i in xrange(0, retries):
			try:
				response = conn.request(type="read", mbean=attrib.mbean, attribute=attrib.attribute)
				break
			except JolokiaError as e:
				#print "Jolokia Error:", e
				continue
			except Exception as e:
				#	print "Exception:", e
				continue
		
		if response == None:
			return JMXResponse(JMXResponse.ERROR, error="empty response")
		
		try:
			if response["error"] != None:
				#for field in response.keys():
				#	print field, response[field]
				return JMXResponse(JMXResponse.ERROR, error=response["error"])
		except:
			pass
		
		return JMXResponse(JMXResponse.SUCCESS, value=response["value"])
	
	def readMultipleAttributes(self, attribs, retries=3):
		conn = self.getConn()
		response = None
		results = {}
		
		for attrib in attribs:
			conn.add_request(type="read", mbean=attrib.mbean, attribute=attrib.attribute)
		
		for i in xrange(0, retries):
			try:
				response = conn.getRequests()
				break
			except JolokiaError as e:
				#print "Jolokia Error:", e
				continue
			except Exception as e:
				#print "Exception:", e
				continue
			
		if response == None:
			return JMXResponse(JMXResponse.ERROR, error="empty_response")
		
		try:
			if response["error"] != None:
				return JMXResponse(JMXResponse.ERROR, error=response["error"])
		except:
			pass
		
		for entry in response:
			try:
				mbean = entry["request"]["mbean"]
				attribute = entry["request"]["attribute"]
				value = entry["value"]
				
				currentAttrib = MBeanAttribute(mbean, attribute)
				if value != None:
					results[currentAttrib] = value
				else:
					results[currentAttrib] = JMXResponse.VALUE_MISSING
			except:
				pass
			
		for missingAttrib in [i for i in attribs if i not in results.keys()]:
			results[missingAttrib] = JMXResponse.VALUE_MISSING
		
		return JMXResponse(JMXResponse.SUCCESS, results=results)
		
	def writeAttribute(self, attrib, value, retries=3):
		conn = self.getConn()
		response = None
		
		for i in xrange(0, retries):
			try:
				response = conn.request(type="write", mbean=attrib.mbean, attribute=attrib.attribute, value=value)
				break
			except JolokiaError as e:
				#print "Jolokia Error:", e
				continue
			except Exception as e:
				#print "Exception:", e
				continue
		
		if response == None:
			return JMXResponse(JMXResponse.ERROR, error="empty response")
		
		try:
			if response["error"] != None:
				#for field in response.keys():
				#	print field, response[field]
				return JMXResponse(JMXResponse.ERROR, error=response["error"])
		except:
			pass
		
		return JMXResponse(JMXResponse.SUCCESS)

class JMXClientExecThread(Thread):
	def __init__(self, hostname, url, timeout, operation, args, retries):
		Thread.__init__(self)
		self.daemon = True
		
		self.client = JMXClient(hostname, url, timeout)
		self.operation = operation
		self.args = args
		self.retries = retries
		self.result = None
	
	def run(self):
		self.result = self.client.execOperation(self.operation, self.args, retries=self.retries)
	
	def hasResult(self):
		return self.result != None
	
	def getResult(self):
		return self.result

class JMXClientReadThread(Thread):
	def __init__(self, hostname, url, timeout, attribute, retries):
		Thread.__init__(self)
		self.daemon = True
		
		self.client = JMXClient(hostname, url, timeout)
		self.attribute = attribute
		
		self.retries = retries
		self.result = None
		
	def run(self):
		self.result = self.client.readAttribute(self.attribute, retries=self.retries)
	
	def hasResult(self):
		return self.result != None
	
	def getResult(self):
		return self.result

class JMXClientReadMultipleThread(Thread):
	def __init__(self, hostname, url, timeout, attributes, retries):
		Thread.__init__(self)
		self.daemon = True
		
		self.client = JMXClient(hostname, url, timeout)
		self.attributes = attributes
		
		self.retries = retries
		self.result = None
		
	def run(self):
		self.result = self.client.readMultipleAttributes(self.attributes, retries=self.retries)
	
	def hasResult(self):
		return self.result != None
	
	def getResult(self):
		return self.result

class JMXClientWriteThread(Thread):
	def __init__(self, hostname, url, timeout, attribute, value, retries):
		Thread.__init__(self)
		self.daemon = True
		
		self.client = JMXClient(hostname, url, timeout)
		self.attribute = attribute
		self.value = value
		
		self.retries = retries
		self.result = None
		
	def run(self):
		self.result = self.client.writeAttribute(self.attribute, self.value, retries=self.retries)
	
	def hasResult(self):
		return self.result != None
	
	def getResult(self):
		return self.result	


class JMXThreadedClient(object):
	def __init__(self, hostnames, url, timeout=3):
		self.hostnames = hostnames
		self.url = url
		self.timeout = timeout
	
	def execOperation(self, op, args=[], retries=3):
		threads = {}
		for host in self.hostnames:
			t = JMXClientExecThread(host, self.url, self.timeout, op, args, retries)
			threads[host] = t
			t.start()
		
		for host in self.hostnames:
			threads[host].join()
			#print "joined", host
		
		results = {}
		
		for host in self.hostnames:
			results[host] = threads[host].getResult()
		
		return results
	
	def readAttribute(self, attrib, retries=3):
		threads = {}
		for host in self.hostnames:
			t = JMXClientReadThread(host, self.url, self.timeout, attrib, retries)
			threads[host] = t
			t.start()
		
		for host in self.hostnames:
			threads[host].join()
			#print "joined", host
		
		results = {}
		
		for host in self.hostnames:
			results[host] = threads[host].getResult()
		
		return results
	
	def readMultipleAttributes(self, attribs, retries=3):
		threads = {}
		for host in self.hostnames:
			t = JMXClientReadMultipleThread(host, self.url, self.timeout, attribs, retries)
			threads[host] = t
			t.start()
		
		for host in self.hostnames:
			threads[host].join()
			#print "joined", host
		
		results = {}
		
		for host in self.hostnames:
			results[host] = threads[host].getResult()
		
		return results
	
	def writeAttribute(self, attrib, value, retries=3):
		threads = {}
		for host in self.hostnames:
			t = JMXClientWriteThread(host, self.url, self.timeout, attrib, value, retries)
			threads[host] = t
			t.start()
		
		for host in self.hostnames:
			threads[host].join()
			#print "joined", host
		
		results = {}
		
		for host in self.hostnames:
			results[host] = threads[host].getResult()
		
		return results

class JMXReaderThread(Thread):
	
	def __init__(self, hostname, attributes, url):
		Thread.__init__(self)
		
		self.daemon = True
		self.attributes = attributes
		self.results = {}
		
		self.jolokia_conn = Jolokia(url % hostname, timeout=3)
		for attrib in self.attributes:
			self.jolokia_conn.add_request(type="read", mbean=attrib.mbean, attribute=attrib.attribute)		
	
	# when this finishes, results will be populated
	def run(self):
		data = None
		
		for i in xrange(0, 2):
			try:
				data = self.jolokia_conn.getRequests()
				break
			except JolokiaError as e:
				#print "Jolokia Error:" , e
				continue
			except Exception as e:
				#print "Exception:", e
				continue
		
		if data == None:
			return
		
		#print data
		for entry in data:
			try:
				mbean = entry["request"]["mbean"]
				attribute = entry["request"]["attribute"]
				value = entry["value"]
				#print mbean, attribute, value
			except KeyError:
				continue
			
			if value == None:
				print "No Data"
				continue
			
			self.results[MBeanAttribute(mbean, attribute)] = value
			
	def hasAllResults(self):
		return len(self.results.keys()) == len(self.attributes)
	
	def getResults(self):
		return self.results
		