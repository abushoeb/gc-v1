from gcwekaparser import *

regression = loadRawRegression("samplemodel.regression")
#saveRegressionFile(regression, "sampleparsed.regression")

regression2 = readRegressionFile("sampleparsed.regression")
d = {
	"nodes" : 9,
	"avg_workload_transition" : 2400.0,
	"integrated_workload_off" : 5000.0,
	"model" : 125,
	"model_minus_1": 300,
	"model_minus_2": 300,
	"model_minus_3": 300,
	"model_minus_4": 300,
	"model_minus_5": 300,
	"model_minus_6": 300,
	"model_minus_7": 300,
	"model_minus_8": 300,
}
latency = applyRegression(regression, d)
print latency

