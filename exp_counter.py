#!/usr/bin/python

import time
import math
import signal
import thread
from threading import Thread
from threading import Lock
from datetime import datetime
from datetime import timedelta

class ExponentialCounter(object):
	
	def __init__(self, alpha, seconds):
		self.alpha = alpha
		self.seconds = seconds
		
		self.sample = 0
		self.current = 0
		
		self.lock = Lock()
		
		t = Thread(target=self.periodicThread)
		t.daemon = True
		t.start()
		
	def periodicThread(self):
		while True:
			time.sleep(self.seconds)
	#		print "Thread firing"
			self.lock.acquire()
			print "sample", self.sample
			#self.current += self.sample
			if self.current == 0:
				self.current = self.sample
			self.current = math.ceil(self.alpha * self.sample + (1.0 - self.alpha) * self.current)
			self.sample = 0
			self.lock.release()
	#		print "current", self.current
	#		print "sample", self.sample
			
	
	def increment(self):
		self.lock.acquire()
		self.sample += 1
		self.lock.release()
	
	def get(self):
		if self.current > 0:
			return int(self.current)
		else:
			return int(self.sample)
		return int(self.sample) + int(self.current)

def workThread(ops_sec, start_dt):
	sleeptime = 1.0 / ops_sec
	
	while True:
		if start_dt <= datetime.now() - timedelta(seconds=30):
			thread.exit()
			
		counter.increment()
		time.sleep(sleeptime)
		
	
if __name__ == "__main__":
	#counter = ExponentialCounter(0.80, 5)
	counter = ExponentialCounter(0.05, 5)
	target = 10000.0
	numthreads = 128
	startDt = datetime.now()	
	
	for i in xrange(0, numthreads):
		t = Thread(target=workThread, args=(target/numthreads,startDt))
		t.daemon = True
		t.start()
	
	while True:
		print counter.get()
		time.sleep(1)
		
		