#!/usr/bin/python
import math
import time
import random
import sys

from datetime import *
from experimentclock import *
from gcsolarpredictor import *
from gcworkloadpredictor import *
from gcclustermanager import *
from gclogger import *
from gcmodel import *
from gcdatacollector import *
from gcresponsetime import *
from gcoptimizer import OnlineOptimizer
from gcoptimizerbatt import OnlineOptimizerBatt
from gcbatterymodel import GCParasolPowerManager

import numpy as np

try:
	from schedulerclient import *
except:
	print "Cannot import schedulerclient"

class GCSchedulerState(object):
	onNodes = []		 	# list of of nodes that are GCNodeState.ON
	offNodes = []			# list of nodes that are GCNodeState.OFF
	
	transitionNodes = []		# list of nodes that are GCNodeState.TRANSITION
	deadlineTransitionNodes = []	# list of nodes that are in GCNodeState.TRANSITION for deadlines (we can't count these for load).
	
	scheduledNodes = []		# list of nodes that have been "ordered" on, but are queued by the cluster manager. e.g. GCNodeState.SCHEDULED
	deadlineScheduledNodes = []	# list of nodes that have been "ordered" on for deadlines, but are queued by the cluster manager. e.g. GCNodeState.SCHEDULED
	
	poweredNodes = 0		# all nodes that are consuming power
	allPoweredNodes = 0		# all nodes consuming power, including deadline nodes
	
	nodeOffTimes = {}
	
class GCScheduler(object):
	# i wilsysl call at each epoch
	# what information do i need
	# need:
	# 1) workload for next epoch
	# 2) solar for next epoch
	# 3) function f(load) -> (nodes required)
	# 4) function f(power) -> (nodes supported)
	
	# input: current node states
	# output: modified node states
	
	WORKLOAD_ERROR_MARGIN = 0
	
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000, oddEven=True):
		self.clock = ExperimentClock.getInstance()
		self.logger = GCLogger.getInstance().get_logger(self)
		
		self.solar_predictor = solar_predictor
		self.workload_predictor = workload_predictor
		self.cluster_manager = GCClusterManager.getInstance()
		self.data_collector = GCDataCollector.getInstance()
		self.response_time_model = response_time_model
		self.power_model = power_model
		self.target_response_time = target_response_time
		self.max_throughput = max_throughput
		self.odd_even = oddEven
		self.deltaLimit = 3
		
		# data structures
		self.nodeOffTimes = {}
	
		self.logger.log("Initialized GCScheduler")
		
	def getState(self):
		# get the current states of all cluster nodes
		nodeStates, nodeStateFlags, nodeOffTimes = self.cluster_manager.getNodeStates()
		
		onNodes = []
		offNodes = []
		transitionNodes = []
		scheduledNodes = []
		deadlineTransitionNodes = []
		deadlineScheduledNodes = []
		
		nodes = MINIMUM_NODES + OPTIONAL_NODES
		
		for n in nodes:
			if nodeStates[n] == GCNodeState.OFF:
				offNodes.append(n)
			elif nodeStates[n] == GCNodeState.ON:
				onNodes.append(n)
			elif nodeStates[n] == GCNodeState.TRANSITION:
				if nodeStateFlags[n] == GCNodeStateFlags.DEADLINE:
					deadlineTransitionNodes.append(n)
				else:
					transitionNodes.append(n)
					
			elif nodeStates[n] == GCNodeState.SCHEDULED:
				if nodeStateFlags[n] == GCNodeStateFlags.DEADLINE:
					deadlineScheduledNodes.append(n)
				else:
					scheduledNodes.append(n)
		
		poweredNodes = len(onNodes + transitionNodes + scheduledNodes)
		allPoweredNodes = len(onNodes + transitionNodes + deadlineTransitionNodes + scheduledNodes + deadlineScheduledNodes)

		state = GCSchedulerState()
		state.onNodes = onNodes
		state.offNodes = offNodes
		state.transitionNodes = transitionNodes
		state.deadlineTransitionNodes = deadlineTransitionNodes
		state.scheduledNodes = scheduledNodes
		state.deadlineScheduledNodes = deadlineScheduledNodes
		state.poweredNodes = poweredNodes
		state.allPoweredNodes = allPoweredNodes
		state.nodeOffTimes = nodeOffTimes
		
		#return onNodes, offNodes, transitionNodes, poweredNodes
		
		return state
	
	def getRecentReadKeys(self, recentReads, consider=OPTIONAL_NODES):
		keys = {}
		maxValue = 0
		
		for node in consider:
			if recentReads[node] > maxValue:
				maxValue = recentReads[node]
		
		if maxValue > 0:
			for node in consider:
				keys[node] = int((recentReads[node]*1.0 / maxValue) * 100)
		else:
			for node in consider:
				keys[node] = 0
		
		return keys
	
	def getSortedCandidates(self, nodesToConsider, recentReads, turnOn=True):
		candidateNodes = list(nodesToConsider)
		recentReadKeys = self.getRecentReadKeys(recentReads)
		recentReadKeysCandidates = self.getRecentReadKeys(recentReads, consider=candidateNodes)
		
		# turning on
		if turnOn:
			# sort them, first by even/odd, then by recent reads
			candidateNodes.sort(key=optionalSortKey)
			candidateNodes.sort(key=lambda(x):recentReadKeys[x], reverse=True)
			self.logger.debug("Candidates for turn on:")
		# turning off
		else:
			# sort them, first by even/odd, then by recent reads
			candidateNodes.sort(key=optionalSortKey, reverse=True)
			candidateNodes.sort(key=lambda(x):recentReadKeys[x])
			self.logger.debug("Candidates for turn off:")
		
		# log the candidates for debugging
		for node in candidateNodes:
			oddEven = OPTIONAL_NODES.index(node) % 2
			self.logger.debug("%s: %d (%d) - %d" % (node, recentReadKeys[node], recentReadKeysCandidates[node], oddEven))
			
		return candidateNodes
	
	def getSortedCandidatesModified(self, nodesToConsider, recentReads, turnOn=True):
		recentReadKeys = self.getRecentReadKeys(recentReads)
		recentReadKeysCandidates = self.getRecentReadKeys(recentReads, consider=nodesToConsider)

		# build a numpy array of recent read values
		recentReadValues = []
		for node in OPTIONAL_NODES:
			recentReadValues.append(recentReads[node])
			self.logger.debug("%s: %d" % (node, recentReads[node]))
			#recentReadValues.append(recentReadKeys[node])
			#self.logger.debug("%s: %d" % (node, recentReadKeys[node]))
		
		# and calc the mean and std. dev.
		arr = np.array(recentReadValues)		
		stdDev = arr.std()
		mean = arr.mean()
		self.logger.debug("Standard Deviation: %f" % stdDev)
		self.logger.debug("Mean: %f" % mean)
		
		# build two lists, hot nodes and regular nodes
		nodesHot = []
		#nodesRegular = []
		
		nodesOdd = []
		nodesEven = []
		
		# hot nodes are > 1 standard deviation away from (over?) the mean
		for node in nodesToConsider:
			#diff = recentReads[node] - mean			
			#if diff > stdDev:
			#	nodesHot.append(node)
			
			if OPTIONAL_NODES.index(node) % 2 == 0:
				nodesEven.append(node)
			else:
				nodesOdd.append(node)
			#else:
			#	nodesRegular.append(node)
		
		# sort hot nodes according to load (recent reads)
		nodesHot.sort(key=lambda(x):recentReadKeys[x], reverse=True)
		
		# sort odd and even the same way
		nodesOdd.sort(key=lambda(x):recentReadKeys[x], reverse=True)
		nodesEven.sort(key=lambda(x):recentReadKeys[x], reverse=True)
		
		# sort regular nodes according to odd/even strategy
		#nodesRegular.sort(key=optionalSortKey, reverse=True)

		# return a combined list
		#nodesAll = list(nodesHot + nodesRegular)
		nodesAll = list(nodesHot + nodesOdd + nodesEven)
		
		self.logger.debug("Hot Nodes:")
		for node in nodesHot:
			self.logger.debug(node)

		self.logger.debug("Odd Nodes:")
		for node in nodesOdd:
			self.logger.debug(node)
			
		self.logger.debug("Even Nodes:")
		for node in nodesEven:
			self.logger.debug(node)
			
		if turnOn == True:
			candidateNodes = list(nodesAll)
			self.logger.debug("Candidates for turn on:")
		else:
			candidateNodes = list(reversed(nodesAll))
			self.logger.debug("Candidates for turn off:")
		
		# log the candidates for debugging
		for node in candidateNodes:
			oddEven = OPTIONAL_NODES.index(node) % 2
			self.logger.debug("%s: %d (%d) - %d" % (node, recentReadKeys[node], recentReadKeysCandidates[node], oddEven))
		
		return candidateNodes
	
	def adjust(self, nodesRequired, nodesSupported):
		self.logger.log("adjust: Adjusting cluster size, nodesRequired %d, nodesSupported %d" % (nodesRequired, nodesSupported))
		
		# get state from cluster manager
		state = self.getState()
		poweredNodes = state.poweredNodes
		allPoweredNodes = state.allPoweredNodes
		nodeOffTimes = state.nodeOffTimes
		
		# including lists of nodes in each state
		onNodes = state.onNodes
		transitionNodes = state.transitionNodes
		offNodes = state.offNodes
		
		self.logger.debug("adjust: got cluster state")
		self.logger.debug("adjust: poweredNodes %d, allPoweredNodes %d" % (poweredNodes, allPoweredNodes))
		
		# check to make sure that we don't go below the minimum nodes
		nodesRequired = self.checkMinimumNodes(nodesRequired)

		# decide if we need to schedule anything
		targetNodes = max(nodesRequired, nodesSupported)
		
		if targetNodes == poweredNodes:
			self.logger.debug("adjust: no changes required")
			return None, []
				
		# get current live data from data collector
		results = self.getLiveResults()
		self.logger.log("%s" % str(results))
		if results:
			timestamps = results.availableTimestamps()
			hasLiveData = True
		else:
			hasLiveData = False
		
		# process the data into a dictionary that we can use
		if hasLiveData:
			self.logger.debug("adjust: has live data for recent reads")
		else:
			self.logger.debug("adjust: no live data, will use zeroes for recent reads")

		recentReads = {}
		for node in OPTIONAL_NODES:
			if hasLiveData:
				try:
					value = results.getNodeValue(timestamps[-1], node, "read_tracker_recent")
					recentReads[node] = value
				except Exception as e:
					print "Exception getting recent reads for %s, using 0" % node
					print e
					recentReads[node] = 0
			else:
				recentReads[node] = 0
		
		recentReadKeys = self.getRecentReadKeys(recentReads)
		
		self.logger.debug("adjust: Got recent read values")
		
		nodeOnCount = len(onNodes)
		nodeOffCount = len(offNodes)
		
		self.logger.debug("adjust: nodeOnCount %d, nodeOffCount %d" % (nodeOnCount, nodeOffCount))
		
		nodesChanged = []
		turningOn = None
		
		# are we bringing nodes up or down?
		# turning on
		if targetNodes > nodeOnCount:
			turningOn = True
			
			# calculate how many we need to turn on
			deltaNodes = targetNodes - nodeOnCount
			
			# limit to self.deltaLimit nodes on per cycle (3 by default but subclasses can override)
			if deltaNodes > self.deltaLimit:
				deltaNodes = self.deltaLimit
			
			self.logger.debug("adjust: adjusting up, deltaNodes: %d" % deltaNodes)
						
			# build a set of candidates for turn on
			candidateNodes = []
			for node in offNodes:
				if node in OPTIONAL_NODES:
					candidateNodes.append(node)
			
			# get the sorted candidates
			#candidateNodes = self.getSortedCandidates(candidateNodes, recentReads, turnOn=True)		
			candidateNodes = self.getSortedCandidatesModified(candidateNodes, recentReads, turnOn=True)		
	
			# turn on as many as necessary
			for i in xrange(0, min(deltaNodes, len(candidateNodes))):
				node = candidateNodes[i]
				self.logger.debug("adjust: turning on %s" % node)
				
				if self.cluster_manager.tryToPromoteDeadlineNode(node) == True:
					continue
				self.cluster_manager.nodeOn(node)
				nodesChanged.append(node)
		# turning off
		elif targetNodes < nodeOnCount:
			turningOn = False
			
			# calculate how many we need to turn off
			deltaNodes = nodeOnCount - targetNodes
			
			self.logger.debug("adjust: adjusting down, deltaNodes: %d" % deltaNodes)
			
			# build a set of candidates for turn off
			candidateNodes = []
			for node in onNodes:
				if node in OPTIONAL_NODES:
					candidateNodes.append(node)
			
			# get the sorted candidates (reverse = True means turn off mode)
			#candidateNodes = self.getSortedCandidates(candidateNodes, recentReads, turnOn=False)
			candidateNodes = self.getSortedCandidatesModified(candidateNodes, recentReads, turnOn=False)
			
			# turn off as many as necessary
			for i in xrange(0, min(deltaNodes, len(candidateNodes))):
				node = candidateNodes[i]
				self.logger.debug("adjust: turning off %s" % node)

				self.cluster_manager.nodeOff(node, delay=5)
				nodesChanged.append(node)
		
		self.cluster_manager.setCompactionParameters(targetNodes)
		
		print turningOn, nodesChanged
		return (turningOn, nodesChanged)
		
	def adjustForLoad(self, nodesRequired, shortest=True):
		self.logger.log("Adjusting for throughput, nodesRequired %d" % nodesRequired)
		
		state = self.getState()
		
		poweredNodes = state.poweredNodes
		allPoweredNodes = state.allPoweredNodes
		offNodes = state.offNodes
		nodeOffTimes = state.nodeOffTimes
		
		#print "poweredNodes", poweredNodes
		#print "allPoweredNodes", allPoweredNodes
		#print "offNodes", offNodes
		
		'''
		print "Throughput"
		print onNodes
		print offNodes
		print transitionNodes
		print poweredNodes, nodesRequired
		'''
		
		# if we need to turn on more nodes for workload
		# result: turn on more nodes (short turn on)
		
		self.logger.debug("poweredNodes %d, nodesRequired %d" % (poweredNodes, nodesRequired))

		if poweredNodes < nodesRequired:
			neededNodeCount = nodesRequired - poweredNodes
			self.logger.debug("neededNodeCount %d" % neededNodeCount)

			if shortest:
				selected = self.selectForTurnOnLoad(offNodes, neededNodeCount, nodeOffTimes)
			else:
				selected = self.selectForTurnOnPower(offNodes, neededNodeCount, nodeOffTimes)
			
			self.logger.debug("selected: %s" % str(selected))

			for n in selected:
				self.logger.log("Turning on %s for load" % n)
				if self.cluster_manager.tryToPromoteDeadlineNode(n) == True:
					continue
				self.cluster_manager.nodeOn(n)#, delay=25)
				#del self.nodeOffTimes[n]
		
		self.logger.debug("Completed adjusting for load")
	
	def adjustForDeadline(self, epochEnd, deadline=timedelta(hours=24)):
		self.logger.log("Adjusting for deadlines, deadline=%s" % str(deadline))
		
		state = self.getState()
		offNodes = state.offNodes
		poweredNodes = state.poweredNodes
		allPoweredNodes = state.allPoweredNodes
		nodeOffTimes = state.nodeOffTimes
		
		now = self.clock.get_current_time()
		
		
		# init bins. we will logically bin the nodes to determine a reasonable turn on schedule
		bins = {}
		for i in xrange(0, 96):
			bins[i] = []
			
		nodeDeadlines = {}
		
		deadlineViolations = []
		
		numOffNodes = len(offNodes)
		numTransitionNodes = len(state.transitionNodes) + len(state.deadlineTransitionNodes) + len(state.scheduledNodes) + len(state.deadlineScheduledNodes)
		epoch = timedelta(minutes=15)
		recoveryPerEpoch = 3
						
		# iterate over all off nodes
		for node in offNodes:
			nodeDeadline = nodeOffTimes[node] + deadline
			print "Node: %s, offTime %s, deadline %s" % (node, str(nodeOffTimes[node]), str(nodeDeadline))
			nodeDeadlines[node] = nodeDeadline
		
		currentEpoch = 95
		
		for node in reversed(sorted(nodeDeadlines.keys())):
			delta = nodeDeadlines[node] - now
			seconds = delta.total_seconds()
			minutes = seconds / 60.0
			epoch = int((minutes / 15.0) - 1)

			print "Node %s due on in %s, epoch t+%d" % (node, str(delta), epoch)
						
			currentBin = bins[epoch]
			while len(currentBin) >= recoveryPerEpoch and epoch > 0:
				epoch -= 1
				currentBin = bins[epoch]
			
			currentBin.append(node)
			# TODO: switch to this mode
			#currentBin.insert(0, node)
			del nodeDeadlines[node]
			print "...binned %s at t+%d" % (node, epoch)
			
		# sort top (current) bin first by odd/even order
		onBin = sorted(bins[0], key=optionalSortKey)
		# then by earliest deadline
		onBin = sorted(onBin, key=lambda(x):nodeDeadlines[x])
		
		# turn on nodes in (sorted) top bin
		for node in onBin:
			self.logger.log("Turning on %s for deadline" % node)
			print "Turning on %s for deadline" % node
			self.cluster_manager.nodeOn(node, method=GCClusterManager.ON_DEADLINE)
		
		self.logger.debug("Completed adjusting for deadlines")
		
	def adjustForPower(self, nodesSupported):
		self.logger.debug("Adjusting for power, nodesSupported %d" % nodesSupported)
		
		state = self.getState()		
		poweredNodes = state.poweredNodes
		allPoweredNodes = state.allPoweredNodes
		offNodes = state.offNodes
		nodeOffTimes = state.nodeOffTimes
		
		'''
		print "Power"
		print onNodes
		print offNodes
		print transitionNodes
		print poweredNodes, nodesSupported
		'''
		
		# if we exceed workload, but have free energy
		# result: turn on more nodes (long turn on)
		self.logger.debug("allPoweredNodes %d, nodesSupported %d" % (allPoweredNodes, nodesSupported))
		
		if poweredNodes < nodesSupported:
			neededNodeCount = nodesSupported - allPoweredNodes
			self.logger.debug("neededNodeCount %d" % neededNodeCount)
			
			selected = self.selectForTurnOnPower(offNodes, neededNodeCount, nodeOffTimes)
			self.logger.debug("selected: %s" % str(selected))
			
			for n in selected:
				self.logger.log("Turning on %s for power" % n)
				self.cluster_manager.nodeOn(n)#, delay=25)
		
		self.logger.debug("Completed adjusting for power")
	
	def adjustForExcess(self, nodesRequired, nodesSupported):
		self.logger.log("Adjusting for excess, nodesRequired %d, nodesSupported %d" % (nodesRequired, nodesSupported))
		
		state = self.getState()
		poweredNodes = state.poweredNodes
		onNodes = state.onNodes
		
		#print "Excess"
		#print "OnNodes", state.onNodes
		#rint "OffNodes", state.offNodes
		#print "TransitionNodes", state.transitionNodes
		#print poweredNodes, nodesRequired, nodesSupported
		
		# if we exceed workload and dont have excess energy
		# result: turn off some nodes
		
		self.logger.debug("poweredNodes %d, nodesRequired %d, nodesSupported %d" % (poweredNodes, nodesRequired, nodesSupported))
		if poweredNodes > nodesRequired or poweredNodes < nodesSupported:
			
			# we need to check onNodes here, because we can't turn them off unless they
			# have finished transitioning
			excessNodeCount = len(onNodes) - max(nodesRequired, nodesSupported)
			
			self.logger.debug("excessNodeCount %d" % excessNodeCount)
			
			# if we have excess nodes
			if excessNodeCount > 0:
				
				# pick the ones to turn off
				selected = self.selectForTurnOff(onNodes, excessNodeCount)
				self.logger.debug("selected: %s" % str(selected))

				# get the current time
				now = self.clock.get_current_time()
				
				# turn each one off and update the off time table
				for n in selected:
					self.logger.log("Turning off %s" % n)
					#print "Turning off %s" % n
					#self.nodeOffTimes[n] = now
					self.cluster_manager.nodeOff(n, delay=5)
		
		self.logger.debug("Completed adjusting for excess")
	
	def selectForTurnOnLoad(self, offNodes, target, offTimes):#=self.nodeOffTimes):
		return self.selectForTurnOnInternal(offNodes, target, offTimes, forLoad=True)
	
	def selectForTurnOnPower(self, offNodes, target, offTimes):#=self.nodeOffTimes):
		return self.selectForTurnOnInternal(offNodes, target, offTimes, forLoad=False)
		
	def selectForTurnOnInternal(self, offNodes, target, offTimes, forLoad=True):
		selected = []
		
		evenNodes = []
		oddNodes = []
		allNodes = []
		length = len(OPTIONAL_NODES)
		for i in range(0, length, 2):
			evenNodes.append(OPTIONAL_NODES[i])
		for i in range(1, length, 2):
			oddNodes.append(OPTIONAL_NODES[i])
		for i in range(0, length):
			allNodes.append(OPTIONAL_NODES[i])
		
		evenOffNodes = []
		oddOffNodes = []
		allOffNodes = []
		for node in evenNodes:
			if node in offNodes:
				evenOffNodes.append(node)
		for node in oddNodes:
			if node in offNodes:
				oddOffNodes.append(node)
		for node in allNodes:
			if node in offNodes:
				allOffNodes.append(node)
		
		if forLoad and self.odd_even:
			print sorted(evenOffNodes + oddOffNodes)
			print sorted(offTimes.keys())
			sortedEvenOffNodes = sorted(evenOffNodes, key=lambda(v):offTimes[v], reverse=True)
			sortedOddOffNodes = sorted(oddOffNodes, key=lambda(v):offTimes[v], reverse=True)
			sortedOffNodes = sortedEvenOffNodes + sortedOddOffNodes
		elif forLoad and not self.odd_even:
			print sorted(allNodes)
			print sorted(offTimes.keys())
			sortedOffNodes = sorted(allOffNodes, key=lambda(v):offTimes[v], reverse=True)
		else:
			sortedEvenOffNodes = sorted(evenOffNodes, key=lambda(v):offTimes[v])
			sortedOddOffNodes = sorted(oddOffNodes, key=lambda(v):offTimes[v])
			sortedOffNodes = sortedEvenOffNodes + sortedOddOffNodes
		
		
		for i in xrange(0, min(target, len(sortedOffNodes))):
			current = sortedOffNodes[i]
			selected.append(current)
		# TODO fix this
		return selected #[3:]

	def selectForTurnOff(self, onNodes, target):
		selected = []
		length = len(OPTIONAL_NODES)
		
		# we iterate, first trying even nodes, then off nodes
		for i in range(0, length, 2) + range(1, length, 2):
			
			if len(selected) == target:
				break
			
			current = OPTIONAL_NODES[i]
			if current in onNodes:
				selected.append(current)
				#print "turning off %s" % current
		
		return selected

	def checkMinimumNodes(self, nodesRequired):
		if nodesRequired < len(MINIMUM_NODES):
			self.logger.info("cannot use fewer than minimum nodes, (trying to use %d nodes)" % nodesRequired)
			return len(MINIMUM_NODES)
		
		return nodesRequired
		
	def schedule(self, intervalMinutes):
		raise NotImplementedError("This class (GCScheduler) is abstract. Please subclass it and override the schedule() method.")
	
	def waitForSchedule(self):
		self.cluster_manager.waitForTransitions()
	
	def getLiveResults(self):
		if self.data_collector == None:
			return None
		
		return self.data_collector.getLiveResults()
	
	def getLastThroughput(self):
		results = self.getLiveResults()
		if results == None:
			return -1
		
		timestamps = results.availableTimestamps()
		
		samples = []
		
		count = 0
		for ts in reversed(timestamps):
			if count > 5:
				break
			
			v = results.getCommonValue(ts, "actual_workload")
			if math.isnan(v):
				continue
			
			samples.append(v)
			count += 1
		
		if len(samples) == 0:
			return -1
		
		return sum(samples) / len(samples)

class GCNoopScheduler(GCScheduler):

	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCNoopScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)

	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCNoopScheduler)")
		self.logger.info("Completed schedule")

class GCStaticScheduler(GCScheduler):

	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000, nodes=NUM_NODES):
		super(GCStaticScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.nodes = nodes

	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCStaticScheduler)")
		self.adjust(self.nodes, self.nodes);
		self.logger.info("Completed schedule")

class GCSpecificScheduler(GCScheduler):
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000, nodes=MINIMUM_NODES+OPTIONAL_NODES):
		super(GCSpecificScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.nodes = nodes
		self.scheduled = False
	
	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCSpecificScheduler)")
		
		if not self.scheduled:
			for node in MINIMUM_NODES+OPTIONAL_NODES:
				if node not in self.nodes:
					self.cluster_manager.nodeOff(node, delay=5)
			
			self.scheduled = True
		self.logger.info("Completed schedule")
	
class GCOptimizerScheduler(GCScheduler):

	def __init__(self, trace=None):
		super(GCOptimizerScheduler, self).__init__(solar_predictor=None, workload_predictor=None, response_time_model=None, power_model=None)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.trace = trace
		
		if self.trace == None:
			raise Exception("GCOptimizerScheduler: No trace specified...")
		
		self.sched = {}
		
		f = open(self.trace, "r")
		data = f.read().strip()
		lines = data.split("\n")
		
		for line in lines:
			time, nodes = line.split(" ")
			dt = datetime.strptime(time, "%H:%M")
			nodes = int(nodes)
			self.sched[dt] = nodes			
			#print dt, nodes

	def schedule(self, intervalMinutes):
		self.logger.log("Starting schedule (GCOptimizerScheduler)")
		current_dt = self.clock.get_current_time()
		index_dt = datetime(month=1, day=1, year=1900, hour=current_dt.hour, minute=current_dt.minute, second=current_dt.second)
		keys = sorted(self.sched.keys())
		nodes = None
		
		#for k1, k2 in grouper(keys, 2):
		for i in xrange(1, len(keys)):
			k1 = keys[i-1]
			k2 = keys[i]
			
			#print "checking", index_dt, k1, k2
			if k1 <= index_dt and index_dt < k2:
				#print "match", k1
				nodes = self.sched[k1]
				break
		
		if nodes == None and index_dt >= keys[-1]:
			nodes = self.sched[keys[-1]]	
		
		assert nodes != None
		print "DEBUG: schedule has %d nodes" % nodes
		
		self.adjustForLoad(nodes)
		self.adjustForExcess(nodes, nodes)
		
		self.logger.log("Completed schedule")
		
class GCOnlineOptimizerScheduler(GCScheduler):
	def __init__(self, solar_predictor, workload_predictor, target_response_time=75, max_throughput=7700, accel_factor=1, use_brown_prices=False, response_time_model=None, workload_perfect=None, use_battery=False):
		super(GCOnlineOptimizerScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, None, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.start_time = self.clock.start_time
		self.accel_factor = accel_factor
		self.use_brown_prices = use_brown_prices
		self.workload_perfect = workload_perfect
		self.use_battery = use_battery
		
		self.peakPower = 0.0
		
		self.electricityModel = GCElectricityPriceModel()
		
		self.baseOperations = 0
		self.baseOperationsLessThanMs = 0
		
		# init optimizer object
		if self.use_battery:
			self.optimizer = OnlineOptimizerBatt()
		else:
			self.optimizer = OnlineOptimizer()
		
		# create an empty histogram, just in case
		buckets = {}
		for i in xrange(0, 1001):
			buckets[i] = 0
		self.emptyHistogram = GCResponseTimeHistogram(buckets=buckets)
		
		# delta limit unlimited
		self.deltaLimit = sys.maxint
		
		self.logger.info("Initialized scheduler")
		self.logger.info("Use brown prices: %s" % str(self.use_brown_prices))
		
	def schedule(self, intervalMinutes, epoch=0, histogram=None):
		self.logger.info("Starting schedule (GCOnlineOptimizerScheduler)")

		if self.use_battery:
			powerManager = GCParasolPowerManager.getInstance()
		
		# save the current time
		scheduleTime = self.clock.get_current_time()
		
		# create a timedelta for the interval
		delta = timedelta(minutes=intervalMinutes)
		
		# get current live data from data collector
		results = self.getLiveResults()
		if results:
			timestamps = results.availableTimestamps()
			hasLiveData = True
		else:
			hasLiveData = False
		
		# first we need to get a raw histogram (as a string) from the data collector
		if histogram == None:
			if not hasLiveData:
				histogram = self.emptyHistogram
			else:
				for i in xrange(0, 3):
					histogram = self.data_collector.getLastValue("histogram_read_%s" % YCSB_HOSTNAME)
					if not histogram.isEmpty():
						break
					
					self.logger.info("error, no histogram value, retrying in a bit...")
					time.sleep(self.data_collector.interval * 1.5)	

				if histogram.isEmpty():
					self.logger.info("error, no histogram value")
					histogram = self.emptyHistogram
				else:
					self.logger.info("got a good histogram")
		else:
			self.logger.info("passed histogram with %d" % histogram.getTotalOperations())
			
		# get the values that we need out of the histogram
		previousOperations = histogram.getTotalOperations()
		previousOperationsLessThanMs = histogram.getTotalOperationsLessThanMs(self.target_response_time)
		self.logger.info("previousOperations %d" % previousOperations)
		self.logger.info("previousOperationsLessThanMs %d" % previousOperationsLessThanMs)

		totalExperimentTime = scheduleTime - self.start_time
		totalExperimentTimeInSeconds = totalExperimentTime.total_seconds()
		self.logger.info("experimentTime %s, experimentTimeInSeconds %d" % (str(totalExperimentTime), totalExperimentTimeInSeconds))
		
		# this is the total number of past epochs
		totalExperimentEpochs = (totalExperimentTimeInSeconds / 60.0) / intervalMinutes
		self.logger.info("totalExperimentEpochs: %0.2f" % totalExperimentEpochs)
		
		if totalExperimentTimeInSeconds == 0:
			previousOperationsPerSecond = 0
			previousOperationsPerSecondLessThanMs = 0
		else:
			previousOperationsPerSecond = (previousOperations / totalExperimentTimeInSeconds) * totalExperimentEpochs * self.accel_factor
			previousOperationsPerSecondLessThanMs = (previousOperationsLessThanMs / totalExperimentTimeInSeconds) * totalExperimentEpochs * self.accel_factor
		
		self.logger.info("previousOperationsPerSecond: %d" % previousOperationsPerSecond)
		self.logger.info("previousOperationsPerSecondLessThanMs: %d" % previousOperationsPerSecondLessThanMs)

		# next, calculate the peak power
		if hasLiveData:
			samples = []
			for ts in reversed(timestamps):
				if ts > scheduleTime:
					continue
				if ts < scheduleTime - delta:
					break
				
				v = results.getCommonValue(ts, "brown_consumed")
				if math.isnan(v):
					continue
				
				samples.append(v)
				
			lastAverageBrown = sum(samples) / len(samples)
			if lastAverageBrown > self.peakPower:
				self.peakPower = lastAverageBrown
				
			self.logger.info("Average brown power for last cycle: %0.2f W" % lastAverageBrown)
			self.logger.info("Current peak power: %0.2f W" % self.peakPower)
			peakPowerForSolver = self.peakPower
		else:
			peakPowerForSolver = float(sys.maxint)
			self.logger.info("No peak power, using %0.2f" % peakPowerForSolver)
		
		peakPowerPriceForSolver = self.electricityModel.getPeakPowerPrice(scheduleTime - delta)
		
		# now, we need to generate 48 hours of data at the specified interval
		workloadPredictions = []
		workloadPerfects = []
		solarPredictions = []
		brownPrices = []
		
		start_dt = datetime.now()
		# start with the timestamp we took earlier
		currentTime = scheduleTime
		while currentTime < scheduleTime + timedelta(hours=24):
			
			# we want the second 24 hours to be a repeat of the first, so make sure that the date
			# always stays the same as the starting date
			dt = datetime(month=self.start_time.month, day=self.start_time.day, year=self.start_time.year, hour=currentTime.hour, minute=currentTime.minute, second=currentTime.second)
			
			# get the predictions
			workloadPrediction = denormalize(self.workload_predictor.getMaxWorkloadForInterval(dt, delta, sample_step=timedelta(seconds=30)), self.max_throughput)
			
			if self.workload_perfect:
				# get the perfect workload (only for debugging)
				workloadPerfect = denormalize(self.workload_perfect.getMaxWorkloadForInterval(dt, delta, sample_step=timedelta(seconds=30)), self.max_throughput)
			else:
				workloadPerfect = 0
				
			if currentTime == scheduleTime:
				lastThroughput = self.getLastThroughput()
				self.logger.info("Getting workload for first prediction epoch:")
				self.logger.info("Last throughput: %0.2f" % lastThroughput)
				self.logger.info("Prediction: %0.2f" % workloadPrediction)
				
				#workloadPrediction = max(self.getLastThroughput(), workloadPrediction)
				if lastThroughput > 0:
					workloadPrediction = lastThroughput
				elif self.workload_perfect:
					self.logger.info("Has workload perfect, this is simulation")
					workloadPrediction = workloadPerfect
							
			solarPrediction = self.solar_predictor.getAvgSolarForInterval(dt, delta)
			
			# use 1.0 if we dont want brown prices
			if self.use_brown_prices:
				brownPrice = self.electricityModel.getEnergyPrice(dt)
			else:
				brownPrice = 1.0
			
			# add them to the lists
			workloadPredictions.append(workloadPrediction)
			workloadPerfects.append(workloadPerfect)
			solarPredictions.append(solarPrediction)
			brownPrices.append(brownPrice)
			
			currentTime += delta
		
		
		ourState = self.getState()
		prevNumNodes = len(ourState.onNodes) + len(ourState.transitionNodes)
		self.logger.info("prevNumNodes for solver: %d" % prevNumNodes)
		
		self.logger.info("Elapsed time to get data: %s" % str(datetime.now() - start_dt))
		
		start_dt = datetime.now()
		
		self.logger.info("%d %d %d" % (len(workloadPredictions), len(solarPredictions), len(brownPrices)))
		
		for i in xrange(0, len(workloadPredictions)):
			self.logger.log("%0.2f %0.2f %0.2f" % (workloadPredictions[i], solarPredictions[i], brownPrices[i]))
			
		#maxWindowSLA = max((96-epoch)+1, 5)
		maxWindowSLA = (96-epoch)+1
		
		# this will return the number of nodes that we should run in the next epoch
		if self.use_battery:
			self.logger.log("maxWindowSLA going to solver: %d" % maxWindowSLA)
			prevBattLevel = powerManager.battery.getBatteryLevel()
			optSched = self.optimizer.getOptSchedule(workloadPredictions, solarPredictions, brownPrices, prevNumNodes, previousOperationsPerSecond, previousOperationsPerSecondLessThanMs, peakPowerForSolver, peakPowerPriceForSolver, maxWindowSLA, prevBattLevel)
		else:
			optSched = self.optimizer.getOptSchedule(workloadPredictions, solarPredictions, brownPrices, prevNumNodes, previousOperationsPerSecond, previousOperationsPerSecondLessThanMs, peakPowerForSolver, peakPowerPriceForSolver, maxWindowSLA)
			
		#print optSched
		self.logger.info("Elapsed time to run optimizer %s" % str(datetime.now() - start_dt))
		
		if optSched == None:
			self.logger.info("Didn't get a solution, we can't schedule anything, so lets bail now...")
			return
		
		assert len(optSched) > 0
		
		# filter out single epoch drops
		#if prevNumNodes == optSched[1] and optSched[0] < prevNumNodes:
		#	self.logger.info("Filtered drop in nodes from %d to %d" % (prevNumNodes, optSched[0]))
		#	nodesRequired = prevNumNodes
		#else:
		# if we used the battery, we not only need to schedule, but we need to change the config of the power manager
		if self.use_battery:
			nodesRequired = optSched[0]["NumNodes"]
			powerManager.setTargetParams(optSched[0]["BPowerBatt"], optSched[0]["GPowerBatt"], optSched[0]["GPowerNet"], optSched[0]["BattPowerLoad"])
		else:
			nodesRequired = optSched[0]
			
		self.logger.info("Scheduling %d nodes" % nodesRequired)
		'''
		self.logger.info("Expected:")
		
		expectedResponse = []
		perfectResponse = []
		aggExpectedResponse = 0.0
		aggPerfectResponse = 0.0
		totalWorkload = 0.0
		
		for i in xrange(0, 96):
			if self.response_time_model:
				expectedResponse.append(self.response_time_model.expectedLatency(optSched[i], workloadPredictions[i]))
				perfectResponse.append(self.response_time_model.expectedLatency(optSched[i], workloadPerfects[i]))
			else:
				expectedResponse.append(0)
				perfectResponse.append(0)
				
		for i in xrange(0, 96):
			aggExpectedResponse += expectedResponse[i]*workloadPredictions[i]
			aggPerfectResponse += perfectResponse[i]*workloadPredictions[i]
			totalWorkload += workloadPredictions[i]
		
		for i in xrange(0, 96):
			try:
				hours = i / 4
				minutes = (i % 4) * 15
				if self.use_battery:
					self.logger.info("t+%02d:%02d\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%d\t%0.2f\t%0.2f" % (hours, minutes, workloadPredictions[i], workloadPerfects[i], solarPredictions[i], brownPrices[i], optSched[i]["NumNodes"], expectedResponse[i], perfectResponse[i]))
				else:
					self.logger.info("t+%02d:%02d\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%d\t%0.2f\t%0.2f" % (hours, minutes, workloadPredictions[i], workloadPerfects[i], solarPredictions[i], brownPrices[i], optSched[i], expectedResponse[i], perfectResponse[i]))
			except Exception as e:
				self.logger.info("Weirdness getting data at index %d, exception: %s" % (i, e))
		
		self.logger.info("Overall expected response time: %0.2f ms" % (aggExpectedResponse / totalWorkload))
		self.logger.info("Overall perfect response time: %0.2f ms" % (aggPerfectResponse / totalWorkload))
		'''
		nodesSupported = nodesRequired
		
		self.adjust(nodesRequired, nodesSupported)
		
		#except Exception as e:
		#	self.logger.info("Exception scheduling, leaving schedule alone: %s" % e)
			
		self.logger.info("Completed schedule")

class GCOnlineOptimizerScheduler2(GCScheduler):
	def __init__(self, solar_predictor, workload_predictor, target_response_time=75, max_throughput=None, accel_factor=1, use_brown_prices=False, epoch=15):
		super(GCOnlineOptimizerScheduler2, self).__init__(solar_predictor, workload_predictor, None, None, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.start_time = self.clock.start_time
		self.accel_factor = accel_factor
		self.use_brown_prices = use_brown_prices
		self.epoch = 15
		
		self.peakPower = 0.0
		
		self.electricityModel = GCElectricityPriceModel()
		
		# create an empty histogram, just in case
		buckets = {}
		for i in xrange(0, 1001):
			buckets[i] = 0
		self.emptyHistogram = GCResponseTimeHistogram(buckets=buckets)
		
		self.initClient()
	
	def initClient(self):
		# init scheduler client
		self.schedulerClient = SchedulerClient("crypt06")
		self.schedulerStarted = False
		self.schedulerClient.setNodes(MINIMUM_NODES + OPTIONAL_NODES)
		self.schedulerClient.setCoveringNodes(MINIMUM_NODES)
		
	def getPeakPowerInfo(self):
		if self.data_collector != None:
			liveData = self.data_collector.getLiveData()
		else:
			liveData = None
		
		if liveData != None:
			results = GCResults(liveData=liveData)
			timestamps = results.availableTimestamps()
			
			samples = []
			for ts in reversed(timestamps):
				if ts > scheduleTime:
					continue
				if ts < scheduleTime - delta:
					break
				
				v = results.getCommonValue(ts, "brown_consumed")
				if math.isnan(v):
					continue
				
				samples.append(v)
				
			lastAverageBrown = sum(samples) / len(samples)
			if averageBrown > self.peakPower:
				self.peakPower = lastAverageBrown
				
			print "Average brown power for last cycle: %0.2f W" % lastAverageBrown
			print "Current peak power: %0.2f W" % self.peakPower
			peakPowerForSolver = self.peakPower
		else:
			peakPowerForSolver = float(sys.maxint)
			print "No peak power, using %0.2f" % peakPowerForSolver
		
		peakPowerPriceForSolver = self.electricityModel.getPeakPowerPrice(scheduleTime - delta)
		
		return peakPowerForSolver, peakPowerPriceForSolver
	
	def getOperationInfo(self, scheduleTime, histogram):
		# first we need to get a raw histogram (as a string) from the data collector
		if histogram == None:
			if self.data_collector == None:
				histogram = self.emptyHistogram
			else:
				for i in xrange(0, 3):
					rawHistogram = self.data_collector.getLastTextData("histogram.read.%s" % YCSB_HOSTNAME)
					if rawHistogram != "":
						break
					print "GCOnlineOptimizerScheduler2: error, no histogram value, retrying in a bit..."
					time.sleep(self.data_collector.results_interval * 1.5)
				
				if rawHistogram == "":
					print "GCOnlineOptimizerScheduler2: error, no histogram value"
					histogram = self.emptyHistogram
				else:
					print "GCOnlineOptimizerScheduler2: got a good histogram"
					# convert the raw histogram into our data structure
					histogram = GCResponseTimeHistogram(rawHistogram)
			
		# get the values that we need out of the histogram
		previousOperations = histogram.getTotalOperations()
		previousOperationsLessThanMs = histogram.getTotalOperationsLessThanMs(self.target_response_time)
		print "previousOperations %d, previousOperationsLessThanMs %d" % (previousOperations, previousOperationsLessThanMs)

		totalExperimentTime = scheduleTime - self.start_time
		totalExperimentTimeInSeconds = totalExperimentTime.total_seconds()
		print "scheduleTime: %s, experimentTime %s, experimentTimeInSeconds %d" % (self.clock.get_current_time(), str(totalExperimentTime), totalExperimentTimeInSeconds)
		
		# this is the total number of past epochs
		totalExperimentEpochs = (totalExperimentTimeInSeconds / 60.0) / self.epoch
		print "totalExperimentEpochs: %0.2f" % totalExperimentEpochs
		
		if totalExperimentTimeInSeconds == 0:
			previousOperationsPerSecond = 0
			previousOperationsPerSecondLessThanMs = 0
		else:
			previousOperationsPerSecond = (previousOperations / totalExperimentTimeInSeconds) * totalExperimentEpochs * self.accel_factor
			previousOperationsPerSecondLessThanMs = (previousOperationsLessThanMs / totalExperimentTimeInSeconds) * totalExperimentEpochs * self.accel_factor
		
		print "previousOperationsPerSecond: %d" % previousOperationsPerSecond
		print "previousOperationsPerSecondLessThanMs: %d" % previousOperationsPerSecondLessThanMs
		
		return previousOperationsPerSecond, previousOperationsPerSecondLessThanMs
	
	def getPredictions(self, scheduleTime):
		# now, we need to generate 48 hours of data at the specified interval
		workloadPredictions = []
		solarPredictions = []
		brownPrices = []

		delta = timedelta(minutes=self.epoch)
		
		# start with the timestamp we took earlier
		currentTime = scheduleTime
		while currentTime < scheduleTime + timedelta(hours=24):
			
			# we want the second 24 hours to be a repeat of the first, so make sure that the date
			# always stays the same as the starting date
			dt = datetime(month=self.start_time.month, day=self.start_time.day, year=self.start_time.year, hour=currentTime.hour, minute=currentTime.minute, second=currentTime.second)
			
			# get the predictions
			workloadPrediction = denormalize(self.workload_predictor.getMaxWorkloadForInterval(dt, delta, sample_step=timedelta(seconds=30)), self.max_throughput)
			solarPrediction = self.solar_predictor.getAvgSolarForInterval(dt, delta)
			
			# use 1.0 if we dont want brown prices
			if self.use_brown_prices:
				brownPrice = self.electricityModel.getEnergyPrice(dt)
			else:
				brownPrice = 1.0
			
			# add them to the lists
			workloadPredictions.append(workloadPrediction)
			solarPredictions.append(solarPrediction)
			brownPrices.append(brownPrice)
			
			currentTime += delta
		
		return workloadPredictions, solarPredictions, brownPrices
	
	def getNodeStateInfo(self, scheduleTime):
		state = self.getState()
		
		onNodes = state.onNodes
		offNodes = state.offNodes
		transitionNodes = state.transitionNodes
		nodeOffTimes = self.nodeOffTimes
		
		# we fill this up
		nodeStates = {}
		
		for node in offNodes:
			timeOffDelta = scheduleTime - self.nodeOffTimes[node]
			seconds = timeOffDelta.total_seconds()
			minutes = seconds / 60.0
			value = int(math.ceil(minutes/self.epoch)) * -1
			nodeStates[node] = value
		
		poweredNodes = onNodes + transitionNodes
		for node in [x for x in poweredNodes if x not in MINIMUM_NODES]:
			nodeStates[node] = 0
		
		return nodeStates
		
	def printSchedule(self, workloadPredictions, solarPredictions, brownPrices, optSched):
		print "Expected:"
		for i in xrange(0, 96):
			try:
				hours = i / 4
				minutes = (i % 4) * 15
				print "t+%02d:%02d\t%0.2f\t%0.2f\t%0.2f\t%d" % (hours, minutes, workloadPredictions[i], solarPredictions[i], brownPrices[i], optSched[i])
			except Exception as e:
				print "Weirdness getting data at index %d" % i, e
	
	def adjustForSchedule(self, scheduleTime, nodeSchedule):
		state = self.getState()
		
		onNodes = state.onNodes
		offNodes = state.offNodes
		transitionNodes = state.transitionNodes
		poweredNodes = onNodes + transitionNodes
		
		turnOn = [x for x in nodeSchedule if x not in poweredNodes]
		turnOff = [x for x in onNodes if x not in nodeSchedule]
		
		for node in turnOn:
			self.logger.log("Turning on %s" % node)
			print "Turning on %s" % node
			self.cluster_manager.nodeOn(node)
			del self.nodeOffTimes[node]
		
		for node in turnOff:
			self.logger.log("Turning off %s" % node)
			print "Turning off %s" % node
			self.cluster_manager.nodeOff(node, delay=5)
			self.nodeOffTimes[node] = scheduleTime
		
	def schedule(self, intervalMinutes, histogram=None):
		self.logger.log("Starting schedule (GCOnlineOptimizerScheduler2)")

		# save the current time
		scheduleTime = self.clock.get_current_time()
		
		# create a timedelta for the interval
		delta = timedelta(minutes=intervalMinutes)
		
		# operation info
		previousOperationsPerSecond, previousOperationsPerSecondLessThanMs = self.getOperationInfo(scheduleTime, histogram)

		# peak power info
		#peakPowerForSolver, peakPowerPriceForSolver = self.getPeakPowerInfo()
		
		# predictions
		workloadPredictions, solarPredictions, brownPrices = self.getPredictions(scheduleTime)
		
		# node state info
		nodeStates = self.getNodeStateInfo(scheduleTime)
		
		# TODO: integrate Inigo's optimizer here
		# this will return the number of nodes that we should run in the next epoch
		
		start_dt = datetime.now()
		
		print "nodeStates"
		print nodeStates
		
		goodSchedule = False
		
		for i in xrange(0, 3):
			if i > 0:
				print "Getting schedule, retry", i
			try:
				self.schedulerClient.setWorkload(workloadPredictions)
				self.schedulerClient.setGreenAvailability(solarPredictions)
				self.schedulerClient.setBrownPrices(brownPrices)
				self.schedulerClient.setPrevLoad(previousOperationsPerSecond, previousOperationsPerSecondLessThanMs)
				self.schedulerClient.setNodeStates(nodeStates)
				
				if not self.schedulerStarted:
					self.schedulerClient.start()
					self.schedulerStarted = True
				
				nodeSchedule = self.schedulerClient.getSchedule()
				goodSchedule = True 
				break
			
			except Exception as e:
				print "Exception scheduling, reiniting client:", e
				self.looger.log("Exception scheduling, reiniting client:", e)
				self.initClient()
			
			time.sleep(3)
		
		if not goodSchedule:
			print "Could not get schedule after three tries, leaving schedule alone"
			
		print nodeSchedule
		
		elapsed_time = datetime.now() - start_dt
		print "Elapsed time to run optimizer", elapsed_time

		self.adjustForSchedule(scheduleTime, nodeSchedule)

		self.logger.log("Completed schedule")

class GCBaselineScheduler(GCScheduler):
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCBaselineScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.fudge = 4
		self.lastThroughput = 0 #denormalize(self.workload_predictor.getWorkloadAt(self.clock.get_current_time()), self.max_throughput)
		
	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCBaselineScheduler)")
		
		now = self.clock.get_current_time()
		delta = timedelta(minutes=intervalMinutes)

		instantaneousThroughput = self.data_collector.getLastValue("actual_workload")
		currentThroughput = float("nan")
		
		# get current live data from data collector
		results = self.getLiveResults()
		if results:
			timestamps = results.availableTimestamps()
			hasLiveData = True
		
		if hasLiveData:
			self.logger.info("got live data...")
			currentThroughput = results.getCommonValue(now, "workload_exp_moving_avg")
			self.logger.info("currentThroughput: %f" % currentThroughput)

		if math.isnan(currentThroughput):
			self.logger.info("Got NaN for throughput, using last saved value...(%0.2f ops/sec)" % self.lastThroughput)
			currentThroughput = self.lastThroughput
		else:
			self.lastThroughput = currentThroughput
				
		self.logger.info("instantaneousThroughput %0.2f, moving average (currentThroughput) %0.2f" % (instantaneousThroughput, currentThroughput))

		nodesRequired = self.response_time_model.nodesRequired(currentThroughput, self.target_response_time)
		self.logger.info("model says %d nodes, adding fudge of %d nodes." % (nodesRequired, self.fudge))
		
		nodesRequired += self.fudge
		nodesSupported = len(MINIMUM_NODES)
		reversedMs = self.response_time_model.expectedLatency(nodesRequired, currentThroughput)
		
		self.logger.info("model says we need %d nodes to hit %d ms" % (nodesRequired, self.target_response_time))
		self.logger.info("reversed, at %d nodes and %f ops/s we expect %d ms" % (nodesRequired, currentThroughput, reversedMs))
		
		self.adjust(nodesRequired, nodesSupported)
		
		self.logger.info("Completed schedule")

class GCLoadScheduler(GCScheduler):
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000, fudge=0):
		super(GCLoadScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.lookAhead = 1
		self.fudge = fudge
		
	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCLoadScheduler)")
		
		now = self.clock.get_current_time()
		delta = timedelta(minutes=intervalMinutes)
		
		# get last throughput from live data
		lastThroughput = self.getLastThroughput()
		
		# get next throughput from predictor
		nextThroughput = denormalize(self.workload_predictor.getMaxWorkloadForInterval(now, delta*self.lookAhead), self.max_throughput) + self.WORKLOAD_ERROR_MARGIN
		
		currentThroughput = max(lastThroughput, nextThroughput)
		self.logger.info("lastThroughput = %f, nextThroughput = %f, currentThroughput = %f" % (lastThroughput, nextThroughput, currentThroughput))
		
		nodesRequired = self.response_time_model.nodesRequired(currentThroughput, self.target_response_time)
		self.logger.info("model says %d nodes, adding fudge of %d" % (nodesRequired, self.fudge))
		
		nodesRequired += self.fudge
		nodesSupported = len(MINIMUM_NODES)
		reversedMs = self.response_time_model.expectedLatency(nodesRequired, currentThroughput)
		
		self.logger.info("model says we need %d nodes to hit %d ms" % (nodesRequired, self.target_response_time))	
		self.logger.info("reversed, at %d nodes and %f ops/s we expect %d ms" % (nodesRequired, currentThroughput, reversedMs))

		self.adjust(nodesRequired, nodesSupported)
		
		self.logger.info("Completed schedule")
		
class GCSolarScheduler(GCScheduler):
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCSolarScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
	
	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCSolarScheduler)")
		
		now = self.clock.get_current_time()
		delta = timedelta(minutes=intervalMinutes)
		# check by the min solar rather than max
		currentPower = self.solar_predictor.getAvgSolarForInterval(now, delta)
		
		nodesRequired = len(MINIMUM_NODES)
		nodesSupported = self.power_model.nodesSupported(currentPower)

		self.adjust(nodesRequired, nodesSupported)
		
		self.logger.info("Completed schedule")
		
class GCSimplePolicyScheduler(GCScheduler):
	
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCSimplePolicyScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.lookAhead = GCConfig.getInstance().getLookAhead()
		
	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCSimplePolicyScheduler)")
		
		now = self.clock.get_current_time()
		delta = timedelta(minutes=intervalMinutes)

		# get last throughput from live data		
		lastThroughput = self.getLastThroughput()
		
		# get next throughput from predictor
		nextThroughput = denormalize(self.workload_predictor.getMaxWorkloadForInterval(now, delta*self.lookAhead), self.max_throughput) + self.WORKLOAD_ERROR_MARGIN
		
		# get the throughput that we will use for the next epoch
		currentThroughput = max(lastThroughput, nextThroughput)
		self.logger.info("lastThroughput = %f, nextThroughput = %f, currentThroughput = %f" % (lastThroughput, nextThroughput, currentThroughput))

		# use avg solar for interval
		currentPower = self.solar_predictor.getAvgSolarForInterval(now, delta)
		
		# how many nodes required for load / supported by solar
		nodesRequired = self.response_time_model.nodesRequired(currentThroughput, self.target_response_time)
		nodesSupported = self.power_model.nodesSupported(currentPower)
		reversedMs = self.response_time_model.expectedLatency(nodesRequired, currentThroughput)

		self.logger.info("model says we need %d nodes to hit %d ms" % (nodesRequired, self.target_response_time))
		self.logger.info("reversed, at %d nodes and %f ops/s we expect %d ms" % (nodesRequired, currentThroughput, reversedMs))
		self.logger.info("solar supports %d nodes" % nodesSupported)
		
		self.adjust(nodesRequired, nodesSupported)
		
		self.logger.info("Completed schedule")

class GCCreditHeuristicScheduler(GCScheduler):
	
	BURN_CREDIT_FACTOR = 0.95
	DEGRADATION_FACTOR = 1.33
	RECOVER = True
	RECOVERY_FACTOR = 0.90
		
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCCreditHeuristicScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.lookAhead = GCConfig.getInstance().getLookAhead()
	
	def getCurrentCumLatency(self):
		results = self.getLiveResults()
		
		if results == None:
			return -1
		
		return self.data_collector.getLastValue("readlatency_99_cum")
	
	def schedule(self, intervalMinutes):
		self.logger.info("Starting schedule (GCCreditHeuristicScheduler)")
		
		now = self.clock.get_current_time()
		delta = timedelta(minutes=intervalMinutes)

		# get all relevant information
		
		# get last throughput from live data		
		lastThroughput = self.getLastThroughput()
		
		# get next throughput from predictor
		nextThroughput = denormalize(self.workload_predictor.getMaxWorkloadForInterval(now, delta*self.lookAhead), self.max_throughput) + self.WORKLOAD_ERROR_MARGIN

		# get the throughput that we will use for the next epoch
		currentThroughput = max(lastThroughput, nextThroughput)
		self.logger.info("lastThroughput = %f, nextThroughput = %f, currentThroughput = %f" % (lastThroughput, nextThroughput, currentThroughput))

		# figure out how much solar we have
		# use avg solar for interval
		currentPower = self.solar_predictor.getAvgSolarForInterval(now, delta)
		
		# how many nodes required for load / supported by solar
		# initial calculation, we may modify later
		nodesRequired = self.response_time_model.nodesRequired(currentThroughput, self.target_response_time)
		nodesSupported = self.power_model.nodesSupported(currentPower)
		
		# see where we are in relation to the SLA
		currentCumLatency = self.getCurrentCumLatency()
		self.logger.info("current cumulative 99th percentile: %0.2f" % currentCumLatency)
		
		thisTargetResponseTime = self.target_response_time
		
		# cases:
		# a) has solar to accrue credit
		# b) has credit to burn
		# c) has no solar and no credit
		
		# if we have excess solar, we guess that we will accure credit in the next cycle
		# schedule normally
		if nodesSupported > nodesRequired:
			self.logger.info("excess solar, try to build credit")		
		
		# else if we have credit
		# note that we won't degrade more than we use brown energy
		elif currentCumLatency < self.target_response_time*self.BURN_CREDIT_FACTOR:
			self.logger.info("burn credit")
			thisTargetResponseTime = math.ceil(self.target_response_time*self.DEGRADATION_FACTOR)
			nodesRequired = self.response_time_model.nodesRequired(currentThroughput, thisTargetResponseTime)

		# if we have exceeded the SLA for some reason
		elif currentCumLatency > self.target_response_time and self.RECOVER == True:
			self.logger.info("exceeded SLA, try to recover")
			thisTargetResponseTime = math.ceil(self.target_response_time*self.RECOVERY_FACTOR)
			nodesRequired = self.response_time_model.nodesRequired(currentThroughput, thisTargetResponseTime)
			
		# else we have no solar and no credit, just do load
		else:
			self.logger.info("no credit, no solar, just do load")
			pass
		
		reversedMs = self.response_time_model.expectedLatency(nodesRequired, currentThroughput)
		self.logger.info("policy says we need %d nodes to hit %d ms" % (nodesRequired, thisTargetResponseTime))
		self.logger.info("reversed, at %d nodes and %f ops/s we expect %d ms" % (nodesRequired, currentThroughput, reversedMs))
		self.logger.info("solar supports %d nodes" % nodesSupported)
		
		self.adjust(nodesRequired, nodesSupported)
		
		self.logger.info("Completed schedule")

class GCLoadDeadlineScheduler(GCScheduler):
	
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCLoadDeadlineScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.debugModel = GCTableResponseTimeModelNew("1-22-UNIFORM-300kbs-MIXED-ONE.model")
		self.lookAhead = 4
		
	def schedule(self, intervalMinutes):
		self.logger.log("Starting schedule (GCLoadDeadlineScheduler)")
		
		now = self.clock.get_current_time()
		delta = timedelta(minutes=intervalMinutes)
	
		lastThroughput = denormalize(self.workload_predictor.getMaxWorkloadForInterval(now - delta, delta), self.max_throughput) + self.WORKLOAD_ERROR_MARGIN
		nextThroughput = denormalize(self.workload_predictor.getMaxWorkloadForInterval(now, delta*self.lookAhead), self.max_throughput) + self.WORKLOAD_ERROR_MARGIN
		actualNextThroughput = denormalize(self.workload_predictor.getAvgWorkloadForInterval(now, delta*self.lookAhead), self.max_throughput)
		
		currentThroughput = max(lastThroughput, nextThroughput)
		print "GCLoadDeadlineScheduler: lastThroughput = %f, nextThroughput = %f, currentThroughput = %f" % (lastThroughput, nextThroughput, currentThroughput)
		print "GCLoadDeadlineScheduler: actualNextThroughput = %f" % actualNextThroughput

		# use avg solar for interval
		currentPower = self.solar_predictor.getAvgSolarForInterval(now, delta)
				
		nodesRequired = self.response_time_model.nodesRequired(currentThroughput, self.target_response_time)
		nodesSupported = len(MINIMUM_NODES)
		#nodesSupported = self.power_model.nodesSupported(currentPower)

		reversedMs = self.debugModel.expectedLatency(nodesRequired, currentThroughput)
		
		print "GCLoadDeadlineScheduler: model says we need %d nodes to hit %d ms" % (nodesRequired, self.target_response_time)
		print "GCLoadDeadlineScheduler: reversed, at %d nodes and %f ops/s we expect %d ms" % (nodesRequired, currentThroughput, reversedMs)
		
		epochEnd = now + delta
		# perform the three possible checks and adjustments
		self.adjustForLoad(nodesRequired, shortest=False)
		#self.adjustForPower(nodesSupported)
		self.adjustForDeadline(epochEnd, deadline=timedelta(hours=6))
		self.adjustForExcess(nodesRequired, nodesSupported)
		
		self.logger.info("Completed schedule")

class GCArbitraryScheduler(GCScheduler):
	
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCArbitraryScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.deltaLimit = sys.maxint
	
	def schedule(self, numNodes):
		self.logger.info("Starting schedule (GCArbitraryScheduler)")
		retVal = self.adjust(numNodes, len(MINIMUM_NODES))
		self.logger.info("Completed schedule")
		
		return retVal

class GCNoModelScheduler(GCScheduler):
	def __init__(self, solar_predictor, workload_predictor, response_time_model, power_model, target_response_time=50, max_throughput=9000):
		super(GCNoModelScheduler, self).__init__(solar_predictor, workload_predictor, response_time_model, power_model, target_response_time, max_throughput)
		self.logger = GCLogger.getInstance().get_logger(self)
		self.deltaLimit = 5

	# the regular schedule() function will be used for initial schedule
	# this will be model-based and will called only once, during experiment initialization
	def schedule(self, intervalMinutes):
		self.logger.info("Starting (model-based) schedule (GCNoModelScheduler)")
		
		# get last throughput from live data
		lastThroughput = self.getLastThroughput()
		
		# see how many nodes we need to support this throughput
		nodesRequired = self.response_time_model.nodesRequired(lastThroughput, self.target_response_time)
		nodesRequired = max(len(MINIMUM_NODES), nodesRequired) + 3
		
		self.logger.info("policy says we need %d nodes to hit %d ms" % (nodesRequired, self.target_response_time))
		
		# schedule this many
		self.adjust(nodesRequired, nodesRequired)
		
		self.logger.info("Completed schedule")		

if __name__ == "__main__":
	start_time = datetime(year=2013, month=10, day=20, hour=0, minute=00, second=0)
	clock = ExperimentClock(start_time=start_time, accel_rate=60*60*15)
	main_logger = GCLogger("gcschedulertest.log", GCLogger.DEBUG, clock)
	#solar_predictor = GCTraceSolarPredictor("ganglia-metrics.csv", datetime(year=2013, month=1, day=5, hour=0, minute=0, second=0))
	#solar_predictor = GCTraceSolarPredictor("ganglia-metrics.csv", start_time)
	#workload_predictor = GCTraceWorkloadPredictor("workload.csv")
	solar_predictor = GCHistoricalSolarPredictor(dts=[start_time], scale=0.35)
	workload_predictor = GCAskTraceWorkloadPredictor("timeline_result.datadjusted", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))
	#workload_predictor = None
	cluster_manager = GCClusterManager(clock, main_logger)
	response_time_model = GCTableResponseTimeModel("1-22-UNIFORM-300kbs-MIXED-ONE.model")
	power_model = GCParasolPowerModel()
		
	#scheduler = GCSimplePolicyScheduler(clock, main_logger, solar_predictor, workload_predictor, cluster_manager, response_time_model, power_model)
	#scheduler = GCNoopScheduler(clock, main_logger, solar_predictor, workload_predictor, cluster_manager, response_time_model, power_model)
	#scheduler = GCLoadScheduler(clock, main_logger, solar_predictor, workload_predictor, cluster_manager, response_time_model, power_model)
	#scheduler = GCSolarScheduler(clock, main_logger, solar_predictor, workload_predictor, cluster_manager, response_time_model, power_model)
#	scheduler = GCOptimizerScheduler(clock, main_logger, cluster_manager, trace="optimization.trace")
	#scheduler = GCOnlineOptimizerScheduler(clock, main_logger, solar_predictor, workload_predictor, cluster_manager, data_collector=None)
	scheduler = GCLoadDeadlineScheduler(clock, main_logger, solar_predictor, workload_predictor, cluster_manager, response_time_model, power_model, target_response_time=75, max_throughput=12400)

	for i in xrange(0, 96):
		clock.__init__(start_time=start_time)
		print "Scheduling epoch %d @ %s" % (i, str(start_time))
		scheduler.schedule(15)
		print "done"
		#time.sleep(15)
		start_time += timedelta(minutes=15)
		time.sleep(1)
	
	#for i in [100, 500, 750, 1000, 1250, 1500, 1750, 2000, 2250, 2500, 3000]:
	#	print scheduler.getNodesRequired(i)
	
	
	
