#!/usr/bin/python

from gcpaperconfig import *
import gcpaperplot
import gcpaperstats

if __name__ == "__main__":
	for workload in plots.keys():
		texfile = "%s/results-%s.tex" % (PAPER_DIR, workload)
		with open(texfile, "w") as f:
			workload_plots = plots[workload]
			
			workload_results = {}
			for experiment in sorted(workload_plots.keys(), key=SortExperimentKey):
				workload_results["%s-%s" % (workload, experiment)] = gcpaperstats.get_stats(workload, experiment, workload_plots[experiment])
			gcpaperstats.output_latex(workload, workload_results, f)
			
			for experiment in sorted(workload_plots.keys(), key=SortExperimentKey):
				if workload_plots[experiment] == ExperimentData.TBD:
					continue
				
				#gcpaperplot.gen_plot_latex(workload, experiment, f)