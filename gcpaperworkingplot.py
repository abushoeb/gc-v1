from gcpaperconfig import *

import sys
import subprocess

if __name__ == "__main__":
	
	raw = set()
	
	for wl in plots:
		for exper in plots[wl]:
			descriptor = plots[wl][exper]
			if descriptor.path != None:
				raw.add(descriptor.path)
	
	
	for datapath in raw:	
		plot_cmd = []
		plot_cmd.append("./gcplotexperiment.py")
		plot_cmd.append("--experiment")
		plot_cmd.append("%s" % datapath)
		
		print "exporting", datapath
		
		subprocess.call(plot_cmd)
	