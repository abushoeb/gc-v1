#!/usr/bin/python

import sqlite3
from gccompactionmanager import Compaction
from gcresponsetime import GCResponseTimeHistogram

def adapt_compaction(compaction):
	return str(compaction)

def convert_compaction(s):
	return Compaction.parseCompaction(s)

def adapt_histogram(histogram):
	return str(histogram)

def convert_histogram(s):
	return GCResponseTimeHistogram(s)

class SpecialType(object):
	
	def __init__(self, name, adapt_func, convert_func):
		self.name = name
		self.adapt_func = adapt_func
		self.convert_func = convert_func

special_types = {
		Compaction : SpecialType("compaction", adapt_compaction, convert_compaction),
		GCResponseTimeHistogram : SpecialType("histogram", adapt_histogram, convert_histogram)
}

for typeName in special_types:
	special_type = special_types[typeName]
	sqlite3.register_adapter(typeName, special_type.adapt_func)
	sqlite3.register_converter(special_type.name, special_type.convert_func)
			
def getSQLiteConn(database, timeout=5):
	con = sqlite3.connect(database, detect_types=sqlite3.PARSE_DECLTYPES, timeout=timeout)
	con.row_factory = sqlite3.Row
	con.text_factory = str
	
	return con
