#!/usr/bin/python

import sqlitedict
import random
import time
import gcutil
from datetime import datetime
from datetime import timedelta

if __name__ == "__main__":
	
	d = sqlitedict.SqliteDict("/home/wkatsak/testdict.db", autocommit=False)
	now = datetime.now()
	l = [i for i in gcutil.dtrange(now, now+timedelta(hours=24), timedelta(seconds=5))]
	print len(l)
	
	while True:
		#print "looping"
		d["key"] = datetime.now()	
		start = datetime.now()
		d["list"] = l
		print (datetime.now() - start).total_seconds()
		print str(d["key"])
		time.sleep(0.01)
		
	