#!/usr/bin/python

import contextlib
import os
from datetime import datetime

class FileKeeper(object):
	OPEN_ONCE = 1
	OPEN_EVERY = 2
	
	def __init__(self, filename, mode):
		self.filename = filename
		self.mode = mode
		self.f = None
	
	@contextlib.contextmanager
	def getFile(self):
		if self.mode == self.OPEN_ONCE:
			
			if not self.f:
				self.f = open(self.filename, "w")
			yield self.f
			
		elif self.mode == self.OPEN_EVERY:
			f = open(self.filename, "w")
			yield f
			f.close()

if __name__ == "__main__":
	
	start = datetime.now()
	keeper = FileKeeper("testfile", FileKeeper.OPEN_EVERY)
	for i in xrange(0, 10000):
		with keeper.getFile() as f:
			f.write("%s\n" % str(start))
	end = datetime.now()
	
	optime = (end-start).total_seconds() / 10000
	
	print "optime with many opens", optime
	
	
	start = datetime.now()
	keeper = FileKeeper("testfile", FileKeeper.OPEN_ONCE)
	for i in xrange(0, 10000):
		with keeper.getFile() as f:
			f.write("%s\n" % str(start))
	end = datetime.now()
	
	optime = (end-start).total_seconds() / 10000
	
	print "optime with one opens", optime