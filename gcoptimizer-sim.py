#!/usr/bin/python

import argparse
import sqlitedict

from datetime import *

from gchelpers import *
from gclogger import *
from gcscheduler import *
from gcresponsetime import *
from gcbatterymodel import GCParasolPowerManager

class CachingWorkloadPredictor(GCWorkloadPredictorBase):
	
	def __init__(self):
		self.cache = sqlitedict.SqliteDict("/tmp/workloadcache.db", autocommit=True)
		self.predictor = None
		
	def getWorkloadAt(self, dt):
		if dt not in self.cache:
			if self.predictor == None:
				self.predictor = GCAskTraceWorkloadPredictor("ask-workload-4.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))#, limit=True, max_value=350.166666667)
			
			prediction = self.predictor.getWorkloadAt(dt)
			self.cache[dt] = prediction
			return prediction
		else:
			return self.cache[dt]
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment", action="store", required=True)
	parser.add_argument("--time", action="store", default="6:00")
	parser.add_argument("--cycles", action="store", type=int, default=96)
	args = parser.parse_args()
	
	print "Loading histograms"
	#responseTime = GCResponseTimeFast(args.experiment, "histogram_read_crypt11")
	results = GCResultsSqliteDisk(args.experiment)
	
	#timestamps = responseTime.availableTimestamps()
	timestamps = results.availableTimestamps()
	
	experimentDt = timestamps[0]
	
	dt = datetime.strptime(args.time, "%H:%M")
	dt = datetime(month=experimentDt.month, day=experimentDt.day, year=experimentDt.year, hour=dt.hour, second=dt.second)
	
	startDt = dt
	#clock = ExperimentClockFake(start_time=datetime(month=startDt.month, day=startDt.day, year=startDt.year), current_time=startDt)
	clock = ExperimentClockFake(start_time=startDt)
	
	logger = GCNullLogger()
	
	#modelFile = "MODEL-5-12-simple-MODIFIED-EXTENDED.model"
	modelFile = "MODEL-10-17-simple-EXTENDED-MODIFIED.model"
	model = GCTableResponseTimeModelNew(modelFile, extrapolate=False)
	
	print "Loading Solar"
	solar_predictor = GCHistoricalSolarPredictor(dts=[startDt], scale=0.35)
	#solar_predictor = GCNullSolarPredictor()
	
	print "Loading Ask Workload"
	#####workload_perfect = GCAskTraceWorkloadPredictor("ask-workload-4.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))#, limit=True, max_value=350.166666667)
	workload_perfect = CachingWorkloadPredictor()
	
	#print "Loading Ask Predictions"
	#workload_predictor = GCAskPredictionWorkloadPredictor("ask_predictions_workload.dat", timedelta(days=1, minutes=-45), interpolate=True, max_value=workload_perfect.max_value, factor=0.91)
	workload_predictor = workload_perfect
	
	cluster_manager = GCClusterManager()
	power_manager = GCParasolPowerManager()
	
	scheduler = GCOnlineOptimizerScheduler(solar_predictor, workload_predictor, accel_factor=4, max_throughput=7000, target_response_time=75, response_time_model=model, workload_perfect=workload_perfect, use_battery=True)
	#scheduler = GCOnlineOptimizerScheduler2(clock, logger, solar_predictor, workload_predictor, cluster_manager, data_collector=None, accel_factor=4, max_throughput=12400, target_response_time=75)
	
	for i in xrange(0, args.cycles):
		currentTime = clock.get_current_time()
		print "Current Time:", currentTime
		cdfTimestamp = find_ge(timestamps, currentTime)
		print "getting CDF at", cdfTimestamp
		histogram = results.getCommonValue(cdfTimestamp, "histogram_read_crypt11")
		print "history", cdfTimestamp, histogram.getTotalOperations()
		
		# do the scheduler
		scheduler.schedule(15, i+1, histogram)
		#time.sleep(30)
		clock.increment_time(timedelta(minutes=15))
