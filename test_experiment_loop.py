#!/usr/bin/python

from experimentclock import *
from datetime import *
import time

class TestExperimenter(object):
	
	def __init__(self, workload="test", run_length=96, epoch=15):
		self.workload = workload
		self.run_length = run_length
		self.epoch = epoch
		self.clock = ExperimentClock(start_time=datetime(month=10, day=20, year=2013), accel_rate=60*15, wait=True)
		
	def run(self):
		self.clock.start()
		
		# the main experiment loop
		startTime = self.clock.get_current_time()
		currentRun = 1
		numRuns = self.run_length		
		epochDelta = timedelta(minutes=self.epoch)
		
		while currentRun <= numRuns:			
			timeNow = self.clock.get_current_time()
			print "[%s] Cycle %d starting @ %s" % (self.workload, currentRun, str(timeNow))

			schedStart = self.clock.get_current_time()		
			try:
				time.sleep(0.25)
				
			except Exception as e:
				print "Exception in scheduler:", e
			
			schedFinish = self.clock.get_current_time() 
			lastSchedTime = schedFinish - schedStart
			print "[%s] Elapsed time in scheduler: %s" % (self.workload, str(lastSchedTime))
			
			print "[%s] Schedule complete for cycle %d @ %s" % (self.workload, currentRun, str(schedFinish))
			
			while self.clock.get_current_time() < startTime + currentRun*epochDelta:
				time.sleep(0.01)
			
			timeNow = self.clock.get_current_time()
			print "[%s] Cycle %d complete @ %s" % (self.workload, currentRun, str(timeNow))
			
			currentRun += 1

if __name__ == "__main__":
	test = TestExperimenter()
	test.run()
