#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *
from gcmodel import *
from gcanalysis import *

from datetime import datetime
from datetime import timedelta

class GCStats(GCExperimentProcessor):
	
	integrate_values = ["power_total", "solar_consumed", "solar_excess", "brown_consumed"]
	cum_values = ["readlatency_95_cum", "readlatency_99_cum", "readlatency_avg_cum", "corrected_cum_latency"]
	avg_values = ["nodes_total"]
	special_values = ["on_peak_brown", "on_peak_brown_cost", "off_peak_brown", "off_peak_brown_cost", "brown_cost_total", "max_window_latency"]

	def __init__(self):
		reqs = self.integrate_values + self.cum_values + self.avg_values
		super(GCStats, self).__init__(reqs)
		
		parser = self.getParser()
		parser.add_argument("--html", action="store_true", default=False)
		parser.add_argument("--sla", action="store", type=int, default=75)

		self.electricityModel = GCElectricityPriceModel()
	
	def integrateValues(self, results):
		integrated = {}

		timestamps = results.availableTimestamps()
		max_i = len(timestamps)
		
		for value in self.integrate_values:
			integrated[value] = 0.0
		
		prevMeasurement = 0.0
		for i in xrange(1, max_i):
			delta = timestamps[i] - timestamps[i-1]
			seconds = delta.total_seconds()
			
			for value in self.integrate_values:
				# get the measurement
				measurement = results.getCommonValue(timestamps[i], value)
				
				# check for nan
				if not math.isnan(measurement):
					# if not nan, save this value, and use the value
					prevMeasurement = measurement
					integrated[value] += measurement * seconds
				else:
					# if nan, use the previous good value
					integrated[value] += prevMeasurement * seconds
				
		return integrated
	
	def averageValues(self, results):
		averaged = {}
		
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)
		
		for value in self.avg_values:
			accum = 0.0
			count = 0
			for i in xrange(0, max_i):
				accum += results.getCommonValue(timestamps[i], value)
				count += 1
				
			average = accum / count
			averaged[value] = average
		
		return averaged
	
	def cumValues(self, results):
		cum = {}
		
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)
		
		# output cum values
		for value in self.cum_values:
			for i in reversed(xrange(0, max_i)):
				measurement = results.getCommonValue(timestamps[i], value)
				if not math.isnan(measurement):
					cum[value] = measurement
					break
					
		return cum
	
	def specialValues(self, results):
		special = {}
		
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)
		
		offPeak = 0.0
		onPeak = 0.0
		offPeakCost = 0.0
		onPeakCost = 0.0
		
		maxWindowLatency = 0.0
		
		for i in xrange(1, max_i):
			delta = timestamps[i] - timestamps[i-1]
			seconds = delta.total_seconds()
			measurement = results.getCommonValue(timestamps[i], "brown_consumed")	
			sample = (measurement * seconds) / 3600.0 / 1000.0
			
			if self.electricityModel.isOnPeak(timestamps[i]):
				onPeak += sample
				onPeakCost += sample*self.electricityModel.getEnergyPrice(timestamps[i])
			else:
				offPeak += sample
				offPeakCost += sample*self.electricityModel.getEnergyPrice(timestamps[i])
		
			# max 20 minute latency
			measurement = results.getCommonValue(timestamps[i], "readlatency_99_window")
			if measurement > maxWindowLatency:
				maxWindowLatency = measurement
						
		special["on_peak_brown"] = onPeak
		special["on_peak_brown_cost"] = onPeakCost
		special["off_peak_brown"] = offPeak
		special["off_peak_brown_cost"] = offPeakCost
		special["brown_cost_total"] = onPeakCost + offPeakCost
		special["max_window_latency"] = maxWindowLatency
		
		return special
		
	def outputText(self, experiment, integrated, averaged, cum, special, output):
		print "%s:" % output		
		
		# output integrated value
		for value in self.integrate_values:
			print "\t%s: %0.2f KWh" % (value, integrated[value]/3600.0/1000.0)
		
		#output cum
		for value in self.cum_values:
			print "\t%s: %0.2f" % (value, cum[value])
		
		# output average values
		for value in self.avg_values:
			print "\t%s: %0.2f" % (value, averaged[value])
	
		# output special values
		for value in self.special_values:
			print "\t%s: %0.2f" % (value, special[value])
	
	def outputHtml(self, experiment, integrated, averaged, cum, special, output):
		tag = "\t<td>%s</td>"		
		fileStr = "%s.svg"
		linkStr = "<a href=\"%s\" target=\"iframe\">%s</a>"
		energyStr = "%0.2f KWh"
		responseTimeStr = "%d ms"
		costStr = "$%0.2f"
		
		brown = integrated["brown_consumed"]/3600.0/1000.0
		green = integrated["solar_consumed"]/3600.0/1000.0
		responseTime = cum["readlatency_99_cum"]
		brownCost = special["brown_cost_total"]
		
		linkLine = tag % linkStr % (fileStr % output, output)
		noneLine = tag % "None"
		brownLine = tag % energyStr % brown
		greenLine = tag % energyStr % green
		brownCostLine = tag % costStr % brownCost
		responseTimeLine = tag % responseTimeStr % responseTime
		
		energyTable = "<table cellpadding=\"0\" cellspacing=\"0\"><tr height=\"14px\"><td width=\"%0.2f\" bgcolor=\"green\"/><td width=\"%0.2f\" bgcolor=\"brown\"></tr></table>" % (brown * 10.0, green * 10.0)
		responseTimeTable = "<table cellpadding=\"0\" cellspacing=\"0\"><tr height=\"14px\"><td width=\"%d\" bgcolor=\"blue\"/><td width=\"%d\" bgcolor=\"red\"/></tr></table>" % (responseTime, responseTime-self.args.sla)
		
		energyTableLine = tag % "\n\t\t%s\n\t" % energyTable
		responseTimeTableLine = tag % "\n\t\t%s\n\t" % responseTimeTable
		
		print "<tr>"
		print linkLine
		for i in xrange(0, 4):
			print noneLine
		print brownLine
		print greenLine
		print brownCostLine
		print energyTableLine
		print responseTimeLine
		print responseTimeTableLine
		print "</tr>"
	
	def processExperiment(self, args, experiment, results, output):
		integrated = self.integrateValues(results)
		averaged = self.averageValues(results)
		cum = self.cumValues(results)
		special = self.specialValues(results)
		
		if self.args.html:
			self.outputHtml(experiment, integrated, averaged, cum, special, output)
		else:
			self.outputText(experiment, integrated, averaged, cum, special, output)
			

if __name__ == "__main__":
	stats = GCStats()
	stats.run()
