#!/usr/bin/python

import sys
import os
import shutil

from gchelpers import parse_timestamp
from gcdict import DiskBackedDictionary
from gccompactionmanager import Compaction
from gcresponsetime import GCResponseTimeHistogram

if __name__ == "__main__":
	
	resultsDir = sys.argv[1]
	newResultsDir = "%s-converted" % resultsDir
	try:
		os.makedirs(newResultsDir)
	except:
		pass
	
	special_files = ["timestamps", "nodes", "epochs"]
	rename = ["ycsb.throughput", "ycsb.target", "histogram"]
	new_class = {"histogram" : GCResponseTimeHistogram, "compactions" : Compaction.parseCompaction}
	
	for special in special_files:
		filename_in = "%s/%s" % (resultsDir, special)
		filename_out = "%s/%s" % (newResultsDir, special)
		shutil.copyfile(filename_in, filename_out)
		
	
	for filename in sorted(os.listdir(resultsDir)):
		if filename in special_files:
			continue
		
		outfile = filename
		
		for r in rename:
			if r in filename:
				outfile = filename.replace(".", "_")
				break
		
		print "starting", filename
		outPath = "%s/%s" % (newResultsDir, outfile)
		if os.path.exists(outPath):
			continue
		
		d = DiskBackedDictionary(outPath, mode=DiskBackedDictionary.READWRITE | DiskBackedDictionary.LAZYSYNC | DiskBackedDictionary.KEEPOPEN)
		
		inPath = "%s/%s" % (resultsDir, filename)
		with open(inPath, "r") as f:
			lines = f.readlines()
			#for line in f:
			for line in lines:
				line = line.strip()
				date, time, value = line.split(" ", 2)
				dtString = date + " " + time
				
				dt = parse_timestamp(dtString)
				
				finalValue = None
				for key in new_class:
					if key == filename.split(".")[0]:
						finalValue = new_class[key](value)
						break

				if not finalValue:
					if value == "nan":
						finalValue = float("nan")
					else:
						finalValue = eval(value)
				
				d[dt] = finalValue
				#print dt, finalValue, type(finalValue)
		
		d.sync()
		
		print "completed", filename
						
			
				
		
		

	