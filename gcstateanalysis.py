#!/usr/bin/python

from gccommon import *

class State(object):
	OFF 	= 0
	TRANS 	= 1
	ON 	= 2
	SCHED 	= 4
	WAIT	= 8
	
	def __init__(self, node, state, timestamps, startIndex, endIndex):
		self.node = node
		self.state = state
		self.timestamps = timestamps
		self.startIndex = startIndex
		self.endIndex = endIndex
		
	def getStartDt(self):
		#print len(self.timestamps), self.startIndex
		return self.timestamps[self.startIndex]
	
	def getEndDt(self):
		#print len(self.timestamps), self.endIndex
		return self.timestamps[self.endIndex]
	
	def __str__(self):
		return "State(node=%s, state=%d, startIndex=%d, endIndex=%d" % (self.node, self.state, self.startIndex, self.endIndex)

class StateAnalyzer(object):
	
	def __init__(self, results):
		self.results = results
		self.nodes = self.results.availableNodes()
		self.extractStates()

	def postprocessTransitions(self, states, timestamps):
		transitions = []
		
		for node in self.nodes:
			for state in states[node]:
				if state.state == State.TRANS:
					transitions.append(state)
		
		transitions.sort(key=lambda(x):x.endIndex)
		
		for i in xrange(1, len(transitions)):
			if transitions[i].startIndex <= transitions[i-1].endIndex:
				node = transitions[i].node
				oldStartIndex = transitions[i].startIndex
				transitions[i].startIndex = min(transitions[i-1].endIndex + 1, len(timestamps) - 1)
				
				waitState = State(node, State.WAIT, timestamps, oldStartIndex, transitions[i].startIndex)
				states[node].append(waitState)

	def extractStates(self):
		states = {}
		timestamps = self.results.availableTimestamps()
		max_i = len(timestamps)
		
		for node in self.nodes:
			states[node] = []
			
			current_i = 0
			while current_i < max_i:
				# get the start index
				startIndex = current_i
				state = self.results.getNodeValue(timestamps[startIndex], node, "state")

				# get the change index
				changeIndex = next((i for i in xrange(current_i, max_i) if self.results.getNodeValue(timestamps[i], node, "state") != state), None)
				if changeIndex == None:
					changeIndex = max_i - 1
				#else:
				#	changeIndex -= 1
				
				nextState = State(node, state, timestamps, startIndex, changeIndex)				
				states[node].append(nextState)

				current_i = changeIndex + 1
				
		self.postprocessTransitions(states, timestamps)
		
		for node in self.nodes:
			states[node].sort(key=lambda(x):x.endIndex)
		self.states = states
	
	def getStates(self):
		return self.states
	
	def getNodes(self):
		return sorted(self.states.keys())
