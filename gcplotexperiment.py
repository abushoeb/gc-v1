#!/usr/bin/python

from gcanalysis import *
from gcplotter import *

class GCPlotExperiment(GCExperimentProcessor):
	
	reqs =	[
		"brown_consumed",
		"solar_consumed",
		"solar_excess",
		"power_total",
		"state",
		"actual_workload",
		"target_workload",
		# Average
		"readlatency_avg_window",
		# 95th
		#"readlatency_95",
		#"writelatency_95",
		#"readlatency_95_cum",
		#"writelatency_95_cum",
		#"readlatency_95_window",
		#"writelatency_95_window",
		#99thoptions
		#"readlatency_99",
		#"writelatency_99",
		"readlatency_99_cum",
		"writelatency_99_cum",
		"insertlatency_99_cum",
		"readlatency_99_window",
		"writelatency_99_window",
		"insertlatency_99_window",
		"cassandra_load",
		"nodes_total",
		"nodes_ready",
		"nodes_transition",
		#"ycsb.throughput",
		#"ycsb.target",
		"any_transitions",
		"workload_moving_avg",
		"workload_exp_moving_avg",
		"avg_readlatency_99",
		"greenhint_delivery_rate",
		"greenhint_delivery_rate_total",
		"greenhint_deliveries",
		"greenhint_deliveries_total",
		#"sstables_greenhints",
		#"sstables_greenhints_index",
		"sstables_data",
		"cassandra_total_compactions",
		"cassandra_read_latency",
		"cassandra_write_latency",
		"cassandra_greenhint_write_latency",
		"cassandra_read_rate",
		"cassandra_write_rate",
		"cassandra_greenhint_write_rate",
		"cassandra_local_reads",
		"cassandra_local_writes",
		"greenhint_reads",
		"greenhint_writes",
		"data_reads",
		"data_writes",
		"data_read_latency",
		"data_write_latency",
		"jvm_cms_time",
		"jvm_parnew_time",
		"jvm_cms_count",
		"jvm_parnew_count",
		"compaction_throughput",
		"total_bytes_compacted",
		"write_tracker_recent",
		"read_tracker_recent",
		"greenhint_tracker_recent",
		"last_prediction",
	]
	
	def __init__(self):
		super(GCPlotExperiment, self).__init__(self.reqs)
		
		parser = self.getParser()
		parser.add_argument("--format", default="pdf")
		parser.add_argument("-p", "--paper", dest="paper", action="store_true")
		parser.add_argument("--limitx", action="store_true", default=False)
		
	def processExperiment(self, args, experiment, results, output):
		SLA = 75.0
		
		if args.paper:
			format = "svg"
		else:
			format = args.format
		
		# Switch the backend depending on desired format
		if format == "pdf":
			pyplot.switch_backend("PDF")
		elif format == "svg":
			pyplot.switch_backend("Cairo")
		elif format == "png":
			pyplot.switch_backend("AGG")
			
		print 'Reading model...'
		#response_time_model = GCTableResponseTimeModelNew("MODEL-4-22-simple.model", extrapolate=False)
		response_time_model = GCTableResponseTimeModelNew("MODEL-10-17-simple-EXTENDED-MODIFIED.model", extrapolate=False)
		#alt_response_time_model = GCTableResponseTimeModelNew("MODEL-10-17-smart-EXTENDED-MODIFIED.model", extrapolate=False)

		print 'Initialize plotter...'
		if args.paper:
			plotter = GCPlotter(xsize=14, ysize=7)
		else:
			if args.limitx:
				plotter = GCPlotter(xsize=30, ysize=50, limitx=results)
			else:
				plotter = GCPlotter(xsize=30, ysize=50)
		
		# Power
		print 'Power...'
		plot = GCPowerPlot(results, title=("" if args.paper else "Power"), ylim=(0,900))
		plot = GCPowerPlotWithBattery(results, title=("" if args.paper else "Power"), ylim=(0,900))
		plotter.addPlot(plot)
		
		plot = GCSimpleCommonPlot(results, "batt_percent", title="Battery Level", ylabel="Percent", ylim=(0,110))
		plotter.addPlot(plot)
		
		# Workload
		print 'Workload...'
		plot1 = GCMultiCommonPlot(results, ["actual_workload", "target_workload", "last_prediction"], title="Workload", ylabel="op/s", ylim=(0, 6000))

		#predictor = GCRegressionWorkloadPredictor(response_time_model.peakThroughput(NUM_NODES, 75), results=results)	
		#plot2 = GCWorkloadPredictionPlot(results, response_time_model, predictor)
			
		#print "Workload prediction..."
		#workload_perfect = GCAskTraceWorkloadPredictor("ask-workload-4.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))
		#workload_predictor = GCAskPredictionWorkloadPredictor("ask_predictions_workload.dat", timedelta(days=1, minutes=-45), interpolate=True, max_value=workload_perfect.max_value, factor=0.91)

#		plot2 = GCWorkloadPredictionPlot(results, response_time_model, workload_predictor);
#		plot = GCCombinedPlot([plot1, plot2, plot3])
		plot = GCCombinedPlot([plot1])
		plotter.addPlot(plot)
		
		print 'Latency...'
		# Merge real and prediction
		plot1 = GCMultiCommonPlot(results, ["readlatency_99_window", "writelatency_99_window", "insertlatency_99_window"], ylim=(0, 150), indexLine=SLA, title="Window Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
		
		# Predictions
		predictor = HistoricalWorkloadPredictor(results, response_time_model)
		#plot5 = GCPredictionModelOverlayPlot(results, model=response_time_model, predictor=workload_predictor, ylim=(0, 150), indexLine=75, mode=GCPredictionModelOverlayPlot.INSTANTANEOUS, continuous=True, title="0", lessnodes=0)		
		plot5 = GCPredictionModelOverlayPlot(results, model=response_time_model, predictor=predictor, ylim=(0, 150), indexLine=75, mode=GCPredictionModelOverlayPlot.INSTANTANEOUS, continuous=True, title="0", lessnodes=0)		
		#plot6 = GCPredictionModelOverlayPlot(results, model=alt_response_time_model, predictor=predictor, ylim=(0, 150), indexLine=75, mode=GCPredictionModelOverlayPlot.INSTANTANEOUS, continuous=True, title="0", lessnodes=0)
		
		#plot = GCCombinedPlot([plot1, plot2], legendPos=None)
		plot = GCCombinedPlot([plot1, plot5], legendPos=None)
		plotter.addPlot(plot)
		
		#plot = GCWindowOverlayPlot(results, 99, 5, ylim=(0, 400))
		#plotter.addPlot(plot)
		
		#plot = GCMultiCommonPlot(results, ["readlatency_99_instant"], ylim=(0, 150), indexLine=SLA, title="Instantaneous Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
		#plotter.addPlot(plot)
		
		#plot = GCMultiCommonPlot(results, ["readlatency_99_cum", "writelatency_99_cum", "insertlatency_99_cum"], ylim=(0, 150), indexLine=SLA, title="Cumulative Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
		#plotter.addPlot(plot)
		
		plot = GCMultiCommonPlot(results, ["corrected_cum_latency"], ylim=(0, 150), indexLine=SLA, title="Cumulative Latency Corrected", ylabel="Response time (ms)") # We add the 75 ms SLA
		plotter.addPlot(plot)
		
		plot = GCCompactionPlot(results)
		plotter.addPlot(plot)
		
		print 'Nodes...'
		plot = GCNodePlot(results, ylim=(0,27))
		plotter.addPlot(plot)

		#plot = GCNodeStatePlot(results)
		#plotter.addPlot(plot)
		
		plot = GCNodeStatePlotNew(results)
		plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "sstables_data", MINIMUM_NODES, title="Data SSTables (min)", legend=True, ylabel="# of SSTables")
		#plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "sstables_data", OPTIONAL_NODES, title="Data SSTables (opt)", legend=True, ylabel="# of SSTables")
		#plotter.addPlot(plot)
		
		plot = GCSimpleCommonPlot(results, "pending_compactions", title="Pending Compactions", ylabel="Compactions")
		plotter.addPlot(plot)
	
		#plot = GCSimpleMultiNodePlot(results, "greenhint_deliveries", MINIMUM_NODES, title="GreenHint Delivery Rate", ylabel="ops/sec")
		#plotter.addPlot(plot)

		#plot = GCSimpleMultiNodePlot(results, "data_read_latency", MINIMUM_NODES, title="Data Read Latency (min)", legend=True, ylabel="us", ylim=(0, 100000))
		#plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "data_read_latency", OPTIONAL_NODES, title="Data Read Latency (opt)", legend=True, ylabel="us", ylim=(0, 100000))
		#plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "data_write_latency", MINIMUM_NODES, title="Data Write Latency (min)", legend=True, ylabel="us", ylim=(0, 100000))
		#plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "data_write_latency", OPTIONAL_NODES, title="Data Write Latency (opt)", legend=True, ylabel="us", ylim=(0, 100000))
		#plotter.addPlot(plot)
		
		print 'System load...'
		plot = GCSimpleMultiNodePlot(results, "cassandra_load", MINIMUM_NODES, title="Load (min)", legend=True, ylim=(0, 10), ylabel="System load")
		plotter.addPlot(plot)
		
		plot = GCSimpleMultiNodePlot(results, "cassandra_load", OPTIONAL_NODES, title="Load (opt)", legend=True, ylim=(0, 10), ylabel="System load")
		plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "gc_OU", MINIMUM_NODES+OPTIONAL_NODES, title="Old Generation", legend=True, ylabel="bytes")
		#plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "gc_GCT", MINIMUM_NODES+OPTIONAL_NODES, title="GC Time", legend=True, ylabel="seconds")
		#plotter.addPlot(plot)
		
		#plot = GCSimpleMultiNodePlot(results, "gc_FGCT", MINIMUM_NODES+OPTIONAL_NODES, title="GC Time", legend=True, ylabel="seconds")
		#plotter.addPlot(plot)

		print 'Plotting...'
		output_file = "%s.%s" % (output, format)
		plotter.doPlot(output=output_file, format=format)
		print "Plot completed, output file: %s" % output_file
		
if __name__ == "__main__":
	plotter = GCPlotExperiment()
	plotter.run()
