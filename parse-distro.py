#!/usr/bin/python

import sys
import re
import matplotlib.pyplot as pyplot

read_total = 0
write_total = 0

read_counts = {}
write_counts = {}

read_keys = {}
write_keys = {}

key_mappings = {}

def print_fields(fields):
	for i in xrange(0, len(fields)):
		print i, fields[i]

def record_mapping(key, nodes):
	global key_mappings

	try:
		assert key_mappings[key] == nodes
	except KeyError:
		key_mappings[key] = nodes
	
def record_read(key, nodes):
	global read_total
	global read_counts
	global read_keys

	read_total += 1
	record_mapping(key, nodes)
	
	try:
		read_keys[key] += 1
	except KeyError:
		read_keys[key] = 1
	
	for node in nodes:
		try:
			read_counts[node] += 1
		except KeyError:
			read_counts[node] = 1

def record_write(key, nodes):
	global write_total
	global write_counts
	global write_keys
	
	write_total += 1
	record_mapping(key, nodes)
	
	try:
		write_keys[key] += 1
	except KeyError:
		write_keys[key] = 1
		
	for node in nodes:
		try:
			write_counts[node] += 1
		except KeyError:
			write_counts[node] = 1

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "Please specify filename..."
		sys.exit()
	
	filename = sys.argv[1]
	f = open(filename, "r")
	
	lines = f.read().strip().split("\n")
	
	i = lines.__iter__()
	read_regex = re.compile("Command/ConsistencyLevel", re.I)
	write_regex = re.compile("Mutations/ConsistencyLevel", re.I)
	
	try:
		while True:
			line = i.next()
			# if we found a read, extract the key
			if read_regex.search(line):
				line = line.replace(",", "")
				line = line.replace("'", "")
				#print line
				fields = line.split(" ")
				#print_fields(fields)
				key_field = fields[10]
				junk, key = key_field.split("=")
				
				# get the next line (that lists the nodes)
				# and record the reads
				nodes = []

				line = i.next()
				while not "StorageProxy" in line:
					#print line
					line = i.next()
					
				line = line.replace(",", "")
				line = line.replace("[", "")
				line = line.replace("]", "")
				line = line.replace("sol032", "")
				line = line.replace("/", "")
				#print line
				fields = line.split()
				#print_fields(fields)
				nodes.append(fields[13])
				nodes.append(fields[14])
				nodes.append(fields[15])
				#if "172.16.18.141" in nodes:
				#	print nodes, fields
				record_read(key, nodes)
			# if we found a write, extract the key
			elif write_regex.search(line):
				line = line.replace(",", "")
				line = line.replace("'", "")
				fields = line.split(" ")
				key_field = fields[10]
				junk, key = key_field.split("=")
				
				# now, get the nodes
				nodes = []
				
				for j in xrange(0, 3):
					line = i.next()
					while not "StorageProxy" in line:
						#print line
						line = i.next()
						
					line = line.replace("sol032", "")
					line = line.replace("/", "")
					fields = line.split(" ")
					nodes.append(fields[10])
					i.next()
				record_write(key, nodes)
	except StopIteration:
		pass

	
	nodes = set()
	for node in read_counts.keys() + write_counts.keys():
		nodes.add(node)

	r_accum = 0.0
	w_accum = 0.0
	for node in nodes:
		r_accum += read_counts[node]
		w_accum += write_counts[node]
	
	r_avg = r_accum / len(nodes)
	w_avg = w_accum / len(nodes)

	print "Num Reads: %d" % read_total
	print "Num Writes: %d" % write_total
	print "Total Ops: %d" % (read_total + write_total)
	print "Avg Read: %0.2f" % r_avg
	print "Avg Write: %0.2f" % w_avg
	
	
	print "%s:\t\t%s\t%s\t%s\t%s\t%s\t%s" % ("Node", "R", "% R", "x Avg", "W", "% W", "x Avg")
	for node in sorted(nodes): #sorted(read_counts.keys(), key=lambda(v):read_counts[v]):
		print "%s:\t%d\t%0.2f\t%0.2f\t%d\t%0.2f\t%0.2f" % (node, read_counts[node], (read_counts[node]*1.0)/read_total, (read_counts[node]/ r_avg), write_counts[node], (write_counts[node]*1.0)/write_total, (write_counts[node] / w_avg))
		
	
	# do histogram
	r_keys = sorted(read_keys.keys(), key=lambda(v):read_keys[v], reverse=True)
	w_keys = sorted(write_keys.keys(), key=lambda(v):write_keys[v], reverse=True)
	
	r_values = []
	r_unique_values = []
	w_values = []
	w_unique_values = []
	
	for i in xrange(0, 250):
		r_key = r_keys[i]
		w_key = w_keys[i]
		r_count = read_keys[r_key]
		w_count = write_keys[w_key]
		
		for j in xrange(0, r_count):
			r_values.append(i)
		r_unique_values.append(i)
		
		for j in xrange(0, w_count):
			w_values.append(i)
		w_unique_values.append(i)

	for i in xrange(0, 10):
		print "Read  Key %d: %5d - Nodes: %s " % (i, read_keys[r_keys[i]], key_mappings[r_keys[i]])
	for i in xrange(0, 10):
		print "Write Key %d: %5d - Nodes: %s" % (i, write_keys[w_keys[i]], key_mappings[w_keys[i]])
				
	#for i in xrange(0, 5):
	#	#print r_keys[i], w_keys[i]
	#	continue

	#	print "Read   %d: %s" % (i, key_mappings[r_keys[i]])
	#	print "Write  %d: %s" % (i, key_mappings[w_keys[i]])
	
	pyplot.hist(r_values, r_unique_values, label="Reads")
	pyplot.show()
	
	pyplot.hist(w_values, w_unique_values, label="Writes")
	pyplot.show()
	
	
	
	
