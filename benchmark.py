#!/usr/bin/python
import sys
from datetime import datetime
from gcresponsetime import *

if __name__ == "__main__":
	f = open(sys.argv[1], "r")
	line = f.readline().strip()
	f.close()
	
	start = datetime.now()
	for i in xrange(0, 1000):
		h = GCResponseTimeHistogram(line)
	end = datetime.now()
	
	elapsed = (end-start).total_seconds() / 1000
	print "elapsed", elapsed
	print h.getPercentile(99)
		
	
