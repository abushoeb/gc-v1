#!/usr/bin/env python2.7

"""
GreenHadoop makes Hadoop aware of solar energy availability.
http://www.research.rutgers.edu/~goiri/
Copyright (C) 2012 Inigo Goiri, Rutgers University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
"""

import os
import time
import signal

import threading

from datetime import datetime, timedelta

from ghadoopcommons import *
from ghadoopmonitor import MonitorMapred

from parasol.commons import *
from parasol.ganglia import *

class Logger(threading.Thread):
	def __init__(self, mapred=None, logname=None, waitNodes=True):
		threading.Thread.__init__(self)
		self.running = True
		self.ready = False
		
		# Monitoring cycles
		self.CYCLE = 2 # Run every 2 seconds
		self.PEAK_PERIOD = 15*60 # 15 minutes
		
		# Monitor
		self.mapred = mapred
		
		self.waitNodes = waitNodes
		
		# Output values
		self.nodePower = {} # node -> power
		self.loadPower = 0.0
		self.capPower = 0.0
		self.solar = 0.0
		self.solarcalc = 0.0
		self.grid = 0.0
		self.net_meter = 0.0
		self.bat_charge = 0.0
		self.bat_discharge = 0.0
		self.bat_level = 0.0
		self.bat_level_error = 0.0
		self.price = 0.0
		self.it = 0.0
		self.powermeter_grid = 0.0
		self.powermeter_si = 0.0
		self.powermeter_hvac = 0.0
		self.peakBrown = 0.0
		
		self.brownHistory = []
		
		# Output files
		self.logname = logname
		if self.logname != None:
			self.filesuffix = self.logname
		else:
			self.logname = str(dateToSeconds(datetime.now()))
			self.filesuffix = self.logname
		# Create log folder
		d = os.path.dirname('logs/'+self.filesuffix+'/')
		if not os.path.exists(d):
			os.makedirs(d)
		
		self.filepower =    None
		self.fileenergy =   None
		self.fileload =     None
		self.filebytesin =  None
		self.filebytesout = None
		self.filesource =   None
		self.filenode =     None
		self.filejob =      None
		
		# Read slaves and masters
		self.slaves = readHostFile(CONF_SLAVES)
		self.masters = readHostFile(CONF_MASTERS)
		
		# Initial time
		self.warmup = True
		self.initialTime = datetime.now()
	
	def run(self):
		# Files
		self.filepower =    open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-power.log', 'a')
		self.fileenergy =   open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-energy.log', 'a')
		self.fileload =     open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-load.log', 'a')
		self.filebytesin =  open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-bytesin.log', 'a')
		self.filebytesout = open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-bytesout.log', 'a')
		self.filenode =     open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-node.log', 'a')
		self.filejob =      open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-job.log', 'a')
		self.filesource =   open('logs/'+self.filesuffix+'/log-'+self.filesuffix+'-source.log', 'a')
		
		# File headers
		self.filepower.write('#' + str(self.filesuffix) + ';' + ';'.join(SWITCHES) + ';' + ';'.join(self.masters+self.slaves) +'\n')
		self.fileenergy.write('#' + str(self.filesuffix) + ';' + ';'.join(SWITCHES) + ';' + ';'.join(self.masters+self.slaves) + '\n')
		self.fileload.write('#' + str(self.filesuffix) + ';' + ';'.join(self.masters+self.slaves) + '\n')
		self.filebytesin.write('#' + str(self.filesuffix) + ';' + ';'.join(self.masters+self.slaves) + '\n')
		self.filebytesout.write('#' + str(self.filesuffix) + ';' + ';'.join(self.masters+self.slaves) + '\n')
		
		self.filenode.write('#' + str(self.filesuffix) + ';onNodes;offNodes;runNodes;runJobs;totalJobs;runTasks;totalTasks;usedSlots\n')
		self.filejob.write('#' + str(self.filesuffix) + ';jobId;job\n')
		self.filesource.write('#' + str(self.filesuffix)+';load;it;cap;solar;solarcalc;grid;net_meter;bat_charge;bat_discharge;bat_level;bat_level_error;price;brown15;peak;meter_grid;meter_si;meter_hvac\n')
		# jobId -> totalTasks
		# taskId -> bool
		logged = {}
		
		# Wait until we see everybody
		if self.waitNodes:
			start = False
			while not start:
				start = True
				for nodeId in self.slaves:
					if nodeId not in self.mapred.nodes:
						start = False
						sleep(0.5)
						break
			print "Everybody ready! Start logging!"
		
		# Account warmup
		self.warmup = True
		self.initialTime = datetime.now()
		
		# Cycle
		loop = 0
		while self.running:
			loop += 1
			previous = datetime.now()
			t = dateToSeconds(previous)
			
			try:
				# Get data from Ganglia
				xml = readGangliaURL(GANGLIA_URL+'?c='+CLUSTER_NAME)
				hosts = parseGangliaXML(xml)
				
				# Check switches first
				powers = []
				energys = []
				
				# Add switches
				for switchId in SWITCHES:
					power = ''
					energy = '-'
					if switchId in hosts:
						switch = hosts[switchId]
						if 'power' in switch:
							power = switch['power'] # W
						if 'energy' in switch:
							energy = switch['energy'] # Wh
					powers.append(power)
					energys.append(energy)
					# Store info in memory
					if power != '-':
						self.nodePower[switchId] = float(power)
						
				# Check all the nodes in the system
				loads = []
				bytesins = []
				bytesouts = []
				for nodeId in self.masters+self.slaves:
					power = '-'
					energy = '-'
					load = '-'
					bytesin = '-'
					bytesout = '-'
					if nodeId in hosts:
						host = hosts[nodeId]
						datemetric = secondsToDate(int(host['reported']))
						# Get power and energy
						if 'power' in host:
							power = host['power'] # W
						if 'energy' in host:
							energy = host['energy'] # Wh
						# Get load (if it is fresh)
						if (datetime.now() - datemetric) < timedelta(seconds=60):
							if 'load_one' in host:
								load = host['load_one']
							if 'bytes_in' in host:
								bytesin = host['bytes_in']
							if 'bytes_out' in host:
								bytesout = host['bytes_out']
					powers.append(power)
					energys.append(energy)
					loads.append(load)
					bytesins.append(bytesin)
					bytesouts.append(bytesout)
					# Store info in memory
					if power != '-':
						self.nodePower[nodeId] = float(power)
				
				# Save metrics into files
				self.filepower.write(str(t)+';'+';'.join(powers)+'\n')
				self.fileenergy.write(str(t)+';'+';'.join(energys)+'\n')
				self.fileload.write(str(t)+';'+';'.join(loads)+'\n')
				self.filebytesin.write(str(t)+';'+';'.join(bytesins)+'\n')
				self.filebytesout.write(str(t)+';'+';'.join(bytesouts)+'\n')
				
				# Get total IT power
				loadPower = 0.0
				for power in powers:
					loadPower += float(power)
				self.loadPower = loadPower
				# Get solar calculated
				if 'prediction' in hosts:
					predictions = hosts['prediction']
					if 'calc' in predictions:
						self.solarcalc = float(predictions['calc'])
				# Get source of power
				if 'power' in hosts:
					sources = hosts['power']
					# Get values for solar
					if 'solar' in sources:
						self.solar = float(sources['solar'])
					# Get values for grid
					if 'grid' in sources:
						self.grid = float(sources['grid'])
						self.brownHistory.append((t, self.grid))
					if 'net_meter' in sources:
						self.net_meter = float(sources['net_meter'])
					# Get values for battery
					if 'bat_charge' in sources:
						self.bat_charge = float(sources['bat_charge'])
					if 'bat_discharge' in sources:
						self.bat_discharge = float(sources['bat_discharge'])
					if 'bat_level' in sources:
						self.bat_level = float(sources['bat_level'])
					if 'bat_level_error' in sources:
						self.bat_level_error = float(sources['bat_level_error'])
					if 'powermeter-grid' in sources:
						self.powermeter_grid = float(sources['powermeter-grid'])
					if 'powermeter-si' in sources:
						self.powermeter_si = float(sources['powermeter-si'])
					if 'powermeter-hvac' in sources:
						self.powermeter_hvac = float(sources['powermeter-hvac'])
					if 'price' in sources:
						self.price = float(sources['price'])
					if 'it' in sources:
						self.it = float(sources['it'])
					
					# Calculate peak
					brownAverage = -1.0
					# Check warmup
					if self.warmup:
						if datetime.now() - self.initialTime > timedelta(seconds=WARMUP_TIME):
							print "Warmup period finished! Start logging peak brown!"
							self.warmup = False
						self.brownHistory = []
					else:
						# Clean old values
						while len(self.brownHistory)>=2 and (self.brownHistory[-2][0] - self.brownHistory[0][0]) > self.PEAK_PERIOD:
							self.brownHistory.pop(0)
					# Calculate new peak
					if len(self.brownHistory)>=2:
						totalperiod = 0.0
						totalbrown = 0.0
						for i in range(0, len(self.brownHistory)-1):
							period = self.brownHistory[i+1][0] - self.brownHistory[i][0]
							brown = self.brownHistory[i][1]
							totalbrown += brown*period
							totalperiod += period
							if totalperiod > self.PEAK_PERIOD:
								break
						#if totalperiod >= self.PEAK_PERIOD:
						# We account for everything
						brownAverage = totalbrown/self.PEAK_PERIOD
					# Peak
					if brownAverage>=0.0 and brownAverage > self.peakBrown:
						self.peakBrown = brownAverage
					# Write values
					self.filesource.write(str(t)+';'+
						str(self.loadPower)+';'+str(self.it)+';'+str(self.capPower)+';'+
						str(self.solar)+';'+str(self.solarcalc)+';'+
						str(self.grid)+';'+str(self.net_meter)+';'+
						str(self.bat_charge)+';'+str(self.bat_discharge)+';'+str(self.bat_level)+';'+str(self.bat_level_error)+';'+
						str(self.price)+';'+str(brownAverage)+';'+str(self.peakBrown)+';'+
						str(self.powermeter_grid)+';'+str(self.powermeter_si)+';'+str(self.powermeter_hvac)+'\n')
			except Exception, e:
				print 'Error getting data from Ganglia:', e
			
			# Get Hadoop nodes and jobs information
			if self.mapred != None:
				try:
					# Get nodes information
					onNodes = 0
					offNodes = 0
					runNodes = 0
					usedSlots = 0
					for nodeId in self.slaves:
						if nodeId in self.mapred.nodes:
							node = self.mapred.nodes[nodeId]
							if node.status == 'UP':
								onNodes += 1
							elif node.status == 'DOWN':
								offNodes += 1
							if len(node.attempts) > 0:
								runNodes += 1
							usedSlots += len(node.attempts)
					
					# Get jobs information
					runJobs = totalJobs = 0
					runTasks = totalTasks = 0
					for jobId in list(self.mapred.jobs.keys()):
						# Jobs
						job = self.mapred.jobs[jobId]
						totalJobs += 1
						if job.isRunning():
							runJobs += 1
						# Log jobs
						elif jobId not in logged:
							self.filejob.write(str(t) + ';' + str(jobId) + ';' + str(job) + '\n')
							logged[jobId] = 0
							for task in list(job.tasks.values()):
								logged[jobId] += 1
								for attemptId in list(task.attempts.keys()):
									attempt = task.attempts[attemptId]
									# Log attempts
									if attempt.attemptId not in logged:
										logged[attempt.attemptId] = True
										self.filejob.write(str(t) + ';' + str(attempt.attemptId) + ';' + str(attempt) + '\n')
						# Tasks
						if jobId in logged:
							# Tasks from finished jobs
							totalTasks += logged[jobId]
						else:
							# Not logged tasks
							for task in list(job.tasks.values()):
								# Tasks
								totalTasks += 1
								for attemptId in list(task.attempts.keys()):
									attempt = task.attempts[attemptId]
									if job.isRunning() and attempt.isRunning():
										runTasks += 1
									# Log attempts
									if attempt.attemptId not in logged and not attempt.isRunning():
										logged[attempt.attemptId] = True
										self.filejob.write(str(t) + ';' + str(attempt.attemptId) + ';' + str(attempt) + '\n')
					# Save node information
					self.filenode.write(str(t)+';'+str(onNodes)+';'+str(offNodes)+';'+str(runNodes)+';'+str(runJobs)+';'+str(totalJobs)+';'+str(runTasks)+';'+str(totalTasks)+';'+str(usedSlots)+'\n')
				except Exception, e:
					print 'Error getting jobs data:', e
			
			# Flush logs periodically
			if loop%20 == 0:
				self.flush()
			
			self.ready = True
			
			# Wait to log every second
			while (datetime.now()-previous) < timedelta(seconds=self.CYCLE):
				#sleep(0.2)
				sleeptime = (timedelta(seconds=self.CYCLE) - (datetime.now()-previous)).total_seconds() - 0.05 # 50ms
				if sleeptime > 0.0:
					sleep(sleeptime)
			
		self.running = False
	
	def flush(self):
		try:
			if self.filepower != None:
				self.filepower.flush()
				os.fsync(self.filepower)
			if self.fileenergy != None:
				self.fileenergy.flush()
				os.fsync(self.fileenergy)
			if self.fileload != None:
				self.fileload.flush()
				os.fsync(self.fileload)
			if self.filebytesin != None:
				self.filebytesin.flush()
				os.fsync(self.filebytesin)
			if self.filebytesout != None:
				self.filebytesout.flush()
				os.fsync(self.filebytesout)
			if self.filesource != None:
				self.filesource.flush()
				os.fsync(self.filesource)
			if self.filenode != None:
				self.filenode.flush()
				os.fsync(self.filenode)
			if self.filejob != None:
				self.filejob.flush()
				os.fsync(self.filejob)
		except Exception, e:
			print 'Error flushing files.'
		
	
	def kill(self):
		self.running = None
		
		while self.running != False:
			sleep(0.2)
		
		# Close open files
		if self.filepower != None:
			self.filepower.close()
		if self.fileenergy != None:
			self.fileenergy.close()
		if self.fileload != None:
			self.fileload.close()
		if self.filebytesin != None:
			self.filebytesin.close()
		if self.filebytesout != None:
			self.filebytesout.close()
		if self.filesource != None:
			self.filesource.close()
		if self.filenode != None:
			self.filenode.close()
		if self.filejob != None:
			self.filejob.close()

if __name__=='__main__':
	running = True

	monitor = MonitorMapred()
	monitor.start()
	logger = Logger(mapred=monitor, waitNodes=False)
	logger.start()
	
	def kill_all(signal, frame):
		print 'Killing daemons!'
		global running
		global monitor
		global logger
		running = False
		monitor.kill()
		logger.kill()
		
	signal.signal(signal.SIGINT, kill_all)
	
	while running:
		sleep(1.0)
	
	# Finishing
	monitor.join()
	logger.join()
	
