#!/usr/bin/python

import argparse
import thread
import math
import time

from gccommon import *
from gcbase import GCManagerBase
from datetime import datetime
from pyjolokia import Jolokia
from pyjolokia import JolokiaError
from threading import Thread
from gcjolokiahelpers import MBeanAttribute
from gcjolokiahelpers import JMXThreadedClient
from gcjolokiahelpers import JMXResponse
from gclogger import GCLogger, GCNullLogger

JOLOKIA_URL="http://%s:8778/jolokia/"

def mbean_uniform(mbean):
	prefix, rest=  mbean.split(":")
	restTokens = rest.split(",")
	restTokens = sorted(restTokens)
	
	restSorted = ",".join(restTokens)
	
	output = "%s:%s" % (prefix, restSorted)
	return output


class GCValueDescriptor(object):
	
	def __init__(self, key, valueType):
		self.key = key
		self.valueType = valueType
		
class GCJolokiaManagerNew(GCManagerBase):
	
	VALUE_ABSOLUTE = 1
	VALUE_DIFF = 2
	
	values = {
		MBeanAttribute("java.lang:type=OperatingSystem", "SystemLoadAverage") : GCValueDescriptor("cassandra_load", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "ReadOperations") : GCValueDescriptor("cassandra_read_rate", VALUE_DIFF),
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "WriteOperations") : GCValueDescriptor("cassandra_write_rate", VALUE_DIFF),
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "GreenHintWriteOperations") : GCValueDescriptor("cassandra_greenhint_write_rate", VALUE_DIFF),
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "RecentReadLatencyMicros") : GCValueDescriptor("cassandra_read_latency", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "RecentWriteLatencyMicros") : GCValueDescriptor("cassandra_write_latency", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "RecentGreenHintWriteLatencyMicros") : GCValueDescriptor("cassandra_greenhint_write_latency", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=CompactionManager", "TotalCompactionsCompleted") : GCValueDescriptor("cassandra_total_compactions", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=CompactionManager", "PendingTasks") : GCValueDescriptor("cassandra_pending_compactions", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=CompactionManager", "TotalBytesCompacted") : GCValueDescriptor("total_bytes_compacted", VALUE_DIFF),
		MBeanAttribute("java.lang:type=GarbageCollector,name=ConcurrentMarkSweep", "CollectionCount") : GCValueDescriptor("jvm_cms_count", VALUE_ABSOLUTE),
		MBeanAttribute("java.lang:type=GarbageCollector,name=ConcurrentMarkSweep", "CollectionTime") : GCValueDescriptor("jvm_cms_time", VALUE_ABSOLUTE),
		MBeanAttribute("java.lang:type=GarbageCollector,name=ParNew", "CollectionCount") : GCValueDescriptor("jvm_parnew_count", VALUE_ABSOLUTE),
		MBeanAttribute("java.lang:type=GarbageCollector,name=ParNew", "CollectionTime") : GCValueDescriptor("jvm_parnew_time", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "LiveSSTableCount") : GCValueDescriptor("sstables_data", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "ReadCount") : GCValueDescriptor("data_reads", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "WriteCount") : GCValueDescriptor("data_writes", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "RecentReadLatencyMicros") : GCValueDescriptor("data_read_latency", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "RecentWriteLatencyMicros") : GCValueDescriptor("data_write_latency", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=GreenCassandraConsistencyManager", "GreenHintDeliveryRate") : GCValueDescriptor("greenhint_delivery_rate", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=GreenCassandraConsistencyManager", "GreenHintDeliveries") : GCValueDescriptor("greenhint_deliveries", VALUE_DIFF),
		MBeanAttribute("org.apache.cassandra.request:type=ReadStage", "CompletedTasks") : GCValueDescriptor("cassandra_local_reads", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.request:type=MutationStage", "CompletedTasks") : GCValueDescriptor("cassandra_local_writes", VALUE_ABSOLUTE),
		MBeanAttribute("org.apache.cassandra.db:type=StorageService", "CompactionThroughputMbPerSec") : GCValueDescriptor("compaction_throughput", VALUE_ABSOLUTE),
	}
	
	def __init__(self, nodes=(MINIMUM_NODES + OPTIONAL_NODES)):
		self.nodes = nodes
		
		# init prev_values for diff values
		self.prev_values = {}
		for node in self.nodes:
			self.prev_values[node] = {}
			for attrib in self.values.keys():
				descriptor = self.values[attrib]
				if descriptor.valueType == self.VALUE_DIFF:
					self.prev_values[node][descriptor.key] = (None, float("NaN"))
		
		self.logger = GCLogger.getInstance().get_logger(self)
		
	def getValuesForLogging(self):
		logging_values = {}
		
		client = JMXThreadedClient(self.nodes, JOLOKIA_URL, timeout=3)
		results = client.readMultipleAttributes(self.values.keys())
		
		now = datetime.now()
		
		for node in self.nodes:
			nodeResults = results[node]
			
			for attrib in self.values.keys():
				descriptor = self.values[attrib]
				key = "%s.%s" % (descriptor.key, node)
				
				# the entire node has no response
				if nodeResults.status == JMXResponse.ERROR:
					current_value = float("NaN")
				# missing a single value
				elif nodeResults.results[attrib] == JMXResponse.VALUE_MISSING:
					current_value = float("NaN")
				# otherwise (good data)
				else:
					current_value = nodeResults.results[attrib]
				
				if descriptor.valueType == self.VALUE_DIFF:
					last_dt, last_value = self.prev_values[node][descriptor.key]
					self.prev_values[node][descriptor.key] = (now, current_value)
					if math.isnan(last_value):
						current_value = float("NaN")
					else:
						current_value = (current_value - last_value) / (now - last_dt).total_seconds()
			
				logging_values[key] = current_value
				
		return logging_values
	
class GCJolokiaManager(GCManagerBase):
	
	VALUE_ABSOLUTE = 1
	VALUE_DIFF = 2
	
	values = [
		("java.lang:type=OperatingSystem", "SystemLoadAverage", "cassandra_load", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=StorageProxy", "ReadOperations", "cassandra_read_rate", VALUE_DIFF),
		("org.apache.cassandra.db:type=StorageProxy", "WriteOperations", "cassandra_write_rate", VALUE_DIFF),
		("org.apache.cassandra.db:type=StorageProxy", "GreenHintWriteOperations", "cassandra_greenhint_write_rate", VALUE_DIFF),
		("org.apache.cassandra.db:type=StorageProxy", "RecentReadLatencyMicros", "cassandra_read_latency", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=StorageProxy", "RecentWriteLatencyMicros", "cassandra_write_latency", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=StorageProxy", "RecentGreenHintWriteLatencyMicros", "cassandra_greenhint_write_latency", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=CompactionManager", "TotalCompactionsCompleted", "cassandra_total_compactions", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=CompactionManager", "PendingTasks", "cassandra_pending_compactions", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=CompactionManager", "TotalBytesCompacted", "total_bytes_compacted", VALUE_DIFF),
		("java.lang:type=GarbageCollector,name=ConcurrentMarkSweep", "CollectionCount", "jvm_cms_count", VALUE_ABSOLUTE),
		("java.lang:type=GarbageCollector,name=ConcurrentMarkSweep", "CollectionTime", "jvm_cms_time", VALUE_ABSOLUTE),
		("java.lang:type=GarbageCollector,name=ParNew", "CollectionCount", "jvm_parnew_count", VALUE_ABSOLUTE),
		("java.lang:type=GarbageCollector,name=ParNew", "CollectionTime", "jvm_parnew_time", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "LiveSSTableCount", "sstables_data", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "ReadCount", "data_reads", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "WriteCount", "data_writes", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "RecentReadLatencyMicros", "data_read_latency", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "RecentWriteLatencyMicros", "data_write_latency", VALUE_ABSOLUTE),
		#("org.apache.cassandra.db:type=ColumnFamilies,keyspace=system,columnfamily=GreenHintsColumnFamily", "LiveSSTableCount", "sstables_greenhints", VALUE_ABSOLUTE),
		#("org.apache.cassandra.db:type=ColumnFamilies,keyspace=system,columnfamily=GreenHintsColumnFamily", "ReadCount", "greenhints_reads", VALUE_ABSOLUTE),
		#("org.apache.cassandra.db:type=ColumnFamilies,keyspace=system,columnfamily=GreenHintsColumnFamily", "WriteCount", "greenhint_writes", VALUE_ABSOLUTE),
		#("org.apache.cassandra.db:type=ColumnFamilies,keyspace=system,columnfamily=GreenHintsIndexColumnFamily", "LiveSSTableCount", "sstables_greenhints_index", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=GreenCassandraConsistencyManager", "GreenHintDeliveryRate", "greenhint_delivery_rate", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=GreenCassandraConsistencyManager", "GreenHintDeliveries", "greenhint_deliveries", VALUE_DIFF),
		("org.apache.cassandra.request:type=ReadStage", "CompletedTasks", "cassandra_local_reads", VALUE_ABSOLUTE),
		("org.apache.cassandra.request:type=MutationStage", "CompletedTasks", "cassandra_local_writes", VALUE_ABSOLUTE),
		("org.apache.cassandra.db:type=StorageService", "CompactionThroughputMbPerSec", "compaction_throughput", VALUE_ABSOLUTE),
	]
	
	def __init__(self, nodes=(MINIMUM_NODES+OPTIONAL_NODES)):
		self.nodes = nodes
		# init prev_values for diff values
		self.prev_values = {}
		for node in self.nodes:
			self.prev_values[node] = {}
			for value in self.values:
				mbean, attribute, description, kind = value
				if kind == self.VALUE_DIFF:
					self.prev_values[node][description] = (None, float("NaN"))
		self.results = {}
		
	def getJolokiaConn(self, host):
		url = JOLOKIA_URL % host
		jolokia_conn = Jolokia(url, timeout=3)
		for value in self.values:
			mbean, attribute, description, kind = value
			jolokia_conn.add_request(type="read", mbean=mbean, attribute=attribute)
		
		return jolokia_conn
	
	def getNodeValuesThread(self, node):
		#j4p = Jolokia(JOLOKIA_URL % node, timeout=1);
		jolokia_conn = self.getJolokiaConn(node)	
		data = None
		values = {}
		
		for i in xrange(0, 2):
			try:
				data = jolokia_conn.getRequests()
				#print data
				break
			except JolokiaError as e:
				print "Jolokia error for %s" % node
				print e
				continue
			except Exception as e:
				print "Random error getting jolokia data for %s, trying again..." % node
				print e
				continue
		
		if data != None:
			for entry in data:
				#mbean = None
				#attribute = None
					
				try:
					mbean = mbean_uniform(entry["request"]["mbean"])
					attribute = entry["request"]["attribute"]
					
					if entry["value"] == None:
						value = float("NaN")
					else:
						value = float(entry["value"])
				except KeyError:
					value = float("NaN")
				except TypeError:
					value = float("NaN")
				
				values[mbean, attribute] = value
		else:
			values = None

		self.results[node] = values
		
		thread.exit()
	
	def getValuesForLogging(self):
		values = {}
		threads = []
		
		for node in self.nodes:
			t = Thread(target=self.getNodeValuesThread, args=(node,))
			t.start()
			threads.append(t)

		for t in threads:
			t.join()
		
		now = datetime.now()
		
		for node in self.nodes:
			for value in self.values:
				mbean, attribute, description, kind = value
				mbean = mbean_uniform(mbean)
				node_results = self.results[node]
				if node_results == None:
					current_value = float("NaN")
				else:
					current_value = node_results[mbean, attribute]	
				
				if kind == self.VALUE_DIFF:
					last_dt, last_value = self.prev_values[node][description]
					self.prev_values[node][description] = (now, current_value)
					if math.isnan(last_value):
						current_value = float("NaN")
					else:
						current_value = (current_value - last_value) / (now - last_dt).total_seconds()

				key = "%s.%s" % (description, node)
				values[key] = current_value

		return values
	
if __name__ == "__main__":
	
	
	if len(sys.argv) == 2:
		nodes = [sys.argv[1]]
	else:
		nodes = MINIMUM_NODES + OPTIONAL_NODES
	
	logger = GCNullLogger()
	
	oldManager = GCJolokiaManager(nodes=nodes)
	newManager = GCJolokiaManagerNew(nodes=nodes)
	
	oldValues = oldManager.getValuesForLogging()
	newValues = newManager.getValuesForLogging()
	time.sleep(5)
	oldValues = oldManager.getValuesForLogging()
	newValues = newManager.getValuesForLogging()
	
	for value in sorted(oldValues.keys()):
		print "oldValues[%s] = %f" % (value, oldValues[value])
		print "newValues[%s] = %f" % (value, newValues[value])
		
	#print ""
	
	#values = manager.getValuesForLogging()
	#for value in sorted(values.keys()):
	#	print "values[%s] = %f" % (value, values[value])
	
		