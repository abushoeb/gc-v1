#!/usr/bin/python

from gcanalysis import *
from gcplotter import *
from gcepochs import *

class GCPlotExperiment(GCExperimentProcessor):
	
	reqs =	[
		"brown_consumed",
		"solar_consumed",
		"solar_excess",
		"power_total",
		"state",
		# Average
		"readlatency_avg_window",
		# 95th
		#"readlatency_95",
		#"writelatency_95",
		#"readlatency_95_cum",
		#"writelatency_95_cum",
		#"readlatency_95_window",
		#"writelatency_95_window",
		#99thoptions
		#"readlatency_99",
		#"writelatency_99",
		"readlatency_99_cum",
		"writelatency_99_cum",
		"insertlatency_99_cum",
		"readlatency_99_window",
		"writelatency_99_window",
		"insertlatency_99_window",
		"cassandra_load",
		"nodes_total",
		"nodes_ready",
		"nodes_transition",
		"ycsb.throughput",
		"ycsb.target",
		"any_transitions",
		"workload_moving_avg",
		"workload_exp_moving_avg",
		"avg_readlatency_99",
		"greenhint_delivery_rate",
		"greenhint_delivery_rate_total",
		"greenhint_deliveries",
		"greenhint_deliveries_total",
		#"sstables_greenhints",
		#"sstables_greenhints_index",
		"sstables_data",
		"cassandra_total_compactions",
		"cassandra_read_latency",
		"cassandra_write_latency",
		"cassandra_greenhint_write_latency",
		"cassandra_read_rate",
		"cassandra_write_rate",
		"cassandra_greenhint_write_rate",
		"cassandra_local_reads",
		"cassandra_local_writes",
		"greenhint_reads",
		"greenhint_writes",
		"data_reads",
		"data_writes",
		"data_read_latency",
		"data_write_latency",
		"jvm_cms_time",
		"jvm_parnew_time",
		"jvm_cms_count",
		"jvm_parnew_count",
		"compaction_throughput",
		"total_bytes_compacted",
		"write_tracker_recent",
		"read_tracker_recent",
		"greenhint_tracker_recent",
	]
	
	def __init__(self):
		super(GCPlotExperiment, self).__init__(self.reqs)
		
		parser = self.getParser()
		parser.add_argument("--format", default="pdf")
		parser.add_argument("-p", "--paper", dest="paper", action="store_true")
		parser.add_argument("--limitx", action="store_true", default=False)
		parser.add_argument("--epochs", action="store_true")
		
	def processExperiment(self, args, experiment, results, output):
		if args.paper:
			format = "svg"
		else:
			format = args.format
		
		# Switch the backend depending on desired format
		if format == "pdf":
			#matplotlib.use("PDF")
			pyplot.switch_backend("PDF")
		elif format == "svg":
			pyplot.switch_backend("Cairo")
		elif format == "png":
			pyplot.switch_backend("AGG")
			
		print 'Reading model...'
		#response_time_model = GCTableResponseTimeModelNew("1-22-UNIFORM-300kbs-MIXED-ONE.model", extrapolate=True)

		print 'Initialize plotter...'
		if args.paper:
			plotter = GCPlotter(xsize=14, ysize=7)
		else:
			if args.limitx:
				plotter = GCPlotter(xsize=30, ysize=50, limitx=results)
			else:
				plotter = GCPlotter(xsize=30, ysize=50)
		
		# Power
		print 'Power...'
		#plot = GCPowerPlot(results, title=("" if args.paper else "Power"))
		#plotter.addPlot(plot)
		
		# Workload
		print 'Workload...'
		if not args.paper:
			#try:
		#		plot = GCMultiCommonPlot(results, ["actual_workload", "target_workload"], title="Workload", ylabel="op/s")
		#	except:
			plot = GCMultiCommonPlot(results, ["ycsb_throughput", "ycsb_target"], title="Workload", ylabel="op/s")
			plotter.addPlot(plot)
			
			if args.epochs:
				epochs = GCEpochs(results.data_dir)
				plot = GCEpochPlot(results, epochs)
				plotter.addPlot(plot)
			
		#plot = GCMultiCommonPlot(results, ["workload_moving_avg", "workload_exp_moving_avg"], title="Workload Moving Avg", ylim=(0,5000), ylabel="op/s")
		#plotter.addPlot(plot)
			
		print "Workload prediction..."
		#workload_predictor = GCAskTraceWorkloadPredictor("ask-workload-4.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))
		#workload_predictor = GCMicrosoftTraceWorkloadPredictor("workload.csv", field=1, shift=timedelta(days=1))
		
		latency_plots = []
		
		# Merge real and prediction
		plot1 = GCMultiCommonPlot(results, ["readlatency_99_window", "writelatency_99_window", "insertlatency_99_window"], ylim=(0, 200), indexLine=50, title="Window Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
		latency_plots.append(plot1)
		
		plot1 = GCMultiCommonPlot(results, ["readlatency_astyanax_99_window", "writelatency_astyanax_99_window", "insertlatency_99_window"], ylim=(0, 200), indexLine=50, title="Window Latency (Asyanax)", ylabel="Response time (ms)") # We add the 75 ms SLA
		latency_plots.append(plot1)
		
		plot = GCCombinedPlot(latency_plots, legendPos=None)
		plotter.addPlot(plot)
		
		if args.epochs:
			epochs = GCEpochs(results.data_dir, f="custom_epochs")
			plot = GCEpochPlot(results, epochs)
			plotter.addPlot(plot)
		
		plot = GCCompactionPlot(results)
		plotter.addPlot(plot)


		#plot = GCLatencyHistogramPlot(results, 99, timedelta(seconds=60), ylim=(0, 150), stride=1)
		#plotter.addPlot(plot)
					
		#plot = GCLatencyHistogramPlot(results, 99, timedelta(seconds=240), ylim=(0, 100))
		#plotter.addPlot(plot)
		
		#plot = GCLatencyHistogramPlot(results, 99, timedelta(minutes=5), ylim=(0, 200))
		#plotter.addPlot(plot)
		
		plot = GCMultiCommonPlot(results, ["readlatency_99_cum", "writelatency_99_cum", "insertlatency_99_cum"], ylim=(0, 200), indexLine=50, title="Cumulative Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
		plotter.addPlot(plot)
		
		# Only for debugging
		if not args.paper:
			print 'Nodes...'
			#plot = GCNodePlot(results)
			#plotter.addPlot(plot)

			#plot = GCNodeStatePlot(results)
			#plotter.addPlot(plot)
			
			#plot = GCNodeStatePlotNew(results)
			#plotter.addPlot(plot)
			
			plot = GCSimpleCommonPlot(results, "pending_compactions", title="Pending Compactions", ylabel="Compactions")
			plotter.addPlot(plot)
			
			print 'System load...'
			plot = GCSimpleMultiNodePlot(results, "cassandra_load", MINIMUM_NODES, title="Load (min)", legend=True, ylim=(0, 10), ylabel="System load")
			plotter.addPlot(plot)
			
			plot = GCSimpleMultiNodePlot(results, "cassandra_load", [i for i in OPTIONAL_NODES if OPTIONAL_NODES.index(i) % 2 == 1], title="Load (odd)", legend=True, ylim=(0, 10), ylabel="System load")
			plotter.addPlot(plot)
			
			plot = GCSimpleMultiNodePlot(results, "cassandra_load", [i for i in OPTIONAL_NODES if OPTIONAL_NODES.index(i) % 2 == 0], title="Load (even)", legend=True, ylim=(0, 10), ylabel="System load")
			plotter.addPlot(plot)
			
			plot = GCSimpleMultiNodePlot(results, "compaction_throughput", MINIMUM_NODES + OPTIONAL_NODES, title="Compaction Limit", legend=False, ylim=(0, 3192), ylabel="Throughput (kb/s)")
			plotter.addPlot(plot)
			
			#plot = GCSimpleMultiNodePlot(results, "gc_OU", MINIMUM_NODES+OPTIONAL_NODES, title="Old Generation", legend=True, ylabel="bytes")
			#plotter.addPlot(plot)
			
			#plot = GCSimpleMultiNodePlot(results, "gc_GCT", MINIMUM_NODES+OPTIONAL_NODES, title="GC Time", legend=True, ylabel="seconds")
			#plotter.addPlot(plot)
			
			#plot = GCSimpleMultiNodePlot(results, "gc_FGCT", MINIMUM_NODES+OPTIONAL_NODES, title="GC Time", legend=True, ylabel="seconds")
			#plotter.addPlot(plot)	

		print 'Plotting...'
		output_file = "%s.%s" % (output, format)
		plotter.doPlot(output=output_file, format=format)
		print "Plot completed, output file: %s" % output_file
		
if __name__ == "__main__":
	plotter = GCPlotExperiment()
	plotter.run()