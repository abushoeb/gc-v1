#!/usr/bin/python

# script to automatically run experiments according to a "config file"
# need to define a python-compliant text file with the following variables defined:

# LEVELS: a python list with a name for each level of recursion
# *_VALUES: a python list with a set of values for each level of recursion. * must match name of LEVEL
# COMMAND: a python string with variables defined for each level in format "$LEVEL"
# ERROR_STOP: a python boolean to indicate if the run should stop if the command returns nonzero
import sys
import imp
import subprocess

def errorOut(string):
	print "Config Error: %s" % string
	sys.exit(-1)

def recurseLevels(levels, values, string):
	nextLevels = list(levels)
	currentLevel = nextLevels.pop(0)
	
	for value in values[currentLevel]:
		nextString = string.replace("$%s" % currentLevel, value)
		
		if len(nextLevels) > 0:
			recurseLevels(nextLevels, values, nextString)		
		else:
			executeCommand(nextString)
			
def executeCommand(commandString):
	commandList = commandString.split(" ")
	result = subprocess.call(commandList)
	
	if result and config.ERROR_STOP:
		print "Error encountered, stopping (error code %d)..." % result
		sys.exit()
	
if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "Usage: %s <config_file>" % sys.argv[0]
		sys.exit(-1)
		
	filename = sys.argv[1]
	try:
		config = imp.load_source("experiment_config", filename)
	except IOError:
		print "File not found: %s" % filename
		sys.exit(-1)
	
	config_dir = dir(config)
	
	if "ERROR_STOP" not in config_dir:
		config.ERROR_STOP = False
		
	if not "LEVELS" in config_dir:
		errorOut("Config var LEVELS not found...")
	
	if not "COMMAND" in config_dir:
		errorOut("Config var COMMAND not found...")
		
	# check command
	for level in config.LEVELS:
		if "$%s" % level not in config.COMMAND:
			errorOut("Config var COMMAND missing level %s" % level)

	values = {}
	# check values
	for level in config.LEVELS:
		listVar = "%s_VALUES" % level
		if listVar not in config_dir:
			errorOut("Config var %s not found..." % listVar)
		values[level] = eval("config.%s" % listVar)
			
	recurseLevels(config.LEVELS, values, config.COMMAND)
	