#!/usr/bin/python

import sys
import hashlib
import matplotlib.pyplot as pyplot

start = "**********************************************"

tokens = {}

def load_tokens():
	f = open("/home/wkatsak/wonko_home/greencassandra/gc.tokens", "r")
	lines = f.read().strip().split("\n")
	
	last_token = 0
	for line in lines:
		node, ip, region, token = line.strip().split(" ")
		token = int(token)
		if region == "minimum":
			tokens[node] = token
			last_token = token
			print node, token
		else:
			break
	
	tokens["sol050"] = 2**127 #last_token*2
	print "sol050", 2**127 #last_token*2
	
	keys = sorted(tokens.keys())
	for i in xrange(1, len(keys)):
		print "%s-%s: %d" % (keys[i-1], keys[i], tokens[keys[i]] -  tokens[keys[i-1]])

def gen_tokens():
	f = open("/home/wkatsak/wonko_home/greencassandra/gc.tokens", "r")
	lines = f.read().strip().split("\n")
	
	nodes = []
	for line in lines:
		node, ip, region, token = line.strip().split(" ")
		if region == "minimum":
			nodes.append(node)
	
	for i in xrange(0, len(nodes)):
		token = i * (2**128 / len(nodes))
		tokens[nodes[i]] = token
		print nodes[i], token
	
	token = token + 1
	tokens["sol050"] = 2**128
	print "sol050", tokens["sol050"]
		
if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "Please specify file..."
		sys.exit()
		
	load_tokens()
	#gen_tokens()
	
	#sys.exit()
	filename = sys.argv[1]
	f = open(filename, "r")
	lines = f.read().strip().split("\n")
	
	while lines[0] != start:
		lines.pop(0)
	lines.pop(0)
	
	key_dict = {}
	txt_key_dict = {}
	txt_all_keys = []
	
	for line in lines:
		fields = line.strip().split(" ")
		if fields[0] == "[OVERALL],":
			break
			
		key = fields[2]
		txt_key = key
		
		key = key.replace("user", "")
		key = int(key)
		txt_all_keys.append(txt_key)
		
		try:
			key_dict[key] += 1
		except KeyError:
			key_dict[key] = 1
			
		try:
			txt_key_dict[txt_key] += 1
		except KeyError:
			txt_key_dict[txt_key] = 1
			

	counts = {}
	counts["sol050"] = 0
	print len(txt_all_keys)
	for key in txt_all_keys:
		m = hashlib.md5()
		m.update(key)
		#digest = int(m.hexdigest(), base=16) % 2**127
		digest = int(m.hexdigest(), base=16) >> 1
		
		#if digest > 2**127:
		#	print "Oh shit: %d" % digest
		#print key, digest
		
		for i in xrange(1, len(tokens.keys())):
			node = sorted(tokens.keys())[i-1]
			next_node = sorted(tokens.keys())[i]
			

			if digest >= tokens[node] and digest < tokens[next_node]:				
				#if  node == "sol041":# or next_node == "sol041":
				#	print "sol041"
				
				try:
					counts[node] += 1
				except KeyError:
					counts[node] = 1
				break
	
	for node in sorted(tokens.keys(), key=lambda(v):counts[v]):
		if node == "sol050":
			continue
		try:
			print "%s: %d %f" % (node, counts[node], counts[node]*1.0/len(txt_all_keys))
		except KeyError:
			print "%s: missing" % node
	total = 0
	for key in counts.keys():
		total += counts[key]

	print "Total %d" % total

	keys = sorted(key_dict.keys(), key=lambda(v):key_dict[v], reverse=True)
	
	values = []
	unique_values = []
	
	for i in xrange(0, 250):
		key = keys[i]
		val = key_dict[key]
		
		this_value = i
		
		for j in xrange(0, val):
			values.append(i)
		
		unique_values.append(i)
	
	pyplot.hist(values, unique_values)
	pyplot.show()

	
		
