#!/usr/bin/python

import matplotlib.pyplot as plot
import datetime
import time
import re
import sys
import random
import gtk


results_folder = "/home/wkatsak/wonko_home/python-results"

def get_file_name(dialog_title, filter_patterns, path="."):
	dialog = gtk.FileChooserDialog(dialog_title, None, gtk.FILE_CHOOSER_ACTION_OPEN, 
		(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK))
	dialog.set_default_response(gtk.RESPONSE_OK)
	dialog.set_current_folder(path)
	filter = gtk.FileFilter()
	filter.set_name("acceptable data files")
	
	for pattern in filter_patterns:
		filter.add_pattern(pattern)

	dialog.add_filter(filter)

	response = dialog.run()

	if response == gtk.RESPONSE_OK:
		filename = dialog.get_filename()
	elif response == gtk.RESPONSE_CANCEL:
		filename = ""

	dialog.destroy()

	return filename

def get_event_times(cassandra_log_file, diff=datetime.timedelta(hours=0)):
	file_in = open(cassandra_log_file, 'r')
	logfile = file_in.read()
	file_in.close()

	lines = logfile.strip().split("\n")

	start_times = list()
	end_times = list()
	event_labels = list()
	
	for line in lines:
		if line == "":
			continue
		data = line.strip().split(" ")
		#print data
		start_date = data[0]
		start_time = data[1].split(".")
		end_date = data[2]
		end_time = data[3].split(".")
		if len(data) >= 5:
			event_name = data[4]
		else:
			event_name = ""

		start_string = start_date + " " + start_time[0]
		end_string = end_date + " " + end_time[0]

#		diff = datetime.timedelta(hours=4)

		start_dt = datetime.datetime.strptime(start_string, "%Y-%m-%d %H:%M:%S")
		start_dt = start_dt.replace(microsecond=int(start_time[1]))
		start_dt += diff

		end_dt = datetime.datetime.strptime(end_string, "%Y-%m-%d %H:%M:%S")
		end_dt = end_dt.replace(microsecond=int(end_time[1]))
		end_dt += diff

		global time0
		global first_time
		if time0 > start_dt:
			time0 = start_dt

		start_dt = first_time + (start_dt - time0)
		end_dt = first_time + (end_dt - time0)

		start_times.append(start_dt)
		end_times.append(end_dt)
		event_labels.append(event_name)

	return start_times, end_times, event_labels


def get_power_data(power_data_file):
	file_in = open(power_data_file, 'r')
	logfile = file_in.read()
	file_in.close()

	lines = logfile.split("\n")
	#lines = logfile.split("[")
	power_regex = re.compile(".*-.*-.*", re.I)
	power_lines = list()

	for line in lines:
		if power_regex.search(line):
			power_lines.append(line)

	time_values = list()
	power_values = list()

	for line in power_lines:
		power_data = line.split(" ")
		datetimestamp = power_data[0] + " " + power_data[1]
		dt = datetime.datetime.strptime(datetimestamp, "%Y-%m-%d %H:%M:%S.%f")
		global time0
		global first_time
		if time0 > dt:
			time0 = dt

		dt = first_time + (dt - time0)

		time_values.append(dt)
		power_values.append(float(power_data[2]))
	
	return time_values, power_values

def get_simple_data(simple_data_file):
	file_in = open(simple_data_file, 'r')
	logfile = file_in.read()
	file_in.close()

	lines = logfile.strip().split("\n")

	time_values = list()
	data_values = list()

	for line in lines:
		data_point = line.split(" ")
#		print data_point
		datetimestamp = data_point[0] + " " + data_point[1]
		dt = datetime.datetime.strptime(datetimestamp, "%Y-%m-%d %H:%M:%S.%f")
#		delta = datetime.timedelta(hours=4)
#		dt += delta

		global time0
		if time0 > dt:
			time0 = dt
		dt = first_time + (dt - time0)

		time_values.append(dt)
		data_values.append(float(data_point[2]))
	
	return time_values, data_values

def get_throughput_data(ycsb_data_file):
	file_in = open(ycsb_data_file, 'r')
	logfile = file_in.read()
	file_in.close()

	lines = logfile.split("\n")
	throughput_regex = re.compile("operations;", re.I)
	throughput_lines = list()

	for line in lines:
		if throughput_regex.search(line):
			throughput_lines.append(line)
	
	time_values = list()
	throughput_values = list()

	for line in throughput_lines:
		throughput_data = line.strip().replace(".", " ").split(" ")
		datetimestamp = throughput_data[0] + " " + throughput_data[1]
		dt = datetime.datetime.strptime(datetimestamp, "%Y-%m-%d %H:%M:%S")
		delta = datetime.timedelta(hours=4)
		new_dt = dt + delta
		#if new_dt.day > dt.day:
		#	new_dt -= datetime.timedelta(hours=24)
		dt = new_dt
		
		global time0
		if time0 > dt:
			time0 = dt
		dt = first_time + (dt - time0)

		time_values.append(dt)
		if len(throughput_data) > 7:
			throughput_values.append(int(throughput_data[7]))
		else:
			throughput_values.append(0)
	
	return time_values, throughput_values

def get_datasize_data(datasize_data_file):
	file_in = open(datasize_data_file, 'r')
	logfile = file_in.read()
	file_in.close()

	lines = logfile.split("\n")
	datasize_lines = list()
	time_values = list()
	datasize_values = list()
	
	datasize_re = re.compile(".*:", re.I)
	for line in lines:
		if datasize_re.search(line):
			datasize_lines.append(line)

	for line in datasize_lines:
		data = line.split(" ")
		datetimestamp = data[0] + " " + data[1]
		dt = datetime.datetime.strptime(datetimestamp, "%Y-%m-%d %H:%M:%S")
		#print dt

		global time0	
		if time0 > dt:
			time0 = dt

		dt = first_time + (dt - time0)

		time_values.append(dt)
		datasize_values.append(float(data[2]))
	
	return time_values, datasize_values

def plot_events(plot, title, start_times, end_times, event_labels):
	counter = 0
	ymax = 1.0
	steps = 0
	
	for i in xrange(len(start_times)):
		if i == 0:
			steps +=1
		elif i > 0:
			if start_times[i] < end_times[i-1]:
				steps += 1
	
	if steps == 0:
		steps = 1

	increment = ymax / steps
	
	for i in xrange(len(start_times)):
		value = counter % 2
		counter += 1	
		if value == 0:
			color = "g"
		else:
			color = "r"

		plot.axvspan(start_times[i], end_times[i], ymin=ymax-increment, ymax=ymax, facecolor=color, alpha=0.5)
		if event_labels[i] != "":
			plot.annotate(event_labels[i], xy=(start_times[i] + ((end_times[i] - start_times[i])/3), (ymax-increment + ymax) / 2),
				size=15,bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 1.0))
	
		if i < len(start_times)-1 and start_times[i+1] < end_times[i]:
			ymax = ymax - increment

	plot.title(title)
	plot.grid(True)

def plot_time_data(plot, title, time_values, data_values):
	plot.plot(time_values, data_values)
	plot.title(title)
	plot.grid(True)

def plot_time_data_ylim(plot, title, time_values, data_values, ylim):
	plot.plot(time_values, data_values)
	plot.ylim(ylim)
	plot.title(title)
	plot.grid(True)

def set_time0(num_graphs):
	global time0
	for i in range(0, num_graphs):
		file_name = sys.argv[2+i]
		diff = datetime.timedelta(hours = 0)

		for ext in diff_table:
			if file_name.count(ext) > 0:
				diff = datetime.timedelta(hours=4)
				break

		file_in = open(file_name, 'r')
		logfile = file_in.read()
		file_in.close()
		lines = logfile.strip().split("\n")
	
		for line in lines:
			if line == "":
				continue
			data = line.strip().split(" ")
			date = data[0]
			time = data[1].split(".")
			dt_string = date + " " + time[0]

			try:
				dt = datetime.datetime.strptime(dt_string, "%Y-%m-%d %H:%M:%S")
			except ValueError as err:
				continue

			dt = dt.replace(microsecond=int(time[1]))
			dt += diff

			if time0 > dt:
				print "Found earlier time in " + file_name + " " + str(dt)
				time0 = dt

##############################

first_time = datetime.datetime.fromtimestamp(0) + datetime.timedelta(hours=5)
time0 = datetime.datetime.utcnow()

diff_table = [".ycsb"]

if len(sys.argv) < 2:
	num_graphs = 1
else:
	num_graphs = int(sys.argv[1])

set_time0(num_graphs)

print "Graphing " + str(num_graphs) + " datafiles..."

plot.rcParams.update({'font.size': 10})
plot.figure(figsize=(30,15), frameon=True)
plots = list()

for i in range(1, num_graphs+1):
	if len(sys.argv) > 2:
		file_name = sys.argv[2+i-1]
	else:
		file_name = get_file_name("Select file to graph...",
#			["major", "minor", "compaction", "flush", "gc", "power",
#				"ycsb", "datasize", "sstables", "livespace", "totalspace", "readlatency", "writelatency"])
			["power.*", "state.*", "solar", "used_solar", "workload", "nodes_on"])


	if file_name == "":
		print "Canceled..."
		sys.exit()

	div = 1.0 / num_graphs
	#print div
	bottom = 1-div - (i-1)*div + 0.015
	height = div - 0.04
	#print "bottom: " + str(bottom)
	#print "height: " + str(height)
	
	if i == 1:
		x = plot.axes([0.02, bottom, 0.96, height])
	else:
		x = plot.axes([0.02, bottom, 0.96, height], sharex=plots[0])

	plots.append(x)		

	if file_name.count(".major") > 0:
		label = "Major Compaction (" + file_name + ")"
		start_times, end_times, event_labels = get_event_times(file_name)
		plot_events(plot, label, start_times, end_times, event_labels)

	elif file_name.count(".minor") > 0:
		label = "Minor Compaction (" + file_name + ")"
		start_times, end_times, event_labels = get_event_times(file_name)
		plot_events(plot, label, start_times, end_times, event_labels)

	elif file_name.count(".compaction") > 0:
		label = "Compaction (" + file_name + ")"
		start_times, end_times, event_labels = get_event_times(file_name)
		plot_events(plot, label, start_times, end_times, event_labels)

	elif file_name.count(".flush") > 0:
		label = "Flushes (" + file_name + ")"
		start_times, end_times, event_labels = get_event_times(file_name)
		plot_events(plot, label, start_times, end_times, event_labels)

	elif file_name.count(".gc") > 0:
		label = "gc (" + file_name + ")"
		start_times, end_times, event_labels = get_event_times(file_name)
		plot_events(plot, label, start_times, end_times, event_labels)

	elif file_name.count(".sstables") > 0:
		label = "SSTable Count (" + file_name + ")"
		start_times, end_times = get_simple_data(file_name)
		plot_time_data(plot, label, start_times, end_times)
	
	elif file_name.count(".livespace") > 0:
		label = "Live Disk Space Used (" + file_name + ")"
		start_times, end_times = get_simple_data(file_name)
		plot_time_data(plot, label, start_times, end_times)
	
	elif file_name.count(".totalspace") > 0:
		label = "Total Disk Space Used (" + file_name + ")"
		start_times, end_times = get_simple_data(file_name)
		plot_time_data(plot, label, start_times, end_times)
	
	elif file_name.count(".readlatency") > 0:
		label = "Read Latency (" + file_name + ")"
		start_times, end_times = get_simple_data(file_name)
		plot_time_data(plot, label, start_times, end_times)

	elif file_name.count(".writelatency") > 0:
		label = "Write Latency (" + file_name + ")"
		start_times, end_times = get_simple_data(file_name)
		plot_time_data(plot, label, start_times, end_times)

	elif file_name.count(".power") > 0:
		label = "Power (" + file_name + ")"
		times, data = get_power_data(file_name)
		plot_time_data_ylim(plot, label, times, data, [20,35])

	elif file_name.count(".ycsb") > 0:
		label = "Throughput (" + file_name + ")"
		times, data = get_throughput_data(file_name)
		plot_time_data(plot, label, times, data)

	elif file_name.count(".datasize") > 0:
		label = "Data Size (" + file_name + ")"
		times, data = get_datasize_data(file_name)
		plot_time_data(plot, label, times, data)

	elif file_name.count(".phases") > 0:
		label = "Phases ( " + file_name + ")"
		start_times, end_times, event_labels = get_event_times(file_name)
		plot_events(plot, label, start_times, end_times, event_labels)
	else:
		label = file_name
		start_times, end_times = get_simple_data(file_name)
		plot_time_data(plot, label, start_times, end_times)

for i in range(0, num_graphs-1):
	plot.setp(plots[i].get_xticklabels(), visible=False)

hour2 = list()
hour2.append(first_time + datetime.timedelta(hours=1))
hour2value = list()
hour2value.append(0)

plot.plot(hour2, hour2value)

if len(sys.argv) <= 2:
	plot.show()
else:
	plot.savefig(sys.argv[num_graphs + 2], format="pdf")


sys.exit()
