#!/usr/bin/python

import argparse
import sqlitedict
import os
import sys

FULL_PLOT = 0
SMALL_PLOT = 1

FULL_PLOT_SCRIPT = "plot-full.plot"
SMALL_PLOT_SCRIPT = "plot-small.plot"

PAPER_DIR = "/home/wkatsak/pubs/greenDC/greencassandra"
PLOT_OUTPUT_DIR = "%s/img" % PAPER_DIR
STATS_CACHE_FILE = "/tmp/paperstats.db"
	
class ExperimentData(object):
	def __init__(self, path=None, plot_type=None, solar_day=None, caveat=False):
		self.path = path
		self.plot_type = plot_type
		self.solar_day = solar_day
		self.caveat = caveat
		
	def __eq__(self, other):
		return hash(self.path) + hash(self.solar_day) == hash(other.path) + hash(other.solar_day)
	
	def __str__(self):
		return str(self.path) + "-" + str(self.solar_day)
	
ExperimentData.TBD = ExperimentData()

class SortExperimentKey(object):
	experiment_keys = {"reactive" : 0, "baseline": 0.5, "baseline-model" : 1, "noop" : 2, "reactive-a" : 3, "credit" : 4, "credit-m" : 5, "opt" : 6, "opt-brownprices" : 7}
	solar_keys = {None : 0, "medsolar" : 1, "lowsolar" : 2}
	
	def __init__(self, experiment, *args):
		fields = experiment.split("-")
		if len(fields) == 3:
			self.solar = fields[2]
			self.experiment = "-".join(fields[0:2])
		elif len(fields) == 2 and fields[1] in ["medsolar", "lowsolar"]:
			self.solar = fields[1]
			self.experiment = fields[0]
		elif len(fields) == 2:
			self.solar = None
			self.experiment = "-".join(fields[0:2])
		elif len(fields) == 1:
			self.solar = None
			self.experiment = fields[0]
		
	def __lt__(self, other):
		if self.experiment_keys[self.experiment] < self.experiment_keys[other.experiment]:
			return True
		elif self.experiment_keys[self.experiment] <= self.experiment_keys[other.experiment] and self.solar_keys[self.solar] < self.solar_keys[other.solar]:
			return True
		else:
			return False

BASELINE_EXPERIMENT = "reactive"

BASELINE_EXPERIMENTS = {
	"ask" : "reactive",
	"messenger" : "reactive",
	"hotmail" : "reactive",
}

MEDSOLAR_SUFFIX = "medsolar"
LOWSOLAR_SUFFIX = "lowsolar"

MEDSOLAR_DAY = "9-29-2013"
LOWSOLAR_DAY = "10-11-2013"

plots = {
	
	"ask": {
		"baseline-model"		:	ExperimentData(path="/results/ask/baseline/rerun", plot_type=FULL_PLOT),
		"baseline-model-medsolar"	:	ExperimentData(path="/results/ask/baseline/rerun", plot_type=FULL_PLOT, solar_day=MEDSOLAR_DAY),
		"baseline-model-lowsolar"	:	ExperimentData(path="/results/ask/baseline/rerun", plot_type=FULL_PLOT, solar_day=LOWSOLAR_DAY),
		
		"noop"				:	ExperimentData(path="/results/ask/noop/rerun_2", plot_type=FULL_PLOT),
		"noop-medsolar"			:	ExperimentData(path="/results/ask/noop/rerun_2", plot_type=FULL_PLOT, solar_day=MEDSOLAR_DAY),
		"noop-lowsolar"			:	ExperimentData(path="/results/ask/noop/rerun_2", plot_type=FULL_PLOT, solar_day=LOWSOLAR_DAY),
		
		"credit-m"			:	ExperimentData(path="/results/ask/credit/tues-repeat", plot_type=FULL_PLOT),
		"credit-m-medsolar"		:	ExperimentData(path="/results/ask/credit/solarsensivity3", plot_type=FULL_PLOT),
		"credit-m-lowsolar"		:	ExperimentData(path="/results/ask/credit/tues-repeat_2", plot_type=FULL_PLOT),
		
		"opt"				:	ExperimentData(path="/results/ask/opt/sunday-debug-plus5", plot_type=FULL_PLOT),
		"opt-medsolar"			:	ExperimentData(path="/results/ask/opt/tuesday-debug-plus5-sensitivity", plot_type=FULL_PLOT),
		"opt-lowsolar"			:	ExperimentData(path="/results/ask/opt/tuesday-debug-plus5-sensitivity_2", plot_type=FULL_PLOT),
		
		"opt-brownprices"		:	ExperimentData(path="/results/ask/opt-brownprices/monday-debug-plus5", plot_type=FULL_PLOT),
		"opt-brownprices-medsolar"	:	ExperimentData(path="/results/ask/opt-brownprices/tuesday-debug-plus5-sensitivity", plot_type=FULL_PLOT),
		"opt-brownprices-lowsolar"	:	ExperimentData(path="/results/ask/opt-brownprices/tuesday-debug-plus5-sensitivity_2", plot_type=FULL_PLOT),
		
		"reactive"			:	ExperimentData(path="/results/ask/reactive/tuesday_4", plot_type=FULL_PLOT),
		"reactive-medsolar"		:	ExperimentData(path="/results/ask/reactive/tuesday_4", plot_type=FULL_PLOT, solar_day=MEDSOLAR_DAY),
		"reactive-lowsolar"		:	ExperimentData(path="/results/ask/reactive/tuesday_4", plot_type=FULL_PLOT, solar_day=LOWSOLAR_DAY),
		
		"reactive-a"			:	ExperimentData(path="/results/ask/reactive-a/sunday", plot_type=FULL_PLOT),
		
		"credit"			:	ExperimentData(path="/results/ask/credit/sunday", plot_type=FULL_PLOT), 
		"credit-medsolar"		:	ExperimentData(path="/results/ask/credit/monday-sensitivity", plot_type=FULL_PLOT), 
		"credit-lowsolar"		:	ExperimentData(path="/results/ask/credit/monday-sensitivity_2", plot_type=FULL_PLOT), 
	},

	"messenger" : {
		"baseline-model"		:	ExperimentData(path="/results/messenger/baseline/fixmonday", plot_type=FULL_PLOT),
		"noop"				:	ExperimentData(path="/results/messenger/noop/fixmonday", plot_type=FULL_PLOT),
		"credit-m"			:	ExperimentData(path="/results/messenger/credit/fixedpredictor", plot_type=FULL_PLOT),
		"opt"				:	ExperimentData(path="/results/messenger/opt/sunday-debug-plus5", plot_type=FULL_PLOT),
		"opt-brownprices"		:	ExperimentData(path="/results/messenger/opt-brownprices/monday-debug-plus5", plot_type=FULL_PLOT),
		"reactive"			:	ExperimentData(path="/results/messenger/reactive/friday_5", plot_type=FULL_PLOT),
		"reactive-a"			:	ExperimentData(path="/results/messenger/reactive-a/sunday", plot_type=FULL_PLOT),
		"credit"			:	ExperimentData(path="/results/messenger/credit/sunday", plot_type=FULL_PLOT),
	},
	
	"hotmail" : {
		"baseline"			:	ExperimentData(path="/results/hotmail/baseline/fixmonday", plot_type=FULL_PLOT),
		"noop"				:	ExperimentData(path="/results/hotmail/noop/fixmonday", plot_type=FULL_PLOT),
		"credit-m"			:	ExperimentData(path="/results/hotmail/credit/fixedpredictor", plot_type=FULL_PLOT),
		"opt"				:	ExperimentData(path="/results/hotmail/opt/sunday-debug-plus5", plot_type=FULL_PLOT),
		"opt-brownprices"		:	ExperimentData(path="/results/hotmail/opt-brownprices/monday-debug-plus5", plot_type=FULL_PLOT),
		"reactive"			:	ExperimentData(path="/results/hotmail/reactive/friday", plot_type=FULL_PLOT),
		"reactive-a"			:	ExperimentData(path="/results/hotmail/reactive-a/sunday", plot_type=FULL_PLOT),
		"credit"			:	ExperimentData(path="/results/hotmail/credit/sunday", plot_type=FULL_PLOT),
	}
}

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--clear", action="store", type=str, required=False)
	parser.add_argument("--clearall", action="store_true", default=False)
	args = parser.parse_args()
	

	
	if args.clearall:
		
		if os.path.exists(STATS_CACHE_FILE):
			print "Removing %s..." % STATS_CACHE_FILE
			os.unlink(STATS_CACHE_FILE)
		else:
			print "No cache file..."
		
		for filename in [f for f in os.listdir(PLOT_OUTPUT_DIR) if ".done" in f]:
			   full_path = "%s/%s" % (PLOT_OUTPUT_DIR, filename)
			   print "Removing %s" % full_path
			   os.unlink(full_path)			   	
	
	elif args.clear:
		# get descriptor
		experiment = args.clear
		fields = experiment.split("-")
		workload = fields[0]
		experiment = "-".join(fields[1:])
		
		try:
			descriptor = plots[workload][experiment]
		except KeyError as e:
			print "%s: unknown experiment" % args.clear
			sys.exit()
			
		# clear cache
		cache = sqlitedict.SqliteDict("/tmp/paperstats.db", autocommit=True)
		
		if str(descriptor) in cache:
			print "Clearing stats for %s from cache..." % str(descriptor)
			del cache[str(descriptor)]
		else:
			print "Couldn't find %s in cache..." % str(descriptor)
		
		# plot
		filename = "%s/%s-%s.done" % (PLOT_OUTPUT_DIR, workload, experiment)
		if os.path.isfile(filename):
			print "Removing %s..." % filename
			os.unlink(filename)
		else:
			print "Couldn't find %s..." % filename
	else:
		parser.print_help()
	