#!/usr/bin/python

from gcanalysis import *
from gcplotter import *

class GCPlotExperiment(GCExperimentProcessor):
	
	reqs =	[
		"brown_consumed",
		"solar_consumed",
		"solar_excess",
		"power_total",
		"state",
		"actual_workload",
		"target_workload",
		"readlatency_99_cum",
		"writelatency_99_cum",
		"readlatency_99_window",
		"writelatency_99_window",
		"nodes_total",
		"nodes_ready",
		"nodes_transition",
	]
	
	def __init__(self):
		super(GCPlotExperiment, self).__init__(self.reqs)
		
		parser = self.getParser()
		parser.add_argument("--format", default="pdf")
		
	def processExperiment(self, args, experiment, results, output):
		# Switch the backend depending on desired format
		format = args.format
		
		'''
		if format == "pdf":
			pyplot.switch_backend("PDF")
		elif format == "svg":
			pyplot.switch_backend("Cairo")
		elif format == "png":
			pyplot.switch_backend("AGG")
		'''
		plotter = GCPlotter(xsize=30, ysize=50)
		
		# Power
		print 'Power...'
		plot = GCPowerPlot(results, title="Power")
		plotter.addPlot(plot)
		
		plot = GCMultiCommonPlot(results, ["actual_workload", "target_workload"], title="Workload", ylim=(0,9000), ylabel="op/s")
		plotter.addPlot(plot)
		
		print 'Latency...'
		plot = GCMultiCommonPlot(results, ["readlatency_99_window", "writelatency_99_window"], ylim=(0, 250), indexLine=75, title="Window Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
		plotter.addPlot(plot)
		
		plot = GCMultiCommonPlot(results, ["readlatency_99_cum", "writelatency_99_cum"], ylim=(0, 200), indexLine=75, title="Cumulative Latency", ylabel="Response time (ms)") # We add the 75 ms SLA
		plotter.addPlot(plot)
			
		plot = GCSLAOverlayPlot(results, 75)
		plotter.addPlot(plot)
		
		print 'Nodes...'
		plot = GCNodeStatePlot(results)
		plotter.addPlot(plot)
		
		plot = GCNodeStatePlotNew(results)
		plotter.addPlot(plot)

		print 'Plotting...'
		output_file = "%s.%s" % (output, format)
		plotter.doPlot(output=output_file, format=format)
		print "Plot completed, output file: %s" % output_file
		
if __name__ == "__main__":
	plotter = GCPlotExperiment()
	plotter.run()