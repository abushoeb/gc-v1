#!/usr/bin/python
import sys

if __name__ == "__main__":
	
	filename = sys.argv[1]
	data = []
	with open(filename, "r") as f:
		for line in f:
			line = line.strip()
			nodes, load, latency = line.split("\t")
			nodes = int(nodes)
			load = float(load)
			latency = float(latency)
			data.append((nodes, load, latency))
	
	
	for i in xrange(1, len(data)):
		prevNodes, prevLoad, prevLatency = data[i-1]
		nodes, load, latency = data[i]
		
		loadDiff = load - prevLoad
		latencyDiff = latency - prevLatency
		
		slope = latencyDiff / loadDiff
		
		#data[i] = (nodes, load, latency, slope)
		print nodes, load, latency, slope
	