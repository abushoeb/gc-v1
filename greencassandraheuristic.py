#!/usr/bin/env python2.7


try:
	#	from gurobipy import *
	import datetime;
	import sys
	from gccommon import *
	from gchelpers import *

except Exception, e:
	print e

import math

GREENBG = '\033[42m'
BLUEBG = '\033[44m'
REDBG = '\033[41m'
ENDC = '\033[0m'

class NodeState:
	OFF = 0
	TRANSITION = 1
	ON = 2

class GCMode:
	NOOP = 1
	LOADONLY = 2
	FULL = 4	
		
class GreenCassandraHeuristic:
	MAX_HOUR = 36 		# of hours to optimize for
	SLOT_LENGTH = 15 	# length of slot in minutes
	NUM_NODES = 0		# number of nodes to optimize for
	MIN_NODES = 0		# number of nodes in the minimum set
	NODE_POWER = 30		# node power in watts
	DEADLINE_LEN = 4*24	# number of slots for deadline

	offInfo = None
	
	def __init__(self, perf, energy, nodes, nodeStates, offInfo, transInfo, mode=GCMode.NOOP):	
		self.mode = mode
		
		# Space
		self.nodes = []
		self.NUM_NODES = len(nodes)
		
		for i in xrange(0, len(nodes)):
			self.nodes.append(nodes[i].name)
			
		## populate the node list
		#for i in xrange(0, self.NUM_NODES):
		#	node = "sol%03d" % i
		#	print node
		#	self.nodes.append(node)
		
		# set up the range of timeslots
		trange = xrange(0, (self.MAX_HOUR * 60)/self.SLOT_LENGTH)
		self.range = list(trange)
		
		# set up the array for node state
		self.nodeState = {}
		self.nodePower = {}
				
		# we assume that all nodes are off at the start
		for n in self.nodes:
			for t in self.range:
				self.nodeState[n, t] = NodeState.OFF
				
		# turn on the minimal set for the entire algorithm
		# since we never turn nodes off, this will stay on
		self.minimum_nodes = []
		for n in nodes:
			if n.region == GCNodeRegion.MINIMUM:
				self.MIN_NODES += 1
				self.minimum_nodes.append(n.name)
				for t in self.range:
					self.nodeState[n.name, t] = NodeState.ON
		
		#print nodeStates
		for n in self.nodes:
			self.nodeState[n, 0] = self.gcNodeStateToNodeState(nodeStates[n])
		
		# set transitions already in motion
		for n in self.nodes:
			if transInfo[n] > 0:
				for i in xrange(1, transInfo[n]+1):
					self.nodeState[n, i] = NodeState.TRANSITION
				#self.nodeState[n, i+1] = NodeState.ON
		
		#GC 
		self.offInfo = offInfo
		
		# How much power each node takes. Assume the same for all nodes
		self.nodePower = {} # node -> power: self.power['sol000'] = 30
		for node in self.nodes:
			self.nodePower[node] = self.NODE_POWER
		
		# these are passed at instantiation
		# perf is the performance requirement at time t
		self.perf = perf
		# green is the green energy availability at time t
		self.green = energy
		
	def gcNodeStateToNodeState(self, state):
		if state == GCNodeState.ON:
			return NodeState.ON
		elif state == GCNodeState.TRANSITION:
			return NodeState.TRANSITION
		elif state == GCNodeState.OFF:
			return NodeState.OFF
		
	# this will return the number of slots required to turn a node on
	# and bring it to consistenct at time t.
	def slotsToConsistency(self, n, t):
		lastOfftime = self.nodeLastOfftime(n,t)
		lastOntime = self.nodeLastOntime(n,t)
		lastTransitiontime = self.nodeLastTransitiontime(n,t)
		#print "slots to consistency: t %d lastoff %d, laston %d, lasttransition %d" % (t, lastOfftime, lastOntime, lastTransitiontime)
		if lastOntime > lastOfftime and lastTransitiontime > lastOfftime and self.nodeState[n,t] != NodeState.OFF:
			return 0
		
		# TODO: check this off by one
		offSlots = t - lastOntime
		slots = int(math.ceil(float(offSlots) / 1.0)) + 1
		#print "...slots %d" % slots
		#if n == "sol015": raw_input("continue?")
		return slots
	
	def tPrimeRange(self, t):
		return xrange(t, len(self.range))
	
	def numNodesInState(self, state, t):
		count = 0
		for node in self.nodes:
#			print "%s:%d" % (node, self.nodeState[node, t]),
			if self.nodeState[node, t] == state:
				count += 1
#				print "++" ,

#			print "\n",
		return count

	def nodesInState(self, state, t):
		nodes = []
		for node in self.nodes:
			if self.nodeState[node, t] == state:
				nodes.append(node)
		return nodes
	
	def nodeLastOfftime(self, n, t):
		foundOff = False

		for tp in reversed(xrange(0, t)):
			if self.nodeState[n, tp] == NodeState.OFF:
				foundOff = True
				continue
			elif foundOff and (self.nodeState[n, tp] == NodeState.ON or self.nodeState[n,tp] == NodeState.TRANSITION):
				return tp+1
			
		# if we got here, we were off at t=0, so we go to offtime list
		# GC
		return -(self.offInfo[n])

	def nodeLastOntime(self, n, t):
		for tp in reversed(xrange(0, t)):
			if self.nodeState[n, tp] == NodeState.ON:
				return tp
			
		# if we got here, we were off at t=0, so we go to offtime list
		# GC
		return -(self.offInfo[n])-1	
	
	def nodeLastTransitiontime(self, n, t):
		for tp in reversed(xrange(0, t)):
			if self.nodeState[n, tp] == NodeState.TRANSITION:
				return tp
			
		return 0
	
	# possibilities
	# 111122220
	# 222200000
	# 001111110
	
	def nodeDeadline(self, n, t):
		if t > 1 and self.nodeState[n,t-1] != NodeState.OFF:
			return (t-1) + self.DEADLINE_LEN
		
		dl = self.nodeLastOfftime(n, t) + self.DEADLINE_LEN
		#print "Deadline: %d" % dl
		return dl

	def sortedByDeadline(self, nodes, t):
		tmp = []
		sorted = []

		for n in nodes:
			tmp.append(n)
			#print "%s: %d" % (n, self.nodeDeadline(n,t))

		while len(tmp) > 0:
			node = None
			deadline = sys.maxint
			for n in tmp:
				dl = self.nodeDeadline(n, t)
				if  dl < deadline:
					node = n
					deadline = dl

			sorted.append(node)
			tmp.remove(node)

#		for n in sorted:
			#print "%s: %d" % (n, self.nodeDeadline(n,t))
			
		return sorted
	
	def sortedByEarliestOntime(self, nodes, t):
		tmp = []
		sorted = []
		
		for n in nodes:
			tmp.append(n)
			
		while len(tmp) > 0:
			node = None
			ontime = -(sys.maxint-1)
			for n in tmp:
				ot = self.nodeLastOntime(n, t)
				if ot >= ontime:
					node = n
					ontime = ot
			
			sorted.append(node)
			tmp.remove(node)
			
		return sorted
	
	# turn on a node n at time t and work backwards to set transition slots
	def nodeOnWithRetro(self, n, t):
		# work backwards to place transition states
		# this is tricky because we need to be ON by t
		# but when we start turning on needs to be sooner
		# and we can't figure the slots we would need if we
		# turned ON at t, so we need to find the right delta-t < t
		# to start turning on such that we are ready at t
		#print "start loop, start t=%d" % t
		#print "last ontime = %d" % self.nodeLastOntime(n,t)
		for tp in reversed(xrange(self.nodeLastOntime(n, t), t)):
			if (tp + self.slotsToConsistency(n, tp)) <= t:
				break
				
		#print "found tp=%d" % tp
		if tp < 1:
			return False
			
		#if n == "sol015": print "onwithretro"
		self.nodeState[n, t] = NodeState.ON
#		print "Turned on %s at %d" % (n, t)	
			
		slots = self.slotsToConsistency(n, tp)
		#print "slots required=%d" % slots
				
		backsetRange = reversed(xrange((t - slots), t))
		for tpp in backsetRange:
			#print "tpp: %d" % tpp
			#assert tpp >=0
			assert tpp >= self.nodeLastOfftime(n, t)
			assert self.nodeState[n, tpp] == NodeState.OFF

			if tpp >= 1 and self.nodeState[n,tpp-1] == NodeState.ON:
				self.nodeState[n,tpp] = NodeState.ON
			else:
				self.nodeState[n, tpp] = NodeState.TRANSITION
		
		return True

	# turn on a node n at time t and work backwards to set transition slots
	def nodeOnWithRetroBestEffort(self, n, t):
		# if we can't do full node on with retro...make a best effort
		# attempt to get it on ASAP
		
		for tp in reversed(xrange(self.nodeLastOntime(n, t), t)):
			if (tp + self.slotsToConsistency(n, tp)) <= t:
				break
				
		#print "found tp=%d" % tp
		if tp < 1:
			tp = 1
			
		slots = self.slotsToConsistency(n, tp)

		setRange = xrange(tp, tp+slots)
		for tpp in setRange:
			assert tpp >= self.nodeLastOfftime(n, t)
			assert self.nodeState[n, tpp] == NodeState.OFF
			self.nodeState[n, tpp] = NodeState.TRANSITION
		
		self.nodeState[n,tp+slots] = NodeState.ON
		
		return True
		
	def getNodeStatesAtT(self, t):
		nodeStates = {}
		
		for n in self.nodes:
			nodeState = None
			if self.nodeState[n,t] == NodeState.ON:
				nodeState = GCNodeState.ON
			elif self.nodeState[n,t] == NodeState.TRANSITION:
				nodeState = GCNodeState.TRANSITION
			elif self.nodeState[n,t] == NodeState.OFF:
				nodeState = GCNodeState.OFF
			assert nodeState != None
			nodeStates[n] = nodeState
		
		return nodeStates

	# run over *hours* timeslots
	def simulate(self, hours):
		if self.mode == GCMode.NOOP:
			for node in self.nodes:
				self.nodeState[node, 1] = NodeState.ON
			return
		
		#self.output(hours)
		if self.mode == GCMode.LOADONLY or self.mode == GCMode.FULL:
			for t in xrange(1, 4*hours):
				self.load(t)
			#self.output(hours)
			#raw_input()
		
		#self.output(hours)
		if self.mode == GCMode.FULL:	
			for t in xrange(1, 4*hours):
				self.energy(t)
			#self.output(hours)
			#self.output(hours)
			#raw_input("continue?")
		#for t in xrange(0, 4*hours):
		#	self.off(t)
		
			self.deadlines()
		#self.output(hours)
	
	def load(self, tp):	
			# copy the state from the previous timeslot
			for n in self.nodes:
				self.nodeState[n, tp] = self.nodeState[n,tp-1]

			# See how many nodes (if any) we are short to meet performance requirements
			nodesNeeded = self.perf[tp] - self.numNodesInState(NodeState.ON, tp)
			#print "needed: " + str(nodesNeeded)
			#print "Nodes Required: %d, Nodes ON %d, Nodes OFF %d, Nodes TRANSITION %d" % (self.perf[tp], self.numNodesInState(NodeState.ON, tp), self.numNodesInState(NodeState.OFF, tp), self.numNodesInState(NodeState.TRANSITION, tp))
			
			if nodesNeeded == 0:
				return
			elif nodesNeeded < 0 and self.MIN_NODES:
				nodesToConsider = self.sortedByEarliestOntime(self.nodesInState(NodeState.ON, tp), tp)
				offCandidates = []
				
				for n in nodesToConsider:
					if n not in self.minimum_nodes:
						offCandidates.append(n)
					
				while nodesNeeded < 0 and len(offCandidates) > 0:
					for n in offCandidates:
						self.nodeState[n, tp] = NodeState.OFF
						nodesNeeded += 1
						offCandidates.remove(n)
						break
				return
			
			while nodesNeeded > 0:
				done = False
				#print "NodesNeeded right now: %d" % nodesNeeded
				# elementary case
				# t = 0 or 1: just turn on the node with no side effects, and burn the first timeslot if necessary
				
				# first pass
				# find nodes that are already on in t-1, and let them still be in in t

				# second pass
				# find the node with the earliest deadline AND
				#	that can be brought up to date in time to meet the requirement
				
				# third pass
				# if we exhaust all nodes in the previous category, consider to eliminate a
				# 	a previous decision to turn a node off
				# 	pick the node with the shortest off period

				# elementary				
				#print "Nodes OFF: %d" % len(self.nodesInState(NodeState.OFF, tp))
				
				# if this is the case, all nodes are already trying to come on,
				# we are just missing the turn on requirement, and can't do anything else.
				if len(self.nodesInState(NodeState.OFF, tp)) <= 0:
					return
				
				offNodes = []
#				for n in self.nodesInState(NodeState.OFF, tp):
#					offNodes.append(n)

				#print "NodesOFF right now: %d" % len(self.nodesInState(NodeState.OFF, tp))
				if tp == 0:
					for n in self.nodesInState(NodeState.OFF, tp):
						#print "%s on in tp=0" % n
						self.nodeState[n,tp] = NodeState.ON
						nodesNeeded -= 1
						done = True
						break
				if done: continue

#				if tp == 1:
#					for n in self.nodesInState(NodeState.OFF, tp):
#						if self.nodeState[n, tp-1] == NodeState.OFF:
#							self.nodeState[n, tp-1] = NodeState.ON
#						#print "%s on in tp=1" % n
#						self.nodeState[n, tp] = NodeState.ON
#						nodesNeeded -= 1
#						done = True
#						break

				if done: continue

				#print "got here and tp = %d" % tp
				
				# we shouldn't be here unless t >= 2
				#assert tp >= 2

				# first pass
				for n in self.nodesInState(NodeState.OFF, tp):
					#print "trying first pass"
					#if a node is already on or in the last slot of a transition (e.g. from the energy optimization), we can turn it on
					if self.nodeState[n, tp - 1] == NodeState.ON or self.nodeState[n, tp-1] == NodeState.TRANSITION:
						self.nodeState[n, tp] = NodeState.ON
						nodesNeeded -= 1
						done = True
						break
				if done: continue


				# this case only comes up if we mispredicted earlier
				#print "got here"
				#nodesOnOrTrans = len(self.nodesInState(NodeState.ON, tp)) + len(self.nodesInState(NodeState.TRANSITION, tp))
				#if nodesOnOrTrans >= nodesNeeded:
				nodesTrans = len(self.nodesInState(NodeState.TRANSITION, tp))
				if nodesNeeded - nodesTrans <= 0:
					#print "can't do any better right now...we screwed the pooch earlier..."
					break

				# second
				for n in self.sortedByDeadline(self.nodesInState(NodeState.OFF, tp), tp):
					#print "on with retro at %d" % tp
					if self.nodeOnWithRetro(n, tp) == True:
						nodesNeeded -= 1
						done = True
						break
				
				if done: continue

						
				for n in self.sortedByDeadline(self.nodesInState(NodeState.OFF, tp), tp):
					#print "on with no retro at %d" % tp
					self.nodeOnWithRetroBestEffort(n,tp)
					nodesNeeded -= 1
					done = True
					break

	def energy(self, tp):
			greenTarget = min(self.NUM_NODES, self.green[tp])
			#wtf?????
			# I think this makes more sense now...
			greenTarget = math.floor(self.green[tp] / self.NODE_POWER)
			
			nodesNeeded = greenTarget - len(self.nodesInState(NodeState.ON, tp)) - len(self.nodesInState(NodeState.TRANSITION, tp))
			#print "self.green[%d] = %d" % (tp, self.green[tp])
			#print "greenTarget[%d] = %d, nodes on %d, nodes transition %d, nodesneeded %d" % (tp, greenTarget, len(self.nodesInState(NodeState.ON, tp)) , len(self.nodesInState(NodeState.TRANSITION, tp)), nodesNeeded)
			#if tp == 60:
			#	raw_input()
				
			# here we try to fill in the green energy by bringing as many nodes as we can up to date.
			# if there are no nodes that are NOT up to date, turn on nodes into normal serving mode

			while nodesNeeded > 0:
				done = False
				#print "Noded needed: %d" % nodesNeeded
				#print "before loop"
				for n in self.nodesInState(NodeState.OFF, tp):
					if tp > 0:
						if self.nodeState[n, tp-1] == NodeState.TRANSITION or self.nodeState[n,tp-1] == NodeState.ON:
							self.nodeState[n,tp] = NodeState.ON
							nodesNeeded -= 1
							done = True
							break
				if done: continue			
				
				for n in self.sortedByDeadline(self.nodesInState(NodeState.OFF, tp), tp):
					slots = self.slotsToConsistency(n, tp)
					#print "node %s, tp %d, slots required: %d, nodeDeadline %d" % (n, tp, slots, self.nodeDeadline(n,tp))
					# if we encounter a slots == 0, we can move to the next block, as there are no nodes needing to be updated
					# remember that we are already sorted by deadline
					if slots == 0:
						break
					# otherwise turn on the node
					for tpp in xrange(tp, tp+slots):
						self.nodeState[n,tpp] = NodeState.TRANSITION
					
					# weird artifact happens, if we turn on before a later scheduled turn on, we might have extra 1s
					# overwrite them
					# GC new
					#self.output(24)
					'''
					for tppp in self.tPrimeRange(tpp+1):
						print "tppp %d" % tppp
						if self.nodeState[n,tppp] == NodeState.TRANSITION:
							self.nodeState[n,tppp] = NodeState.OFF
							continue
						
						if self.nodeState[n,tppp] == NodeState.ON:
							self.nodeState[n,tppp] = NodeState.OFF
							self.nodeOnWithRetro(n, tppp)
							break
						#raw_input()
						#self.output(24)
					'''

					nodesNeeded -= 1
					done = True
					break					
				if done: continue
				
				# if we get here, we didn't turn on any nodes, so pretend we did so we don't infinite loop
				nodesNeeded -= 1


	def off(self, t):
		# for each tp after t
		for tp in self.tPrimeRange(t):
			excessNodes = self.numNodesInState(NodeState.ON, tp) - self.perf[tp]
			while excessNodes > 0:
				for n in self.nodesInState(NodeState.ON, tp):
					self.nodeState[n, tp] = NodeState.OFF
					excessNodes -= 1
					#print "turned one off"
					break

	def deadlines(self):
		violations = 0
		for tp in self.tPrimeRange(0):
			for n in self.nodesInState(NodeState.OFF, tp):
#				print "deadline for %s at %d = %d" %(n, tp, self.nodeDeadline(n,tp))
				nodeDeadline = self.nodeDeadline(n, tp)
				#print "Deadline %d" % nodeDeadline
				if nodeDeadline <= tp:
					print "Detected deadline violation for %s @ t=%d, fixing..." % (n, tp)
					slots = self.slotsToConsistency(n, tp)
					for i in xrange(tp-slots, tp):
						print "at tp=%d, state=%d" % (i, self.nodeState[n, i])
					self.nodeOnWithRetro(n, nodeDeadline)

	def output(self, hours):
		slots = hours * (60/self.SLOT_LENGTH)

		# Nodes up
		self.numOnNodes = {}
		self.numTransitionNodes = {}

		for t in range(0, slots):
			self.numOnNodes[t] = 0
			self.numTransitionNodes[t] = 0
			for n in self.nodes:
				self.numOnNodes[t] += 1 if self.nodeState[n,t] == NodeState.ON else 0
				self.numTransitionNodes[t] += 1 if self.nodeState[n,t] == NodeState.TRANSITION else 0

		print 'On Together'
		for n in range(0, len(self.nodes)):
			out = ' '
			for t in range(0, slots):
#				out += ' ' if n < len(self.nodes) - self.numOnNodes[t] else (BLUEBG+' '+ENDC)

				if n < len(self.nodes) - self.numOnNodes[t] - self.numTransitionNodes[t]:
					out += ' '
				elif n < len(self.nodes) - self.numOnNodes[t]:
					out += (REDBG+' '+ENDC)
				else:
					out += (BLUEBG+' '+ENDC)
				
			print '      ', out


		print 'On serving'
		for n in range(0, len(self.nodes)):
			out = ' '
			for t in range(0, slots):
				out += ' ' if n < len(self.nodes) - self.numOnNodes[t] else (BLUEBG+' '+ENDC)
			print '      ', out

		print 'On in transition'
		for n in range(0, len(self.nodes)):
			out = ' '
			for t in range(0, slots):
				out += ' ' if n < len(self.nodes) - self.numTransitionNodes[t] else (REDBG+' '+ENDC)
			print '      ', out

		# Green
		print 'Green'
		for n in range(0, self.NUM_NODES):
			out = ' '
			for t in range(0, slots):
				if self.NUM_NODES-n < self.green[t]/self.NODE_POWER:
					out += GREENBG+' '+ENDC
				else:
					out += ' '
			print '      ', out
		# Performance
		print 'Min performance'
		for n in range(0, self.NUM_NODES):
			out = ' '
			for t in range(0, slots):
				if self.NUM_NODES-n <= math.ceil(self.perf[t]):
					out += BLUEBG+' '+ENDC
				else:
					out += ' '
			print '      ', out

		# Node states
		print 'Node States'
		out = ""
		for n in self.nodes:
			out += "%s: " %n
			for t in range(0, slots):
				out += "%d" % self.nodeState[n,t]
			out += "\n"
		print out

