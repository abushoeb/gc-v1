#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *
from gcmodel import *
from gcanalysis import *

from datetime import datetime
from datetime import timedelta

class GCDataExport(GCExperimentProcessor):

	#reqs = ["brown_consumed", "solar_consumed", "used_solar", "readlatency_99_window", "readlatency_99_cum", "target_workload", "any_transitions", "corrected_cum_latency"]
	reqs = ["brown_consumed", "solar_consumed", "used_solar", "readlatency_99_window", "corrected_cum_latency", "target_workload", "any_transitions"]

	def __init__(self):
		super(GCDataExport, self).__init__(self.reqs)
		
		parser = self.getParser()
		# no extra parameters
		
	def processExperiment(self, args, experiment, results, output):
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)
		
		f = open("%s.export" % output, "w")
		string = "#Time"
		for value in self.reqs:
			string += "\t%s" % value
		string += "\n"
		f.write(string)
		
		for i in xrange(0, max_i):
			data = {}
			
			for value in self.reqs:
				dt = timestamps[i]
				measurement = results.getCommonValue(dt, value)
				data[value] = measurement
			
			string = "%s" % datetime.strftime(dt, "%m-%d-%H:%M:%S")
			for value in self.reqs:
				string += "\t%0.2f" % data[value]
			
			string += "\n"
			f.write(string)
			
		f.close()
		
if __name__ == "__main__":
	exporter = GCDataExport()
	exporter.run()