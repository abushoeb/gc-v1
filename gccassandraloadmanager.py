#!/usr/bin/python

import argparse
import thread

from gccommon import *
from gcbase import GCManagerBase
from gclogger import GCLogger
from datetime import datetime
from pyjolokia import Jolokia
from pyjolokia import JolokiaError
from threading import Thread

JOLOKIA_URL="http://%s:8778/jolokia/"

def mbean_uniform(mbean):
	prefix, rest=  mbean.split(":")
	restTokens = rest.split(",")
	restTokens = sorted(restTokens)
	
	restSorted = ",".join(restTokens)
	
	output = "%s:%s" % (prefix, restSorted)
	return output

	
class GCCassandraLoadManager(GCManagerBase):
	
	VALUE_ABSOLUTE = 1
	VALUE_DIFF = 2
	
	values = [
		("org.apache.cassandra.db:type=StorageProxy", "ReadTrackerRecent", "read_tracker_recent"),
		("org.apache.cassandra.db:type=StorageProxy", "ReadTrackerTotals", "read_tracker_total"),
		("org.apache.cassandra.db:type=StorageProxy", "WriteTrackerRecent", "write_tracker_recent"),
		("org.apache.cassandra.db:type=StorageProxy", "WriteTrackerTotals", "write_tracker_total"),
		("org.apache.cassandra.db:type=StorageProxy", "GreenHintTrackerRecent", "greenhint_tracker_recent"),
		("org.apache.cassandra.db:type=StorageProxy", "GreenHintTrackerTotals", "greenhint_tracker_total"),
	]

	def __init__(self, nodes=(MINIMUM_NODES+OPTIONAL_NODES)):
		self.nodes = nodes
		
		#print "Using nodes = ", self.nodes
		
		self.default = {}
		for node in MINIMUM_NODES+OPTIONAL_NODES:
			self.default[node] = 0
			
		self.results = {}
		
		self.logger = GCLogger.getInstance().get_logger(self)
		
	def getJolokiaConn(self, host):
		url = JOLOKIA_URL % host
		jolokia_conn = Jolokia(url, timeout=3)
		for value in self.values:
			mbean, attribute, description = value
			jolokia_conn.add_request(type="read", mbean=mbean, attribute=attribute)
		
		return jolokia_conn
	
	def getNodeValuesThread(self, node):
		jolokia_conn = self.getJolokiaConn(node)	
		data = None
		values = {}
		
		hasGoodData = False
		
		while not hasGoodData:
			
			for i in xrange(0, 2):
				try:
					data = jolokia_conn.getRequests()
					break
				except JolokiaError:
					#print "Jolokia error for %s" % node
					continue
				except:
					print "Random error getting jolokia data for %s, trying again..." % node
					continue
			
			if data != None:
				for entry in data:
					try:
						mbean = mbean_uniform(entry["request"]["mbean"])
						attribute = entry["request"]["attribute"]
						if entry["value"] == {}:
							value = self.default
						else:
							'''
							# ensure that we have a value for all nodes
							# otherwise throw the exception
							for n in OPTIONAL_NODES:
								r = entry["value"][n]
							'''	
							value = entry["value"]
					except KeyError:
						value = self.default
					
					values[mbean, attribute] = value
				break
			else:
				values = None
				break
				
		self.results[node] = values
		thread.exit()
	
	def getValuesForLogging(self):
		values = {}
		threads = []
		
		for node in self.nodes:
			t = Thread(target=self.getNodeValuesThread, args=(node,))
			t.start()
			threads.append(t)


		for t in threads:
			t.join()
		
		now = datetime.now()
		
		for value in self.values:
			mbean, attribute, description = value
			mbean = mbean_uniform(mbean)
			
			data = {}

			for sampling_node in self.nodes:			
				node_results = self.results[sampling_node]
				if node_results == None:
					current_value = self.default
				else:
					current_value = node_results[mbean, attribute]
				
				#print sampling_node
				#print current_value
				
				for node in MINIMUM_NODES + OPTIONAL_NODES:
					try:
						x = data[node]
					except KeyError:
						data[node] = 0
						
					try:
						data[node] += current_value[node]
					except KeyError:
						pass
			
			for node in data.keys():
				key = "%s.%s" % (description, node)
				values[key] = data[node]
			
		return values
	
	def resetTrackers(self):
		for node in self.nodes:
			jolokia_conn = self.getJolokiaConn(node)
			try:
				jolokia_conn.request(type="exec", mbean="org.apache.cassandra.db:type=StorageProxy", operation="resetAllTrackers")
				self.logger.info("reset tracker for %s" % node)
			except Exception as e:
				self.logger.info("error resetting measurements for %s" % node)
				self.logger.info("Exception: %s" % e)
		
if __name__ == "__main__":
	
	if len(sys.argv) == 1:
		hosts = MINIMUM_NODES + OPTIONAL_NODES
	
	if len(sys.argv) > 1:
		host = sys.argv[1]
		hosts = [host]
	if len(sys.argv) > 2:
		host = sys.argv[2]
		hosts = hosts + [host]
	
	from gclogger import GCNullLogger
	logger = GCNullLogger()
	
	manager = GCCassandraLoadManager(nodes=hosts)
	
	values = manager.getValuesForLogging()
	#time.sleep(5)
	#values = manager.getValuesForLogging()
	
	for value in sorted(values.keys()):
		print "values[%s] = %f" % (value, values[value])
		
	#print ""
	
	#values = manager.getValuesForLogging()
	#for value in sorted(values.keys()):
	#	print "values[%s] = %f" % (value, values[value])
	
		