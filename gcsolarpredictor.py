#!/usr/bin/python

import argparse
import os
import sys
import time
import paramiko
import socket
from gccommon import *
from gchelpers import interpolate_dt
from gcutil import dtiter
from gcutil import dtrange
from gcbase import GCSolarPredictorBase
from gclogger import GCLogger
from gclogger import GCNullLogger
from datetime import datetime
from datetime import timedelta
import math
import tarfile
import matplotlib.pyplot as plot
from threading import Thread
from multiprocessing import Process
from matplotlib.dates import date2num
from bisect import *
from gchelpers import find_le
from gchelpers import find_lt
from gchelpers import find_ge
from gchelpers import find_gt
import matplotlib.dates
from experimentclock import ExperimentClock
from experimentclock import ExperimentClockFake

SLOT_LENGTH = 15

class GCTraceSolarPredictor(GCSolarPredictorBase):
	green_data = None
	start_dt = None
	
	def __init__(self, energy_file, start_dt, scale=0.58, accel=1.0):
		# use midnight on the day specified to load
		predictor_dt = datetime(year=start_dt.year, month=start_dt.month, day=start_dt.day)
		self.start_dt = predictor_dt
		self.accel = float(accel)
		self.scale = scale
		self.green_data = self.loadGreen(energy_file)
		
		for line in self.green_data:
			dt, value = line
			#print dt.strftime("%Y-%m-%d %H:%M:%S"), dt.tzinfo, value

	def getSolarAt(self, dt):
		for i in xrange(0, len(self.green_data)):
			data_dt, data_item = self.green_data[i]
			next_data_dt, next_data_item = self.green_data[i+1]
			
			#print dt, data_dt, next_data_dt
			if dt >= data_dt and dt < next_data_dt:
				data = data_dt, data_item
				next_data = next_data_dt, next_data_item
				return interpolate(data, next_data, dt) * self.scale
	
	def loadGreen(self, filename):
		energy = {}
		time_counter = 0
		f = open(filename, 'r')
		green_data = f.read().replace("\"", "")
		lines = green_data.split("\n")
		dt_incr = timedelta(minutes=SLOT_LENGTH)
		counter = 0
		green_tuples = []
		
		for i in xrange(0, 7):
			#dt_counter = datetime.strptime("2012-12-04T00:00:00", "%Y-%m-%dT%H:%M:%S")
			#start = None
			for line in lines:
				counter += 1
				if line == "":
					continue
				fields = line.split(",")
				dt = datetime.strptime(fields[0], "%Y-%m-%dT%H:%M:%S-05:00")
				dt = dt - timedelta(hours=1)
				#dt = dt + timedelta(hours=5)
				new_dt = self.start_dt + timedelta(days=i, hours=dt.hour, minutes=dt.minute, seconds=dt.second)
				
				#if start == None:
				#	start = new_dt
				
			#	delta = new_dt - start
			#	new_dt = start + (delta * int(self.accel))
				
				#print dt, dt_counter, dt_counter+dt_incr
				#if dt >= dt_counter and dt < (dt_counter + dt_incr):
				#	energy[time_counter] = float(fields[1]) * 2.0
				#	dt_counter += dt_incr
				#	time_counter += 1gcworkloadpredictor.py

				green_tuples.append((new_dt, float(fields[1]) * 2.0))
		#for entry in energy:
		#   print entry, energy[entry]

		return green_tuples

ARCHIVE_PATH = "/home/goiri/bak/historic2/Parasol"
INTERNAL_PATH = "scratch/historic2/Parasol"
TMP_PATH = "/tmp/solarpredictor"

SUNNY_BOYS = ["SB2120075882", "SB2120071645"]

class GCHistoricalSolarPredictor(GCSolarPredictorBase):
	
	loadedData = {}
	preppedData = None
	
	# Preheat the predictor with some dates
	def __init__(self, dts=None, scale=0.58, experiment_dt=None, solar_dt=None):
		
		if experiment_dt != None and solar_dt != None:
			self.shift = datetime(month=solar_dt.month, day=solar_dt.day, year=solar_dt.year) - datetime(month=experiment_dt.month, day=experiment_dt.day, year=experiment_dt.year)
		else:
			self.shift = timedelta()
		
		self.logger = GCLogger.getInstance().get_logger(self)
		
		if dts:
			self.logger.info("Preheating GCHistoricalSolarPredictor...")
			for dt in dts:
				self.load_data_for_dt(dt)
				
		self.scale = scale
		self.cachedData = {}
	
	def getSolarAt(self, dt):
		dt = dt + self.shift
		
		data = self.prep_data(dt)
		values = {}
		
		for sb in SUNNY_BOYS:
			'''
			prev = None
			#for item in self.loadedData[path_ext][sb]:
			for item in data[sb]:				
				if prev == None:
					prev = item
					continue
				prev_dt, prev_value = prev
				item_dt, item_value = item
				
				if dt == prev_dt:
					values[sb] = prev_value
					break
				elif dt == item_dt:
					values[sb] = item_value	
					break
				elif dt >= prev_dt and dt <= item_dt:
					inter = interpolate(prev, item, dt)
					values[sb] = inter
					break
				#else:
					#print "didn't match"
					#print dt, prev_dt, item_dt
					#values[sb] = 0
					
				prev = item
			
			'''
			l = data[sb]
			
			# if we find an exact match
			i = bisect_left(l, (dt,))
			if i != len(l) and l[i][0] == dt:
				#print "exact match"
				values[sb] = l[i][1]
				continue
			# otherwise
			left = None
			right = None
			i = bisect_left(l, (dt,))
			if i:
				left = l[i-1]
			
			i = bisect_right(l, (dt,))
			if i != len(l):
				right = l[i]
			
			if left == None and right != None:
				print "left is none"
				values[sb] = right[1]
				continue
			elif right == None and left != None:
				print "right is none"
				values[sb] = left[1]
				continue
			elif left != None and right != None:
				#print "interpolating"
				#print "interpolating", left, right
				inter = interpolate_dt(left, right, dt)
				values[sb] = inter
				continue
			else:
				values[sb] = 0
				continue
			
		value = values[SUNNY_BOYS[0]]*2 # + values[SUNNY_BOYS[1]]
		
		return value * self.scale
		
	def dt_set(self, dt):
		#return [dt]
		return [dt-timedelta(days=1), dt, dt+timedelta(days=1)]
		
	def prep_data(self, dt):
		key_dt = datetime(month=dt.month, day=dt.day, year=dt.year)
		
		try:
			data = self.cachedData[key_dt]
		except KeyError:
			self.load_data_for_dt(dt)
			path_exts = self.get_path_exts(dt)		
			data = {}
			for sb in SUNNY_BOYS:
				data[sb] = []
			
			for sb in SUNNY_BOYS:
				for path_ext in path_exts:
					for item in self.loadedData[path_ext][sb]:
						data[sb].append(item)
			
			self.cachedData[key_dt] = data
			
		return data
	
	def load_data_for_dt(self, dt):
		path_exts = self.get_path_exts(dt)
		threads = []
		for path_ext in path_exts:
			t = Thread(target=self.load_data, args=(path_ext,))
			t.daemon = True
			#self.load_data(path_ext)
			threads.append(t)
			t.start()
		
		for t in threads:
			t.join()
			
	def get_path_exts(self, dt):
		path_exts = []
		for this_dt in self.dt_set(dt):
			path_ext = "%04d/%02d/%02d" % (this_dt.year, this_dt.month, this_dt.day)
			path_exts.append(path_ext)
		return path_exts
	
	def load_data(self, path_ext):
		if path_ext in self.loadedData.keys():
			return

		self.loadedData[path_ext] = {}
		
		#print "Loading data for " + path_ext
		if not self.load_from_tmp(path_ext):
			self.load_from_tar(path_ext)

		self.logger.info("Done loading data for %s" % path_ext)

	def load_from_tar(self, path_ext):
		tar_path = "%s/%s.tgz" % (ARCHIVE_PATH, path_ext)
		tar = tarfile.open(tar_path)
		
		for sb in SUNNY_BOYS:
			self.loadedData[path_ext][sb] = []
			internal_dirs = "%s/%s/%s" % (INTERNAL_PATH, path_ext, sb)
			internal_path = "%s/powerOut.rrd" % (internal_dirs)
			contents = tar.extractfile(internal_path).read()
			
			# we got the file from the tar
			# lets cache it in tmp in case we need it again soon
			tmp_dirs = "%s/%s" % (TMP_PATH, internal_dirs)
			tmp_path = "%s/%s" % (TMP_PATH, internal_path)
			
			if not os.path.exists(tmp_dirs):
				os.makedirs(tmp_dirs)
				
			with open(tmp_path, "w") as f:
				f.write(contents)
			
			#print "tar extracted for ", sb
			for line in contents.strip().split("\n"):
				ts, val = line.split(":")
				dt = datetime.fromtimestamp(int(ts))
				value = int(val)
				#print "loaded solar for", dt, value
				self.loadedData[path_ext][sb].append((dt, value))
		
		self.logger.info("Loaded from tgz")
		
	def load_from_tmp(self, path_ext):
		for sb in SUNNY_BOYS:
			self.loadedData[path_ext][sb] = []
			internal_dirs = "%s/%s/%s" % (INTERNAL_PATH, path_ext, sb)
			internal_path = "%s/powerOut.rrd" % (internal_dirs)
			tmp_path = "%s/%s" % (TMP_PATH, internal_path)

			try:
				with open(tmp_path, "r") as f:
					contents = f.read()
			
				for line in contents.strip().split("\n"):
					ts, val = line.split(":")
					dt = datetime.fromtimestamp(int(ts))
					value = int(val)
					#print "loaded solar for", dt, value
					self.loadedData[path_ext][sb].append((dt, value))
			except Exception as e:
				#print e
				return False
		
		self.logger.info("Loaded from tmp")
		return True

class GCSlopeSolarPredictor(GCSolarPredictorBase):
	
	# currentTime=a datetime that represents when we start
	# startValue=start value
	# changeRate=rate per second (can be positive or negative)
	def __init__(self, currentTime, startValue, changeRate):
		self.startValue = startValue
		self.changeRate = changeRate
		self.startTime = currentTime
	
	def getSolarAt(self, dt):
		delta = dt - self.startTime
		minutes = delta.total_seconds() / 60.0
		return self.startValue + (minutes*self.changeRate)

class GCQuadraticSolarPredictor(GCSolarPredictorBase):
	
	def __init__(self, date=datetime.now(), peakHour=12, hoursOfDaylight=8, maxSolar=820.0):
		#self.startDate = startDate
		#self.hoursOfDaylight = hoursOfDaylight
		noonDt = datetime(year=date.year, month=date.month, day=date.day, hour=peakHour, minute=0, second=0)
		
		self.noon = date2num(noonDt)
		self.hours = hoursOfDaylight / 24.0;
		self.sunUp = self.noon - (self.hours / 2.0)
		self.sunDown = self.noon + (self.hours / 2.0)
		self.maxSolar = maxSolar

	def func(self, dt):
		x1 = self.sunUp
		y1 = 0.0
		x2 = self.sunDown
		y2 = 0.0
		x3 = self.noon
		y3 = self.maxSolar
		
		#print x1, y1
		#print x2, y2
		#print x3, y3
		
		a = ((y2-y1)*(x1-x3) + (y3-y1)*(x2-x1)) / ((x1-x3)*(pow(x2, 2.0)-pow(x1, 2.0)) + (x2-x1)*(pow(x3, 2.0) - pow(x1, 2.0)))
		b = ((y2-y1) - a*(pow(x2, 2.0) - pow(x1, 2.0))) / (x2-x1)
		c = y1 - a*pow(x1, 2.0) - b*x1
		
		x = date2num(dt)
		
		return a * pow(x, 2.0) + b*x + c
		
	def getSolarAt(self, dt):
		numDt = date2num(dt)
		if numDt < self.sunUp or numDt > self.sunDown:
			return 0.0
		
		return self.func(dt)

class GCMdSolarPredictor(GCSolarPredictorBase):
	
	def __init__(self, solar_dt, experiment_dt=None, extraHours=7):
		self.clock = ExperimentClock.getInstance()
		self.logger = GCLogger.getInstance().get_logger(self)
		
		if experiment_dt != None and solar_dt != None:
			self.shift = datetime(month=solar_dt.month, day=solar_dt.day, year=solar_dt.year) - datetime(month=experiment_dt.month, day=experiment_dt.day, year=experiment_dt.year)
		else:
			self.shift = timedelta()
		self.logger.info("Shift value is %s" % str(self.shift))
		
		filename = "solar-predictions-%02d-%02d-%02d.dat" % (solar_dt.year, solar_dt.month, solar_dt.day)
		
		iterBase = datetime(year=solar_dt.year, month=solar_dt.month, day=solar_dt.day)
		iterBase = iterBase - timedelta(minutes=60)
		dtI = dtiter(iterBase, timedelta(hours=1))
		self.data = {}
		
		self.logger.info("Loading predictions from file %s" % filename)
		if extraHours > 0:
			self.logger.info("Will add %d extra empty lines" % extraHours)
		
		# add a dummy first line
		currentLineDt = dtI.next()
		self.data[currentLineDt] = {}
		for nextDt in dtrange(currentLineDt, timedelta(hours=24), timedelta(hours=1)):
			fakeValue = 0.0
			self.data[currentLineDt][nextDt] = fakeValue
			self.logger.info("-----Added fake value for %s: %0.2f" % (str(nextDt), fakeValue))
	
		with open(filename, "r") as f:
			for line in f:
				currentLineDt = dtI.next()
				self.data[currentLineDt] = {}
				self.logger.info("Loading line for %s" % str(currentLineDt))
				
				line = line.strip()
				predictions = line.split()
				
				dtSubI = dtiter(currentLineDt, timedelta(hours=1))
		    
				for prediction in predictions:
					prediction = float(prediction)
					nextDt = dtSubI.next()
					self.data[currentLineDt][nextDt] = prediction
					self.logger.info("-----Found value for %s: %0.2f" % (str(nextDt), prediction))
			
		for i in xrange(0, extraHours):
			currentLineDt = dtI.next()
			self.data[currentLineDt] = {}
			self.logger.info("Adding fake line for %s" % str(currentLineDt))
			
			for nextDt in dtrange(currentLineDt, timedelta(hours=24), timedelta(hours=1)):
				fakeValue = 0.0
				self.data[currentLineDt][nextDt] = fakeValue
				self.logger.info("-----Added fake value for %s: %0.2f" % (str(nextDt), fakeValue))
	
	def getSolarAt(self, dt):
		originalDt = dt + self.shift
		dt = originalDt - timedelta(minutes=30)
		
		keys = sorted(self.data.keys())
		originalNow = self.clock.get_current_time() + self.shift
		now = originalNow - timedelta(minutes=30)
		
		#print dt, now, originalDt, originalNow
		
		key = find_le(keys, now)
		currentData = self.data[key]
		dts = sorted(currentData.keys())

		lowDt = find_le(dts, dt)
		highDt = find_gt(dts, dt)
		
		lowValue = currentData[lowDt]
		highValue = currentData[highDt]
		
		if lowValue == 0:
			value = 0
		else:
			value = interpolate_dt((lowDt, lowValue), (highDt, highValue), dt)
		
		self.logger.log("Got prediction: target=%s, now=%s, row=%s, prediction=%0.2f" % (str(originalDt), str(originalNow), str(key), value))
		return value

class GCNullSolarPredictor(GCSolarPredictorBase):
	
	def __init__(self):
		pass
	
	def getSolarAt(self, dt):
		return 0.0

# Incomplete
class GCRegressionSolarPredictor(GCSolarPredictorBase):
	def __init__(self):
		self.data_collector = GCDataCollector.getInstance()
		self.logger = GCLogger.getInstance().get_logger(self)
		self.last_prediction = float("NaN")
	
	def getValuesForLogger(self):
		values = {}
		values["last_solar_prediction"] = self.last_prediction
		return values
	
	def getTextDataForLogger(self):
		return {}
	
	# go backwards from dt for delta and calculate a regression function
	# return (slope, intercept) tuple
	def getRegressionFunc(self, dt, delta=timedelta(minutes=15)):
		pass
		
if __name__ == "__main__":	
	parser = argparse.ArgumentParser()
	parser.add_argument("--source", action="store", nargs="+", choices=["historical", "predicted"], required=True)
	parser.add_argument("--plot", action="store_true")
	parser.add_argument("--export", action="store_true")
	parser.add_argument("--day", action="store", default="10-20-2013")
	args = parser.parse_args()
	
	clock = ExperimentClockFake()
	logger = GCNullLogger()
	
	startDt = datetime.strptime(args.day, "%m-%d-%Y")
	#startDt = startDt + timedelta(hours=6)
	
	if args.plot:
		fig, ax = plot.subplots()
		ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter("%H:%M"))
	
		for source in args.source:
			clock.set_current_time(startDt)
			if source == "historical":
				predictor = GCHistoricalSolarPredictor(dts=[startDt], scale=0.35)
			elif source == "predicted":
				predictor = GCMdSolarPredictor(startDt)
			
			xs = []
			ys = []
		
			for dt in dtrange(startDt, timedelta(hours=24), timedelta(minutes=15)):
				#print "Sampling dt", dt, "reference clock", clock.get_current_time()
				xs.append(dt)
				#ys.append(predictor.getSolarAt(dt))
				ys.append(predictor.getAvgSolarForInterval(dt, timedelta(minutes=15)))
				clock.increment_time(timedelta(minutes=15))
				
			plot.xticks(rotation=70)
			plot.plot(xs, ys, label=source)
		
		plot.legend(bbox_to_anchor=(1.0, 1.0), loc=4, ncol=1, borderaxespad=0.0)
		#plot.ylim((0, 8500))
		plot.ylim((0, 1000))
		plot.savefig("solar-%s.pdf" % args.day, format="pdf")
	
	if args.export:
		for source in args.source:
			clock.set_current_time(startDt)
			if source == "historical":
				predictor = GCHistoricalSolarPredictor(dts=[startDt], scale=0.35)
			elif source == "predicted":
				predictor = GCMdSolarPredictor(startDt)
			
			with open("solar-%s-%0.2d-%0.2d-%0.2d.export" % (source, startDt.year, startDt.month, startDt.day), "w") as f:
				for dt in dtrange(startDt, timedelta(hours=24), timedelta(minutes=15)):
					data = predictor.getAvgSolarForInterval(dt, timedelta(minutes=15))
					timeStr = dt.strftime("%H:%S")
					f.write("%s %0.2f\n" % (timeStr, data))
				
				     
