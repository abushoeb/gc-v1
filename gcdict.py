#!/usr/bin/python

import os
import cPickle

from datetime import datetime
from datetime import timedelta
from gccompactionmanager import Compaction
from gcutil import dtrange
from contextlib import contextmanager
from threading import Lock

class TypeHandler(object):
	
	def parseValue(self, valueString):
		pass
	
	def valueString(self, value):
		pass

class SimpleTypeHander(TypeHandler):
	
	def __init__(self, type):
		self.type = type
	
	def parseValue(self, valueString):
		return self.type(valueString)
	
	def valueString(self, value):
		return str(value)

class DatetimeHandler(TypeHandler):
	def __init__(self):
		super(DatetimeHandler, self).__init__()
	
	def parseValue(self, valueString):
		return datetime.strptime(valueString, "%Y-%m-%d %H:%M:%S.%f")
	
	def valueString(self, value):
		return datetime.strftime(value, "%Y-%m-%d %H:%M:%S.%f")

class CompactionHandler(TypeHandler):
	def __init__(self):
		super(CompactionHandler, self).__init__()
	
	def parseValue(self, valueString):
		return Compaction.parseCompaction(valueString)
	
	def valueString(self, value):
		return str(value)
	
class DictionaryMetadata(object):
	
	def __init__(self):
		self.metadataDict = {}
	
	def __contains__(self, key):
		return key in self.metadataDict
	
	def getOffset(self, key):
		offset, valueType = self.metadataDict[key]
		return offset
	
	def getKeyType(self, key):
		return type(key)
	
	def getValueType(self, key):
		offset, valueType = self.metadataDict[key]
		return valueType
	
	def setMetadata(self, key, offset, valueType):
		self.metadataDict[key] = (offset, valueType)
	
	def keys(self):
		return self.metadataDict.keys()
	
# this should be able to cache recently added items (if we want)
class DiskBackedDictionary(object):
	READONLY = 1
	READWRITE = 2
	KEEPOPEN = 4
	LAZYSYNC = 8
	LAZYLOAD = 16
	
	typeHandlers = {
		datetime : DatetimeHandler(),
		Compaction : CompactionHandler()
	}
	
	@staticmethod
	def addTypeHandler(t, handler):
		DiskBackedDictionary.typeHandlers[t] = handler
	
	@staticmethod
	def removeTypeHandler(t):
		del DiskBackedDictionary.typeHandlers[t]
	
	def __init__(self, dictName, mode=READWRITE):
		self.dictName = dictName
		self.mode = mode
		self.f = None
		self.f_lock = Lock()
		self.metadata_lock = Lock()

		if os.path.exists(self.getDataFilename()) and not os.path.exists(self.getMetadataFilename()):
			raise Exception("Found data file but missing metadata file")
		
		# touch the files
		open(self.getDataFilename(), "a").close()
		open(self.getMetadataFilename(), "a").close()
		
		self.metadataLoaded = False
		
		if not (mode & self.LAZYLOAD):
			self.loadMetadata()
	
	def __del__(self):
		if self.mode & self.READWRITE:
			self.sync()
		
		if self.f and not self.f.closed:
			self.f.close()
		
	@contextmanager
	def getDataFile(self):
		if self.mode & self.KEEPOPEN:
			self.f_lock.acquire()
			if not self.f:
				self.f = open(self.getDataFilename(), "r+")
			yield self.f
			self.f_lock.release()
		else:
			self.f_lock.acquire()
			f = open(self.getDataFilename(), "r+")
			yield f
			f.close()
			self.f_lock.release()
			
	def getTypeHandler(self, t):
		if t in self.typeHandlers:
			return self.typeHandlers[t]
		
		return SimpleTypeHander(type=t)
	
	def getDataFilename(self):
		return "%s" % self.dictName
	
	def getMetadataFilename(self):
		return "%s-metadata" % self.dictName
	
	def loadMetadata(self):
		metadataFile = self.getMetadataFilename()
		
		with open(metadataFile, "rb") as f:
			try:
				self.metadata = cPickle.load(f)
			except:
				self.metadata = DictionaryMetadata()
		
		self.metadataLoaded = True
		
		return
	
	def saveMetadata(self):
		metadataFile = self.getMetadataFilename()
		with open(metadataFile, "wb") as f:
			cPickle.dump(self.metadata, f)
			os.fsync(f.fileno())
	
	def getMetadata(self):
		if not self.metadataLoaded:
			self.loadMetadata()
		
		return self.metadata
	
	def processLine(self, line):
		line = line.strip()
		keyString, valueString = line.split("=")
		keyString = keyString.strip()
		valueString = valueString.strip()
		
		return keyString, valueString
	
	def keys(self):
		return self.getMetadata().keys()

	def __iter__(self):
		return self.getMetadata().keys().__iter__()
	
	def __getitem__(self, key):
		try:
			readOffset = self.getMetadata().getOffset(key)
		except KeyError as e:
			raise KeyError("DiskBackedDictionary[%s]: %s" % (self.dictName, key))
		
		# read the line
		#with open(self.getDataFilename(), "r") as f:
		with self.getDataFile() as f:
			f.seek(readOffset)
			line = f.readline()
		
		# split the line into key and value
		keyString, valueString = self.processLine(line)
		
		# parse the key and value to python types
		readKey = self.getTypeHandler(self.getMetadata().getKeyType(key)).parseValue(keyString)
		readValue = self.getTypeHandler(self.getMetadata().getValueType(key)).parseValue(valueString)
			
		# sanity check
		assert key == readKey
		
		return readValue
	
	def __setitem__(self, key, value):
		if self.mode == self.READONLY:
			raise Exception("Dictionary opened read only")
		
		#with open(self.getDataFilename(), "r+") as f:
		with self.getDataFile() as f:
			# if we already have an offset, seek there
			if key in self.getMetadata():
				writeOffset = self.getMetadata().getOffset(key)
				f.seek(writeOffset)
				while f.read(1) != '\n':
					continue
				endOffset = f.tell()
				
				f.seek(writeOffset)
				blank = ""
				for i in xrange(0, endOffset-writeOffset):
					blank += '\0'
				
				f.write(blank)
				f.seek(writeOffset)
				
			# if we don't have an offset, seek to eof
			else:
				f.seek(0, 2)
			
			# save where the offset actually is now
			actualWriteOffset = f.tell()
			
			# process the key and value for writing
			keyString = self.getTypeHandler(type(key)).valueString(key)
			valueString = self.getTypeHandler(type(value)).valueString(value)
			
			# write the line
			line = "%s = %s\n" % (keyString, valueString)
			f.write(line)
			
			if not (self.mode & self.LAZYSYNC):
				#print "flushing file"
				f.flush()
		
		# update the metadata
		with self.metadata_lock:
			self.getMetadata().setMetadata(key, actualWriteOffset, type(value))
		
		if not (self.mode & self.LAZYSYNC):
			#print "syncing metadata"
			#self.saveMetadata()
			self.sync()
		
		return
	
	def sync(self):
		if self.f and not self.f.closed:
			self.f.flush()
			os.fsync(self.f.fileno())
		
		with self.metadata_lock:	
			self.saveMetadata()

if __name__ == "__main__":
	
	import random
	
	'''
	d = DiskBackedDictionary("dictionary", SimpleKeyHandler(int), SimpleValueHandler(float), mode=DiskBackedDictionary.READWRITE)
	
	for i in xrange(0, 100):
		d[i] = float(i*100)
	
	for i in xrange(0, 100):
		#print "d[%d] = %f" % (i, d[i])
		pass
	'''
	counter = 0.0
	
	dbd = DiskBackedDictionary("dictionarydt", mode=DiskBackedDictionary.READWRITE | DiskBackedDictionary.KEEPOPEN)
	d = {}
	startDt = datetime(month=1, day=1, year=2014)
	
	OPERATIONS = 100
	
	# basis loop
	dt = startDt
	start = datetime.now()
	for i in xrange(0, OPERATIONS):
		dt += timedelta(minutes=15)
	end = datetime.now()
	basisTime = (end - start).total_seconds()
		
	# disk write
	dt = startDt
	start = datetime.now()
	for i in xrange(0, OPERATIONS):
		dbd[dt] = i
		dt += timedelta(minutes=15)
	end = datetime.now()
	diskWriteTime = (end - start).total_seconds()
	
	# mem write
	dt = startDt
	start = datetime.now()
	for i in xrange(0, OPERATIONS):
		d[dt] = i
		dt += timedelta(minutes=15)
	end = datetime.now()
	memWriteTime = (end - start).total_seconds()
	
	# disk read
	dt = startDt
	start = datetime.now()
	for i in xrange(0, OPERATIONS):
		x = dbd[dt]
		dt += timedelta(minutes=15)
	end = datetime.now()
	diskReadTime = (end - start).total_seconds()
	
	# mem read
	dt = startDt
	start = datetime.now()
	for i in xrange(0, OPERATIONS):
		x = d[dt]
		dt += timedelta(minutes=15)
	end = datetime.now()
	memReadTime = (end - start).total_seconds()
	
	
	print "disk time per write op", (diskWriteTime - basisTime) / OPERATIONS
	print "mem time per write op", (memWriteTime - basisTime) / OPERATIONS
	
	print "disk time per read op", (diskReadTime - basisTime) / OPERATIONS
	print "mem time per read op", (memReadTime - basisTime) / OPERATIONS
	
