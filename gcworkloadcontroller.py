#!/usr/bin/python

import time
import socket
import thread
from threading import Thread
from gccommon import *
from gchelpers import denormalize
from gcbase import GCManagerBase
from gclogger import GCLogger
from experimentclock import ExperimentClock

class GCWorkloadController(GCManagerBase):
	clock = None
	workload_generator = None
	ycsb_manager = None
	max_load_rate = None
	current_rate = 0.0
	
	update_stop = False
	
	# this requires a clock, a workload generator, and a YCSB manager to be passed in
	# as well as the max load rate (for denormalization) and an interval (in seconds)
	# at which to update the load generator
	def __init__(self, workload_generator, ycsb_manager, max_load_rate, interval=30, noop=False):
		self.clock = ExperimentClock.getInstance()
		self.logger = GCLogger.getInstance().get_logger(self)
		self.workload_generator = workload_generator
		self.ycsb_manager = ycsb_manager
		self.max_load_rate = max_load_rate
		self.interval = interval
		self.noop = noop
		self.logger.log("Initialized GCWorkloadController, max_load_rate=%d, interval=%d" % (self.max_load_rate, self.interval))
	
	def update_thread(self):
		while not self.update_stop:
			timeNow = self.clock.get_current_time()
			targetNormalized = self.workload_generator.getWorkloadAt(timeNow)
			targetOpsPerSec = denormalize(targetNormalized, self.max_load_rate)
			self.ycsb_manager.setTarget(targetOpsPerSec)
			self.logger.debug("Set workload target to %f at workload time %s" % (targetOpsPerSec, timeNow))
			time.sleep(self.interval)

		print "Stopped workload controller"		
		thread.exit()
	
	def start(self):
		if self.noop:
			print "GCWorkloadController: starting with noop, no thread"
		else:
			#self.ycsb_manager.start()
			#time.sleep(10)
			t = Thread(target=self.update_thread)
			t.start()
	
	def stop(self):
		self.update_stop = True
		#self.ycsb_manager.stop()
		
	def getValuesForLogging(self):
		values = {}
		ycsb_values = self.ycsb_manager.getValuesForLogging()
		
		for value in ycsb_values:
			if value == "ycsb_throughput":
				values["actual_workload"] = ycsb_values[value]
			elif value == "ycsb_target":
				values["target_workload"] = ycsb_values[value]
			else:
				values[value] = ycsb_values[value]
		
		return values
	
if __name__ == "__main__":
	#controller = GCWorkloadController(
	pass