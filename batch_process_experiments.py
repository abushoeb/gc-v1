#!/usr/bin/python

import argparse
import sys
import os
import subprocess

PLOTTER_CMD = "./gcplotter.py --experiment %s --paper --output %s"
#STATS_CMD = "./gcstats.py --experiment %d --output %s"
EXPORT_CMD = "./gcdataexport.py --experiment %s --output %s"


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--file", action="store", required=True)
	parser.add_argument("--hours", action="store", type=int, default=24)
	parser.add_argument("--dir", action="store", type=str, default=".")
	parser.add_argument("--mode", action="store", choices=["plot", "stats", "export"], required=True)
	
	args = parser.parse_args()
	
	f = open(args.file, "r")
	lines = f.read().strip().split("\n")
	
	for line in lines:
		experiment, output = line.split()
		
		if args.mode == "plot":
			CMD = PLOTTER_CMD
		elif args.mode == "export":
			CMD = EXPORT_CMD
		
		output_path = args.dir + "/" + output
		CMD = CMD % (experiment, output_path)
		
		print CMD
		subprocess.call(CMD.split())
	
	