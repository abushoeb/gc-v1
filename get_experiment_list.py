#!/usr/bin/python

import argparse

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("files", nargs="+")
	args = parser.parse_args()
	
	string = ""
	
	for file in args.files:
		f = open(file, "r")
		data = f.read().strip()
		lines = data.split("\n")
		
		for line in lines:
			fields = line.split()
			string += fields[0] + " "
	
	print string