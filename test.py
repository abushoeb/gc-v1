class Test:
	def func(self):
		print "Hello, World!"

	def print_funcs(self):
		print "Globals: "
		for f in globals():
			print f

		print "Locals: "
		for f in locals():
			print f

t = Test()
t.print_funcs()
