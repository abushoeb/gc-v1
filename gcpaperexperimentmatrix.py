#!/usr/bin/python

import itertools

criteria_sort = {
	"sla"		:	1,
	"solar" 	:	2,
	"brownprices"	:	3,
	"peakpower"	:	4,
}

criteria = {
	"sla"		:	["50", "75", "100"],
	"solar" 	:	["fullsolar", "medsolar", "nosolar"],
	"brownprices"	:	["nobrownprices", "brownprices"],
	"peakpower"	:	["nopeakpower", "peakpower"],
}

class Experiment(object):
	
	def __init__(self, name, criteria):
		self.name = name
		self.criteria = criteria
	
	def __repr__(self):
		return "Experiment(%s)" % self.name
	
workloads = ["ask", "hotmail", "messenger", "google"]

experiments = [
	Experiment("baseline", {"solar" : False, "sla" : True, "brownprices" : False, "peakpower" : False}),
	Experiment("simple", {"solar" : True, "sla" : True, "brownprices" : False, "peakpower" : False}),
	Experiment("credit", {"solar" : True, "sla" : True, "brownprices" : False, "peakpower" : False}),
	Experiment("opt", {"solar" : True, "sla" : True, "brownprices" : True, "peakpower" : True}),
]

def doCriteria(e, baseString, criteriaList=None):
	#print "in doCriteria", e, baseString, criteriaList
	
	if criteriaList == None:
		criteriaList = sorted(criteria, key=lambda(x):criteria_sort[x])
		criteriaList = [i for i in criteriaList if e.criteria[i]]
	
	if len(criteriaList) == 0:
		print baseString
		return
	
	c = criteriaList[0]
	nextList = list(criteriaList)
	nextList.pop(0)
	
	#print nextList
	if e.criteria[c]:
	#	print "match", criteria[c]
		for criteriaValue in criteria[c]:
			nextString = "%s-%s" % (baseString, criteriaValue)
			doCriteria(e, nextString, nextList)
	#else:
	#	print "no match", criteria[c]

if __name__ == "__main__":
	for w in workloads:
		workloadString = "%s" % w
		
		for e in experiments:
			experimentString = "%s-%s" % (workloadString, e.name)
			#print e, "experimentString", experimentString
			doCriteria(e, experimentString)