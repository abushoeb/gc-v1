#!/usr/bin/python

from gcanalysis import *

class GCDeadlineAnalysis(GCExperimentProcessor):
	
	reqs = ["state"]
	
	def __init__(self):
		super(GCDeadlineAnalysis, self).__init__(self.reqs)
		
		parser = self.getParser()
		parser.add_argument("--deadline", action="store", type=int, required=True)
	
	def processExperiment(self, args, experiment, results, output):
		timestamps = results.availableTimestamps()
		max_i = len(timestamps) - 1
		max_dt = timestamps[-1]
		dl_delta = timedelta(hours=args.deadline)
		
		for node in OPTIONAL_NODES:
			current_i = 0
			while current_i < max_i:
				# find first off time
				offIndex = next((i for i in xrange(current_i, max_i) if results.getNodeValue(timestamps[i], node, "state") == 0), None)
				if offIndex == None:
					print "%s never off, done" % node
					break
					
				off_dt = timestamps[offIndex]
				
				onIndex = next((i for i in xrange(offIndex, max_i) if results.getNodeValue(timestamps[i], node, "state") == 2), None)
				if onIndex == None and max_dt > off_dt + dl_delta:
					violation = max_dt - off_dt - dl_delta
					#print "DV: %s off @ %s, never turned back on @ %s, violation of %s" % (node, str(off_dt), str(max_dt), str(violation))
					print "DV: %s off @ %s, never turned back on, violation of %s" % (node, str(off_dt), str(violation))
					break
				elif onIndex == None:
					#print "DV: %s off @ %s, never turned back on, no violation" % (node, str(off_dt))
					break
				
				on_dt = timestamps[onIndex]
				if on_dt > off_dt + dl_delta:
					violation = on_dt - off_dt - dl_delta
					print "DV: %s off @ %s, on @ %s, violation of %s" % (node, str(off_dt), str(on_dt), str(violation))
				
				current_i = onIndex + 1

if __name__ == "__main__":
	processor = GCDeadlineAnalysis()
	processor.run()