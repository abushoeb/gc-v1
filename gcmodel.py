#!/usr/bin/python

import sys
import math
import argparse

from gccommon import *
from gcutil import interpolate
from gcutil import interpolateFloat
from gcutil import MultilevelInterpolatingDict
from datetime import datetime
from datetime import timedelta
from gchelpers import find_le
from gchelpers import find_gt
from scipy import stats
import matplotlib.pyplot as pyplot

class GCPowerModel(object):
	
	def nodesSupported(self, power):
		pass

class GCParasolPowerModel(GCPowerModel):
	
	def nodesSupported(self, power):
		power_per_node = 25.5
		return min(30, int(power / power_per_node))

class GCResponseTimeModel(object):
	
	def nodesRequired(self, target, latency):
		pass

class GCObsoleteResponseTimeModel(GCResponseTimeModel):
	
	table = {0:0, 10:2400, 20:4700, 30:7000}
	#table = {0:0, 10:750, 20:1500, 30:2250}
	#table = {0:0, 10:600, 20:1250, 30:1900}
	#table = {0:0, 10:2000, 20:3800, 30:6500}
		
	def nodesRequired(self, target, latency):
		
		if target >= self.table[0] and target < self.table[10]:
			return 10
		
		elif target >= self.table[10] and target < self.table[20]:
			p1 = (self.table[10], 10)
			p2 = (self.table[20], 20)
		
		elif target >= self.table[20] and target < self.table[30]:
			p1 = (self.table[20], 20)
			p2 = (self.table[30], 30)
		else:
			return 30
			
		return int(math.ceil(interpolate(p1, p2, target)))

class GCTableResponseTimeModel(GCResponseTimeModel):
	
	# (target, latency) -> nodes
	nodeLookup = {}
	
	def __init__(self, model_file):
		print "GCTableResponseTimeModel: Loading model file: %s" % model_file

		f = open(model_file, "r")
		lines = f.read().strip().split("\n")
		for line in lines:
			nodes, target, latency = line.split("\t")
			nodes = int(nodes)
			target = int(target)
			latency = float(latency)
			#print nodes, target, latency
			
			try:
				targetDict = self.nodeLookup[target]
			except KeyError:
				self.nodeLookup[target] = {}
				targetDict = self.nodeLookup[target]
			
			targetDict[latency] = nodes
	
	def getNodesForLatency(self, target, required_latency):
		targetDict = self.nodeLookup[target]
		latencies = sorted(targetDict.keys())
		
		if required_latency <= latencies[0]:
		#	print "boundary latency low"
			return len(MINIMUM_NODES + OPTIONAL_NODES)
		elif required_latency >= latencies[-1]:
		#	print "boundary latency high"
			return len(MINIMUM_NODES)
		
		for i in xrange(1, len(latencies)):
			if required_latency >= latencies[i-1] and required_latency <= latencies[i]:
				latencyLow = latencies[i-1]
				latencyHigh = latencies[i]
				break
		
		return interpolate((latencyLow, targetDict[latencyLow]), (latencyHigh, targetDict[latencyHigh]), required_latency)
		
	def nodesRequired(self, target, latency):
		targets = sorted(self.nodeLookup.keys())
		#latency -= 20
		
		# if we are off the table, return max # of nodes
		if target >= targets[-1]:
		#	print "boundary target high"
			return len(MINIMUM_NODES + OPTIONAL_NODES)
		elif target <= targets[0]:
		#	print "boundary target low"
			return len(MINIMUM_NODES)
		
		# find the boundary targets
		for i in xrange(1, len(targets)):
			if target >= targets[i-1] and target <= targets[i]:
				targetLow = targets[i-1]
				targetHigh = targets[i]
				break
		
		nodesLow = self.getNodesForLatency(targetLow, latency)
		nodesHigh = self.getNodesForLatency(targetHigh, latency)
			
		# TODO, check this
		result = int(math.ceil(interpolate((targetLow, nodesLow), (targetHigh, nodesHigh), target)))
		return result
		
		#return int(nodesHigh)


#class ModelPoint(object):
#	
#	def __init__(self, throughput, latency):
		
class GCTableResponseTimeModelNew(GCResponseTimeModel):
	
	def __init__(self, model_file, extrapolate=False):
		#print "GCTableResponseTimeModelNew: Loading model file: %s" % model_file
		
		self.model_file = model_file
		
		# throughput, latency -> nodes
		self.nodesDict = MultilevelInterpolatingDict(extrapolate=extrapolate)

		# nodes, latency -> throughput
		self.throughputDict = MultilevelInterpolatingDict(extrapolate=extrapolate)
	
		# nodes, throughput -> latency
		self.latencyDict = MultilevelInterpolatingDict(extrapolate=extrapolate)
		
		f = open(model_file, "r")
		lines = f.read().strip().split("\n")
		
		nodesValues = []
		
		modelPoints = {}
		
		for line in lines:
			nodes, throughput, latency = line.split("\t")
			nodes = int(nodes)
			throughput = int(throughput)
			latency = float(latency)
			nodesValues.append(nodes)
			
			if not nodes in modelPoints:
				modelPoints[nodes] = []
			
			modelPoints[nodes].append((throughput, latency))
		
		newPoints = {}
		'''
		for nodes in modelPoints.keys():
			newPoints[nodes] = []
			
			points = modelPoints[nodes]
			for i in xrange(0, len(points)-1):
				thisThroughput, thisLatency = points[i]
				nextThroughput, nextLatency = points[i+1]
				
				if thisThroughput % 1000 == 0 and nextThroughput % 1000 == 0:
					newThroughput = (thisThroughput + nextThroughput) / 2
					newLatency = interpolate((thisThroughput, thisLatency), (nextThroughput, nextLatency), newThroughput)
					newPoints[nodes].append((newThroughput, newLatency))
				
					#print thisThroughput, thisLatency, nextThroughput, nextLatency, newThroughput, newLatency
					print "Generated new point:", newThroughput, newLatency
		
		for nodes in newPoints.keys():
			for points in newPoints[nodes]:
				modelPoints[nodes].append(points)
			modelPoints[nodes].sort()
		'''
		
		for nodes in modelPoints.keys():
			
			for point in modelPoints[nodes]:
				throughput, latency = point
				self.nodesDict[throughput, latency] = nodes
				self.throughputDict[nodes, latency] = throughput
				self.latencyDict[nodes, throughput] = latency
				#print nodes, throughput, latency
		
		if not extrapolate:
			self.nodesDict.setBounds((min(nodesValues), max(nodesValues)))
		
		#print self.nodesDict.baseDict
		#print sorted(self.nodesDict.baseDict.keys())
		#sys.exit()
	
	def nodesRequired(self, throughput, latency):
		try:
			#print "getting data for", throughput, latency
			rawValue = self.nodesDict[throughput, latency]
			#rawValue = self.nodesDict[latency, throughput]
			print "raw value", rawValue
			result = int(math.ceil(rawValue))
			# TODO check this
			#result = int(round(rawValue))
			#result = int(math.floor(rawValue))
			#print rawValue, result
			#result = int(math.ceil(self.nodesDict[latency, throughput]))
			return result
		except KeyErrorLow as e:
			print e
			if e.depth == 1:
				#print "max"
				return len(MINIMUM_NODES + OPTIONAL_NODES)
			else:
				#print "min"
				return len(MINIMUM_NODES)

		except KeyErrorHigh as e:
			print e
			if e.depth == 1:
				#print "min"
				return len(MINIMUM_NODES)
			else:
				#print "max"
				return len(MINIMUM_NODES + OPTIONAL_NODES)

	def peakThroughput(self, nodes, latency):
		result = int(math.ceil(self.throughputDict[nodes, latency]))
		return result
	
	def expectedLatency(self, nodes, throughput):
		try:
			result = self.latencyDict[nodes, throughput]
			#result = self.latencyDict[throughput, nodes]
		except:
			result = -1
			
		return result

class GCElectricityPriceModel(object):
	
	# energy prices (per KWh)
	SUMMER_OFF_PEAK_PRICE = 0.08
	SUMMER_ON_PEAK_PRICE = 0.13
	NORMAL_OFF_PEAK_PRICE = 0.08
	NORMAL_ON_PEAK_PRICE = 0.12
	
	# inclusive bounds for summer (months)
	SUMMER_START = 6
	SUMMER_END = 9
	
	# inclusive bounds for on-peak (hours)
	ON_PEAK_START = 9
	ON_PEAK_END = 22
	
	# peak power prices (per kilowatt)
	SUMMER_PEAK_POWER_PRICE = 13.61
	NORMAL_PEAK_POWER_PRICE = 5.59
	
	def __init__(self):
		pass
	
	def isSummer(self, dt):
		if dt.month >= self.SUMMER_START and dt.month <= self.SUMMER_END:
			return True
		else:
			return False
		
	def isOnPeak(self, dt):
		if dt.hour >= self.ON_PEAK_START and dt.hour <= self.ON_PEAK_END:
			return True
		else:
			return False
	
	
	def getEnergyPrice(self, dt):
		if self.isSummer(dt) and not self.isOnPeak(dt):
			return self.SUMMER_OFF_PEAK_PRICE
		elif self.isSummer(dt) and self.isOnPeak(dt):
			return self.SUMMER_ON_PEAK_PRICE
		elif not self.isSummer(dt) and not self.isOnPeak(dt):
			return self.NORMAL_OFF_PEAK_PRICE
		elif not self.isSummer(dt) and self.isOnPeak(dt):
			return self.NORMAL_ON_PEAK_PRICE
		
	def getPeakPowerPrice(self, dt):
		if self.isSummer(dt):
			return self.SUMMER_PEAK_POWER_PRICE
		else:
			return self.NORMAL_PEAK_POWER_PRICE

if __name__ == "__main__":
	
	RESPONSE_DEFAULT = 75
	
	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers(dest="command")
	
	maxload_parser = subparsers.add_parser("maxload")
	maxload_parser.add_argument("--model", action="store", nargs="+", required=True)
	maxload_parser.add_argument("--extrapolate", action="store_true", default=False)
	maxload_parser.add_argument("--response", action="store", type=int, default=RESPONSE_DEFAULT)
	maxload_parser.add_argument("--nodes", action="store", type=int, default=27)
	
	nodesreq_parser = subparsers.add_parser("nodesreq")
	nodesreq_parser.add_argument("--model", action="store", nargs="+", required=True)
	nodesreq_parser.add_argument("--extrapolate", action="store_true", default=False)
	nodesreq_parser.add_argument("--response", action="store", type=int, default=RESPONSE_DEFAULT)
	nodesreq_parser.add_argument("--load", action="store", type=int, required=True)
	
	response_parser = subparsers.add_parser("response")
	response_parser.add_argument("--model", action="store", nargs="+", required=True)
	response_parser.add_argument("--extrapolate", action="store_true", default=False)
	response_parser.add_argument("--nodes", action="store", type=int, default=27)
	response_parser.add_argument("--load", action="store", type=int, required=True)

	power_parser = subparsers.add_parser("power")
	power_parser.add_argument("--dt", action="store", type=str, required=True)

	testpower_parser = subparsers.add_parser("testpower")

	plotmodel_parser = subparsers.add_parser("plotmodel")
	plotmodel_parser.add_argument("--model", action="store", nargs="+", required=True)
	plotmodel_parser.add_argument("--extrapolate", action="store_true", default=False)
	plotmodel_parser.add_argument("--response", action="store", type=int, default=RESPONSE_DEFAULT)

	improve_parser = subparsers.add_parser("improve")
	improve_parser.add_argument("--model", action="store", nargs="+", required=True)
	improve_parser.add_argument("--extrapolate", action="store_true", default=False)
	improve_parser.add_argument("--response", action="store", type=int, default=RESPONSE_DEFAULT)
	improve_parser.add_argument("--start", action="store", type=int, required=True)
	improve_parser.add_argument("--stop", action="store", type=int, required=True)
	improve_parser.add_argument("--step", action="store", type=int, required=True)

	plotfullcurves_parser = subparsers.add_parser("plotfullcurves")
	plotfullcurves_parser.add_argument("--model", action="store", nargs="+", required=True)
	plotfullcurves_parser.add_argument("--extrapolate", action="store_true", default=False)
	plotfullcurves_parser.add_argument("--response", action="store", type=int, default=RESPONSE_DEFAULT)
	plotfullcurves_parser.add_argument("--start", action="store", type=int, required=True)
	plotfullcurves_parser.add_argument("--stop", action="store", type=int, required=True)
	plotfullcurves_parser.add_argument("--step", action="store", type=int, required=True)
	plotfullcurves_parser.add_argument("--nodestep", action="store", type=int, default=1)

	export_parser = subparsers.add_parser("export")
	export_parser.add_argument("--model", action="store", nargs="+", required=True)
	export_parser.add_argument("--extrapolate", action="store_true", default=False)
	export_parser.add_argument("--response", action="store", type=int, default=RESPONSE_DEFAULT)
	export_parser.add_argument("--start", action="store", type=int, required=True)
	export_parser.add_argument("--stop", action="store", type=int, required=True)
	export_parser.add_argument("--step", action="store", type=int, required=True)
	export_parser.add_argument("--nodestep", action="store", type=int, default=3)

	point_parser = subparsers.add_parser("point")
	point_parser.add_argument("--model", action="store", nargs="+", required=True)
	point_parser.add_argument("--extrapolate", action="store_true", default=False)
	point_parser.add_argument("--response", action="store", type=int, default=RESPONSE_DEFAULT)
	
	fillpoints_parser = subparsers.add_parser("fillpoints")
	fillpoints_parser.add_argument("--model", action="store", nargs="+", required=True)
	fillpoints_parser.add_argument("--extrapolate", action="store_true", default=False)
	
	consistency_parser = subparsers.add_parser("consistency")
	consistency_parser.add_argument("--pattern", action="store", required=True)
	consistency_parser.add_argument("--extrapolate", action="store_true", default=False)
	
	consistency2_parser = subparsers.add_parser("consistency2")
	consistency2_parser.add_argument("--pattern", action="store", required=True)
	consistency2_parser.add_argument("--target", action="store", type=int, default=4500)
	consistency2_parser.add_argument("--extrapolate", action="store_true", default=False)
	
	args = parser.parse_args()
	
	if "model" in vars(args):
		#oldModel = GCTableResponseTimeModel(args.model)
		#newModel = GCTableResponseTimeModelNew(args.model, args.extrapolate)
		oldModels = {}
		models = {}
		
		for model in args.model:
			oldModels[model] = GCTableResponseTimeModel(model)
			models[model] = GCTableResponseTimeModelNew(model, args.extrapolate)		
		
	if args.command == "maxload":
		print "Max load for response time %0.2f at %d nodes is %0.2f" % (args.response, args.nodes, models[args.model[0]].peakThroughput(args.nodes, args.response))
	
	elif args.command == "nodesreq":
		print "(Old Model) Nodes for latency %0.2f at load %d is %d" % (args.response, args.load, oldModels[args.model[0]].nodesRequired(args.load, args.response))
		print "Nodes for latency %0.2f at load %d is %d" % (args.response, args.load, models[args.model[0]].nodesRequired(args.load, args.response))
	
	elif args.command == "response":
		print "Expected response time for nodes %d at load %d is %0.2f ms" % (args.nodes, args.load, models[args.model[0]].expectedLatency(args.nodes, args.load))
	
	elif args.command == "power":
		model = GCElectricityPriceModel()
		dt = datetime.strptime(args.dt, "%m-%d-%Y %H:%M")
		print "Date/Time: %s" % str(dt)
		print "On Peak:", model.isOnPeak(dt)
		print "Summer:", model.isSummer(dt)
		print "Price per KWh:", model.getEnergyPrice(dt)
		print "Price for Peak Power (per KW):", model.getPeakPowerPrice(dt)
	
	elif args.command == "testpower":
		model = GCElectricityPriceModel()
		dtSummer = datetime(month=7, day=1, year=2013)
		dtNormal = datetime(month=10, day=1, year=2013)
		dts = [dtSummer, dtNormal]
		
		for dt in dts:
			for i in xrange(0, 96):
				print "%s: %0.2f/KWh, peak power %0.2f/KW" % (str(dt), model.getEnergyPrice(dt), model.getPeakPowerPrice(dt))
				dt += timedelta(minutes=15)
	
	elif args.command == "plotmodel":
		#models = [("oldModel", oldModel), ("newModel", newModel)]
		#models = [("newModel", newModel)]
		
		for modelName in models:
			model = models[modelName]
			xs = []
			ys = []
			
			for t in xrange(1000, 7000, 50):
				xs.append(t)
				req = model.nodesRequired(t, args.response)
				ys.append(req)
				print t, req

			pyplot.plot(xs, ys, label=modelName)
			#pyplot.xticks([float(t) for t in xs])
				
		pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0, prop={'size':6})
		pyplot.ylim(0, 30)
		pyplot.show()
		outFile = "plotmodel.pdf"
		pyplot.savefig(outFile, format="pdf")
		print "plot in %s" % outFile
		#print "showing plot "
	
	elif args.command == "improve":
		model = newModel
		for load in xrange(args.start, args.stop+1, args.step):
			nodesReq = model.nodesRequired(load, args.response)
			print "%d : %d" % (load, nodesReq)
	
	elif args.command == "plotfullcurves":
		model = models[args.model[0]]
		
		for nodes in xrange(9, 27+1, args.nodestep):
			xs = []
			ys = []	
				
			for load in xrange(args.start, args.stop+1, args.step):
				responseTime = model.expectedLatency(nodes, load)
				if responseTime > 0:
					xs.append(load)
					ys.append(responseTime)
				print ys
			
			pyplot.plot(xs, ys, label="%d" % nodes)
					
		pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0, prop={'size':6})
		pyplot.ylim(0, 100)
		pyplot.xlim(0, args.stop + args.step)
		pyplot.xticks([i for i in xrange(args.start, args.stop+1, args.step)])
		pyplot.show()
		outFile = "%s-fullcurves.pdf" % args.model[0].replace(".model", "")
		pyplot.savefig(outFile, format="pdf")
		print "plot in %s" % outFile
	
	
	elif args.command == "export":
		model = models[args.model[0]]
		
		for nodes in xrange(9, 27+1, args.nodestep):
			with open("model-%d.export" % nodes, "w") as f:
				for load in xrange(args.start, args.stop+1, args.step):
					responseTime = model.expectedLatency(nodes, load)
					if responseTime > 0:
						f.write("%0.2f %0.2f\n" % (load, responseTime))
	
	elif args.command == "point":
		model = newModel
		
		xs = []
		ys = []
			
		for node in xrange(9, 27+1, 1):
			xs.append(node)
			ys.append(newModel.peakThroughput(node, args.response))
		
		for i in xrange(0, len(xs)):
			print xs[i], ys[i]
	
	elif args.command == "fillpoints":
		modelFile = args.model[0]
		print modelFile
		data = {}
		
		with open(modelFile, "r") as f:
			for line in f:
				line = line.strip()
				nodes, load, response = line.split("\t")
				nodes = int(nodes)
				load = int(load)
				response = float(response)
				
				if nodes not in data:
					data[nodes] = {}
				
				data[nodes][load] = response
		
		# interpolate in between any values we have
		maxes = []
		for node in sorted(data):
			loads = data[node]
			keys = sorted(loads.keys())
			
			minimumLoad = keys[0]
			maximumLoad = keys[-1]
			maxes.append(maximumLoad)
			
			for load in xrange(minimumLoad, maximumLoad+1, 250):
				if load in loads:
					continue
				
				lowLoad = find_le(keys, load)
				highLoad = find_gt(keys, load)
				
				new = interpolateFloat(lowLoad, loads[lowLoad], highLoad, loads[highLoad], load)
				loads[load] = new

			for load in sorted(loads):
				print node, load, loads[load]
		
		globalMaximumLoad = max(maxes)
		
		# extrapolate lines out based on linear regression
		for node in sorted(data):
			loads = data[node]
			keys = sorted(loads.keys())
			
			maximumLoad = keys[-1]
			
			print keys
			prevLoads = []
			prevLatencies = []
			for i in reversed(xrange(1, 8+1)):
				loadValue = keys[-1*i]
				latencyValue = loads[loadValue]
				prevLoads.append(loadValue)
				prevLatencies.append(latencyValue)
			
			print "loads", prevLoads
			print "latencies", prevLatencies
			
			slope, intercept, r_value, p_value, std_err = stats.linregress(prevLoads, prevLatencies)
			
			for load in xrange(maximumLoad+250, globalMaximumLoad+1, 250):
				extrapolated = slope*load + intercept
				loads[load] = extrapolated
				print "extrapolated", node, load, extrapolated
		
		outputFile = modelFile.replace(".model", "")
		outputFile = "%s-EXTENDED.model" % outputFile
		
		with open(outputFile, "w") as f:
			for node in sorted(data):
				loads = data[node]
				for load in sorted(loads):
					if load < 1000:
						continue
					f.write("%d\t%d\t%0.2f\n" % (node, load, loads[load]))
		
		print "Wrote extended model to %s" % outputFile

	elif args.command == "consistency":
		files = os.listdir(".")
		nodes = [9, 18, 27]
		
		modelObjs = {}
		datapoints = {}
		
		for f in files:
			if args.pattern in f and ".model" in f:
				name = f
				model = GCTableResponseTimeModelNew(name, args.extrapolate)
				datapoints[name] = []
				
				for node in nodes:
					try:
						maxThroughput = model.peakThroughput(node, 75.0)
					except Exception as e:
						#if node == 0:
						#	maxThroughput = 0
						#else:
						maxThroughput = float("NaN")
						
					print node, maxThroughput
					datapoints[name].append((maxThroughput, node))
		
		
		
		for name in sorted(datapoints):
			print name, datapoints[name]
			
			xs = []
			ys = []
			
			for point in datapoints[name]:
				throughput, node = point
				xs.append(node)
				ys.append(throughput)
			
			print xs, ys
			pyplot.plot(xs, ys, 'k')
			pyplot.plot(xs, ys, 'o', label=name)
		
		pyplot.legend(bbox_to_anchor=(1.0, 0.0), loc=4, ncol=1, borderaxespad=0.0, prop={'size':8})
		pyplot.savefig("%s.pdf" % args.pattern, format="pdf")
		#pyplot.show()
		
	elif args.command == "consistency2":
		files = os.listdir(".")
		nodes = [9, 18, 27]
		target = args.target
		
		modelObjs = {}
		datapoints = {}
		
		for f in files:
			if args.pattern in f and ".model" in f:
				name = f
				model = GCTableResponseTimeModelNew(name, args.extrapolate)
				datapoints[name] = []
				
				for node in nodes:
					try:
						latency = model.expectedLatency(node, target)
					except Exception as e:
						latency = float("NaN")
					
					if latency == -1:
						latency = float("NaN")
						
					print node, latency
					datapoints[name].append((latency, node))
		
		for name in sorted(datapoints):
			print name, datapoints[name]
			
			xs = []
			ys = []
			
			for point in datapoints[name]:
				throughput, node = point
				xs.append(node)
				ys.append(throughput)
			
			print xs, ys
			pyplot.plot(xs, ys, 'k')
			pyplot.plot(xs, ys, 'o', label=name)
		
		pyplot.ylim(0, 275)
		pyplot.legend(bbox_to_anchor=(1.0, 1.0), loc=1, ncol=1, borderaxespad=0.0, prop={'size':8})
		pyplot.savefig("consistency2-%s-%d.pdf" % (args.pattern, args.target), format="pdf")
		