#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcmodel import *
from gcanalysis import *

from datetime import datetime
from datetime import timedelta

class GCTransitionAnalysis(GCExperimentProcessor):
	reqs = ["actual_workload", "nodes_ready", "readlatency_99_window"]
	fields = ["latency", "nodes", "avg_workload_transition", "off_len", "transition_len", "integrated_workload_off", "model", "model_minus_1", "model_minus_2", "model_minus_3", "model_minus_4", "model_minus_5", "model_minus_6", "model_minus_7", "model_minus_8"]
	
	def __init__(self):
		super(GCTransitionAnalysis, self).__init__(self.reqs)
		
		parser = self.getParser()
		parser.add_argument("--file", action="store_true", default=False)
		parser.add_argument("--model", action="store", required=True)
	
	def getScreenHeader(self):
		header = "#"
		
		for field in self.fields:
			header += " %s\t" % field
		
		return header
	
	def getFileHeader(self):
		header = ""
		header += "@RELATION transition_analysis\n"
		
		for field in self.fields:
			header += "@ATTRIBUTE %s NUMERIC\n" % field

		header += "@DATA\n"
		
		return header
	
	def setup(self):
		self.model = GCTableResponseTimeModelNew(self.args.model, extrapolate=True)
		
		if self.args.file:
			if not self.args.output:
				print "--output required for --file"
				sys.exit()
				
			self.file = open("%s.arff" % self.args.output, "w")
			self.file.write("%s\n" % self.getFileHeader())
	
	def cleanup(self):
		if self.args.file:
			self.file.close()
		
	def processExperiment(self, args, experiment, results, output):
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)

		data = []
		
		transitionAnalyzer = TransitionAnalyzer(results)
		
		for transition in transitionAnalyzer.transitions:
			latency = transition.maxTransitionLatency
			nodes = transition.nodesReady
			avg_workload_transition = transition.avgTransitionWorkload
			off_len = (transition.transDt - transition.offDt).total_seconds()
			transition_len = (transition.onDt - transition.transDt).total_seconds()
			integrated_workload_off = transition.integratedOffWorkload
			
			model = self.model.expectedLatency(nodes, avg_workload_transition)
			model_minus_1 = self.model.expectedLatency(nodes-1, avg_workload_transition)
			model_minus_2 = self.model.expectedLatency(nodes-2, avg_workload_transition)
			model_minus_3 = self.model.expectedLatency(nodes-3, avg_workload_transition)
			model_minus_4 = self.model.expectedLatency(nodes-4, avg_workload_transition)
			model_minus_5 = self.model.expectedLatency(nodes-5, avg_workload_transition)
			model_minus_6 = self.model.expectedLatency(nodes-6, avg_workload_transition)
			model_minus_7 = self.model.expectedLatency(nodes-7, avg_workload_transition)
			model_minus_8 = self.model.expectedLatency(nodes-8, avg_workload_transition)
			model_tuple = (model, model_minus_1, model_minus_2, model_minus_3, model_minus_4, model_minus_5, model_minus_6, model_minus_7, model_minus_8)

			tuple = (latency, nodes, avg_workload_transition, off_len, transition_len, integrated_workload_off) + model_tuple
			hasNan = False
			for t in tuple:
				if math.isnan(t):
					hasNan = True
					break
			if not hasNan:
				data.append(tuple)
		
		if args.file:
			self.outputFile(experiment, data)
		else:
			self.outputPrint(experiment, data)
	
	def outputPrint(self, experiment, data):
		print self.getScreenHeader()
		
		print "# %s" % experiment
		for entry in data:
			for t in entry:
				print "%15.2f\t" % t,
			print ""
		pass
	
	def outputFile(self, experiment, data):
		f = self.file
		
		for entry in data:
			for i in xrange(0, len(entry)):
				t = entry[i]
				if i > 0:
					f.write(",")
				f.write("%0.2f" % t)
			f.write("\n")

if __name__ == "__main__":
	processor = GCTransitionAnalysis()
	processor.run()
