import matplotlib.pyplot as plt  
import datetime  
t=[datetime.datetime(2010, 12, 2, 22, 0),
	datetime.datetime(2010, 12, 2, 22, 1),
	datetime.datetime(2010, 12, 2, 22, 2),
	datetime.datetime(2010, 12, 2, 22, 3)]  
y=[4,6,9,3]  
interval=1.0/24.0  #1hr intervals, but maplotlib dates have base of 1 day  
ax = plt.subplot(111)  
ax.bar(t, y, width=interval)  
ax.xaxis_date()   
plt.show()
