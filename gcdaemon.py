#!/usr/bin/python

import sys
import os
import argparse
import signal
import subprocess

from daemon import *

class GCDaemon(Daemon):
	def __init__(self, name, cmd, stdout='/dev/null', stderr='/dev/null'):
		self.cmd = cmd
		self.daemon_pid_file = "/tmp/%s.daemon.pid" % name
		self.process_pid_file = "/tmp/%s.process.pid" % name
		
		if os.path.exists(self.process_pid_file):
			pf = file(self.process_pid_file, "r")
			self.process_pid = int(pf.read().strip())
		else:
			self.process_pid = None
		
		if stdout == None:
			stdout = '/dev/null'
		if stderr == None:
			stderr = '/dev/null'
			
		super(GCDaemon, self).__init__(self.daemon_pid_file, stdout=stdout, stderr=stderr)
		
	def del_process_pid(self):
		os.remove(self.process_pid_file)
		
	def is_running(self):
		return os.path.exists(self.daemon_pid_file) or os.path.exists(self.process_pid_file)
		
	def run(self):
		process = subprocess.Popen(self.cmd, stdout=file(self.stdout, "w"), stderr=file(self.stderr, "w"))
		pf = file(self.process_pid_file, "w")
		pf.write("%d\n" % process.pid)
		pf.close()
		process.wait()
		self.del_process_pid()
		sys.exit(0)
		
	def kill(self):
		if (self.process_pid != None):
			pgid = os.getpgid(self.process_pid)
			#print "process is pid ", self.process_pid
			#print "pgid is ", pgid
			#os.kill(self.process_pid, signal.SIGKILL)
			os.killpg(pgid, signal.SIGKILL)
			self.del_process_pid()
			
if __name__ == "__main__":
	parser = argparse.ArgumentParser("greencassandra python daemonizer");
	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument('--start', dest="action", action="store_const", const="start")
	group.add_argument('--restart', dest="action", action="store_const", const="restart")
	group.add_argument('--stop', dest="action", action="store_const", const="stop")
	group.add_argument('--isrunning', dest="action", action="store_const", const="isrunning")
	
	parser.add_argument("--name", dest="name", action="store", required=True)
	parser.add_argument("--out", dest="out", action="store", required=False)
	parser.add_argument("--err", dest="err", action="store", required=False)
	parser.add_argument("cmd", nargs=argparse.REMAINDER)
	
	args = parser.parse_args()
	daemon = GCDaemon(args.name, args.cmd, stdout=args.out, stderr=args.err)

	if args.action == "start":	
		daemon.start()
	elif args.action == "restart":
		daemon.restart()
	elif args.action == "stop":
		daemon.stop()
	elif args.action == "isrunning":
		if daemon.is_running():
			print 1
		else:
			print 0
