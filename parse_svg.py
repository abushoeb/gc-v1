#!/usr/bin/python

import sys
import sqlitedict

from xml.dom import minidom
from svg.path import *

from datetime import datetime
from datetime import timedelta

#DOC = "plotdata.svg"
#DOC = "plotdatanoborder.svg"
DOC = "plotdata-modified.svg"

XUNITS = 168.0*60.0*60.0
YUNITS = 1.0

def getBounds(all_path_strings):
	hasXBound = False
	hasYBound = False
	
	for path in all_path_strings:
		p = parse_path(path)
		
		for line in p:
			start_x, start_y = line.start.real, line.start.imag
			end_x, end_y = line.end.real, line.end.imag
			
			#print start_x, start_y, "-", end_x, end_y
			
			# find bounds
			if not hasXBound and start_y == end_y:
				xBound = abs(start_x - end_x)
				hasXBound = True
			
			if not hasYBound and start_x == end_x:
				yBound = abs(start_y - end_y)
				hasYBound = True

			if hasXBound and hasYBound:
				#print "has bounds"
				return xBound, yBound
			
if __name__ == "__main__":
	doc = minidom.parse(DOC)
	#all_path_strings = [path.getAttribute("d") for path in doc.getElementsByTagName("path")]
	#xBound, yBound = getBounds(all_path_strings)

	# get the box
	boxPoints = []
	for element in doc.getElementsByTagName("path"):
		if element.getAttribute("id") == "path274":
			box = element.getAttribute("d")
			parsed = parse_path(box)
			for l in parsed:
				boxPoints.append((l.start.real, l.start.imag))
				boxPoints.append((l.end.real, l.end.imag))
			break
	
	boxPoints.sort()
	boxxs = [point[0] for point in boxPoints]
	boxys = [point[1] for point in boxPoints]
	
	boxMinX = min(boxxs)
	boxMaxX = max(boxxs)
	boxMinY = min(boxys)
	boxMaxY = max(boxys)

	# get the plot data
	for element in doc.getElementsByTagName("g"):
		if element.getAttribute("id") == "g278":
			data_path_strings = [path.getAttribute("d") for path in element.getElementsByTagName("path")]
		
	doc.unlink()
	
	
	#extract the points
	points = []	
	for path in data_path_strings:
		p = parse_path(path)
		
		for line in p:
			start_x, start_y = line.start.real, line.start.imag
			end_x, end_y = line.end.real, line.end.imag
			
			#print start_x, start_y, "-", end_x, end_y
			
			points.append((start_x, start_y))
			#points.append((end_x, end_y))
	
	# get the x range
	xs = [point[0] for point in points]
	minX = min(xs)
	maxX = max(xs)
	
	ys = [point[1] for point in points]
	minY = min(ys)
	maxY = max(ys)
	#print minY, maxY
	
	# normalize the points, then denormalize them to seconds
	normalizedPoints = []
	for point in points:
		# normalize the data
		#ydiff = yBound - max
		
		#print minY, boxMinX
		#sys.exit()
		
		
		#normalizedX = (point[0] - minX) / (maxX - minX)
		#normalizedY = (point[1] - ydiff) / (yBound - ydiff)
		
		normalizedX = (point[0] - boxMinX) / (boxMaxX - boxMinX)
		normalizedY = (point[1] - minY) / (boxMaxY - minY)
		
		#print point[0], normalizedX, point[1], normalizedY
		
		# normalized X needs to be transformed into seconds
		x = normalizedX * XUNITS
		
		# we want Y output as normalized, so we leave it alonge
		y = normalizedY
		
		normalizedPoints.append((x,y))
	
	normalizedPoints.sort()
	
	for point in normalizedPoints:
		print point[0], point[1]
		
	start_dt = datetime(month=6, day=23, year=2014, hour=0, minute=0)
	
	db = sqlitedict.SqliteDict("/home/wkatsak/wonko_home/greencassandra/wierman.db", autocommit=True, flag='c')
	
	data = {}
	for point in normalizedPoints:
		dt = start_dt + timedelta(seconds=point[0])
		#print dt, point[1]
		data[dt] = point[1]
	
	db["workload"] = data
	db["hello"] = "foo"
	db.commit()