#!/usr/bin/python

import sys

if __name__ == "__main__":
	
	filename = sys.argv[1]
	
	with open(filename, "r") as f:
		for line in f:
			line = line.strip()
			nodes, load, latency = line.split()
			nodes = int(nodes)
			load = int(load)
			latency = float(latency)
			if nodes % 3 == 0:
				print "%d\t%d\t%0.2f" % (nodes, load, latency)
		