#!/usr/bin/python

import sys
import os
import os.path
import socket
from gcutil import GCSingleton

GC1_NODES = ["williamovna.rutgers.edu", "crypt06", "zontik", "sol032", "sol033", "sol034", "sol035", "sol036", "sol037", "sol038", "sol039", "sol040", "sol041", "sol043", "sol044", "sol045", "sol046", "sol047", "sol048", "sol050", "sol051", "sol052", "sol053", "sol054", "sol056", "sol058", "sol059", "sol060", "sol061", "sol062"]
GC2_NODES = ["zontik2", "sol000", "sol003", "sol006", "sol007", "sol008", "sol009", "sol010", "sol011", "sol012", "sol013", "sol014", "sol015", "sol016", "sol017", "sol019", "sol021", "sol022", "sol023", "sol024", "sol025", "sol026", "sol027", "sol028", "sol029", "sol030", "sol031", "sol063"]

HOMEDIR="/home/shoeb"
#HOMEDIR="/home/wkatsak"
#os.path.expanduser("~")
if os.path.exists("%s/wonko_home" % HOMEDIR):
	CASSANDRA_SCRIPT_PATH = "%s/wonko_home/cassandra_scripts" % HOMEDIR
	GC_PATH = "%s/wonko_home/greencassandra" % HOMEDIR
else:
	CASSANDRA_SCRIPT_PATH="%s/cassandra_scripts" % HOMEDIR
	GC_PATH = "%s/greencassandra" % HOMEDIR

CASSANDRA_PATH="/opt/cassandra"
NODETOOL_PATH=CASSANDRA_PATH+"/bin/nodetool"
USERNAME="wkatsak"

#RESULTS_DIR="%s/greencassandra/results" % HOMEDIR
RESULTS_DIR="/results"
RESULTS_INTERVAL=1

# load the nodes from the (script) config file
MINIMUM_NODES = []
OPTIONAL_NODES = []

hostname = socket.gethostname()

load_config = False
if hostname in GC1_NODES:
	config_file = open(CASSANDRA_SCRIPT_PATH + "/cassandra.config")
	EXECUTE_NODE="zontik.cs"
	YCSB_HOSTNAME="crypt11"
	CLUSTER_NAME="GC1"
	load_config = True
elif hostname in GC2_NODES:
	config_file = open(CASSANDRA_SCRIPT_PATH + "/cassandra2.config")
	EXECUTE_NODE="172.16.18.23"
	YCSB_HOSTNAME="crypt12"
	CLUSTER_NAME="GC2"
	load_config = True
	# Abu Shoeb modified on Apr 08, 2016
else:
	EXECUTE_NODE="172.16.28.25"
	YCSB_HOSTNAME="shoeb-dell"
	CLUSTER_NAME="shoeb-dell"
	load_config = False

CURRENT_EXPERIMENT_FILE = "%s/current-experiment-%s" % (GC_PATH, CLUSTER_NAME)

if load_config:	
	lines = config_file.read().split("\n")

	for line in lines:
		if line == "" or line == "#!/bin/bash":
			continue
		try:
			var, value = (line.replace("\"", "").split("="))
		except:
			continue
		
		if var == "MINIMUM_NODES":
			for node in value.split(" "):
				MINIMUM_NODES.append(node)
		elif var == "OPTIONAL_NODES":
			for node in value.split(" "):
				OPTIONAL_NODES.append(node)
		elif var == "FIRST_NODE":
			FIRST_NODE = value
		
NUM_NODES = len(MINIMUM_NODES) + len(OPTIONAL_NODES)
MAX_LOAD_RATE=1800

YCSB_RECORD_COUNT=72000000
YCSB_RECORD_INSERT_ORDER="hashed"

BASE_COMPACTION_THROUGHPUT=2048

DROP_TIMESTAMPS=70

#print MINIMUM_NODES
#print OPTIONAL_NODES

def setCurrentExperiment(path):
	with open(CURRENT_EXPERIMENT_FILE, "w") as f:
		f.write("%s\n" % path)

def getCurrentExperiment():
	try:
		with open(CURRENT_EXPERIMENT_FILE, "r") as f:
			path = f.readline().strip()
			return path
	except:
		raise Exception("getCurrentExperiment: No experiment currently running...")
	
def clearCurrentExperiment():
	if os.path.exists(CURRENT_EXPERIMENT_FILE):
		os.remove(CURRENT_EXPERIMENT_FILE)
	
class GCNodeRegion:
	MINIMUM = 0
	OPTIONAL = 1
	
class GCNodeState:
	SCHEDULED = 4
	ON = 2
	TRANSITION = 1
	OFF = 0

class GCNodeStateFlags:
	DEADLINE = 8
	NONE = 0
	
class GCNode:
	name = None
	ip = None
	region = None
	offTime = None
	transitionTime = None
	transitionSlotsRequired = None
	state = None
	stateFlags = None
	
	def __init__(self, name, ip, region, state, stateFlags=GCNodeStateFlags.NONE):
		self.name = name
		self.ip = ip
		self.region = region
		self.offTime = None
		self.transitionTime = None
		self.transitionSlotsRequired = None
		self.state = state
		self.stateFlags = stateFlags

class GCConfig(GCSingleton):
	
	def __init__(self):
		super(GCConfig, self).__init__(GCConfig)
		
		self.lookAhead = 1
		self.configDict = {}
			
	def getLookAhead(self):
		return self.lookAhead
	
	def setLookAhead(self, lookAhead):
		self.lookAhead = lookAhead
	
	def getValue(self, key):
		try:
			return self.configDict[key]
		except KeyError as e:
			raise Exception("Config param %s not set..." % key)
	
	def setValue(self, key, value):
		self.configDict[key] = value
	
	def __getitem__(self, key):
		return self.getValue(key)
	
	def __setitem__(self, key, value):
		return self.setValue(key, value)
	
	