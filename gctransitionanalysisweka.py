#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcmodel import *
from gcanalysis import *

from datetime import datetime
from datetime import timedelta

class GCTransitionAnalysis(GCExperimentProcessor):
	reqs = ["actual_workload", "any_transitions", "nodes_ready", "readlatency_99_window"]
	fields = ["nodes", "workload", "latency", "transition", "transition_len", "off_len", "integrated_workload", "integrated_workload_sq", "expected_latency", "expected_latency_minus_4", "expected_latency_minus_6", "expected_latency_minus_8"]
	
	def __init__(self):
		super(GCTransitionAnalysis, self).__init__(self.reqs)
		
		parser = self.getParser()
		parser.add_argument("--file", action="store_true", default=False)
		parser.add_argument("--model", action="store", required=True)
	
	def getScreenHeader(self):
		header = "#"
		
		for field in self.fields:
			header += " %s\t" % field
		
		return header
	
	def getFileHeader(self):
		header = ""
		header += "@RELATION transition_analysis\n"
		
		for field in self.fields:
			header += "@ATTRIBUTE %s NUMERIC\n" % field

		header += "@DATA\n"
		
		return header
	
	def setup(self):
		self.model = GCTableResponseTimeModelNew(self.args.model, extrapolate=True)
		
		if self.args.file:
			if not self.args.output:
				print "--output required for --file"
				sys.exit()
				
			self.file = open("%s.arff" % self.args.output, "w")
			self.file.write("%s\n" % self.getFileHeader())
	
	def cleanup(self):
		if self.args.file:
			self.file.close()
		
	def processExperiment(self, args, experiment, results, output):
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)

		data = []
		
		transitionAnalyzer = TransitionAnalyzer(results)
		
		for i in xrange(1, max_i):
			dt = timestamps[i]
			workload = results.getCommonValue(dt, "actual_workload")
			prev_any_transitions = results.getCommonValue(dt, "any_transitions")
			any_transitions = results.getCommonValue(dt, "any_transitions")
			nodes = results.getCommonValue(dt, "nodes_ready")
			latency = results.getCommonValue(dt, "readlatency_99_window")
			expected_latency = self.model.expectedLatency(nodes, workload)
			expected_latency_minus_4 = self.model.expectedLatency(nodes - 4, workload)
			expected_latency_minus_6 = self.model.expectedLatency(nodes - 6, workload)
			expected_latency_minus_8 = self.model.expectedLatency(nodes - 8, workload)
			
			if prev_any_transitions != any_transitions:
				continue
			
			if any_transitions > 0:
				transition_len = transitionAnalyzer.getTransitionLength(dt)
				off_len = transitionAnalyzer.getOffLength(dt)
				integrated_workload = transitionAnalyzer.getIntegratedWorkload(dt) / 100000
				integrated_workload_sq = integrated_workload * integrated_workload
			else:
				transition_len = 0.0
				off_len = 0.0
				integrated_workload = 0.0
				integrated_workload_sq = 0.0			
			
			tuple = (nodes, workload, latency, any_transitions, transition_len, off_len, integrated_workload, integrated_workload_sq, expected_latency, expected_latency_minus_4, expected_latency_minus_6, expected_latency_minus_8)
			hasNan = False
			for t in tuple:
				if math.isnan(t):
					hasNan = True
					break
			if not hasNan:
				data.append(tuple)
						
		if args.file:
			self.outputFile(experiment, data)
		else:
			self.outputPrint(experiment, data)
	
	def outputPrint(self, experiment, data):
		print self.getScreenHeader()
		
		print "# %s" % experiment
		for entry in data:
			for t in entry:
				print "%12.2f\t" % t,
			print ""
		pass
	
	def outputFile(self, experiment, data):
		f = self.file
		
		for entry in data:
			for i in xrange(0, len(entry)):
				t = entry[i]
				if i > 0:
					f.write(",")
				f.write("%0.2f" % t)
			f.write("\n")

if __name__ == "__main__":
	processor = GCTransitionAnalysis()
	processor.run()
