#!/usr/bin/python

import time
from gccommon import *
from gchelpers import execute_remote_command_sync
from gclogger import GCLogger

class YCSBCassandraWorkload(object):
	workload = "com.yahoo.ycsb.workloads.CoreWorkload"
	
	def __init__(self):
		self.debug = False
		self.readallfields = True
		self.writeallfields = False
		
		self.recordcount = 0
		self.operationcount = 0
			
		self.fieldcount = 0
		self.fieldlength = 0
		self.insertorder="ordered"
		self.requestdistribution = "uniform"
		
		self.threadcount = 1
		self.targetthroughput = 0
		self.maxexecutiontime = 0
		
		self.readproportion = 0.0
		self.updateproportion = 0.0
		self.insertproportion = 0.0
		self.deleteproportion = 0.0
		self.readmodifywriteproportion = 0.0
		
		self.readconsistencylevel = "ONE"
		self.writeconsistencylevel = "ONE"
		self.scanconsistencylevel = "ONE"
		self.deleteconsistencylevel = "ONE"
		
		self.histogram_window = 60
		
		self.table = "usertable"
		
		self.hosts = list()
		
		self.optParams = {}
		
	def toParams(self):
		paramString = ""
		paramString += "-threads %d " % self.threadcount
		paramString += "-target %d " % self.targetthroughput
		paramString += "-p workload=%s " % self.workload
		if self.maxexecutiontime > 0:
			paramString += "-p maxexecutiontime=%d " % self.maxexecutiontime
		paramString += "-p debug=%s " % str(self.debug).lower()
		paramString += "-p readallfields=%s " % str(self.readallfields).lower()
		paramString += "-p writeallfields=%s " % str(self.writeallfields).lower()
		paramString += "-p requestdistribution=%s " % self.requestdistribution
		paramString += "-p insertorder=%s " % self.insertorder
		paramString += "-p recordcount=%d " % self.recordcount
		paramString += "-p operationcount=%d " % self.operationcount
		paramString += "-p fieldcount=%d " % self.fieldcount
		paramString += "-p fieldlength=%d " % self.fieldlength
		paramString += "-p readproportion=%.02f " % self.readproportion
		paramString += "-p updateproportion=%.02f " % self.updateproportion
		paramString += "-p insertproportion=%.02f " % self.insertproportion
		paramString += "-p deleteproportion=%.02f " % self.deleteproportion
		paramString += "-p readmodifywriteproportion=%.02f " % self.readmodifywriteproportion
		paramString += "-p cassandra.readconsistencylevel=\"%s\" " % self.readconsistencylevel
		paramString += "-p cassandra.writeconsistencylevel=\"%s\" " % self.writeconsistencylevel
		paramString += "-p cassandra.scanconsistencylevel=\"%s\" " % self.scanconsistencylevel
		paramString += "-p cassandra.deleteconsistencylevel=\"%s\" " % self.deleteconsistencylevel
		paramString += "-p table=%s " % self.table
		paramString += "-p histogram.window=%s " % self.histogram_window
		
		if len(self.hosts) > 0:
			hostsString = ""
			iter = self.hosts.__iter__()
			n = next(iter, None)
			while n != None:
				hostsString += n
				
				n = next(iter, None)
				if n != None:
					hostsString += ","
			paramString += "-p hosts=\"%s\" " % hostsString
		
		for param in self.optParams:
			paramString += "-p %s=\"%s\" " % (param, str(self.optParams[param]))
		
		return paramString.strip()

class YCSBGreenCassandraWorkload(YCSBCassandraWorkload):
	
	def __init__(self, hosts, recordcount, table=None, readconsistency=None, writeconsistency=None, scanconsistency=None, deleteconsistency=None, requestdistribution=None):
		super(YCSBGreenCassandraWorkload, self).__init__()
		self.hosts = hosts
		self.recordcount = recordcount
		self.fieldcount = 10
		self.fieldlength = 100
		self.insertorder = "hashed"
		if table != None:
			self.table = table
		if readconsistency != None:
			self.readconsistencylevel = readconsistency
		if writeconsistency != None:	
			self.writeconsistencylevel = writeconsistency
		if scanconsistency != None:
			self.scanconsistencylevel = scanconsistency
		if deleteconsistency != None:	
			self.deleteconsistencylevel = deleteconsistency
		if requestdistribution != None:
			self.requestdistribution = requestdistribution
		
class YCSBGreenCassandraUpdateHeavyWorkload(YCSBGreenCassandraWorkload):
	def __init__(self, hosts, recordcount, table=None, readconsistency=None, writeconsistency=None, scanconsistency=None, deleteconsistency=None):
		super(YCSBGreenCassandraUpdateHeavyWorkload, self).__init__(hosts, recordcount,
									table=table,
									readconsistency=readconsistency,
									writeconsistency=writeconsistency,
									scanconsistency=scanconsistency,
									deleteconsistency=deleteconsistency,
									requestdistribution="zipfian")
		self.readproportion = 0.5
		self.writeproportion = 0.5

class YCSBGreenCassandraReadMostlyWorkload(YCSBGreenCassandraWorkload):
	def __init__(self, hosts, recordcount, table=None, readconsistency=None, writeconsistency=None, scanconsistency=None, deleteconsistency=None):
		super(YCSBGreenCassandraReadHeavyWorkload, self).__init__(hosts, recordcount,
									table=table,
									readconsistency=readconsistency,
									writeconsistency=writeconsistency,
									scanconsistency=scanconsistency,
									deleteconsistency=deleteconsistency,
									requestdistribution="zipfian")
		self.readproportion = 0.95
		self.writeproportion = 0.05

class YCSBGreenCassandraReadOnlyWorkload(YCSBGreenCassandraWorkload):
	def __init__(self, hosts, recordcount, table=None, readconsistency=None, writeconsistency=None, scanconsistency=None, deleteconsistency=None):
		super(YCSBGreenCassandraReadOnlyWorkload, self).__init__(hosts, recordcount,
									table=table,
									readconsistency=readconsistency,
									writeconsistency=writeconsistency,
									scanconsistency=scanconsistency,
									deleteconsistency=deleteconsistency,
									requestdistribution="zipfian")
		self.readproportion = 1.0

class YCSBGreenCassandraReadLatestWorkload(YCSBGreenCassandraWorkload):
	def __init__(self, hosts, recordcount, table=None, readconsistency=None, writeconsistency=None, scanconsistency=None, deleteconsistency=None):
		super(YCSBGreenCassandraReadLatestWorkload, self).__init__(hosts, recordcount,
									table=table,
									readconsistency=readconsistency,
									writeconsistency=writeconsistency,
									scanconsistency=scanconsistency,
									deleteconsistency=deleteconsistency,
									requestdistribution="latest")
		self.readproportion = 0.95
		self.insertproportion = 0.05
			
class YCSBGreenCassandraCustomWorkload(YCSBGreenCassandraWorkload):
	def __init__(self, hosts, recordcount, read=0.0, write=0.0, insert=0.0, scan=0.0, readmodifywrite=0.0, table=None, readconsistency=None, writeconsistency=None, scanconsistency=None, deleteconsistency=None, requestdistribution=None):
		super(YCSBGreenCassandraCustomWorkload, self).__init__(hosts, recordcount,
									table=table,
									readconsistency=readconsistency,
									writeconsistency=writeconsistency,
									scanconsistency=scanconsistency,
									deleteconsistency=deleteconsistency,
									requestdistribution=requestdistribution)
		self.readproportion = read
		self.updateproportion = write
		self.insertproportion = insert
		self.scanproportion = scan
		self.readmodifywriteproportion = readmodifywrite

YCSB_PATH="/home/wkatsak/YCSB-devel"

class YCSBRun(object):
	
	def __init__(self, workload, hostname, latency_file="/dev/null", throughput_file="/dev/null", client="cassandra-10"):
		self.workload = workload
		self.hostname = hostname
		self.logger = GCLogger.getInstance().get_logger(self)
		self.latency_file = latency_file
		self.throughput_file = throughput_file
		self.client = client
	
	def start(self):	
		ycsb_command = YCSB_PATH + "/bin/ycsb run %s -s " % self.client
		ycsb_command += self.workload.toParams()

		if self.is_running():
			self.logger.info("YCSB already running on %s...stopping" % self.hostname)
			self.stop()
		
		self.logger.info("Executing YCSB on %s" % self.hostname)
		self.logger.info("YCSB Command:")
		self.logger.info("%s" % ycsb_command)
		
		remote_cmd = "%s/gcdaemon.py --start --name ycsb --out %s --err %s %s" % (GC_PATH, self.latency_file, self.throughput_file, ycsb_command)
		print remote_cmd
		
		out, err = execute_remote_command_sync(self.hostname, USERNAME, remote_cmd)
		
		self.logger.info("Out: %s" % str(out))
		self.logger.info("Err: %s" % str(err))
		
		time.sleep(5)
		self.logger.info("Started YCSB on %s" % self.hostname)	
	
	def stop(self):
		remote_cmd = "%s/gcdaemon.py --stop --name ycsb" % GC_PATH
		out, err = execute_remote_command_sync(self.hostname, USERNAME, remote_cmd)
		
		self.logger.info("Stopped YCSB on %s" % self.hostname)
			
	def is_running(self):
		check_cmd = "%s/gcdaemon.py --isrunning --name ycsb" % GC_PATH
#		print check_cmd
		out, err = execute_remote_command_sync(self.hostname, USERNAME, check_cmd)
		if len(out) > 0:
			if int(out[0].strip()) == 1:
				return True
		
		return False
		
	def wait(self):
		while True:
			if self.is_running():
				sys.stdout.write(". ")
				sys.stdout.flush()
			else:
				sys.stdout.write("\n")
				sys.stdout.flush()
				break;
					
			time.sleep(30)
		
		self.logger.log("Completed YCSB execution")
	
	def run(self):
		self.start()
		self.wait()

class YCSBMultipleRun(object):
	
	def __init__(self, workload, ycsb_hosts, base_latency_file="/dev/null", base_throughput_file="/dev/null", client="cassandra-10"):
		self.workload = workload
		self.logger = GCLogger.getInstance().get_logger(self)
		self.client = client
		
		self.ycsb_runs = []
		
		for host in ycsb_hosts:
			if base_latency_file != "/dev/null":
				latency_file = "%s.%s" % (base_latency_file, host)
			else:
				latency_file = base_latency_file
			
			if base_throughput_file != "/dev/null":
				throughput_file = "%s.%s" % (base_throughput_file, host)
			else:
				throughput_file = "/dev/null"
				
			ycsb_run = YCSBRun(workload, host, latency_file, throughput_file, client=self.client)
			self.ycsb_runs.append(ycsb_run)
	
	def start(self):
		for run in self.ycsb_runs:
			run.start()
	
	def stop(self):
		for run in self.ycsb_runs:
			run.stop()
	
	def wait(self):
		for run in self.ycsb_runs:
			run.wait()
		
	def run(self):
		self.start()
		self.wait()
		
if __name__ == "__main__":

	from gccontroller import  *
	wl = YCSBGreenCassandraReadWorkload(MINIMUM_NODES)
	wl.threadcount = 60
	wl.maxexecutiontime = 10
	
	print wl.toParams()
	
	clock = ExperimentClock(datetime.now(), 1)
	main_logger = GCLogger("testycsb.log", GCLogger.DEBUG, clock)
	
	run = YCSBRun(wl, YCSB_HOSTNAME, main_logger, "ycsbout")
	run.run()
	
