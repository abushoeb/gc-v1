#!/usr/bin/python

import subprocess
import sys
import os

from gcpaperconfig import *
import gcpaperstats

def do_plot(workload, experiment, descriptor):
	if descriptor == ExperimentData.TBD:
		return
	
	experiment = "%s-%s" % (workload, experiment)
	output_file = "%s/%s" % (PLOT_OUTPUT_DIR, experiment)
	
	done_file = "%s.done" % output_file
	if os.path.exists(done_file):
		print "%s exists, skipping export" % done_file
		#return
	else:
		export_cmd = []
		export_cmd.append("./gcdataexport2.py")
		export_cmd.append("--experiment")
		export_cmd.append("%s" % descriptor.path)
		#export_cmd.append("--subsample")
		#export_cmd.append("5")
		export_cmd.append("--output")
		export_cmd.append("%s" % output_file)
		
		if descriptor.solar_day != None:
			export_cmd.append("--solarday")
			export_cmd.append("%s" % descriptor.solar_day)
		
		print "Exporting data from %s to %s.export" % (descriptor.path, experiment)
		subprocess.call(export_cmd)
	
		plot_cmd = []
		plot_cmd.append("gnuplot")
		plot_cmd.append("-e")
		plot_cmd.append("INPUTFILE='%s.export'" % output_file)
		plot_cmd.append("-e")
		plot_cmd.append("OUTPUTFILE='%s'" % output_file)
		print plot_cmd
		
		if descriptor.plot_type == FULL_PLOT:
			plot_cmd.append(FULL_PLOT_SCRIPT)
		elif descriptor.plot_type == SMALL_PLOT:
			plot_cmd.append(SMALL_PLOT_SCRIPT)
		
		print "Plotting %s to %s.svg" % (descriptor.path, output_file)
		subprocess.call(plot_cmd)
		
		convert_cmd = []
		convert_cmd.append("inkscape")
		convert_cmd.append("%s.svg" % output_file)
		convert_cmd.append("--export-pdf=%s.pdf" % output_file)
		convert_cmd.append("--export-area-drawing")
		print "Converting %s.svg to %s.pdf" % (experiment, output_file)
		subprocess.call(convert_cmd)
	
		# touch a done file
		touch_cmd = []
		touch_cmd.append("touch")
		touch_cmd.append(done_file)
		
		print "Touching done file"
		subprocess.call(touch_cmd)

def gen_plot_latex(workload, experiment, f=sys.stdout):
	description = "%s-%s" % (workload, experiment)
	
	f.write("\\begin{figure}[t!]\n")
	f.write("\t\\includegraphics[width=0.95\linewidth]{img/%s.pdf}\n" % description)
	f.write("\t\\caption{%s}\n" % description)
	f.write("\t\\label{fig:%s}\n" % description)
	f.write("\\end{figure}\n")
	f.write("\n")
	f.flush()
	
if __name__ == "__main__":
	
	for workload in plots.keys():
		workload_plots = plots[workload]
		for experiment in workload_plots.keys():
			do_plot(workload, experiment, workload_plots[experiment])
		
		
		
	
	
