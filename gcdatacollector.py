#!/usr/bin/python
from gccommon import *
from gcbase import *
from gchelpers import *
from gcresults import *
from experimentclock import ExperimentClock
from gclogger import GCLogger
from gcutil import GCSingleton
from gcdict import DiskBackedDictionary
from gcutil import LockFile
from gccompactionmanager import Compaction
from gcresults import GCResultsSQL, GCResultsSqliteLive

from threading import Thread
from threading import Semaphore
from threading import Lock
from threading import Condition
from Queue import Queue

from datetime import datetime

import os
import shutil
import signal
import thread
import cPickle
import sqlite3
import gcsqlite

class GCDataCollectorDataStore(object):
	pass

class DiskBackedDictionaryDataStore(GCDataCollectorDataStore):
	
	def __init__(self, path, nodes):
		self.path = path
		self.nodes = nodes
		self.dictionaries = {}
		self.timestamps = []
		self.timestamps_lock = Lock()
	
	def start(self):
		# create the nodes file
		nodes_file_path = "%s/%s" % (self.path, "nodes")
		with open(nodes_file_path, "w") as f:
			for node in sorted(self.nodes):
				f.write("%s\n" % node)
		
		# touch the epoch file
		self.epoch_file_path = "%s/%s" % (self.path, "epochs")
		f = open(self.epoch_file_path, "w")
		f.close()
	
	def stop(self):
		self.sync()
	
	def sync(self):
		self.syncMetadata()
	
	def storeValue(self, dt, key, value):
		d = self.getDictionaryFor(key)
		d[dt] = value
	
	def storeValues(self, dt, values_dict):
		for key in values_dict:
			value = values_dict[key]
			self.storeValue(dt, key, value)
			
	def storeTimestamp(self, dt):
		with self.timestamps_lock:
			self.timestamps.append(dt)	
			self.writeTimestamp(dt)
	
	def epochBegin(self, epochName, dt):
		with open(self.epoch_file_path, "a") as f:
			timestamp = dt
			time_str = datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
			line = "%s,%s," % (epochName, time_str)
			f.write(line)
	
	def epochEnd(self, dt):
		with open(self.epoch_file_path, "a") as f:
			timestamp = dt
			time_str = datetime.strftime(timestamp, "%Y-%m-%d %H:%M:%S.%f")
			line = "%s\n" % (time_str)
			f.write(line)
	
	def getLiveResults(self):
		# snapshot the timestamps here
		timestamps = list(self.timestamps)
		
		# copy the list of nodes
		nodes = list(self.nodes)
		
		return GCResultsDiskLive(dictionaries=self.dictionaries, timestamps=timestamps, nodes=nodes)
	
	def getLastValue(self, key):
		return self.getDictionaryFor(key)[self.timestamps[-1]]
	
	def getLastTextData(self, key):
		return self.getDictionaryFor(key)[self.timestamps[-1]]

	# after this are internal helpers
	
	def getDictionaryFor(self, value):
		if value not in self.dictionaries:
			filePath = "%s/%s" % (self.path, value)
			self.dictionaries[value] = DiskBackedDictionary(filePath, mode=DiskBackedDictionary.READWRITE | DiskBackedDictionary.LAZYSYNC)
		
		return self.dictionaries[value]

	def writeTimestamp(self, dt):
		timestamp_file = "%s/%s" % (self.path, "timestamps")
		with open(timestamp_file, "a") as f:
			f.write("%s\n" % datetime.strftime(dt, "%Y-%m-%d %H:%M:%S.%f"))
	
	def pickleTimestamps(self):
		timestamps_synced_file = "%s/%s" % (self.path, "timestamps-synced")
		
		with open(timestamps_synced_file, "wb") as f, self.timestamps_lock:
			cPickle.dump(self.timestamps, f)
			os.fsync(f.fileno())
			
	def syncMetadata(self):
		with LockFile("%s/sync.lock" % self.path):
			for key in self.dictionaries:
				self.dictionaries[key].sync()
			
			self.pickleTimestamps()

class SQLiteDataStore(GCDataCollectorDataStore):
	
	def __init__(self, path, nodes):
		self.path = path
		self.nodes = nodes
		self.keys = set()
		self.currentEpoch = None
		
	#	self.con = gcsqlite.getSQLiteConn("%s/sqlite-data.db" % self.path)
	
	def getCon(self):
		return gcsqlite.getSQLiteConn("%s/sqlite-data.db" % self.path, timeout=30)
	
	def start(self):
		# build nodes table
		con = self.getCon()
		with con:
			con.execute("create table nodes(node)")
			for node in self.nodes:
				con.execute("insert into nodes(node) values (?)", (node,))
		
		# create epochs table
		with con:
			con.execute("create table epochs(name, begin, end)")
		
		# create timestamps table
		with con:
			con.execute("create table timestamps(timestamp timestamp)")
			
		# create data table
		with con:
			con.execute("create table data(timestamp timestamp primary key)")
			
		with con:
			con.execute("create table keys(key)")
			
	def stop(self):
		pass
	
	def sync(self):
		pass
	
	def storeValue(self, dt, key, value):
		key = self.filterKey(key)
		con = self.getCon()
		with con:
			#self.initTable(key, value, con)
			self.checkOrAddColumn(key, value, con)
			
			try:
				#con.execute("insert or replace into '%s'(timestamp, value) values(?,?)" % key, (dt, value))
				con.execute("update data set %s = ? where timestamp = ?" % key, (value, dt))
			except Exception as e:
				print e, dt, key, value
	
	def storeValues(self, dt, values_dict):
		con = self.getCon()
		with con:	
			con.execute("insert or replace into data(timestamp) values(?)",  (dt,))
			
			for key in values_dict:
				value = values_dict[key]
				key = self.filterKey(key)
				
				#self.initTable(key, value, con)
				self.checkOrAddColumn(key, value, con)
				
				try:
					#con.execute("insert or replace into '%s'(timestamp, value) values(?,?)" % key, (dt, value))
					con.execute("update data set %s = ? where timestamp = ?" % key, (value, dt))
				except Exception as e:
					print e, dt, key, value
					
	def storeTimestamp(self, dt):
		con = self.getCon()
		
		with con:
			con.execute("insert into timestamps(timestamp) values(?)",  (dt,))
	
	def epochBegin(self, epochName, dt):
		self.currentEpoch = epochName
		con = self.getCon()
		with con:
			con.execute("insert into epochs(name, begin) values (?, ?)", (epochName, dt))
	
	def epochEnd(self, dt):
		con = self.getCon()
		with con:
			con.execute("update epochs set end=? where name=?", (dt, self.currentEpoch))
		
		self.currentEpoch = None
	
	def getLiveResults(self):
		return GCResultsSQL(self.path)
	
	def getLastValue(self, key):
		return self.getValue(self.getMaxTimestamp(), key)
	
	def getLastTextData(self, key):
		return self.getValue(self.getMaxTimestamp(), key)
	
	# helpers
	# need to call this with a connection held
	def initTable(self, key, value, con):
		if key not in self.keys:
			if type(value) in gcsqlite.special_types:
				valueString = "value %s" % gcsqlite.special_types[type(value)].name
			else:
				valueString = "value"
			
			sql = "create table '%s'(timestamp timestamp primary key, %s)" % (key, valueString)
			#print sql
			con.execute(sql)
			self.keys.add(key)
	
	def checkOrAddColumn(self, key, value, con):
		if key not in self.keys:
			if type(value) in gcsqlite.special_types:
				valueString = "%s %s" % (key, gcsqlite.special_types[type(value)].name)
			else:
				valueString = "%s" % key
				
			sql = "alter table data add column %s" % valueString
			con.execute(sql)
			
			sql = "insert into keys(key) values(?)"
			con.execute(sql, (key,))
			
			self.keys.add(key)
			
	def getMaxTimestamp(self):
		con = self.getCon()
		cur = con.cursor()
		#cur.execute("select timestamp as \"timestamp [timestamp]\" from timestamps order by timestamp desc limit 1")
		cur.execute("select timestamp from timestamps order by timestamp desc limit 1")
		for row in cur:
			print "max timestamp", row["timestamp"]
			return row["timestamp"]
		
	def getValue(self, dt, key):
		key = self.filterKey(key)
		con = self.getCon()
		cur = con.cursor()
		#cur.execute("select value from '%s' where timestamp=?" % key, (dt, ))
		sql = "select %s from data where timestamp=?" % key
		#print sql
		cur.execute(sql, (dt, ))
		
		for row in cur:
			return row[key]
	
	def filterKey(self, key):
		return key.replace(".", "$")
	
class SqlitedictDataStore(GCDataCollectorDataStore):
	def __init__(self, path, nodes):
		self.path = os.path.realpath(path)
		self.currentEpoch = None
		self.sqlitedicts = {}
		self.timestamps = []
		self.epochs = {}
		#self.keys = set()
		
		#self.dbfile = os.path.realpath("%s/sqlitedict.db" % self.path)
		#print self.dbfile
		# use this to bootstrap the results
		#self.keydict = sqlitedict.SqliteDict("%s/sqlitedict.db", tablename="keys", autocommit=True, flag='c')
		
		# create a dict for metadata
		self.metadata = sqlitedict.SqliteDict("%s/metadata.db" % self.path, autocommit=True, flag='c')

		# create a dict for ep
		# store the nodes
		self.metadata["nodes"] = sorted(nodes)
	
	def start(self):
		pass
	
	def stop(self):
		pass
		
	def sync(self):
		pass
	
	def storeValue(self, dt, key, value):
		d = self.getDictionaryFor(key)
		d[dt] = value
		
	def storeValues(self, dt, values_dict):
		for key in values_dict:
			#internalKey = key.replace(".", "$")
			#d = self.getDictionaryFor(key)
			#d[dt] = values_dict[key]
			self.storeValue(dt, key, values_dict[key])
		
	def storeTimestamp(self, dt):
		self.timestamps.append(dt)
		self.metadata["timestamps"] = self.timestamps
	
	def epochBegin(self, epochName, dt):
		#print "Began epoch %s @ %s" % (epochName, dt)
		
		#d = self.getDictionaryFor("epochs")
		self.epochs[epochName] = (dt, None)
		self.currentEpoch = epochName
	
	def epochEnd(self, dt):
		#print "Ended epoch %s @ %s" % (self.currentEpoch, dt)
		
		#d = self.getDictionaryFor("epochs")
		start, end = self.epochs[self.currentEpoch]
		self.epochs[self.currentEpoch] = (start, dt)
		self.currentEpoch = None
		self.metadata["epochs"] = self.epochs
	
	def getLiveResults(self):
		return GCResultsSqliteLive(self.sqlitedicts, self.metadata)
	
	def getLastValue(self, key):
		#internalKey = key.replace(".", "$")
		dt = self.timestamps[-1]
		d = self.getDictionaryFor(key)
		
		return d[dt]
	
	def getLastTextData(self, key):
		#internalKey = key.replace(".", "$")
		dt = self.timestamps[-1]
		d = self.getDictionaryFor(key)
		
		return d[dt]
	
	def getValue(self, key, dt):
		d = self.getDictionaryFor(key)
		return d[dt]

	# internal helpers
	def getDictionaryFor(self, key):
		if key in self.sqlitedicts:
			return self.sqlitedicts[key]
		else:
			try:
				dbfile = "%s/%s.db" % (self.path, key)
				newDict = sqlitedict.SqliteDict(dbfile, autocommit=True, flag='c')
			except Exception as e:
				print dbfile
				print e
				
			#print newDict
			#sys.exit()
		
			self.sqlitedicts[key] = newDict
			#self.keys.add(key)
			self.metadata["keys"] = sorted(list(self.sqlitedicts.keys()))
			return newDict
		
# new GCDataCollector is be a singleton class with individual threads inside
# we use new DiskBackedDictionary to store the data by default
class GCDataCollector(GCSingleton):

	class GCDataCollectorMainThread(Thread):
		def __init__(self, collector):
			Thread.__init__(self)
			self.daemon = True
			
			self.collector = collector
			self.stopFlag = False
			self.timestampSet = set()
			
			self.logger = GCLogger.getInstance().get_logger(self)

		def run(self):
			while not self.stopFlag:
				start_dt = datetime.now()
				sample_dt = ExperimentClock.getInstance().get_current_time()
				
				self.logger.log("Starting data collection cycle")
								
				managerThreads = {}
				managerResults = {}
				
				# create a new condition variable store for this cycle
				self.collector.startCycle(sample_dt)

				doneQueue = Queue()

				# start a manager thread for each manager
				for manager in self.collector.managers:
					t = GCDataCollector.GCDataCollectorManagerThread(manager, self.collector, sample_dt, doneQueue)
					managerThreads[manager] = t
					t.start()

				# join each manager thread
				# and collect the results
				#for manager in self.collector.managers:
				#	t = managerThreads[manager]
				#	t.join()
				#	for key in t.results:
				#		managerResults[key] = t.results[key]

				# new code to do this that doesn't require the threads to join in order
				# we need this to support managers that have dependencies on other managers'
				# current values
				for i in xrange(0, len(managerThreads)):
					t = doneQueue.get()
					assert t in managerThreads.values()
					t.join()
					#print "joined", t
					#print "saving results", t.results.keys(), t.results.values()
					self.collector.data_store.storeValues(sample_dt, t.results)

					for key in t.results:
						#print "signaling", key
						self.collector.cycleConditionStore.signalCondition(key)

				# store values
				# TODO: check to make sure that moving this into the previous loop is ok
				#self.collector.data_store.storeValues(sample_dt, managerResults)
				
				# don't record a duplicate timestamp more than once
				# this is mainly to avoid duplicates during warmup
				if sample_dt not in self.timestampSet:
					self.timestampSet.add(sample_dt)
					self.collector.data_store.storeTimestamp(sample_dt)

				end_dt = datetime.now()
				collection_delta = end_dt - start_dt
				
				self.logger.log("Finished data collection cycle, elapsed time %0.2f seconds" % (collection_delta.total_seconds()))
				
				sleep_delta = timedelta(seconds=self.collector.interval) - collection_delta
				sleep_time = sleep_delta.total_seconds()
				
				if sleep_time >= 0:
					time.sleep(sleep_time)
			
			thread.exit()
			
		def stop(self):
			self.stopFlag = True
			
	class GCDataCollectorManagerThread(Thread):
		def __init__(self, manager, collector, sample_dt, doneQueue):
			Thread.__init__(self)
			self.daemon = True
			self.manager = manager
			self.collector = collector
			self.sample_dt = sample_dt
			self.doneQueue = doneQueue
			self.logger = GCLogger.getInstance().get_logger(self)
			
			self.results = {}
			
		def run(self):
			start = datetime.now()
			values = self.manager.getValuesForLogging()
			textValues = self.manager.getTextDataForLogging()
			end = datetime.now()
			elapsed = (end - start).total_seconds()
			self.logger.log("Finished collecting values for %s, elapsed time %0.2f seconds" % (self.manager.__class__.__name__, elapsed))
			
			for key in values:
				#d = self.collector.getDictionaryFor(key)
				#d[self.sample_dt] = values[key]
				#self.collector.data_store.storeValue(self.sample_dt, key, values[key])
				self.results[key] = values[key]

			for key in textValues:
				#d = self.collector.getDictionaryFor(key)
				#d[self.sample_dt] = textValues[key]
				#self.collector.data_store.storeValue(self.sample_dt, key, testValues[key])
				self.results[key] = textValues[key]

			# put ourselves on the done queue
			self.doneQueue.put(self)

			thread.exit()

	class GCDataCollectorSyncThread(Thread):
		def __init__(self, collector, interval=timedelta(minutes=5)):
			Thread.__init__(self)
			self.daemon = True
			self.collector = collector
			self.interval = interval.total_seconds()
			self.logger = GCLogger.getInstance().get_logger(self)
			self.stopFlag = False
			
		def run(self):
			while not self.stopFlag:
				time.sleep(self.interval)
				self.logger.log("Starting data sync...")
				start = datetime.now()
				self.collector.data_store.sync()
				elapsed = (datetime.now() - start).total_seconds()
				self.logger.log("Finished data sync, elapsed time %0.2f seconds" % elapsed)
			
			thread.exit()
			
		def stop(self):
			self.stopFlag = True

	class GCDataCollectorConditionStore(object):
		def __init__(self):
			self.lock = Lock()
			self.conditionFlags = {}
			self.conditionVariables = {}

		# this MUST be called with lock held
		def initConditionVariable(self, key):
			if key not in self.conditionVariables:
				self.conditionVariables[key] = Condition()
				self.conditionFlags[key] = False

		def waitCondition(self, key):
			#print "waiting condition", key
			with self.lock:
				self.initConditionVariable(key)

			condition = self.conditionVariables[key]
			condition.acquire()
			while not self.conditionFlags[key]:
				condition.wait()
			condition.release()
			#print "done"

		# this cannot be called before initConditionVariable for a particular key
		def signalConditionCore(self, key):
			condition = self.conditionVariables[key]
			condition.acquire()
			self.conditionFlags[key] = True
			condition.notifyAll()
			condition.release()

		def signalCondition(self, key):
			with self.lock:
				self.initConditionVariable(key)

			self.signalConditionCore(key)

		def signalMultipleConditions(self, keys):
			with self.lock:
				for key in keys:
					self.initConditionVariable(key)

			for key in keys:
				self.signalConditionCore(key)

	def __init__(self, experiment_path,  data_store=SqlitedictDataStore, interval=RESULTS_INTERVAL, nodes=MINIMUM_NODES+OPTIONAL_NODES):
		super(GCDataCollector, self).__init__(GCDataCollector)

		self.path = experiment_path.getPath()
		self.interval = interval
		self.nodes = nodes
		self.managers = []

		# init the data store
		self.data_store = data_store(self.path, self.nodes)

		self.cycleConditionStore = None
		self.cycleDt = None
		self.cycleLock = Lock()

	def register(self, manager):
		self.managers.append(manager)
	
	def start(self):
		self.data_store.start()
		
		self.collectorThread = GCDataCollector.GCDataCollectorMainThread(self)
		self.collectorThread.start()
		
		self.syncThread = GCDataCollector.GCDataCollectorSyncThread(self)
		self.syncThread.start()
	
	def stop(self):
		self.syncThread.stop()
		self.collectorThread.stop()
		self.collectorThread.join()

		self.data_store.stop()

	def getExperimentPath(self):
		return self.path

	def getLastValue(self, key):
		return self.data_store.getLastValue(key)

	def getLastTextData(self, key):
		return self.data_store.getLastTextData(key)

	def epochBegin(self, epochName):
		self.data_store.epochBegin(epochName, ExperimentClock.getInstance().get_current_time())

	def epochEnd(self):
		self.data_store.epochEnd(ExperimentClock.getInstance().get_current_time())
	
	def getLiveResults(self):
		return self.data_store.getLiveResults()

	def startCycle(self, dt):
		with self.cycleLock:
			self.cycleDt = dt
			self.cycleConditionStore = GCDataCollector.GCDataCollectorConditionStore()

	def getCurrentValue(self, key):
		with self.cycleLock:
			self.cycleConditionStore.waitCondition(key)
			v = self.data_store.getValue(key, self.cycleDt)
		return v

	def getCurrentValues(self, keys):
		values = {}
		with self.cycleLock:
			for key in keys:
				self.cycleConditionStore.waitCondition(key)
				v = self.data_store.getValue(key, self.cycleDt)
				values[key] = v

		return values

import random
class RandomManager(GCManagerBase):
	def __init__(self):
		pass
	
	def getValuesForLogging(self):
		d = {}
		d["random"] = random.random()
		return d
	
	def getTextDataForLogging(self):
		return {}
	

class ConstManager(GCManagerBase):
	def __init__(self, value=5, node=None):
		self.value = value
		self.node = node
	
	def getValuesForLogging(self):
		d = {}
		key = "const"
		if self.node:
			key = key + "." + self.node
			
		d[key] = self.value
		return d
	
	def getTextDataForLogging(self):
		return {}

class IncreasingManager(GCManagerBase):
	def __init__(self, base=0, incr=1):
		self.base = base
		self.incr = incr
		self.current = self.base

	def getValuesForLogging(self):
		time.sleep(2)
		d = {}
		d["incr"] = self.current
		self.current += self.incr
		return d

class MultiplierManager(GCManagerBase):
	def __init__(self, value="incr", multiplier=2):
		self.value = value
		self.multiplier = multiplier
		self.dataCollector = GCDataCollector.getInstance()

	def getValuesForLogging(self):
		d = {}
		v = self.dataCollector.getCurrentValue(self.value)
		d["%s*%d" % (self.value, self.multiplier)] = v * self.multiplier
		return d

if __name__ == "__main__":
	def sigint(signum, frame):
		collector.stop()

	from gcdataloc import GCLegacyExperimentPath
	from gclogger import GCNullLogger
	import os

	logger = GCNullLogger()

	signal.signal(signal.SIGINT, sigint)

	#clock = ExperimentClock(datetime.now(), 1)

	path = GCLegacyExperimentPath(".", "testexper")
	path.makeDirs()
	collector = GCDataCollector(path, interval=5, data_store=SqlitedictDataStore)

	#manager = GCYCSBManager([YCSB_HOSTNAME], GCNullLogger())
	manager = RandomManager()
	manager2 = ConstManager()
	manager3 = ConstManager(value=42, node="sol032")

	multiplierManager = MultiplierManager()
	incrManager = IncreasingManager()

	collector.register(multiplierManager)
	collector.register(manager)
	collector.register(manager2)
	collector.register(manager3)
	collector.register(incrManager)

	#from gcycsblatencymanager import GCYCSBLatencyManager
	#manager = GCYCSBLatencyManager(["crypt11"])
	#collector.register(manager)
	collector.start()

	import code
	code.interact(local=locals())

	'''
	time.sleep(10)
	collector.epochBegin("one")
	time.sleep(10)
	collector.epochEnd()
	time.sleep(10)
	collector.epochBegin("two")
	time.sleep(10)
	collector.epochEnd()
	time.sleep(10)
	collector.epochBegin("three")
	time.sleep(10)
	collector.epochEnd()
	time.sleep(10)
	'''

	'''
	while True:
		liveData = collector.getLiveData()
		results = GCResults(liveData=liveData)
		
		print len(results.availableTimestamps())
		#print results.availableCommonValues()
		if len(results.availableTimestamps()) > 0:
			ts = results.availableTimestamps()[-1]
			print results.getCommonValue(ts, "readlatency_99_window")
		
		time.sleep(5)
		
	collector.stop()
	'''
	#collector.join()
		
		
		
