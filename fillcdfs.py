#!/usr/bin/python

import sys
from gccdf import *

if __name__ == "__main__":
	model = sys.argv[1]
	node_list = []
	load_list = []
	
	data = {}
	
	for filename in sorted(os.listdir(".")):
		if filename.startswith('%s-' % model) and filename.endswith('.cdf'):
			try:
				# Get nodes and load
				match_str = '%s-(\d+)-(\d+).0.cdf' % model
				nodes, load = re.match(match_str, filename).groups()
				nodes = int(nodes)
				load = int(load)
				node_list.append(nodes)
				load_list.append(load)
				
				print nodes, load
				
				# Read CDF
				with open("." + '/' +filename) as fcdf:
					cdf = readCDF(fcdf)
					
				
				if nodes not in data:
					data[nodes] = {}
					
				
				data[nodes][load] = cdf

			except Exception, e:
				print "Cannot parse file", filename
				print e
	
	maxes = []
	for node in sorted(data):
		loads = data[node]
		keys = sorted(loads.keys())
		
		minimumLoad = keys[0]
		maximumLoad = keys[-1]
		maxes.append(maximumLoad)
		
		for load in xrange(minimumLoad, maximumLoad+1, 250):
			if load in loads:
				continue
			
			lowLoad = find_le(keys, load)
			highLoad = find_gt(keys, load)
			
			new = interpolateCDFs(lowLoad, loads[lowLoad], highLoad, loads[highLoad], load)
			loads[load] = new

		for load in sorted(loads):
			print node, load, loads[load]
			
	globalMaximumLoad = max(maxes)
	
	for node in sorted(data):
		loads = data[node]
		keys = sorted(loads.keys())
		
		maximumLoad = keys[-1]

		first = keys[-2]
		last = keys[-1]
		
		for load in xrange(maximumLoad+250, globalMaximumLoad+1, 250):
			extrapolated = extrapolateCDFs(first, loads[first], last, loads[last], load)
			loads[load] = extrapolated
			print "extrapolated", node, load, extrapolated

	output = model + "-EXTENDED"
	testModelFile = model + "-TEST-cdf.model"
	testF = open(testModelFile, "w")
	
	for node in sorted(data):
		loads = data[node]
		
		for load in sorted(loads):
			if load < 1000:
				continue
			
			outputFile = "%s-%d-%d.0.cdf" % (output, node, load)
			
			with open(outputFile, "w") as f:
				print node, load, data[node][load]
				testF.write("%d\t%d\t%0.2f\n" % (node, load, float(str(data[node][load]))))
				data[node][load].write(f)
	
	testF.close()
			