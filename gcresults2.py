#!/usr/bin/python

import sys
import os
from gccommon import *
from gchelpers import *
from datetime import datetime
from datetime import timedelta

def load_time_values(filename):
	new_dict = {}
	try:
		file = open(filename, "r")
	except:
		return None
		
	data = file.read()
	lines = data.strip().split("\n")
	for line in lines:
		fields = line.split(" ")
		timestamp = fields[0] + " " + fields[1]
		dt = parse_timestamp(timestamp)
		value = float(fields[2])
		new_dict[dt] = value

	return new_dict

class GCResultsDep:
	COMMON = 1
	NODE = 2
	
	def __init__(self, value, type, synth=False):
		self.value = value
		self.type = type
		self.synth = synth

class GCResults:
	COMMON_VALUES = ["actual_workload", "target_workload", "solar", "used_solar", "ycsb.target", "ycsb.throughput", "target_workload_normalized",  "nodes_required", "readlatency_95", "writelatency_95", "readlatency_avg", "writelatency_avg", "readlatency_95_cum", "writelatency_95_cum"]
	NODE_VALUES = ["state", "power", "load_one", "cpu_user", "cpu_system", "cassandra_load", "cassandra_read_rate", "cassandra_write_rate", "cassandra_read_latency", "cassandra_write_latency", "cassandra_total_compactions", "jvm_parnew_time", "jvm_parnew_count", "jvm_cms_time", "jvm_cms_count"]

	SYNTH_COMMON_VALUES = {
		"power_transition" : "powerTransition",
		"power_ready" : "powerReady",
		"power_total" : "powerTotal",
		"solar_consumed" : "solarConsumed",
		"solar_excess" : "solarExcess",
		"brown_consumed" : "brownConsumed",
		"nodes_off" : "nodesOff",
		"nodes_transition" : "nodesTransition",
		"nodes_ready" : "nodesReady",
		"nodes_total" : "nodesTotal",
		"cassandra_cluster_load" : "cassandraClusterLoad",
		"cluster_cpu_total" : "clusterCpuTotal"
	}
	SYNTH_NODE_VALUES = {
		"cpu_total" : "cpuTotal"
	}
	
	SYNTH_VALUE_DEPS = {
		"power_transition" : [
			GCResultsDep("state", GCResultsDep.NODE),
			GCResultsDep("power", GCResultsDep.NODE)
		],
		"power_ready" : [
			GCResultsDep("state", GCResultsDep.NODE),
			GCResultsDep("power", GCResultsDep.NODE)
		],
		"power_total" : [
			GCResultsDep("power_transition", GCResultsDep.COMMON, synth=True),
			GCResultsDep("power_ready", GCResultsDep.COMMON, synth=True)
		],
		"solar_consumed" : [
			GCResultsDep("power_total", GCResultsDep.COMMON, synth=True),
			GCResultsDep("used_solar", GCResultsDep.COMMON)
		],
		"solar_excess" : [
			GCResultsDep("power_total", GCResultsDep.COMMON, synth=True),
			GCResultsDep("used_solar", GCResultsDep.COMMON)
		],
		"brown_consumed" : [
			GCResultsDep("power_total", GCResultsDep.COMMON, synth=True),
			GCResultsDep("used_solar", GCResultsDep.COMMON)
		],
		"nodes_off" : [
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"nodes_transition" : [
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"nodes_ready" : [
			GCResultsDep("state", GCResultsDep.NODE),
		],
		"nodes_total" : [
			GCResultsDep("nodes_transition", GCResultsDep.COMMON, synth=True),
			GCResultsDep("nodes_ready", GCResultsDep.COMMON, synth=True)
		],
		"cassandra_cluster_load" : [
			GCResultsDep("cassandra_load", GCResultsDep.NODE),
		],
		"cpu_total" : [
			GCResultsDep("cpu_user", GCResultsDep.NODE),
			GCResultsDep("cpu_system", GCResultsDep.NODE)
		],
		"cluster_cpu_total" : [
			GCResultsDep("cpu_total", GCResultsDep.NODE, synth=True),
		]
		
	}

	nodes = None
	timestamps = None
	node_values = None
	common_values = None
	cached_synth_common_values = None
	cached_synth_node_values = None
	
	common_values_available = None
	node_values_available = None
	
	def __init__(self, data_dir, nodes=None, required=None):
		# load which nodes we have data for
		if nodes == None:
			nodes_file = "%s/nodes" % data_dir
			if os.path.exists(nodes_file):
				f = open(nodes_file)
				data = f.read()
				f.close()
				nodes = data.strip().split("\n")
				self.nodes = sorted(nodes)
			else:
				print "No nodes file, extrapolating..."
				tmp = []
				files = os.listdir(data_dir)
				for file in files:
					if file.startswith("state."):
						prefix, node = file.split(".")
						tmp.append(node)
					self.nodes = sorted(tmp)
		else:
			self.nodes = sorted(nodes)

		if required == None:
			self.requiredList = self.COMMON_VALUES + self.NODE_VALUES + self.SYNTH_COMMON_VALUES.keys() + self.SYNTH_NODE_VALUES.keys()
		else:
			self.requiredList = self.buildRequiredList(required)
		
		print "GCResults: required: ", self.requiredList
		
		# load data for individual nodes
		self.node_values = {}
		self.node_values_available = {}
		for node in self.nodes:
			self.node_values[node] = {}
			self.node_values_available[node] = []

			for value in self.NODE_VALUES:
				if value not in self.requiredList:
					continue
				
				values = load_time_values("%s/%s.%s" % (data_dir, value, node))
				if values != None:
					self.node_values[node][value] = values
					self.node_values_available[node].append(value)
				else:
					if value in self.requiredList:
						raise Exception("Could not load required node value: %s.%s" % (node, value))

		# load data common for all nodes
		self.common_values = {}
		self.common_values_available = []
		for value in self.COMMON_VALUES:
			if value not in self.requiredList:
					continue
			
			values = load_time_values("%s/%s" % (data_dir, value))
			if values != None:
				self.common_values[value] = values
				self.common_values_available.append(value)
			else:
				if value in self.requiredList:
					raise Exception("Could not load required common value: %s" % value)

		# init synth cache for common values
		self.cached_synth_common_values = {}
		for value in self.SYNTH_COMMON_VALUES.keys():
			if value not in self.requiredList:
					continue
			
			self.cached_synth_common_values[value] = {}

		# init synth cache for node values
		self.cached_synth_node_values = {}
		for node in self.nodes:
			self.cached_synth_node_values[node] = {}
			for value in self.SYNTH_NODE_VALUES.keys():
				if value not in self.requiredList:
					continue
				
				self.cached_synth_node_values[node][value] = {}

		# save available timestamps
		if len(self.availableCommonValues()) > 0:
			self.timestamps = sorted(self.common_values[self.availableCommonValues()[0]].keys())
			#self.timestamps = sorted(self.common_values[self.requiredList[2]].keys())
		elif len(self.availableNodeValues()) > 0:
			if len(self.nodes) < 1:
				raise Exception("No values...")
			self.timestamps = sorted(self.node_values[self.nodes[0]][self.availableNodeValues()[0]].keys())
	
		print "GCResults: data loaded"
		
	def resolveDependencies(self, value, requiredList, depth=0):
		prefix = ""
		for i in xrange(0, depth):
			prefix += "... "

		requiredList.append(value)
		deps = self.SYNTH_VALUE_DEPS[value]
		for dep in deps:
			#print "%sResolving synth: %s" % (prefix, dep.value)
			if dep.synth == False and dep.value not in requiredList:
			#	print "%sDep %s is native, adding..." % (prefix, dep.value)
				requiredList.append(dep.value)
			elif dep.synth == True:
			#	print "%sDep %s is synth, recursing..." % (prefix, dep.value)
				self.resolveDependencies(dep.value, requiredList, depth+1)
		return

	def buildRequiredList(self, required_values):
		requiredList = []
		
		# lets assume that native values (non synth values) don't need to be enumerated
		# we can just add them to the list
		
		for value in required_values:
			if value in self.SYNTH_COMMON_VALUES.keys() or value in self.SYNTH_NODE_VALUES.keys():
				self.resolveDependencies(value, requiredList)
			else:
				requiredList.append(value)
			
			'''
			if value in self.COMMON_VALUES or value in self.NODE_VALUES:
			#	print "Value is native: adding %s to list..." % value
				requiredList.append(value)
			else:
			#	print "Value is synth: resolving dependencies for %s..." % value
				self.resolveDependencies(value, requiredList)
			'''
		return requiredList	
	
	# user functions
	def availableCommonValues(self):
		return sorted(self.common_values_available)

	def availableNodeValues(self):
		available = []
		for value in self.NODE_VALUES + self.SYNTH_NODE_VALUES.keys():
			if value in self.requiredList:
				available.append(value)
		
		return sorted(available)

	def availableTimestamps(self):
		return self.timestamps

	def availableNodes(self):
		return self.nodes

	def getCommonValue(self, dt, value):
		#if dt not in self.timestamps:
		#	return None

		if value in self.common_values_available:
			return self.common_values[value][dt]

		elif value in self.SYNTH_COMMON_VALUES.keys():
			return self.getSynthCommonValue(dt, value)

		else:
			return None
			
	def getNodeValue(self, dt, node, value):
		#if dt not in self.timestamps or node not in self.nodes:
		#	return None
		
		if value in self.node_values_available[node]:
			return self.node_values[node][value][dt]
		elif value in self.SYNTH_NODE_VALUES.keys():
			return self.getSynthNodeValue(dt, node, value)
		else:
			return None

	# internal functions to dereference value functions
	def getSynthCommonValue(self, dt, value):
		try:		
			#if dt in self.cached_synth_common_values[value].keys():
			v = self.cached_synth_common_values[value][dt]
		except KeyError:
			#else:
			func = getattr(self, self.SYNTH_COMMON_VALUES[value])
			v = func(dt)
			self.cached_synth_common_values[value][dt] = v

		return v

	def getSynthNodeValue(self, dt, node, value):
		try:
			#if dt in self.cached_synth_node_values[node][value].keys():
			v = self.cached_synth_node_values[node][value][dt]
		except KeyError:
			#else:
			func = getattr(self, self.SYNTH_NODE_VALUES[value])
			v = func(dt, node)
			self.cached_synth_node_values[node][value][dt] = v
		
		return v

	# Synth value implementation functions
	# Reference to any other values used for composition must be made through get***Value function(s).

	# power_transition
	def powerTransition(self, dt):
		power = 0.0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 1:
				power += self.getNodeValue(dt, node, "power")
		return power

	# power_ready
	def powerReady(self, dt):
		power = 0.0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 2:
				power += self.getNodeValue(dt, node, "power")
		return power

	# power_total
	def powerTotal(self, dt):
		return self.getCommonValue(dt, "power_transition") + self.getCommonValue(dt, "power_ready")

	# solar_consumed
	def solarConsumed(self, dt):
		power_total = self.getCommonValue(dt, "power_total")
		solar_total = self.getCommonValue(dt, "used_solar")

		if solar_total > power_total:
			return power_total
		else:
			return solar_total

	# solar_excess
	def solarExcess(self, dt):
		power_total = self.getCommonValue(dt, "power_total")
		solar_total = self.getCommonValue(dt, "used_solar")

		if solar_total > power_total:
			return solar_total - power_total
		else:
			return 0.0

	# brown_consumed
	def brownConsumed(self, dt):
		power_total = self.getCommonValue(dt, "power_total")
		solar_total = self.getCommonValue(dt, "used_solar")

		if power_total > solar_total:
			return power_total - solar_total
		else:
			return 0.0

	# nodes_off
	def nodesOff(self, dt):
		count = 0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 0:
				count += 1
		return count

	# nodes_transition
	def nodesTransition(self, dt):
		count = 0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 1:
				count += 1
		return count

	# nodes_ready
	def nodesReady(self, dt):
		count = 0
		for node in self.nodes:
			if self.getNodeValue(dt, node, "state") == 2:
				count += 1
		return count

	# nodes_total
	def nodesTotal(self, dt):
		return self.getCommonValue(dt, "nodes_transition") + self.getCommonValue(dt, "nodes_ready")

	# load average of entire cassandra cluster
	def cassandraClusterLoad(self, dt):
		total = 0.0
		for node in self.nodes:
			total += self.getNodeValue(dt, node, "cassandra_load")
		return total / len(self.nodes)
	
	# cpu_total
	def cpuTotal(self, dt, node):
		return self.getNodeValue(dt, node, "cpu_user") + self.getNodeValue(dt, node, "cpu_system")
	
	# cluster_cpu_total
	def clusterCpuTotal(self, dt):
		total = 0.0
		for node in self.nodes:
			total += self.getNodeValue(dt, node, "cpu_total")
		
		return total
	
	# testing functions
	def testOne(self):
		dts = self.availableTimestamps()
		dt = dts[len(dts) / 2]
		node = self.nodes[0]

		print "Common Values:"
		for value in self.availableCommonValues():
			print "%s\t= %s" % (value, self.getCommonValue(dt, value))

		print "Node Values:"
		for value in self.availableNodeValues():
			print "%s[%s]\t= %s" % (value, node, self.getNodeValue(dt, node, value))

	def testAll(self):
		print "Starting test..."
		for dt in self.availableTimestamps():
			for value in self.availableCommonValues():
				v = self.getCommonValue(dt, value)
				if v == None:
					print "Error: ", dt, value
					continue
					
			for node in self.availableNodes():
				for value in self.availableNodeValues():
					v = self.getNodeValue(dt, node, value)
					if v == None:
						print "Error: ", dt, node, value
						continue
		print "Completed test..."
		
if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "Please specify dir..."
		sys.exit()

	results = GCResults(sys.argv[1])
	print "Data loaded..."
	#results.testOne()
	results.testAll()
