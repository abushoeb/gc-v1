#!/usr/bin/python

import math

class Replica(object):
	MIN = 1
	OPT = 2
	
	def __init__(self, region, current=False):
		self.current = current
		self.on = True
		self.region = region
	
	def __str__(self):
		if self.on:
			if self.current:
				return "C"
			else:
				return "S"
		else:
			return "X"
	
	def __repr__(self):
		return self.__str__()
	
	def turnOff(self):
		self.on = False
	
	def turnOn(self):
		self.on = True
		
	def readableGood(self):
		return self.on and self.current
	
	def readableStale(self):
		return self.on and not self.current
	
	def setStale(self):
		self.current = False
	
	def setGood(self):
		self.current = True

class OperationSimulator(object):
	ONE = 1
	QUORUM = 2
	ALL = 3
	GREEN_QUORUM = 4

	def __init__(self, minR, optR, consistency):
		self.consistency = consistency
		
		if self.consistency == ONE:
			self.min_responses = 0
			self.responses = 1
			
		elif self.consistency == QUORUM:
			self.min_responses = 0
			self.responses = quorum(minR + optR)
		
		elif self.consistency == ALL:
			self.min_responses = 0
			self.responses = minR + optR
		
		elif self.consistency == GREEN_QUORUM:
			self.min_responses = quorum(minR)
			self.responses = quorum(minR + optR)
	
class ReadSimulator(OperationSimulator):

	def response(self, replica):
		# only consider on, good replicas
		if replica.readableGood():		
			# if min, decrement min responses
			if replica.region == Replica.MIN:
				self.min_responses -= 1
			
			# always decrement total responses
			self.responses -= 1
	
	def satisfied(self):
		return self.min_responses <= 0 and self.responses <= 0
	
	def read(self, replicas):
		for replica in replicas:
			self.response(replicas)
		
		return self.satisfied()
	
class WriteSimulator(OperationSimulator):

	def write(self, replicas):
		minReplicas = [r for r in replicas if r.region == Replica.MIN]
		optReplicas = [r for r in replicas if r.region == Replica.OPT]
		
		for r in minReplicas:
			if self.min_responses <= 0:
				break
			r.setGood()
			self.min_responses -= 1
			self.responses -= 1
		
		for r in optReplicas:
			if self.response <= 0:
				break
			r.setGood()
			self.responses -= 1
		
def quorum(n):
	return n / 2 + 1

def hasGoodQuorum(replicas):
	
	currentR = 0
	for replica in replicas:
		if replica.on:
			currentR += 1
		
	currentQuorum = quorum(currentR)
	#print "currentR", currentR, "currentQuorum", currentQuorum
	readableStale = 0
	for replica in replicas:
		if replica.readableStale():
			readableStale += 1
		
	return readableStale < currentQuorum
	
def doAnalysis(r):
	for i in xrange(1, r):
		quorumCount = quorum(r)
		
		minSet = []
		optSet = []
		
		minR = i
		optR = r-i
		
		minWait = quorum(minR)
		#minWait = max(minR, quorum(r))
		
		minIdea = math.floor(r / 2.0)
		
		# build the sets
		
		# mins
		for j in xrange(0, minR):
			#minSet.append(Replica(True if quorumCount > 0 else False))
			if minWait > 0:
				minSet.append(Replica(Replica.OPT, True))
				minWait -= 1
				quorumCount -= 1
			else:
				minSet.append(Replica(Replica.OPT, True))
		
		# opts
		for j in xrange(0, optR):
			optSet.append(Replica(Replica.OPT, True if quorumCount > 0 else False))
			quorumCount -= 1
	       
		print "R: %d, minR: %d, optR: %d, quorumR: %d, minIdea: %d" % (r, minR, optR, quorum(r), minIdea)
		print str(minSet), str(optSet), quorum(r)
		
		for j in xrange(1, len(optSet)+1):
			optSet[j-1].turnOff()
			
			if not hasGoodQuorum(minSet) or not hasGoodQuorum(minSet+optSet):
			#if not hasGoodQuorum(minSet+optSet):
				print str(minSet), str(optSet), quorum(r-j), "FAIL"
			else:
				print str(minSet), str(optSet), quorum(r-j)
	
if __name__ == "__main__":
	
	#r = 7
	#doAnalysis(7)
	
	for i in xrange(2, 9):
		doAnalysis(i)
		
		
		
		
	
		
	