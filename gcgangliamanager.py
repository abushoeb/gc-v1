#!/usr/bin/python

import argparse

from gccommon import *
from gcbase import GCManagerBase
from gclogger import GCLogger, GCNullLogger
from datetime import datetime
from parasol.ganglia import *

GANGLIA_URL="http://sol.cs.rutgers.edu/ganglia2/getgangliaxml.php?c=Parasol"

class GCGangliaManager(GCManagerBase):
	
	node_values = ["power", "load_one", "cpu_user", "cpu_system"]
	
	def __init__(self, nodes=(MINIMUM_NODES+OPTIONAL_NODES)):
		self.nodes = nodes
		self.logger = GCLogger.getInstance().get_logger(self)
	
	def getNodeValue(self, value, node, data, values_dict):
		try:
			v = data[node][value]
			key = "%s.%s" % (value, node)
			values_dict[key] = float(v)
		except Exception as e:
			self.logger.log("Error getting value from %s: %s" % (node, e))
			values_dict[key] = float("nan")
	
	def getValuesForLogging(self):
		values = {}
		
		for i in xrange(0, 3):
			try:
				s = readGangliaURL(GANGLIA_URL)
				break
			except:
				continue
		
		data = parseGangliaXML(s)
		
		for node in self.nodes:
			for value in self.node_values:
				self.getNodeValue(value, node, data, values)
		
		values["solar"] = float(data["power"]["solar"])	
		return values

'''
for field in data["power"]:
	#print "%s: %s" % (field, data[field])
	print "%s" % field

for field in data["sol000"]:
	print "%s" % field


print data["sol000"]["power"]
print data["power"]["solar"]
'''

if __name__ == "__main__":
	logger = GCNullLogger()
	manager = GCGangliaManager()
	
	values = manager.getValuesForLogging()
	
	for value in sorted(values.keys()):
		print "values[%s] = %f" % (value, values[value])