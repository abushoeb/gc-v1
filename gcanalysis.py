#!/usr/bin/python

import argparse
import os

from gccommon import *
from gcresults import GCResultsLegacy
from gcresults import GCResultsDisk
from gcresults import GCResultsSQL
from gcresults import GCResultsOverride
from gcresults import GCResultsSqliteDisk
from gcresults import GCResultsType

from gcsolarpredictor import GCQuadraticSolarPredictor
from gcsolarpredictor import GCHistoricalSolarPredictor
from gclogger import GCLogger, GCNullLogger
from experimentclock import ExperimentClock

from datetime import datetime
from datetime import timedelta

class GCStaticSolarResultsOverride(GCResultsOverride):
	def __init__(self):
		super(GCStaticSolarResultsOverride, self).__init__("used_solar", GCResultsOverride.COMMON)

		self.dt = datetime(month=1, day=1, year=2014)
		self.predictor = GCQuadraticSolarPredictor(self.dt, maxSolar=100.0)
		
	def getCommonValue(self, dt):
		correctedDt = datetime(month=self.dt.month, day=self.dt.day, year=self.dt.year, hour=dt.hour, minute=dt.minute, second=dt.second)
		return self.predictor.getSolarAt(correctedDt)
	

class GCHistoricalSolarResultsOverride(GCResultsOverride):
	def __init__(self, dataDt):
		super(GCHistoricalSolarResultsOverride, self).__init__("used_solar", GCResultsOverride.COMMON)
		self.dt = dataDt
		self.predictor = GCHistoricalSolarPredictor(dts=[self.dt], scale=0.35)
	
	def getCommonValue(self, dt):
		correctedDt = datetime(month=self.dt.month, day=self.dt.day, year=self.dt.year, hour=dt.hour, minute=dt.minute, second=dt.second)
		return self.predictor.getSolarAt(correctedDt)

class GCEmptySolarResultsOverride(GCResultsOverride):
	def __init__(self):
		super(GCEmptySolarResultsOverride, self).__init__("used_solar", GCResultsOverride.COMMON)
	
	def getCommonValue(self, dt):	
		return 0.0
	
class GCExperimentProcessor(object):
	
	def __init__(self, reqs, printCacheStats=True):
		parser = argparse.ArgumentParser()
		group = parser.add_mutually_exclusive_group(required=True)
		group.add_argument("--experiment", action="store", type=str)
		group.add_argument("--list", action="store", nargs="+", type=str)
		group.add_argument("--pattern", action="store", type=str)
		group.add_argument("--current", action="store_true")
		
		parser.add_argument("--output", action="store", type=str)
		
		group = parser.add_mutually_exclusive_group()
		group.add_argument("--hours", action="store", type=int)
		group.add_argument("--minutes", action="store", type=int)
		
		parser.add_argument("--subsample", action="store", type=int, default=1)
		parser.add_argument("--solarday", action="store", type=str)
		
		parser.add_argument("--droplast", action="store_true", default=False)
		#parser.add_argument("--nocache", action="store_true", default=False)

		self.parser = parser
		self.reqs = reqs
		self.printCacheStats = printCacheStats
		
		self.clock = ExperimentClock()
		self.logger = GCNullLogger()

	def __del__(self):
		ExperimentClock.delInstance()
		GCLogger.delInstance()

	def setup(self):
		pass
	
	def cleanup(self):
		pass
	
	def getParser(self):
		return self.parser
	
	def getDefaultOutput(self, experiment):
		output = experiment.replace("/", "-")
		if output[0] == '-':
			output = output.replace("-", "", 1)
			
		return output
	
	def processArgs(self):
		args = self.parser.parse_args()
		self.args = args	
		
		self.subsample = args.subsample
		self.droplast = args.droplast
		
		if args.hours:
			self.max_timedelta = timedelta(hours=args.hours)
		elif args.minutes:
			self.max_timedelta = timedelta(minutes=args.minutes)
		else:
			self.max_timedelta = timedelta(hours=24)
		
		if args.solarday == "none":
			self.solarOverride = GCEmptySolarResultsOverride()
		elif args.solarday == "test":
			self.solarOverride = GCStaticSolarResultsOverride()
		elif args.solarday != None:
			ts = datetime.strptime(args.solarday, "%m-%d-%Y")
			self.solarOverride = GCHistoricalSolarResultsOverride(ts)
		else:
			self.solarOverride = None
		
		self.experiments = []
		self.outputs = []
		
		if args.experiment:
			self.experiments.append(args.experiment)
			if args.output:
				self.outputs.append(args.output)
			else:
				output = self.getDefaultOutput(args.experiment)
				self.outputs.append(output)
		elif args.list:
			for experiment in args.list:
				self.experiments.append(experiment)
				self.outputs.append("%s" % (experiment))
		elif args.pattern:
			files = os.listdir(".")
			for f in sorted(files):
				if f.startswith(args.patterm) and not "." in f:
					self.experiments.append(f)
			self.experiments.sort(key=lambda(v):"-".join(reversed(v.split("-"))))
		elif args.current:
			currentExperiment = getCurrentExperiment()
			#print "Using current experiment: %s" % currentExperiment
			self.experiments.append(currentExperiment)
			
			if args.output:
				self.outputs.append(args.output)
			else:
				output = self.getDefaultOutput(currentExperiment)
				self.outputs.append(output)
			
	def processExperiment(self, args, experiment, results, output):
		raise NotImplementedException("This class is intended to be subclassed...")
	
	def run(self):
		self.processArgs()
		self.setup()
		
		assert len(self.experiments) == len(self.outputs)

		for i in xrange(0, len(self.experiments)):
			directory = self.experiments[i]
			data_format = GCResultsType.getType(directory)
				
			if data_format == GCResultsType.OLD_FORMAT:
				results = GCResultsLegacy(self.experiments[i], required=self.reqs, max_time=self.max_timedelta, roundStart=True, debug=False, subsample=self.subsample, drop_last=self.droplast)
			elif data_format == GCResultsType.NEW_FORMAT:
				#print "Using GCResultsDisk"
				results = GCResultsDisk(self.experiments[i], max_time=self.max_timedelta, roundStart=True, subsample=self.subsample, drop_last=self.droplast)
			elif data_format == GCResultsType.SQL_FORMAT:
				results = GCResultsSQL(self.experiments[i], max_time=self.max_timedelta, roundStart=True, subsample=self.subsample, drop_last=self.droplast)
			elif data_format == GCResultsType.SQLITEDICT_FORMAT:
				results = GCResultsSqliteDisk(self.experiments[i], max_time=self.max_timedelta, roundStart=True, subsample=self.subsample, drop_last=self.droplast)
				
			if self.solarOverride != None:
				results.addOverride(self.solarOverride)
			#print results	
			self.processExperiment(self.args, self.experiments[i], results, self.outputs[i])
			if self.printCacheStats:
				print "Results Cache Hit Rate for %s was %0.2f" % (self.experiments[i], results.cache.hitRate())
		
		self.cleanup()
