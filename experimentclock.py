#!/usr/bin/python

from datetime import datetime
from datetime import timedelta
from gcutil import GCSingleton

class ExperimentClock(GCSingleton):
	
	instance = None
	
	def __init__(self, start_time=datetime.now(), accel_rate=1, wait=False):
		super(ExperimentClock, self).__init__(ExperimentClock)
		
		self.accel_rate = accel_rate
		self.start_time = start_time
		self.started = False
		if not wait:
			self.start()
			
		#if ExperimentClock.instance != None:
		#	raise Exception("Singleton class already instantiated")
		#else:
		#	ExperimentClock.instance = self

	#@staticmethod
	#def setInstance(instance):
	#	ExperimentClock.instance = instance
	
	#@staticmethod
	#def getInstance():
	#	return ExperimentClock.instance

	def calc_current_time(self):
		real_now = datetime.now()
		delta = real_now - self.real_start_time
		fake_now = self.start_time + delta*self.accel_rate
		
		return fake_now
	
	def print_current_time(self):
		if not self.started:
			print self.start_time
		else:
			print self.calc_current_time()
	
	def get_current_time(self):
		if not self.started:
			return self.start_time
		else:
			return self.calc_current_time()
	
	def start(self):
		self.real_start_time = datetime.now()
		self.started = True
		
	def __str__(self):
		return "%s [%s]" % (self.get_current_time(), datetime.now())

class ExperimentClockFake(ExperimentClock):
	
	def __init__(self, start_time=datetime.now()):
		super(ExperimentClockFake, self).__init__(start_time=start_time)
		self.current_time = start_time

	def increment_time(self, delta):
		self.current_time += delta
		
	def get_current_time(self):
		return self.current_time
	
	def set_current_time(self, dt):
		self.current_time = dt
	
	def __str__(self):
		return "%s" % self.get_current_time()
	
if __name__ == "__main__":
	
	clock = ExperimentClock(accel_rate=2)
	
	for i in xrange(0, 100):
		print clock.get_current_time()
