#!/usr/bin/python

import argparse
import sys
import os
import random

from gccommon import *
from gcbase import *
from gchelpers import *
from gcplotter import *
from gcycsbplot import *
from gcresults import *
from gcepochs import *

from datetime import datetime
from datetime import timedelta


class Transition(object):
	def __init__(self, tid, node, offDt, offIndex, transDt, transIndex, onDt, onIndex):
		self.tid = tid
		self.node = node
		self.offDt = offDt
		self.offIndex = offIndex
		self.transDt = transDt
		self.transIndex = transIndex
		self.onDt = onDt
		self.onIndex = onIndex
	
	def __str__(self):
		return "Transition(tid=%d, node=%s, offDt=%s, transDt=%s, onDt=%s" % (self.tid, self.node, str(self.offDt), str(self.transDt), str(self.onDt))
	
def integrate(value, min_i, max_i):
	integrated = 0.0
	
	for i in xrange(min_i+1, max_i):
		delta = timestamps[i] - timestamps[i-1]
		seconds = delta.total_seconds()
		
		measurement = results.getCommonValue(timestamps[i], value)
		integrated += measurement * seconds
	
	return integrated
	
def average(value, min_i, max_i):
	averaged = 0.0
	
	accum = 0.0
	count = 0
	for i in xrange(min_i, max_i):
		accum += results.getCommonValue(timestamps[i], value)
		count += 1
				
	averaged = accum / count
	
	return averaged
	
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("--experiments", action="store", nargs="+", required=True)
	parser.add_argument("--hours", action="store", type=int, default=24)
	
	args = parser.parse_args()
	reqs = ["state", "nodes_ready", "actual_workload"]
	
	# find all experiment files
	experiments = []
	for experiment_arg in args.experiments:
		files = os.listdir(".")
		for f in sorted(files):
			if f.startswith(experiment_arg) and not "." in f:
				experiments.append(f)

	#experiments = sorted(experiments, key=lambda(v):"-".join(reversed(v.split("-"))))
	experiments = sorted(experiments, key=lambda(v):"-".join(v.split("-")))
	#print >> sys.stderr, experiments
	
	print "#%9s\t%12s\t%12s\t%8s\t%5s\t%5s\t%5s\t%5s" % ("Off Time", "Missed", "Missed+R", "Trans Time", "Trans Load", "Load Factor", "Nodes",  "Trans Ratio")

	for experiment in experiments:
		print "# %s" % experiment
		
		if args.hours == 0:
			max_timedelta = None
			drop_last = True
		else:
			max_timedelta = timedelta(hours=args.hours)
			drop_last = False
		
		results = GCResults(experiment, required=reqs, debug=False, max_time=max_timedelta, drop_last=drop_last)
		timestamps = results.availableTimestamps()
		max_i = len(timestamps)

		tids = 0
		transitions = []
		
		for node in OPTIONAL_NODES:
			#print "considering %s" % node
			current_i = 0
			while current_i < max_i:
				#current_dt = timestamps[current_i]
				
				# find first off time
				offIndex = next((i for i in xrange(current_i, max_i) if results.getNodeValue(timestamps[i], node, "state") == 0), None)
				if offIndex == None:
					#print "%s never off, done" % node
					break
					
				off_dt = timestamps[offIndex]
				
				transIndex = next((i for i in xrange(offIndex, max_i) if results.getNodeValue(timestamps[i], node, "state") == 1), None)
				if transIndex == None:
					#print "%s off @ %s, never turned back on, no good data" % (node, str(off_dt))
					break
				
				trans_dt = timestamps[transIndex]
				
				
				onIndex = next((i for i in xrange(transIndex, max_i) if results.getNodeValue(timestamps[i], node, "state") == 2), None)
				if onIndex == None:
					#print "%s transition @ %s, never resolved, weird" % (node, str(trans_dt))
					break
				
				on_dt = timestamps[onIndex]
				
				'''
				# if we get here, we have an off, a transition, and an on, we can extrapolate the data
				#print "Off @ %s, transition @ %s, on @ %s, good for data" % (str(off_dt), str(trans_dt), str(on_dt))				
				
				# integrate the load from the off time leading up to the transition period
				offLoadIntegrated = integrate("actual_workload", offIndex, transIndex)
				offDelta = trans_dt - off_dt
				offTimeSeconds = offDelta.total_seconds()
				
				# integrate the load including the transition time
				totalLoadIntegrated = integrate("actual_workload", offIndex, onIndex)
				
				# measure the transition time and avg load from transition to on
				transLoadAvg = average("actual_workload", transIndex, onIndex)
				transDelta = on_dt - trans_dt
				transTimeSeconds = transDelta.total_seconds()
				
				transTimeRatio = transTimeSeconds / offTimeSeconds
				print "%9.2f\t%12.2f\t%12.2f\t%8.2f\t%5.2f\t\t%5.2f" % (offTimeSeconds, offLoadIntegrated, totalLoadIntegrated, transTimeSeconds, transLoadAvg, transTimeRatio)
				'''
				
				transitions.append(Transition(tids, node, off_dt, offIndex, trans_dt, transIndex, on_dt, onIndex))
				tids += 1
				
				current_i = onIndex + 1
			
		transitions.sort(key=lambda(x):x.onDt)
		transitions.reverse()
		
		for i in xrange(0, len(transitions)):
			if i == len(transitions)-1:
				transIndex = transitions[i].transIndex
			else:
				transIndex = max(transitions[i].transIndex, transitions[i+1].onIndex)
			
			offIndex = transitions[i].offIndex
			onIndex = transitions[i].onIndex
			
			trans_dt = timestamps[transIndex]
			off_dt = timestamps[offIndex]
			on_dt = timestamps[onIndex]
			
			# integrate the load from the off time leading up to the transition period
			offLoadIntegrated = integrate("actual_workload", offIndex, transIndex)
			offDelta = trans_dt - off_dt
			offTimeSeconds = offDelta.total_seconds()
			
			# integrate the load including the transition time
			totalLoadIntegrated = integrate("actual_workload", offIndex, onIndex)
			
			# measure the transition time and avg load from transition to on
			transLoadAvg = average("actual_workload", transIndex, onIndex)
			transNodesAvg = average("nodes_ready", transIndex, onIndex)
			
			# generate a "load factor"
			accum = 0.0
			count = 0
			for j in xrange(transIndex, onIndex):
				nodes = results.getCommonValue(timestamps[j], "nodes_ready")
				load = results.getCommonValue(timestamps[j], "actual_workload")
				loadPerNode = load / nodes
				accum += loadPerNode
				count +=1
			transLoadFactor = accum / count
			
			transDelta = on_dt - trans_dt
			transTimeSeconds = transDelta.total_seconds()
			
			transTimeRatio = transTimeSeconds / offTimeSeconds
			print "%9.2f\t%12.2f\t%12.2f\t%8.2f\t%5.2f\t\t%5.2f\t\t%d\t%5.2f" % (offTimeSeconds, offLoadIntegrated, totalLoadIntegrated, transTimeSeconds, transLoadAvg, transLoadFactor, transNodesAvg, transTimeRatio), off_dt, trans_dt, on_dt
