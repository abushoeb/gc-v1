#!/usr/bin/python

import argparse
import math

from gccommon import *
from gcbase import GCManagerBase
from gclogger import GCLogger
from threading import Thread
from datetime import datetime
from pyjolokia import Jolokia
from pyjolokia import JolokiaError

JOLOKIA_URL = "http://%s:7777/jolokia/"

class GCYCSBManager(GCManagerBase):
	
	ADD = 0
	AVG = 1
	CUSTOM = 2
	
	values = [
		("com.yahoo.ycsb.ClientController:type=ClientController", "Target", "ycsb_target", ADD),
		("com.yahoo.ycsb.ClientController:type=ClientController", "CurrentThroughput", "ycsb_throughput", ADD),

		#("com.yahoo.ycsb.ClientController:type=ClientController", "ReadLatency50", "readlatency_50_cum", AVG),
		#("com.yahoo.ycsb.ClientController:type=ClientController", "ReadLatency75", "readlatency_75_cum", AVG),

		("com.yahoo.ycsb.ClientController:type=ClientController", "ReadLatencyAvg", "readlatency_avg_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "ReadLatency90", "readlatency_90_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "ReadLatency95", "readlatency_95_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "ReadLatency99", "readlatency_99_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "ReadLatency999", "readlatency_999_cum", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowReadLatencyAvg", "readlatency_avg_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowReadLatency90", "readlatency_90_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowReadLatency95", "readlatency_95_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowReadLatency99", "readlatency_99_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowReadLatency999", "readlatency_999_window", AVG),
		
		#("com.yahoo.ycsb.ClientController:type=ClientController", "WriteLatency50", "writelatency_50_cum", AVG),
		#("com.yahoo.ycsb.ClientController:type=ClientController", "WriteLatency75", "writelatency_75_cum", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "WriteLatencyAvg", "writelatency_avg_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WriteLatency90", "writelatency_90_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WriteLatency95", "writelatency_95_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WriteLatency99", "writelatency_99_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WriteLatency999", "writelatency_999_cum", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowWriteLatencyAvg", "writelatency_avg_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowWriteLatency90", "writelatency_90_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowWriteLatency95", "writelatency_95_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowWriteLatency99", "writelatency_99_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowWriteLatency999", "writelatency_999_window", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "InsertLatencyAvg", "insertlatency_avg_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "InsertLatency90", "insertlatency_90_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "InsertLatency95", "insertlatency_95_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "InsertLatency99", "insertlatency_99_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "InsertLatency999", "insertlatency_999_cum", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowInsertLatencyAvg", "insertlatency_avg_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowInsertLatency90", "insertlatency_90_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowInsertLatency95", "insertlatency_95_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowInsertLatency99", "insertlatency_99_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowInsertLatency999", "insertlatency_999_window", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "AstyanaxReadLatencyAvg", "readlatency_astyanax_avg_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "AstyanaxReadLatency99", "readlatency_astyanax_99_cum", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "AstyanaxWriteLatencyAvg", "writelatency_astyanax_avg_cum", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "AstyanaxWriteLatency99", "writelatency_astyanax_99_cum", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowAstyanaxReadLatencyAvg", "readlatency_astyanax_avg_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowAstyanaxReadLatency99", "readlatency_astyanax_99_window", AVG),
		
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowAstyanaxWriteLatencyAvg", "writelatency_astyanax_avg_window", AVG),
		("com.yahoo.ycsb.ClientController:type=ClientController", "WindowAstyanaxWriteLatency99", "writelatency_astyanax_99_window", AVG),
		
		#("Speed4J: name=hector-ycsb", "READ.success_/count", "read_count"),
		#("Speed4J: name=hector-ycsb", "READ.success_/avg", "readlatency_avg", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/stddev", "readlatency_stddev", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/min", "readlatency_min", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/max", "readlatency_max", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/50", "readlatency_50", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/75", "readlatency_75", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/90", "readlatency_90", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/95", "readlatency_95", AVG),
		#("Speed4J: name=hector-ycsb", "READ.success_/99", "readlatency_99", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/count", "write_count"),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/avg", "writelatency_avg", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/stddev", "writelatency_stddev", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/min", "writelatency_min", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/max", "writelatency_max", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/50", "writelatency_50", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/75", "writelatency_75", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/90", "writelatency_90", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/95", "writelatency_95", AVG),
		#("Speed4J: name=hector-ycsb", "WRITE.success_/99", "writelatency_99", AVG),
	]
	
	# needs a list of YCSB hosts
	def __init__(self, ycsb_hosts):
		
		self.ycsb_hosts = ycsb_hosts
		self.jolokia_conns = {}
		self.results = {}
		self.logger = GCLogger.getInstance().get_logger(self)
		
	def getJolokiaConn(self, host):
		url = JOLOKIA_URL % host
		jolokia_conn = Jolokia(url, timeout=100)
		for value in self.values:
			mbean, attribute, description, combine = value
			jolokia_conn.add_request(type="read", mbean=mbean, attribute=attribute)
		
		return jolokia_conn
		
	def getValuesFromHostThread(self, host):
		data = None
		values = {}
		jolokia_conn = self.getJolokiaConn(host)
		
		for i in xrange(0, 3):
			try:
				data = jolokia_conn.getRequests()
				break
			except JolokiaError as e:
				#print "Jolokia error"
				#print e
				continue
			except Exception as e:
				#print "Random error getting jolokia data, trying again..."
				#print e
				continue
		
		if data != None:
			for entry in data:
				#print data
				try:
					attribute = entry["request"]["attribute"]
					if entry["value"] == None:
						#print "weird none"
						value = float("NaN")
					else:
						value = float(entry["value"])
				except KeyError:
					#return None
					value = float("NaN")
				except TypeError:
					return None
				
				values[attribute] = value
		else:
			values = None
		
		self.results[host] = values

	def setValue(self, host, valueToSet, value):
		#print "setting", valueToSet, "to", value
		jolokia_conn = self.getJolokiaConn(host)
		
		for i in xrange(0, 3):
			try:
				jolokia_conn.request(type="write", mbean="com.yahoo.ycsb.ClientController:type=ClientController", attribute=valueToSet, value=value)
				break
			except JolokiaError as e:
				print "Jolokia error", e
				continue
			except:
				print "Random error setting target, trying again..."
	
	def addValueFromHosts(self, value):
		#value_total = 0.0
		value_total = float("NaN")
		
		for host in self.ycsb_hosts:
			host_results = self.results[host]
			if host_results == None:
				return float("NaN")
			try:
				v = float(host_results[value])
			except KeyError:
				return float("NaN")
			
			#if math.isnan(v):
			#	continue
			
			if math.isnan(value_total):
				value_total = 0.0

			value_total += v
		
		return value_total
	
	def avgValueFromHosts(self, value, weight_value):
		#weighted_total = 0.0
		#total_weight = 0.0
		weighted_total = float("NaN")
		total_weight = float("NaN")

		for host in self.ycsb_hosts:
			host_results = self.results[host]
			if host_results == None:
				return float("NaN")
			try:
				v = float(host_results[value])
			except KeyError:
				return float("NaN")
			
			try:
				weight = float(host_results[weight_value])
			except KeyError:
				return loat("NaN")
			
			if weight == 0: # or math.isnan(weight):
				continue
			
			elif not math.isnan(v) and math.isnan(weighted_total) and math.isnan(total_weight):
				weighted_total = 0.0
				total_weight = 0.0
				
			weighted_total += v*weight
			total_weight += weight
			
		if total_weight > 0:
			return weighted_total / total_weight
		else:
			return float("NaN")
	
	def getValuesForLogging(self):
		# init empty dict
		value_dict = {}
		for value in self.values:
			#print value
			mbean, attribute, description, combine = value
			value_dict[description] = 0.0
		
		# dispatch threads to get values
		threads = {}
		for ycsb_host in self.ycsb_hosts:
			threads[ycsb_host] = Thread(target=self.getValuesFromHostThread, args=(ycsb_host, ))
			threads[ycsb_host].start()
		
		for ycsb_host in self.ycsb_hosts:
			threads[ycsb_host].join()
			
		# combine values
		for value in self.values:
			mbean, attribute, description, combine = value
			if combine == self.AVG:
				continue
			
			val = self.addValueFromHosts(attribute)
			value_dict[description] += val
		
		for value in self.values:
			mbean, attribute, description, combine = value
			if combine == self.ADD:
				continue
			
			val = self.avgValueFromHosts(attribute, "CurrentThroughput")
			value_dict[description] += val
				
		return value_dict
	
	def setTarget(self, target):
		num_hosts = len(self.ycsb_hosts)
		each_target = target / num_hosts
		#print "setting target %d, each machine %d" % (target, each_target)
		self.logger.debug("Setting overall YCSB target to %d" % target)
		for host in self.ycsb_hosts:
			self.logger.debug("Setting target on YCSB host %s to %d" % (host, each_target))
			self.setValue(host, "Target", int(each_target))
	
	def resetMeasurements(self):
		for host in self.ycsb_hosts:
			jolokia_conn = self.getJolokiaConn(host)
			try:
				jolokia_conn.request(type="exec", mbean="com.yahoo.ycsb.ClientController:type=ClientController", operation="resetMeasurements")
			except:
				print "error resetting measurements"
		
	#def setReportingInterval(self, interval):
	#	self.setValue("ReportingInterval", interval)

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--hosts", action="store", nargs="+", default=[YCSB_HOSTNAME])
	parser.add_argument("--target", action="store", type=int, required=False)
	parser.add_argument("--reset", action="store_true", default=False)
	
	args = parser.parse_args()
	from gclogger import GCNullLogger
	logger = GCNullLogger()
	
	manager = GCYCSBManager(args.hosts)
	
	if args.target:
		manager.setTarget(args.target)
	elif args.reset:
		manager.resetMeasurements()
	else:
		values = manager.getValuesForLogging()
		
		hostStr = "[%s]"
		hosts = ""
		for h in args.hosts:
			hosts += " %s " % h
		hostStr = hostStr % hosts
		
		for value in sorted(values.keys()):
			print "%s: values[%s] = %f" % (hostStr, value, values[value])
		