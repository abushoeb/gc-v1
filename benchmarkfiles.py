#!/usr/bin/python

import sys
from datetime import datetime

if __name__ == "__main__":
	
	start = datetime.now()
	f1 = open("testfile1", "w")
	for i in xrange(0, 10000):
		f1.write("%s\n" % str(start))
	f1.close()
	end = datetime.now()
	
	optime = (end-start).total_seconds() / 10000
	print "optime with one open", optime
	
	
	start = datetime.now()
	for i in xrange(0, 10000):
		with open("testfile2", "w") as f2:
			f2.write("%s\n" % str(start))
	end = datetime.now()
	
	optime = (end-start).total_seconds() / 10000
	
	print "optime with many opens", optime
	