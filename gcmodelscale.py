#!/usr/bin/python

import sys
import math
from gcmodel import *
from gccdf import *

if __name__ == "__main__":
	MODEL_FILE = "1-22-UNIFORM-300kbs-MIXED-ONE.model"
	model = GCTableResponseTimeModelNew(MODEL_FILE)
	
	f = open(MODEL_FILE, "r")
	data = f.read().strip()
	lines = data.split("\n")
	
	first = 63.0, 6000 
	second = 81.0, 7000
	
	scale_load = interpolate(first, second, 75.0)

	model_max_load = model.peakThroughput(27, 75.0)
	
	factor = model_max_load / scale_load
	
	for line in lines:
		nodes, load, latency = line.split("\t")
		nodes = int(nodes)
		load = int(load)
		latency = float(latency)
		
		print "%d\t%d\t%0.2f" % (nodes, load, math.ceil(latency*factor))
	