#!/usr/bin/python

import argparse
import thread

from datetime import datetime

from gccommon import *
from gcbase import *
from gchelpers import *
from gcjolokiahelpers import *
from gclogger import *

JOLOKIA_URL="http://%s:8778/jolokia/"

class GCValueDescriptor(object):
	
	def __init__(self, key):
		self.key = key
		
class GCCassandraLatencyManager(GCManagerBase):
	
	VALUE_ABSOLUTE = 1
	VALUE_DIFF = 2
	
	values = {
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "TotalReadLatencyHistogramMicros") : GCValueDescriptor("cassandra_read_histogram"),
		MBeanAttribute("org.apache.cassandra.db:type=StorageProxy", "TotalWriteLatencyHistogramMicros") : GCValueDescriptor("cassandra_write_histogram"),
		MBeanAttribute("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "LifetimeReadLatencyHistogramMicros") : GCValueDescriptor("data_read_histogram"),
		MBeanAttribute("org.apache.cassandra.db:type=ColumnFamilies,keyspace=usertable,columnfamily=data", "LifetimeWriteLatencyHistogramMicros") : GCValueDescriptor("data_write_histogram"), 
	}
	
	def __init__(self, nodes=(MINIMUM_NODES + OPTIONAL_NODES), logger=GCNullLogger()):
		self.nodes = nodes
		self.logger = logger.get_logger(self)
	
	def getTextValuesForLogging(self):
		logging_values = {}
		
		client = JMXThreadedClient(self.nodes, JOLOKIA_URL, timeout=3)
		results = client.readMultipleAttributes(self.values.keys())
		
		for node in self.nodes:
			nodeResults = results[node]
			
			for attrib in self.values.keys():
				descriptor = self.values[attrib]
				key = "%s.%s" % (descriptor.key, node)
				
				# the entire node has no response
				if nodeResults.status == JMXResponse.ERROR:
					current_value = "{}"
				# missing a single value
				elif nodeResults.results[attrib] == JMXResponse.VALUE_MISSING:
					current_value = "{}"
				# otherwise (good data)
				else:
					histogramArray = nodeResults.results[attrib]
					
					offsets = {}
					last = 1
					offsets[0] = last
					for i in xrange(1, len(histogramArray)-1):
						nxt = round(last * 1.2)
						if nxt == last:
							nxt += 1
						offsets[i] = nxt
						last = nxt
						
					for i in xrange(1, len(histogramArray)-1):
						print "%s[%d] = %d" % (descriptor.key, offsets[i], histogramArray[i])
					
		
		return logging_values

if __name__ == "__main__":
	
	manager = GCCassandraLatencyManager(nodes=["sol032"])
	
	manager.getTextValuesForLogging()
	
	