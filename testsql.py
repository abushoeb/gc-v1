#!/usr/bin/python

from gcdatacollector import SQLiteDataStore, SqlitedictDataStore
from datetime import datetime, timedelta
from gccompactionmanager import Compaction

if __name__ == "__main__":
	
	store = SqlitedictDataStore("test2", ["node1", "node2"])
	store.start()
	now = datetime.now()
	store.storeTimestamp(now)
	store.storeValue(now, "hello", 7)
	store.storeValue(now, "hello", 9)
	#store.storeValue(datetime.now(), "hello", 8)
	store.storeValue(now, "compactions", Compaction())
	#print store.getMaxTimestamp()
	print store.getLastValue("hello") + 8
	print store.getLastValue("compactions")
	
	store.epochBegin("helloEpoch", now)
	store.epochEnd(now+timedelta(minutes=5))
	store.epochBegin("newEpoch", now+timedelta(hours=24))
	
	results = store.getLiveResults()
	print results
	print "common", results.availableCommonValues()
	print "nodevalues", results.availableNodeValues()
	print "timestamps", results.availableTimestamps()
	
	print results.getCommonValue(now, "hello")
	print results.getCommonValue(now, "compactions")
	print results.getCommonValue(results.availableTimestamps()[-1], "hello")
	
	print "hello" in results.availableCommonValues()
	
	
	
	
	