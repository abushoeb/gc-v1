#!/usr/bin/python

from gccommon import *
from experimentclock import ExperimentClock
from gcdatacollector import GCDataCollector
from gclogger import GCLogger
from datetime import datetime, timedelta

import math
import time

class GCMLController(object):
	
	def __init__(self, sla, scheduler):
		self.config = GCConfig.getInstance()
		self.clock = ExperimentClock.getInstance()
		self.data_collector = GCDataCollector.getInstance()
		self.logger = GCLogger.getInstance().get_logger(self)
		self.sla = sla
		self.scheduler = scheduler
		self.emergencyCounter = 0
	
	def getInstantLatency(self):
		liveResults = self.data_collector.getLiveResults()
		timestamps = liveResults.availableTimestamps()
		
		instantLatency = liveResults.getCommonValue(timestamps[-1], "readlatency_99_instant")
		
		return instantLatency
		
	def getWindowLatency(self, windowDelta):
		try:
			now = self.clock.get_current_time()
			liveResults = self.data_collector.getLiveResults()

			timestamps = liveResults.availableTimestamps()
			
			if len(timestamps) == 0:
				self.logger.info("No live data yet...")
				windowLatencyFromHistogram = 0.0
				
			elif len(timestamps) == 1:
				histogram = liveResults.getCommonValue(timestamps[-1], "read_histogram")
				windowLatencyFromHistogram = histogram.getPercentile(99)
				
			else:
				dtIndex = 0
				for i in reversed(xrange(0, len(timestamps))):
					if timestamps[i] < (now - windowDelta):
						dtIndex = i+1
						break
				
				prevHistogram = liveResults.getCommonValue(timestamps[dtIndex], "read_histogram")
				lastHistogram = liveResults.getCommonValue(timestamps[-1], "read_histogram")
				windowHistogram = lastHistogram - prevHistogram
				windowLatencyFromHistogram = windowHistogram.getPercentile(99)
			
		except Exception as e:
			self.logger.info("Error taking histogram diff: %s" % e)
			windowLatencyFromHistogram = 0.0
		
		return windowLatencyFromHistogram
	
	def getCumLatency(self):
		try:
			cumLatency = self.data_collector.getLastValue("readlatency_99_cum")
		except Exception as e:
			self.logger.info("Error getting cum latency: %s" % e)
			cumLatency = 0.0
		
		return cumLatency
	
	# the scheduler gets the state from the cluster manager. short circuit to take advantage
	# of that implementation for these functions
		
	def getOnNodes(self):
		state = self.scheduler.getState()
		return state.onNodes
	
	def getOffNodes(self):
		state = self.scheduler.getState()
		return state.offNodes
	
	def getTransitionNodes(self):
		state = self.scheduler.getState()
		return state.transitionNodes
	
	# this will attempt to schedule up or down nodeDelta nodes
	# will check bounds
	def scheduleDelta(self, nodeDelta):
		self.logger.info("Starting scheduleDelta: nodeDelta %d" % nodeDelta)
		
		# get state from cluster manager
		state = self.scheduler.getState()
		
		# get lists of nodes in each state
		onNodes = state.onNodes
		transitionNodes = state.transitionNodes
		offNodes = state.offNodes
		
		numNodesOn = len(onNodes) - len(MINIMUM_NODES)
		numNodesOff = len(offNodes)
		numNodesTransition = len(transitionNodes)
		
		if nodeDelta > 0 and (numNodesOff < nodeDelta):
			self.logger.info("Only %d optional nodes off, ordered +%d, changing delta." % (numNodesOff, nodeDelta))
			nodeDelta = numNodesOff
			
		elif nodeDelta < 0 and (numNodesOn < abs(nodeDelta)):
			self.logger.info("Only %d optional nodes on, ordered -%d, changing delta." % (numNodesOn, abs(nodeDelta)))
			nodeDelta = -numNodesOn
		
		if abs(nodeDelta) > 0:
			# calculate the target
			numNodesOn += len(MINIMUM_NODES)
			target = numNodesOn + nodeDelta
			
			# do adjustment
			self.logger.info("nodes on %d, nodeDelta %d, target %d" % (numNodesOn, nodeDelta, target))
			self.scheduler.adjust(target, target)
		else:
			self.logger.info("nodeDelta=0, nothing to do")
		
		self.logger.info("Completed scheduleDelta")
	
	
	def loop(self, untilDt):
		raise Exception("This method is not implemented. Please subclass this class and implement loop()")
	
			
	def sleepUntil(self, untilDt):
		self.logger.info("sleepUntil: now %s, until %s" % (str(self.clock.get_current_time()), str(untilDt)))
		
		while self.clock.get_current_time() < untilDt:
			remaining_time_seconds = (untilDt - self.clock.get_current_time()).total_seconds() / self.config["accelRate"]
			sleep_time = remaining_time_seconds / 2.0
			if sleep_time < 0.01:
				sleep_time = 0.01
			time.sleep(sleep_time)
	
	def microSleep(self, seconds):
		untilDt = datetime.now() + timedelta(seconds=seconds)
		while datetime.now() < untilDt:
			remaining_time_seconds = (untilDt - datetime.now()).total_seconds()
			sleep_time = remaining_time_seconds / 2.0
			if sleep_time < 0.01:
				sleep_time = 0.01
			time.sleep(sleep_time)
		
	def shortTermDelta(self, latencyTarget):
		# calculate the thresholds
		highThreshold = latencyTarget + (latencyTarget * self.config["mlHighSLAPercentage"])
		lowThreshold = latencyTarget + (latencyTarget * self.config["mlLowSLAPercentage"])
		
		# get the current window latency
		windowLatency = self.getWindowLatency(timedelta(minutes=self.config["mlWindowMins"]))
		
		self.logger.info("Current window latency: %0.2f, target: %0.2f" % (windowLatency, latencyTarget))
		self.logger.info("Bounds: (>= %0.2f ms, <= %0.2f ms" % (lowThreshold, highThreshold))
		
		# if we are in bounds, we are done this cycle
		if windowLatency >= lowThreshold and windowLatency <= highThreshold:
			self.logger.info("In bounds")
			nodeDelta = 0
		
		# otherwise, check bounds conditions
		# low latency
		elif windowLatency < lowThreshold:
			self.logger.info("Below lower threshold")
			
			diff = lowThreshold - windowLatency
			
			if diff <= self.config["mlSmallDiff"]:
				nodeDelta = -1 * self.config["mlSmallStep"]
			else:
				nodeDelta = -1 * self.config["mlLargeStep"]

		# high latency
		elif windowLatency > highThreshold:
			self.logger.info("Above upper threshold")
			
			diff = windowLatency - highThreshold
			
			if diff <= self.config["mlSmallDiff"]:
				nodeDelta = self.config["mlSmallStep"]
			elif diff <= self.config["mlLargeDiff"]:
				nodeDelta = self.config["mlLargeStep"]
			else:
				nodeDelta = 1000

		return nodeDelta

class GCMLControllerReactive(GCMLController):
		
	# this is the entire algorithm
	def loop(self, untilDt):
		
		# overall, we loop until we hit the entire experiment time
		while self.clock.get_current_time() < untilDt:
			self.logger.info("Starting ML controller loop(reactive)...")
			
			# calculate how long this outer loop cycle will last (experiment epoch)
			outerLoopTime = self.clock.get_current_time() + timedelta(minutes=self.config["mlEpochMins"])
			self.logger.info("Loop will end at %s" % outerLoopTime)
				
			# we use this to signal if we need to continue (restart) the outer loop
			continueOuter = False
		
			# sleep extra two data collector cycles (real time)
			self.microSleep(self.data_collector.interval * 2)
			
			# counter for consecutive violations
			emergencyCounter = 0
			
			# inner loop for emergency valve
			# only loop until the time outer loop would normally end
			self.logger.info("Entering emergency valve loop...")
			#instantMeasurements = []
			while self.clock.get_current_time() < outerLoopTime:			
				# sleep data collector cycle (real time)
				self.microSleep(self.data_collector.interval)
				
				# get the instantaneous response time
				instant = self.getInstantLatency()
				#instantMeasurements.append(instant)
				self.logger.info("Instant latency is %0.2f ms" % instant)
				
				# if the instantaneous latency exceeds the emergency threshold
				threshold = self.config["mlEmergencyThreshold"]
				# for this many checks
				emergencyCounterThreshold = self.config["mlEmergencyCountThreshold"]
				if instant >= threshold:
					# ensure some number of consecutive violating samples before we trip
					emergencyCounter += 1
					if emergencyCounter < emergencyCounterThreshold:
						self.logger.info("Emergency counter == %d, not enough yet..." % emergencyCounter)
						continue
					else:
						self.logger.info("Emergency counter == %d, tripped..." % emergencyCounter)
						
					nodeDelta = self.config["mlEmergencyNodes"]
					self.logger.info("Exceeded threshold of %0.2f ms, for %d checks, adding %d nodes" % (threshold, emergencyCounterThreshold, nodeDelta))
					# add some emergency nodes
					self.scheduleDelta(nodeDelta)
					# wait for transitions
					self.scheduler.waitForSchedule()
					self.logger.info("Schedule done (inner loop)...")
					
					# indicate that we want to continue the outer loop
					continueOuter = True
					# break out
					break
				# this will require violations to be consecutive
				#else:
				#	emergencyCounter = 0
				
			# if the continue flag is set, restart loop
			if continueOuter:
				continue
			
			# otherwise, run the regular cycle
			
			# see what we need to adjust to hit the SLA (regular policy)
			nodeDelta = self.shortTermDelta(self.sla)
			
			# schedule it
			self.scheduleDelta(nodeDelta)
			self.logger.info("Schedule done (outer loop)...")
			
			# wait for transitions
			self.scheduler.waitForSchedule()

class GCMLControllerReactiveA(GCMLController):

	# this is the entire algorithm
	def loop(self, untilDt):
		
		# overall, we loop until we hit the entire experiment time
		while self.clock.get_current_time() < untilDt:
			self.logger.info("Starting ML controller loop(reactive-a)...")
			
			# calculate how long this loop cycle will last (experiment epoch)
			cycleTime = self.clock.get_current_time() + timedelta(minutes=self.config["mlEpochMins"])
			self.logger.info("Loop will end at %s" % cycleTime)

			# sleep until it is time to schedule
			self.sleepUntil(cycleTime)

			# see what we need to adjust to hit the SLA (regular policy)
			nodeDelta = self.shortTermDelta(self.sla)
			
			# schedule it
			self.scheduleDelta(nodeDelta)
			self.logger.info("Schedule done...")
			
			# wait for transitions
			self.scheduler.waitForSchedule()

			self.logger.info("Completed ML controller loop (reactive-a)...")
		
class GCMLControllerCredit(GCMLController):
	BURN_CREDIT_FACTOR = 0.95
	DEGRADATION_FACTOR = 1.33
	RECOVER = True
	RECOVERY_FACTOR = 0.90
	
	def __init__(self, sla, scheduler, solar_predictor, power_model):
		super(GCMLControllerCredit, self).__init__(sla, scheduler)
		self.solar_predictor = solar_predictor
		self.power_model = power_model
			
	def calcOverallTarget(self):
		# get the current cum latency
		cumLatency = self.getCumLatency()
		self.logger.info("cum 99th percentile latency is %0.2f" % cumLatency)
		
		# if we are low on cum latency
		if cumLatency < self.sla*self.BURN_CREDIT_FACTOR:
			self.logger.info("try to burn credit")
			latencyTarget = math.ceil(self.sla*self.DEGRADATION_FACTOR)
		
		# else if we have exceeded the SLA
		elif cumLatency > self.sla:
			self.logger.info("exceeded SLA, try to recover")
			latencyTarget = math.ceil(self.sla*self.RECOVERY_FACTOR)
		# otherwise, we are within our target range, just run normally
		else:
			self.logger.info("all good, just work normally")
			latencyTarget = self.sla
		
		return latencyTarget

	# idea: given a delta, we can calculate how many nodes we think we need right now.
	# if we have power for more, increase the delta, subsuming the behavior of the
	# lower layers
	def solarDelta(self, baseDelta):
		# get state from cluster manager
		state = self.scheduler.getState()
		onNodes = state.onNodes	
		numNodesOn = len(onNodes)
		
		now = self.clock.get_current_time()
		solarAvailable = self.solar_predictor.getAvgSolarForInterval(now, timedelta(minutes=self.config["mlEpochMins"]))
		nodesSupported = self.power_model.nodesSupported(solarAvailable)
		
		if numNodesOn + baseDelta < nodesSupported:
			nodeDelta = nodesSupported - numNodesOn
		else:
			nodeDelta = baseDelta
		
		self.logger.info("solarDelta: baseDelta=%d, numNodesOn=%d, nodesSupported=%d, nodeDelta=%d" % (baseDelta, numNodesOn, nodesSupported, nodeDelta))
		return nodeDelta
	
	# this is the main algorithm
	def loop(self, untilDt):
		# overall, we loop until we hit the entire experiment time
		while self.clock.get_current_time() < untilDt:
			self.logger.info("Starting ML controller loop(credit)...")
			
			# calculate how long this loop cycle will last (experiment epoch)
			cycleTime = self.clock.get_current_time() + timedelta(minutes=self.config["mlEpochMins"])
			self.logger.info("Loop will end at %s" % cycleTime)

			# sleep until it is time to schedule
			self.sleepUntil(cycleTime)

			# calc the target based on current cumulative latency
			latencyTarget = self.calcOverallTarget()

			# calc the delta given this target
			nodeDelta = self.shortTermDelta(latencyTarget)
			
			# possibly overide this delta based on the solar energy business
			nodeDelta = self.solarDelta(nodeDelta)
			
			# schedule it
			self.scheduleDelta(nodeDelta)
			self.logger.info("Schedule done...")
			
			# wait for transitions
			self.scheduler.waitForSchedule()
			self.logger.info("Transitions complete")

			self.logger.info("Completed ML controller loop (credit)...")