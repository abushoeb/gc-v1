#!/usr/bin/python
import ConfigParser
import os
import sys
import time
import paramiko
import socket
import SocketServer
import signal
import argparse

from gccommon import *
from gchelpers import wait_with_status
from gcclustermanager import GCClusterManager
from gcworkloadcontroller import GCWorkloadController
from gcgangliamanager import GCGangliaManager
from gcsolarmanager import GCSolarManager
from gclogger import GCLogger
from gcsolarpredictor import GCHistoricalSolarPredictor, GCNullSolarPredictor, GCMdSolarPredictor
from gcworkloadpredictor import GCMicrosoftTraceWorkloadPredictor, GCAskTraceWorkloadPredictor, GCRegressionWorkloadPredictor, GCStaticWorkloadPredictor
from gcdatacollector import GCDataCollector
from gcycsb import YCSBGreenCassandraCustomWorkload, YCSBMultipleRun
from gcycsbmanager import GCYCSBManager
from gcycsblatencymanager import GCYCSBLatencyManager
from gcscheduler import GCNoopScheduler, GCBaselineScheduler, GCCreditHeuristicScheduler, GCStaticScheduler, GCOnlineOptimizerScheduler, GCNoModelScheduler
from gcjolokiamanager import GCJolokiaManagerNew
from gccassandraloadmanager import GCCassandraLoadManager
from gcdataloc import GCOrganizedExperimentPath
from gccompactionmanager import GCCompactionManager, CompactionController
from gcmodel import GCTableResponseTimeModelNew
from gcmodel import GCParasolPowerModel
from gcjstatmanager import GCJStatManager
from gcutil import OrderedSemaphore
from gcutil import TokenOrderedSemaphore
from experimentclock import ExperimentClock
from datetime import datetime
from datetime import timedelta
from gcmlcontroller import GCMLControllerReactive, GCMLControllerReactiveA, GCMLControllerCredit
from gcbatterymodel import GCParasolPowerManager

import pystuck
from meliae import scanner

YCSB_HOSTNAMES=[YCSB_HOSTNAME]

class GCController2(object):
	
	def __init__(self, args):

		self.start_time = datetime.strptime("%s %s" % (args.date, args.time), "%m-%d-%Y %H:%M")
		self.solar_dt = datetime.strptime("%s %s" % (args.solarday, args.time), "%m-%d-%Y %H:%M")
		self.epoch = args.epoch
		self.run_length = args.cycles
		self.accel_rate = args.accel
		
		if args.experiment_id != None and args.experiment_id != "":
			self.experiment_id = args.experiment_id
		else:
			self.experiment_id = None
		
		self.sched = args.scheduler
		self.workload = args.workload
		self.target_response_time = args.response
		self.internal_target_response_time = self.target_response_time * 1.0
		self.noload = args.noload
		self.norepair = args.norepair
		self.nowarmup = args.nowarmup
		self.nosolar = args.nosolar
		self.warmup_rate = args.warmuprate
		self.warmup_len = args.warmuplen
		self.days = args.days
		
		self.sig_counter = 0

	def run(self):
		pystuck.run_server()
				
		# experiment id
		id_tuple = (self.workload, self.sched)
		experiment_path = GCOrganizedExperimentPath(RESULTS_DIR, id_tuple, experiment_id=self.experiment_id)
		
		self.logfile = "%s.log" % experiment_path.getPath().replace(RESULTS_DIR, "").replace("/", "-")[1:]
		self.clock = ExperimentClock(start_time=self.start_time, accel_rate=self.accel_rate, wait=True)
		self.main_logger = GCLogger(self.logfile, GCLogger.DEBUG, stdout=True, stderr=True)
		self.logger = self.main_logger.get_logger(self)
		
		self.logger.info("Starting GreenCassandra controller...")
		self.logger.info("Starting dt is %s" % str(self.start_time))
		self.logger.info("Running for %d epochs of %d minutes..." % (self.run_length, self.epoch))
		self.logger.info("Workload is %s" % self.workload)
		self.logger.info("Results path: %s" % experiment_path.getPath())
		
		self.logger.info("Starting experiment in 10 seconds...")
		for i in reversed(xrange(0, 10+1)):
			    time.sleep(1)
			    self.logger.info("%d..." % i)
			    
		# create the experiment directory
		experiment_path.makeDirs()
		
		# write the current experiment file
		setCurrentExperiment(experiment_path.getPath())
		
		# set up the data collector
		self.data_collector = GCDataCollector(experiment_path, interval=5)
		
		if self.nosolar:
			self.solar_predictor_perfect = GCNullSolarPredictor()
			self.solar_predictor = self.solar_predictor_perfect
		else:
			# use the real historical solar day and different workload (non-shifted)
			# self.solar_predictor = GCHistoricalSolarPredictor(dts=[self.start_time, self.start_time+timedelta(days=1)], scale=0.35)
			self.solar_predictor_perfect = GCHistoricalSolarPredictor(dts=[self.solar_dt, self.solar_dt+timedelta(days=1)], scale=0.35, experiment_dt=self.start_time, solar_dt=self.solar_dt)
			# use perfect prediction for optimization
			if "opt" in self.sched:
				self.solar_predictor = self.solar_predictor_perfect
			# otherwise use Md's predictor
			else:
				self.solar_predictor = GCMdSolarPredictor(solar_dt=self.solar_dt, experiment_dt=self.start_time)
		
		# init model	
		if self.sched == "baseline":
			#modelFile = "MODEL-5-12-smart-EXTENDED-EXTENDED-MODIFIED.model"
			#modelFile = "MODEL-5-12-simple-MODIFIED-EXTENDED-MODIFIED.model"
			#baseModelFile = "MODEL-5-12-simple-MODIFIED-EXTENDED-MODIFIED.model"
			
			modelFile = "MODEL-10-17-simple-EXTENDED-MODIFIED.model"
			baseModelFile = modelFile
		else:
			#modelFile = "MODEL-5-12-smart-EXTENDED-EXTENDED.model"
			#baseModelFile = "MODEL-5-12-simple-MODIFIED-EXTENDED.model"
			#modelFile = "READ-6-27-ZIPFIAN-ONE-ONE-simple-EXTENDED.model"
			
			modelFile = "MODEL-10-17-simple-EXTENDED-MODIFIED.model"
			baseModelFile = modelFile
		
		self.base_response_time_model = GCTableResponseTimeModelNew(baseModelFile, extrapolate=False)
		self.response_time_model = GCTableResponseTimeModelNew(modelFile, extrapolate=False)
		
		self.power_model = GCParasolPowerModel()
		
		self.max_throughput = self.base_response_time_model.peakThroughput(NUM_NODES, self.target_response_time)
		self.logger.info("Peak throughput is %d" % self.max_throughput)
		
		self.config = GCConfig()
		self.config["accelRate"] = self.accel_rate
		
		if self.workload == "hotmail":	
			self.config.setLookAhead(1)
			
			workload_field = 1
			self.workload_perfect_knowledge = GCMicrosoftTraceWorkloadPredictor("workload.csv", field=workload_field, shift=timedelta(days=1))
			self.opt_workload_predictor = self.workload_perfect_knowledge
			
			self.workload_predictor = GCRegressionWorkloadPredictor(self.max_throughput)
			self.data_collector.register(self.workload_predictor)
			
		elif self.workload == "messenger":
			self.config.setLookAhead(1)
			
			workload_field = 2
			self.workload_perfect_knowledge = GCMicrosoftTraceWorkloadPredictor("workload.csv", field=workload_field, shift=timedelta(days=1))
			self.opt_workload_predictor = self.workload_perfect_knowledge
			
			self.workload_predictor = GCRegressionWorkloadPredictor(self.max_throughput)
			self.data_collector.register(self.workload_predictor)
			
		elif self.workload == "ask":
			self.config.setLookAhead(1)
			
			self.workload_perfect_knowledge = GCAskTraceWorkloadPredictor("ask-workload-4.dat", mode=GCAskTraceWorkloadPredictor.PERIODICAVG, shift=timedelta(days=1))
			#self.workload_predictor = GCAskTraceWorkloadPredictor("ask_predictions.dat", mode=GCAskTraceWorkloadPredictor.RAW, shift=timedelta(days=1), interpolate=True)
			#self.workload_predictor = GCAskPredictionWorkloadPredictor("ask_predictions_workload.dat", shift=timedelta(days=5), interpolate=True)
			#####self.opt_workload_predictor = GCAskPredictionWorkloadPredictor("ask_predictions_workload.dat", timedelta(days=5, minutes=-45), interpolate=True, max_value=self.workload_perfect_knowledge.max_value, factor=0.91, clock=self.clock)
			self.opt_workload_predictor = self.workload_perfect_knowledge
			
			self.workload_predictor = GCRegressionWorkloadPredictor(self.max_throughput)
			self.data_collector.register(self.workload_predictor)
			
		elif self.workload == "static":
			self.workload_perfect_knowledge = GCStaticWorkloadPredictor(4000, self.max_throughput)
			self.opt_workload_predictor = self.workload_perfect_knowledge

			self.workload_predictor = self.workload_perfect_knowledge
			
		self.compaction_manager = GCCompactionManager()
		self.compaction_controller = CompactionController(manager=self.compaction_manager, count=3, semaphore_class=TokenOrderedSemaphore)
		self.cluster_manager = GCClusterManager(mode=GCClusterManager.CASSANDRA_SLEEP, early_on=True, simultaneous_on=1, compaction_controller=self.compaction_controller)#, no_repair=self.norepair)
		self.ganglia_manager = GCGangliaManager()
		self.solar_manager = GCSolarManager(self.solar_predictor, self.solar_predictor_perfect)
		self.ycsb_manager = GCYCSBManager(YCSB_HOSTNAMES)
		self.ycsb_latency_manager = GCYCSBLatencyManager(YCSB_HOSTNAMES)
		self.jolokia_manager = GCJolokiaManagerNew()
		self.cassandra_load_manager = GCCassandraLoadManager()
		#self.jstat_manager = GCJStatManager()
		self.power_manager = GCParasolPowerManager()
		#self.power_manager.setTargetBattPowerLoad(500.0)
		#self.power_manager.setTargetGreenPowerBatt(250.0)
		#self.power_manager.setTargetBrownPowerBatt(250.0)
		#self.power_manager.setTargetGreenPowerNet(500.0)
		
		# set up workload controller
		self.workload_controller = GCWorkloadController(self.workload_perfect_knowledge, self.ycsb_manager, self.max_throughput, interval=30/self.accel_rate, noop=self.noload)

		if self.sched == "noop":
			self.scheduler = GCNoopScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput)
		elif self.sched == "baseline":
			self.scheduler = GCBaselineScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput)
		elif self.sched == "load":
			self.scheduler = GCLoadScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput, fudge=0)
		elif self.sched == "solar":
			self.scheduler = GCSolarScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput)
		elif self.sched == "simple":
			self.scheduler = GCSimplePolicyScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput)
		elif self.sched == "deadline":
			self.scheduler = GCLoadDeadlineScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput)
		elif self.sched == "static":
			self.scheduler = GCStaticScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput, nodes=15)
		elif self.sched == "specific":
			nodes = MINIMUM_NODES + ["sol041", "sol044", "sol046", "sol048", "sol051", "sol053", "sol056", "sol063"]
			self.scheduler = GCSpecificScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput, nodes=nodes)
		elif self.sched == "credit-m":
			self.scheduler = GCCreditHeuristicScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput)
		elif self.sched == "opt":
			self.scheduler = GCOnlineOptimizerScheduler(self.solar_predictor, self.opt_workload_predictor, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput, accel_factor=self.accel_rate, use_brown_prices=False, response_time_model=self.response_time_model)
		elif self.sched == "opt-battery":
			self.scheduler = GCOnlineOptimizerScheduler(self.solar_predictor, self.opt_workload_predictor, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput, accel_factor=self.accel_rate, use_brown_prices=False, response_time_model=self.response_time_model, use_battery=True)
		elif self.sched == "opt-brownprices":
			self.scheduler = GCOnlineOptimizerScheduler(self.solar_predictor, self.opt_workload_predictor, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput, accel_factor=self.accel_rate, use_brown_prices=True, response_time_model=self.response_time_model)
		elif self.sched == "opt2":
			self.scheduler = GCOnlineOptimizerScheduler2(self.solar_predictor, self.workload_predictor, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput, accel_factor=self.accel_rate)
		elif self.sched in ["reactive", "reactive-a", "credit"]:
			self.scheduler = GCNoModelScheduler(self.solar_predictor, self.workload_predictor, self.response_time_model, self.power_model, target_response_time=self.internal_target_response_time, max_throughput=self.max_throughput)
		
		# register the managers with the data collector
		self.data_collector.register(self.cluster_manager)
		self.data_collector.register(self.workload_controller)
		self.data_collector.register(self.ganglia_manager)
		self.data_collector.register(self.solar_manager)
		self.data_collector.register(self.jolokia_manager)
		self.data_collector.register(self.cassandra_load_manager)
		self.data_collector.register(self.ycsb_latency_manager)
		self.data_collector.register(self.compaction_manager)
		#self.data_collector.register(self.jstat_manager)
		self.data_collector.register(self.power_manager)
		
		# set up YCSB stuff
		target_nodes = (MINIMUM_NODES + OPTIONAL_NODES)
		#target_nodes = MINIMUM_NODES
		#self.ycsb_workload = YCSBGreenCassandraReadLatestWorkload(target_nodes, YCSB_RECORD_COUNT, readconsistency="ONE", writeconsistency="ALL")
		self.ycsb_workload = YCSBGreenCassandraCustomWorkload(target_nodes, YCSB_RECORD_COUNT, readconsistency="ONE", writeconsistency="ONE", read=0.95, write=0.05, requestdistribution="zipfian")
		
		self.ycsb_workload.threadcount = 512
		self.ycsb_workload.maxexecutiontime = ((self.epoch * self.run_length) + 5 + 5) * 60
		self.ycsb_workload.targetthroughput = -1
		self.ycsb_workload.histogram_window = 300
		self.insertorder = YCSB_RECORD_INSERT_ORDER
		
		#ycsb_latency_output_path = self.data_collector.getExperimentPath() + "/ycsb.latency_output"
		#ycsb_throughput_output_path = self.data_collector.getExperimentPath() + "/ycsb.throughput_output"
		ycsb_latency_output_path = "/dev/null"
		ycsb_throughput_output_path = "/dev/null"
		
		self.ycsb_run = YCSBMultipleRun(self.ycsb_workload, YCSB_HOSTNAMES,
						base_latency_file=ycsb_latency_output_path,
						base_throughput_file=ycsb_throughput_output_path,
						client="cassandra-hector")

		self.logger.info("Starting compaction controller")
		self.compaction_controller.start()
		
		# start the YCSB processes (they will start with no load generation yet)
		self.logger.info("Starting YCSB")
		self.ycsb_run.start()
		time.sleep(10)
		
		# start the data collector
		self.logger.info("Starting data collector")
		self.data_collector.start()

		if not self.nowarmup:
			self.logger.info("Warming up at %d ops/sec" % self.warmup_rate)
			self.ycsb_manager.setTarget(self.warmup_rate)
			# warm up for 15 mins before we collect data
			wait_with_status(self.warmup_len*60, status_interval=15)
			self.ycsb_manager.setTarget(1000)
			wait_with_status(15*60, status_interval=15)
			self.ycsb_manager.setTarget(-1)
		else:
			self.logger.info("Settling")
			wait_with_status(5*60, status_interval=15)
			#wait_with_status(30, status_interval=15)
			self.ycsb_manager.setTarget(-1)
		
		self.logger.info("Here we go!")

		# run the scheduler once before we start collecting data
		# this is to start with the correct number of nodes rather
		# than waiting for the first cycle
		self.logger.info("Running initial schedule")
		self.scheduler.schedule(self.epoch)
		wait_with_status(30, status_interval=5)

		# reset the YCSB measurments (histograms)
		# so that we don't see data from the warmups
		self.logger.info("Resetting histograms")
		self.ycsb_manager.resetMeasurements()

		self.logger.info("Starting workload controller")
		self.workload_controller.start()
		#wait_with_status(30, status_interval=5)
		wait_with_status(5*60, status_interval=5)

		# start the clock
		self.logger.info("Starting clock")
		self.clock.start()
		
		# used in both controller types
		startTime = self.clock.get_current_time()
		numRuns = self.run_length		
		epochDelta = timedelta(minutes=self.epoch)

		# regular greencassandra case
		if not self.sched in ["reactive", "reactive-a", "credit"]:
			# hacky thing to allow running an experiment for multiple identical days
			# relies on the fact that we know how to manipulate the solar and workload predictor's internal shift values		
			for dayShift in xrange(0, self.days):
				self.logger.info("Starting a day cycle, dayShift=%d" % dayShift)
				self.logger.info("Starting shifts: PN=%s, P=%s, S=%s" % (str(self.workload_perfect_knowledge.shift), str(self.workload_predictor.shift), str(self.solar_predictor.shift)))
				self.workload_perfect_knowledge.shift = self.workload_perfect_knowledge.shift - timedelta(dayShift)
				self.workload_predictor.shift = self.workload_predictor.shift - timedelta(dayShift)
				self.solar_predictor.shift = self.solar_predictor.shift - timedelta(dayShift)
				self.logger.info("Modified shifts: PN=%s, P=%s, S=%s" % (str(self.workload_perfect_knowledge.shift), str(self.workload_predictor.shift), str(self.solar_predictor.shift)))
			
				# the main experiment loop
				currentRun = 1
				
				while currentRun <= numRuns:
					timeNow = self.clock.get_current_time()
					self.logger.info("Cycle %d starting @ %s" % (currentRun, str(timeNow)))
					self.data_collector.epochBegin("Cycle-%d" % currentRun)

					schedStart = self.clock.get_current_time()			
					try:
						if "opt" in self.sched:
							self.scheduler.schedule(self.epoch, epoch=currentRun)
						else:
							self.scheduler.schedule(self.epoch)
						
					except Exception as e:
						print "Exception in scheduler:", e
					
					schedFinish = self.clock.get_current_time() 
					lastSchedTime = schedFinish - schedStart
					self.logger.info("Elapsed time in scheduler: %s" % str(lastSchedTime))
					self.logger.info("Schedule complete for cycle %d @ %s" % (currentRun, str(schedFinish)))
					
					remainingRuns = numRuns - currentRun + 1
					remainingTime = remainingRuns * epochDelta / self.accel_rate
					self.logger.info("Cycles Remaining: %d" % remainingRuns)
					self.logger.info("Wall Clock Time Remaining: %s" % str(remainingTime))
					
					while self.clock.get_current_time() < startTime + currentRun*epochDelta:
						remaining_time = ((startTime + currentRun*epochDelta) - self.clock.get_current_time()).total_seconds() / self.accel_rate
						sleep_time =  remaining_time / 2.0
						if sleep_time < 0.01:
							sleep_time = 0.01
						self.logger.debug("remaining time (wall clock) in cycle %0.5f seconds, sleep for %0.5f seconds" % (remaining_time, sleep_time))
						time.sleep(sleep_time)
					
					timeNow = self.clock.get_current_time()
					self.logger.info("Cycle %d complete @ %s" % (currentRun, str(timeNow)))
					self.data_collector.epochEnd()
					
					currentRun += 1
		
		# otherwise, model-less controller
		else:			
			if self.sched == "reactive":
				# parameters
				self.config["mlEpochMins"] = 5
				self.config["mlWindowMins"] = 5
				
				self.config["mlSmallDiff"] = 5
				self.config["mlLargeDiff"] = 10
				
				self.config["mlSmallStep"] = 1
				self.config["mlLargeStep"] = 3
				
				self.config["mlHighSLAPercentage"] = -0.15
				self.config["mlLowSLAPercentage"] = -0.25

				self.config["mlEmergencyThreshold"] = 75.0
				self.config["mlEmergencyNodes"] = 5
				self.config["mlEmergencyCountThreshold"] = 2
				
				mlController = GCMLControllerReactive(self.target_response_time, self.scheduler)
				untilDt = startTime + numRuns*epochDelta
				self.logger.info("Running from %s to %s..." % (str(startTime), str(untilDt)))
				mlController.loop(untilDt)

			elif self.sched == "reactive-a":
				# parameters
				self.config["mlEpochMins"] = 5
				self.config["mlWindowMins"] = 5

				self.config["mlSmallDiff"] = 10
				self.config["mlLargeDiff"] = 20
				
				self.config["mlSmallStep"] = 1
				self.config["mlLargeStep"] = 3
				
				self.config["mlHighSLAPercentage"] = 0.0
				self.config["mlLowSLAPercentage"] = -0.10
				
				mlController = GCMLControllerReactiveA(self.target_response_time, self.scheduler)
				untilDt = startTime + numRuns*epochDelta
				self.logger.info("Running from %s to %s..." % (str(startTime), str(untilDt)))
				mlController.loop(untilDt)

			elif self.sched == "credit":
				# parameters
				self.config["mlEpochMins"] = 5
				self.config["mlWindowMins"] = 5

				self.config["mlSmallDiff"] = 10
				self.config["mlLargeDiff"] = 20
				
				self.config["mlSmallStep"] = 1
				self.config["mlLargeStep"] = 3
				
				self.config["mlHighSLAPercentage"] = 0.0
				self.config["mlLowSLAPercentage"] = -0.10
				
				mlController = GCMLControllerCredit(self.target_response_time, self.scheduler, self.solar_predictor, self.power_model)
				untilDt = startTime + numRuns*epochDelta
				self.logger.info("Running from %s to %s..." % (str(startTime), str(untilDt)))
				mlController.loop(untilDt)
		
		print "Running YCSB tail"
		self.logger.info("Running YCSB tail")
		wait_with_status(60, status_interval=5)
		
		self.logger.log("Shutting down")
		self.shutdown()
		
		self.logger.info("Exiting...")
		sys.exit()
		
	def shutdown(self):
		if self.ycsb_run:
			self.logger.log("Stopping YCSB")
			self.ycsb_run.stop()
		
		if self.workload_controller:
			self.logger.log("Stopping workload controller")
			self.workload_controller.stop()
		
		if self.data_collector:
			self.logger.log("Stopping data collector")
			self.data_collector.stop()
		
		if self.compaction_controller:
			self.logger.log("Stopping compaction controller")
			self.compaction_controller.stop()
		
		self.logger.log("Waiting")
		wait_with_status(10, status_interval=1)
		
		if self.cluster_manager:
			self.logger.log("Shutting down cluster manager")
			self.cluster_manager.shutdown(turnOn=False)
		
		self.logger.log("Waiting")
		wait_with_status(10, status_interval=1)
		
		clearCurrentExperiment()

	def should_interrupt(self):
		self.sig_counter += 1
		self.logger.info("SIGINT %d" % self.sig_counter)
		
		if self.sig_counter > 2:
			self.shutdown()
			sys.exit(-1)
		
def sig_handler(signum, frame):
	if controller:
		controller.should_interrupt()

def sig_usr1(signum, frame):
	print "dumping heap profile for debugging"
	scanner.dump_all_objects('gccontroller2.json')
	print "...done"
	
	return
	
if __name__ == "__main__":
	signal.signal(signal.SIGINT, sig_handler)
	signal.signal(signal.SIGHUP, sig_handler)
	signal.signal(signal.SIGUSR1, sig_usr1)

	parser = argparse.ArgumentParser()
	parser.add_argument("--experiment_id", action="store")
	parser.add_argument("--cycles", action="store", type=int, required=True)
	parser.add_argument("--epoch", action="store", type=int, required=True)
	parser.add_argument("--accel", action="store", type=int, default=1)
	parser.add_argument("--date", action="store", type=str, default="10-20-2013")
	parser.add_argument("--time", action="store", type=str, default="6:00")
	parser.add_argument("--scheduler", action="store", choices=["noop", "baseline", "load", "solar", "simple", "deadline", "static", "opt", "opt-battery", "opt-brownprices", "opt2", "specific", "credit-m", "reactive", "reactive-a", "credit"], type=str, required=True)
	parser.add_argument("--workload", action="store", choices=["messenger", "hotmail", "ask", "static"], type=str, required=True)
	parser.add_argument("--response", action="store", default=75)
	parser.add_argument("--noload", action="store_true", default=False)
	parser.add_argument("--norepair", action="store_true", default=False)
	parser.add_argument("--nowarmup", action="store_true", default=False)
	parser.add_argument("--solarday", action="store", default="10-20-2013")
	parser.add_argument("--nosolar", action="store_true", default=False)
	parser.add_argument("--warmuprate", action="store", type=int, default=3000)
	parser.add_argument("--warmuplen", action="store", type=int, default=15)
	parser.add_argument("--days", action="store", type=int, default=1)
	
	args = parser.parse_args()
	controller = GCController2(args)
	controller.run()
